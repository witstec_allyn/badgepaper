package cn.com.heaton.blelibrary.ble.request;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

public class BitmapBleBmpManagement {

//    /**
//     * 发送图片
//     *
//     * @param bitmap 要转换的bitmap
//     */
//    public static byte[] getBmpProtocolData(Bitmap bitmap, boolean isRed) {
//        int w = bitmap.getWidth(), h = bitmap.getHeight();
//        int[] pixels = new int[w * h];
//        bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
//        return addBMP_RGB_888(pixels, w, h, isRed);
//    }

    /**
     * 发送每一张图片
     */
    public static byte[] getBmpProtocolEach(byte[] eachByte, boolean isRed, int packageSize, int index) {
        ByteBuffer loadData = ByteBuffer.allocate(eachByte.length + 4);
        loadData.put(IntSplitToByte(packageSize, 2));
        loadData.put(IntSplitToByte(index, 2));
        loadData.put(eachByte);
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        byte[] totalLoadDataZy = addBMP_RGB_888_Escape(totalLoadData);
        int instruction = isRed ? 33062 : 33060;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length + totalLoadDataZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadDataZy, 0, buffer, 7 + verification.length, totalLoadDataZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 分包发送
     */
    public static byte[] getBmpProtocolEach(int instruction, byte[] dataBytes, int packageSize, int index) {
        ByteBuffer loadData = ByteBuffer.allocate(dataBytes.length + 2);
        loadData.put(IntSplitToByte(packageSize, 1));
        loadData.put(IntSplitToByte(index, 1));
        loadData.put(dataBytes);
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        byte[] totalLoadDataZy = addBMP_RGB_888_Escape(totalLoadData);
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length + totalLoadDataZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadDataZy, 0, buffer, 7 + verification.length, totalLoadDataZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 按照协议把图片带头带尾转成字节数组
     */
    public static byte[] getBmpProtocolDataJson() {
        int instruction = 32768;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplitChange((short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(0, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, 7 + verification.length, 1);
        return buffer;
    }

    /**
     * 修改型号
     *
     * @param byteData
     * @return
     */
    public static byte[] getBmpProtocolUpdateType(byte[] byteData) {
        byte[] byteZy = addBMP_RGB_888_Escape(byteData);
        int instruction = 32774;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(byteData, byteData.length, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length + byteZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(byteData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(byteZy, 0, buffer, 7 + verification.length, byteZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 写入驱动
     *
     * @param byteData
     * @return
     */
    public static byte[] getBmpProtocolAddDrive(byte[] byteData) {
        byte[] byteZy = addBMP_RGB_888_Escape(byteData);
        int instruction = 32778;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(byteData, byteData.length, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length + byteZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(byteData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(byteZy, 0, buffer, 7 + verification.length, byteZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 读取屏幕型号
     * @param byteData
     * @return
     */
    public static byte[] getBmpProtocolGetDrive(byte[] byteData) {
        byte[] byteZy = addBMP_RGB_888_Escape(byteData);
        int instruction = 32776;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(byteData, byteData.length, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length + byteZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(byteData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(byteZy, 0, buffer, 7 + verification.length, byteZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 修改mac地址
     *
     * @param byteData
     * @return
     */
    public static byte[] getBmpProtocolUpdateMac(byte[] byteData) {
        byte[] byteZy = addBMP_RGB_888_Escape(byteData);
        int instruction = 32770;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(byteData, byteData.length, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length + byteZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(byteData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(byteZy, 0, buffer, 7 + verification.length, byteZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 发送图片命令
     *
     * @param isRed
     * @return
     */
    public static byte[] getBmpProtocolCommand(boolean isRed) {
        int instruction = isRed ? 32806 : 32804;
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplit(0, (short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(0, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, 7 + verification.length, 1);
        return buffer;
    }

    /**
     * 发送命令不带负载
     *
     * @param instruction
     * @return
     */
    public static byte[] getBmpProtocolDataChange(int instruction) {
        byte[] verification = addBMP_RGB_888_Escape(IntSplitToByte(IntSplitChange((short) instruction), 1));
        byte[] buffer = new byte[8 + verification.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(0, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, 7 + verification.length, 1);
        return buffer;
    }

    /**
     * 写入固件版本号
     *
     * @return
     */
    public static byte[] setOteVersion(byte[] byteData) {
        byte[] byteZy = addBMP_RGB_888_Escape(byteData);
        int instruction = 32772;
        byte[] buffer = new byte[9 + byteZy.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(byteData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(IntSplitToByte(IntSplit(byteData, byteData.length, (short) instruction), 1), 0, buffer, 7, 1);
        System.arraycopy(byteZy, 0, buffer, 8, byteZy.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 负载字节相加
     *
     * @param escape
     * @return
     */
    private static short LiabilitySuperimposed(byte[] escape) {
        short resultInt = 0;
        for (byte b : escape) {
            resultInt += b;
            resultInt &= 0xFF;
        }
        return resultInt;
    }

    /**
     * 字符串拆分
     *
     * @return
     */
    private static int IntSplit(int len, short command) {
//        short cks = LiabilitySuperimposed(escape);
        short cks = 0;
        cks += (command & 0xff);
        cks &= 0xFF;

        cks += ((command & 0xff00) >> 8);
        cks &= 0xFF;

        cks += (len & 0xff);
        cks &= 0xFF;

        cks += ((len & 0xff00) >> 8);
        cks &= 0xFF;

        cks += ((len & 0xff0000) >> 16);
        cks &= 0xFF;

        cks += ((len & 0xff000000) >> 24);
        cks &= 0xFF;
        return cks % 256;
    }

    private static int IntSplit(byte[] byteData, int len, short command) {
        short cks = LiabilitySuperimposed(byteData);
        cks += (command & 0xff);
        cks &= 0xFF;

        cks += ((command & 0xff00) >> 8);
        cks &= 0xFF;

        cks += (len & 0xff);
        cks &= 0xFF;

        cks += ((len & 0xff00) >> 8);
        cks &= 0xFF;

        cks += ((len & 0xff0000) >> 16);
        cks &= 0xFF;

        cks += ((len & 0xff000000) >> 24);
        cks &= 0xFF;
        return cks % 256;
    }

    private static int IntSplitChange(short command) {
        short cks = 0;
        cks += (command & 0xff);
        cks &= 0xFF;

        cks += ((command & 0xff00) >> 8);
        cks &= 0xFF;

        int quyu = cks % 256;

        return quyu;
    }


    /**
     * 添加转义符
     *
     * @param buffer
     * @return
     */
    private static byte[] addBMP_RGB_888_Escape(byte[] buffer) {
        int indexZy = 0;
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == (byte) 0x85 || buffer[i] == (byte) 0x90 || buffer[i] == (byte) 0x95) {
                indexZy++;
            }
        }
        ByteBuffer bufferzy = ByteBuffer.allocate(buffer.length + indexZy);
        int indexxb = 0;
        while (indexxb < buffer.length) {
            if (buffer[indexxb] == (byte) 0x85 || buffer[indexxb] == (byte) 0x90 || buffer[indexxb] == (byte) 0x95) {
                bufferzy.put((byte) 0x90);
                bufferzy.put(buffer[indexxb]);
            } else {
                bufferzy.put(buffer[indexxb]);
            }
            indexxb++;
        }
        bufferzy.flip();
        byte[] valueArray = new byte[bufferzy.remaining()];
        bufferzy.get(valueArray);
        return valueArray;
    }

    /**
     * int转byte，指定字节数
     *
     * @param intData
     * @param strLength
     * @return
     */
    public static byte[] IntSplitToByte(int intData, int strLength) {
        String str = Integer.toHexString(intData);
        int strLen = str.length();
        StringBuffer sb;
        while (strLen < strLength * 2) {
            sb = new StringBuffer();
            sb.append("0").append(str);// 左补0
            str = sb.toString();
            strLen = str.length();
        }
        byte[] buffer = new byte[strLength];
        int index = 0;
        for (int i = 0; i < strLength; i++) {
            byte[] bytes = StringToBytes(str.substring(index, index + 2));
            System.arraycopy(bytes, 0, buffer, i, bytes.length);
            index = index + 2;
        }
        return buffer;
    }

    public static byte[] toStrByte(int intData, int strLength) {
        String str = Integer.toHexString(intData);
        byte[] buffer = new byte[strLength];
        if (str.length() == 4) {
            int index = 0;
            for (int i = 0; i < strLength; i++) {
                byte[] bytes = StringToBytes(str.substring(index, index + 2));
                System.arraycopy(bytes, 0, buffer, i, bytes.length);
                index = index + 2;
            }
        } else {
            buffer[0] = (byte) intData;
        }
        return buffer;
    }

    public static String IntSplitToString(String intData, int strLength) {
        int strLen = intData.length();
        StringBuffer sb = null;
        while (strLen < strLength) {
            sb = new StringBuffer();
            sb.append("0").append(intData);// 左补0
            intData = sb.toString();
            strLen = intData.length();
        }
        assert sb != null;
        String strBuff = sb.toString();
        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0; i < strBuff.length(); i++) {
            stringBuffer.append(strBuff.charAt(i));
            if (i < strBuff.length() - 1) {
                stringBuffer.append(",");
            }
        }
        return stringBuffer.toString();
    }

    /**
     * 将16进制字符串转换为byte[]
     *
     * @param str
     * @return
     */
    private static byte[] StringToBytes(String str) {
        if (str == null || str.trim().equals("")) {
            return new byte[0];
        }
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < str.length() / 2; i++) {
            String subStr = str.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }
        return bytes;
    }


    /**
     * 分包发送数据
     *
     * @param data 发送数据
     */
    public static List<byte[]> writeEntity(byte[] data, int size) {
        if (data == null) {
            return null;
        }
        List<byte[]> packDataList = new LinkedList<>();
        int index = 0;
        int runSize = 0;
        int lastDataSize;
        int length = data.length;
        while (index < length) {
            byte[] txBuffer = new byte[size];
            for (int i = 0; i < size; i++) {
                if (index < length) {
                    txBuffer[i] = data[index++];
                }
                runSize++;
            }
            if (length == index) {
                lastDataSize = size - (runSize - index);
                byte[] lastBuffer = new byte[lastDataSize];
                System.arraycopy(txBuffer, 0, lastBuffer, 0, lastDataSize);
                packDataList.add(lastBuffer);
            } else {
                packDataList.add(txBuffer);
            }
        }
        return packDataList;
    }

    public static Bitmap setGenerateImage() {
        int width = 400;
        int height = 300;
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);//画笔属性是实心圆
        canvas.drawCircle(width / 4, height / 2, 60, paint);
        canvas.save();
        canvas.restore();
        Paint paint2 = new Paint();
        paint2.setColor(Color.BLACK);
        paint2.setStyle(Paint.Style.FILL);//画笔属性是实心圆
        paint2.setStrokeWidth(8);//设置画笔粗细
        canvas.drawCircle(width - width / 4, height / 2, 60, paint2);
        canvas.save();
        canvas.restore();
        return bitmap;
    }

    public static Bitmap setGenerate128296() {
        int width = 296;
        int height = 128;
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);//画笔属性是实心圆
        canvas.drawCircle(width / 4, height / 2, 40, paint);
        canvas.save();
        canvas.restore();
        Paint paint2 = new Paint();
        paint2.setColor(Color.BLACK);
        paint2.setStyle(Paint.Style.FILL);//画笔属性是实心圆
        paint2.setStrokeWidth(8);//设置画笔粗细
        canvas.drawCircle(width - width / 4, height / 2, 40, paint2);
        canvas.save();
        canvas.restore();
        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        String str = "ABCDEF";
        Rect rect = new Rect();
        textPaint.getTextBounds(str, 0, str.length(), rect);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(30);
        textPaint.setStyle(Paint.Style.FILL);
        //该方法即为设置基线上那个点究竟是left,center,还是right 这里我设置为center
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setTypeface(Typeface.DEFAULT_BOLD);
        Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
        textPaint.setTypeface(font);
        canvas.drawText(str, 0, 30, textPaint);
        canvas.save();
        canvas.restore();
        return adjustPhotoRotation(bitmap, 270);
    }


    /**
     * @param orientationDegree 0 - 360 范围
     * @return
     */
    public static Bitmap adjustPhotoRotation(Bitmap bm, final int orientationDegree) {

        Matrix m = new Matrix();
        m.setRotate(orientationDegree, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);

        try {
            Bitmap bm1 = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);

            return bm1;

        } catch (OutOfMemoryError ex) {
        }
        return null;

    }
}
