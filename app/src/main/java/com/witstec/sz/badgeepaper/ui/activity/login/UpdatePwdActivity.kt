package com.witstec.sz.badgeepaper.ui.activity.login

import android.os.Bundle
import android.text.TextUtils
import com.witstec.sz.badgeepaper.Constant
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.myStartActivity
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.saveStringData
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.utils.ActivityManager
import com.witstec.sz.badgeepaper.utils.ToastUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.layout_retrieve_pwd_email_2.*
import java.util.concurrent.TimeUnit

class UpdatePwdActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_pwd)
        setToolbar(getString(R.string.item_pwd_update))
        //点击修改密码
        btn_submit_retrieve.setOnClickListener {
            if (TextUtils.isEmpty(ed_input_retrieve_pwd.text.toString()) || TextUtils.isEmpty(
                    ed_input_retrieve_pwd_to.text.toString()
                )
            ) {
                ToastUtils.show(getString(R.string.edit_is_null))
                return@setOnClickListener
            }

            //显示进度对话框
            showDialog()
            addDisposable(ApiRepository.changePwd(
                ed_input_retrieve_pwd.text.toString(),
                ed_input_retrieve_pwd_to.text.toString(),
                this
            )
                .subscribe({
                    saveStringData(Constant.TOKEN, "")
                    saveStringData(Constant.AVATAR_USER_INFO, "")
                    saveStringData(Constant.USER_NAME, "")
                    saveStringData(Constant.UID, "")
                    saveStringData(Constant.REFRESH_TOKEN, "")
                    saveStringData(Constant.USER_NAME_INFO, "")
                    addDisposable(
                        Observable.timer(800, TimeUnit.MILLISECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                dialogDismiss()
                                ToastUtils.show(getString(R.string.item_pwd_update_success))
                                ActivityManager.getInstance().finishAllActivity()
                                myStartActivity(this, LoginActivity::class.java)
                            })
                }, {
                    dialogDismiss()
                    it.message?.let { it1 -> ToastUtils.show(it1) }
                })
            )
        }
    }
}
