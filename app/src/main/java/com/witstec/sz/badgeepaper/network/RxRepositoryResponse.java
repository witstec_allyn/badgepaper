package com.witstec.sz.badgeepaper.network;

import android.content.Context;

import com.google.gson.Gson;
import com.witstec.sz.badgeepaper.model.bean.ErrorRequest;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class RxRepositoryResponse {

    /**
     * 先检查 status 是否正常，!=0 则直接抛出
     * 把 Observable<ResponseWrapper<T>> 转换为 Observable<T>
     */
    private static <T> Observable<T> unWrap(Observable<Response<T>> response, Context context) {
        return response.flatMap(
                responseWrapper -> {
                    if (!responseWrapper.isSuccessful()) {
                        assert responseWrapper.errorBody() != null;
                        String errorJson = responseWrapper.errorBody().string();
                        ErrorRequest errorData = new Gson().fromJson(errorJson, ErrorRequest.class);
                        ExceptionUtils.throwApiCodeException(errorData.getErrcode(), context);
                    }
                    if (responseWrapper.body() != null) {
                        return Observable.just(responseWrapper.body());
                    } else {
                        return Observable.just((T) new Object());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 解除 Wrapper，拿到 data
     * @param <T> model
     */
    public static <T> ObservableTransformer<Response<T>, T> unWrap(Context context) {
        return upstream -> unWrap(upstream,context);
    }


    /**
     * 验证检查 status 是否正常，!=0 则直接抛出
     */
    public static ObservableTransformer<Response, Response> verify(Context context) {
        return upstream -> verify(upstream,context);
    }

    /**
     * 验证检查 status 是否正常，!=0 则直接抛出
     */
    private static Observable<Response> verify(Observable<Response> observable, Context context) {
        return observable.flatMap(
                responseWrapper -> {
                    if (!responseWrapper.isSuccessful()) {
                        assert responseWrapper.errorBody() != null;
                        String errorJson = responseWrapper.errorBody().string();
                        ErrorRequest errorData = new Gson().fromJson(errorJson, ErrorRequest.class);
                        ExceptionUtils.throwApiCodeException(errorData.getErrcode(), context);
                    }
                    assert responseWrapper.body() != null;
                    return Observable.just(responseWrapper);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    /**
     * 如果订阅结果在 interval 时间内得到，则延迟 interval - tTimed.time() 之后再发生
     *
     * @param interval 间隔时间
     * @param <T>      model
     */
    public static <T> ObservableTransformer<T, T> delayInterval(final long interval) {
        return upstream -> upstream.timeInterval()
                .flatMap(tTimed -> {
                    if (tTimed.time() < interval) {
                        return Observable.just(tTimed.value())
                                .delay(interval - tTimed.time(), TimeUnit.MILLISECONDS);
                    }
                    return Observable.just(tTimed.value());
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
