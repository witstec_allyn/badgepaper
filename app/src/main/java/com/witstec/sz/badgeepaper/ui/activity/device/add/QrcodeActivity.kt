package com.witstec.sz.badgeepaper.ui.activity.device.add

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.com.heaton.blelibrary.ble.Ble
import com.mylhyl.zxing.scanner.ScannerOptions
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.GattType
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.AddLoadingActivity
import com.witstec.sz.badgeepaper.utils.StringUtils
import com.witstec.sz.badgeepaper.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_qrcode.*

class QrcodeActivity : AppBaseActivity() {

    private var isOpen: Boolean = false
    private val REQUESTCODE: Int = 0x02

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcode)
        setToolbar(getString(R.string.add_device))
        requestBLEPermission()
    }

    private fun requestBLEPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA
            ),
            REQUESTCODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUESTCODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (grantResults.size !== 0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        // 判断用户是否 点击了不再提醒。(检测该权限是否还可以申请)
                        val b = shouldShowRequestPermissionRationale(permissions[0])
                        if (!b) {
                            // 用户还是想用我的 APP 的
                            // 提示用户去应用设置界面手动开启权限
                        } else
                            finish()
                    } else {
                        initView()
                    }
                } else {
                    initView()
                }
            }
        }
    }

    private fun initView() {
        val builder = ScannerOptions.Builder()
        builder.setFrameStrokeColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setFrameStrokeWidth(1.5f)
            .setFrameSize(240, 220)
            .setFrameCornerLength(22)
            .setCreateQrThumbnail(false)
            .setFrameCornerWidth(2)
            .setFrameCornerColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setFrameCornerInside(true)
            .setLaserLineColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setLaserLineHeight(18)
            .setLaserStyle(ScannerOptions.LaserStyle.RES_GRID, R.mipmap.zfb_grid_scan_line)//网格图
            .setFrameCornerColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setScanFullScreen(false)
            .setFrameHide(false)
            .setFrameCornerHide(false)
            .setLaserMoveFullScreen(false)
            .setTipText(getString(R.string.the_qr_code_or_barcode))
            .setTipTextSize(14)
            .setTipTextColor(resources.getColor(R.color.white))
            .setCameraZoomRatio(0.0)
        scanner_view.setScannerOptions(builder.build())
        scanner_view.setOnScannerCompletionListener { rawResult, parsedResult, barcode ->
            vibrate()
            if (scanner_view != null) {
                scanner_view.restartPreviewAfterDelay(0)
            }
            var macStr = rawResult.text

            if (!macStr.contains(":")) {
                val appadMac = StringBuffer()
                for (it in macStr.indices) {
                    appadMac.append(macStr[it])
                    if (it % 2 == 1) {
                        appadMac.append(":")
                    }
                }
                macStr = appadMac.toString().substring(0, appadMac.toString().length - 1)
            }
            if (StringUtils.checkBluetoothAddress(macStr)) {
                App.device_name = "WTCard"
                val intent = Intent()
                intent.setClass(this@QrcodeActivity, AddLoadingActivity::class.java)
                intent.putExtra("extra_qrcode", macStr)
                intent.putExtra("ota_enum", GattType.QR_CODE)
                startActivity(intent)
                finish()
            } else {
                ToastUtils.show(getString(R.string.no_barCode))
            }
        }

        btn_open_light.setOnClickListener {
            if (isOpen) {
                btn_open_light.setBackgroundResource(R.mipmap.ic_dp_off)
                scanner_view.toggleLight(false)
                isOpen = false
            } else {
                btn_open_light.setBackgroundResource(R.mipmap.ic_dp_on)
                scanner_view.toggleLight(true)
                isOpen = true
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUESTCODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val i = ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
                if (i != PackageManager.PERMISSION_GRANTED) {
                    // 提示用户应该去应用设置界面手动开启权限
                } else {
                    //权限申请成功
                    initView()
                }
            }
        } else if (requestCode == Ble.REQUEST_ENABLE_BT) {
            initView()
        }
    }

    override fun onResume() {
        super.onResume()
        if (scanner_view != null) {
            scanner_view.onResume()
        }
    }

    override fun onPause() {
        super.onPause()
        if (scanner_view != null) {
            scanner_view.onPause()
        }
    }

    private fun vibrate() {
        val vibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
        vibrator.vibrate(300)
    }

}
