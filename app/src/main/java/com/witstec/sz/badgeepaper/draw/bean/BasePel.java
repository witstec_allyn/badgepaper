package com.witstec.sz.badgeepaper.draw.bean;

import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.RectF;

import com.witstec.sz.badgeepaper.Constants;


public class BasePel {
    /**
     * 以下四个点主要用于选中编辑画动画边框，选中会重新初始化
     */
    /**
     * 左上角
     */
    public PointF leftTopPoint;
    /**
     * 右上角
     */
    public PointF rightTopPoint;
    /**
     * 右下角f
     */
    public PointF rightBottomPoint;
    /**
     * 左下角
     */
    public PointF leftBottomPoint;
    /**
     * 拖动图标坐标
     */
    public RectF dragBtnRect;
    /**
     * 拖动图标
     */
    public Bitmap dragBitmap;

    /**
     * 以下参数保存文件时会进行保存
     */

    /**
     * x偏移
     */
    public float transDx;
    /**
     * y偏移
     */
    public float transDy;
    /**
     * 放大倍数
     */
    public float scale = 1;
    /**
     * 旋转角度
     */
    public float angle;
    /**
     * 中心点
     */
    public PointF centerPoint;
    /**
     * 开始点
     */
    public PointF beginPoint;
    /**
     * 最初区域右下角坐标
     */
    public PointF bottomRightPointF;
    /**
     * 画笔颜色
     */
    public int paintColor;
    /**\
     * 文档大小 1 2 3 4 5
     */
    public int textSize=2;

    /**
     * 图片效果 1-200
     */
    public int image_effect_size= Constants.image_effect_size;

    /**
     * 是否为虚线 1=虚线  0=实线
     */
    public int dotted_line = 0;

    /**
     * 是否填充  1填充红色  0填充黑色  -1不填充
     */
    public int fillColor=-1;
    /**
     * 发送时改变状态
     */
    public boolean isOriginalImage = false;

    /**
     * 是否为IOS平台
     */
    public boolean isIos = false;
    /**
     * 画笔粗细  1-2-3
     */
    public int pnWidth = 1;
}
