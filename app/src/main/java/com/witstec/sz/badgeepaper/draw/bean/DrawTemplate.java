package com.witstec.sz.badgeepaper.draw.bean;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

@Table("DrawTemplate")
public class DrawTemplate {

    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Column("xmlPath")
    private String xmlPath;
    @Column("imagePath")
    private String imagePath;
    @Column("createTime")
    private String createTime;
    @Column("deviceType")
    private String deviceType;
    @Column("fileName")
    private String fileName;
    @Column("tagName")
    private String tagName;
    @Column("tagId")
    private int tagId;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public DrawTemplate(String xmlPath, String imagePath, String createTime, String deviceType, String fileName, String tagName, int tagId) {
        this.xmlPath = xmlPath;
        this.imagePath = imagePath;
        this.createTime = createTime;
        this.deviceType = deviceType;
        this.fileName = fileName;
        this.tagName = tagName;
        this.tagId = tagId;
    }

    public DrawTemplate(String xmlPath, String imagePath, String createTime, String deviceType, String fileName) {
        this.xmlPath = xmlPath;
        this.imagePath = imagePath;
        this.createTime = createTime;
        this.deviceType = deviceType;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
