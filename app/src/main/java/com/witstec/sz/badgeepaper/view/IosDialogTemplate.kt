package com.witstec.sz.badgeepaper.view

import android.app.Dialog
import android.content.Context
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.TemplateTypeBean
import com.witstec.sz.badgeepaper.ui.adapter.SelectTemplateTypeAdapter


class IosDialogTemplate(private val context: Context) {
    private var dialog: Dialog? = null
    private var container: LinearLayout? = null
    private var titleTv: TextView? = null
    private var msgTv: TextView? = null
    private var negBtn: Button? = null
    private var posBtn: Button? = null
    private var edit_msg: EditText? = null
    private var img_line: View? = null
    private val display: Display
    private var showTitle = false
    private var showMsg = false
    private var showPosBtn = false
    private var showNegBtn = false
    private var typeId = 1
    init {
        val windowManager = context
            .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        display = windowManager.defaultDisplay
    }

    fun init(types: List<TemplateTypeBean>): IosDialogTemplate {
        // 获取Dialog布局
        val view = LayoutInflater.from(context).inflate(R.layout.view_alertdialog_modelo, null)
        // 获取自定义Dialog布局中的控件
        container = view.findViewById<View>(R.id.container) as LinearLayout
        titleTv = view.findViewById<View>(R.id.txt_title) as TextView
        titleTv!!.visibility = View.GONE
        msgTv = view.findViewById<View>(R.id.txt_msg) as TextView
        msgTv!!.visibility = View.GONE
        negBtn = view.findViewById<View>(R.id.btn_neg) as Button
        negBtn!!.visibility = View.GONE
        posBtn = view.findViewById<View>(R.id.btn_pos) as Button
        posBtn!!.visibility = View.GONE
        img_line = view.findViewById(R.id.img_line) as View
        edit_msg = view.findViewById(R.id.ed_msg) as EditText
        val recycler = view.findViewById(R.id.recyclerview_type) as RecyclerView
        edit_msg!!.visibility = View.GONE
        img_line!!.visibility = View.GONE

        // 定义Dialog布局和参数
        dialog = Dialog(context, R.style.AlertDialogStyle)
        dialog!!.setContentView(view)

        // 调整dialog背景大小
        container!!.layoutParams = FrameLayout.LayoutParams(
            (display.width * 0.85).toInt(),
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val adapter = SelectTemplateTypeAdapter()
        recycler.adapter = adapter
        recycler.layoutManager = GridLayoutManager(context, 2)
        adapter.replaceData(types)
        adapter.setOnItemClickListener { adapter, view, position ->
            val baseList = adapter.data as List<TemplateTypeBean>
            typeId = baseList[position].typeId
            baseList.forEach {
                it.isSelect = false
            }
            adapter.notifyDataSetChanged()
            val newBean = baseList[position]
            newBean.isSelect = true
            adapter.notifyItemChanged(position, newBean)
        }
        return this
    }

    fun setTitle(title: String): IosDialogTemplate {
        showTitle = true
        if ("" == title) {
            titleTv!!.setText(R.string.prompt)
        } else {
            titleTv!!.text = title
        }
        return this
    }

    fun setMsg(msg: String): IosDialogTemplate {
        showMsg = true
        if ("" == msg) {
            msgTv!!.setText(R.string.prompt)
        } else {
            msgTv!!.text = msg
        }
        return this
    }

    fun setEditTextContext(context: String, hint: String): IosDialogTemplate {
        edit_msg!!.visibility = View.VISIBLE
        msgTv!!.visibility = View.GONE
        edit_msg!!.hint = hint
        edit_msg!!.setText(context)
        edit_msg!!.setSelection(context.length)
        return this
    }

    fun getEditContext(): EditText {
        return edit_msg!!
    }

    fun setCancelable(cancel: Boolean): IosDialogTemplate {
        dialog!!.setCancelable(cancel)
        return this
    }

    fun setPositiveButton(text: String, listener: View.OnClickListener): IosDialogTemplate {
        showPosBtn = true
        if ("" == text) {
            posBtn!!.setText(R.string.fixed)
        } else {
            posBtn!!.text = text
        }
        posBtn!!.setOnClickListener { v ->
            listener.onClick(v)
            dialog!!.dismiss()
        }
        return this
    }

    fun setPositiveButton(
        text: String,
        listener: View.OnClickListener,
        edit: EditOnClickListener
    ): IosDialogTemplate {
        showPosBtn = true
        if ("" == text) {
            posBtn!!.setText(R.string.fixed)
        } else {
            posBtn!!.text = text
        }
        posBtn!!.setOnClickListener { v ->
            listener.onClick(v)
            edit.editContext(getEditContext().text.toString(), typeId)
            dialog!!.dismiss()
        }
        return this
    }

    fun setNegativeButton(text: String, listener: View.OnClickListener): IosDialogTemplate {
        showNegBtn = true
        if ("" == text) {
            negBtn!!.setText(R.string.cancel)
        } else {
            negBtn!!.text = text
        }
        negBtn!!.setOnClickListener { v ->
            listener.onClick(v)
            dialog!!.dismiss()
        }
        return this
    }

    private fun setLayout() {
        if (!showTitle && !showMsg) {
            titleTv!!.setText(R.string.prompt)
            titleTv!!.visibility = View.VISIBLE
        }

        if (showTitle) {
            titleTv!!.visibility = View.VISIBLE
        }

        if (showMsg) {
            msgTv!!.visibility = View.VISIBLE
        }

        if (!showPosBtn && !showNegBtn) {
            posBtn!!.setText(R.string.fixed)
            posBtn!!.visibility = View.VISIBLE
            posBtn!!.setOnClickListener { dialog!!.dismiss() }
        }

        if (showPosBtn && showNegBtn) {
            posBtn!!.visibility = View.VISIBLE
            negBtn!!.visibility = View.VISIBLE
            img_line!!.visibility = View.VISIBLE
        }

        if (showPosBtn && !showNegBtn) {
            posBtn!!.visibility = View.VISIBLE
        }

        if (!showPosBtn && showNegBtn) {
            negBtn!!.visibility = View.VISIBLE
        }
    }

    fun show() {
        setLayout()
        if (dialog != null)
            dialog!!.show()
    }

    interface EditOnClickListener {
        fun editContext(context: String, typeId: Int)
    }
}