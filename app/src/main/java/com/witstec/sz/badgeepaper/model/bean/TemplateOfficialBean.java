package com.witstec.sz.badgeepaper.model.bean;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.NotNull;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

@Table("TemplateOfficialBean")
public class TemplateOfficialBean {

    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @NotNull
    @Column("templateId")
    private String templateId;
    @NotNull
    @Column("templateName")
    private String templateName;
    @NotNull
    @Column("templateType")
    private String templateType;
    @NotNull
    @Column("templateImage")
    private String templateImage;
    @NotNull
    @Column("templateFileUrl")
    private String templateFileUrl;
    @NotNull
    @Column("templateCreateTime")
    private String templateCreateTime;
    @Column("inputDataJson")
    private String inputDataJson = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TemplateOfficialBean(String templateId, String templateName, String templateType, String templateImageUrl, String templateFileUrl, String templateCreateTime) {
        this.templateId = templateId;
        this.templateName = templateName;
        this.templateType = templateType;
        this.templateImage = templateImageUrl;
        this.templateFileUrl = templateFileUrl;
        this.templateCreateTime = templateCreateTime;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getTemplateImage() {
        return templateImage;
    }

    public void setTemplateImage(String templateImage) {
        this.templateImage = templateImage;
    }

    public String getTemplateFileUrl() {
        return templateFileUrl;
    }

    public void setTemplateFileUrl(String templateFileUrl) {
        this.templateFileUrl = templateFileUrl;
    }

    public String getTemplateCreateTime() {
        return templateCreateTime;
    }

    public void setTemplateCreateTime(String templateCreateTime) {
        this.templateCreateTime = templateCreateTime;
    }


    public String getInputDataJson() {
        return inputDataJson;
    }

    public void setInputDataJson(String inputDataJson) {
        this.inputDataJson = inputDataJson;
    }

}
