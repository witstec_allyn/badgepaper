package com.witstec.sz.badgeepaper.network;

import com.google.gson.annotations.SerializedName;

public class ResponseWrapper<T> {
    @SerializedName("success")
    private boolean success;
    @SerializedName("status")
    private String status;
    @SerializedName("errcode")
    private int errcode;
    @SerializedName("result")
    private T result;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseWrapper{" +
                "code=" + status +
                ", msg='" + status + '\'' +
                ", data=" + result +
                '}';
    }
}
