package com.witstec.sz.badgeepaper.ui.activity.device.longConnection

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothProfile
import android.os.SystemClock
import android.text.TextUtils
import android.view.View
import cn.com.heaton.blelibrary.ble.Ble
import com.blakequ.bluetooth_manager_lib.connect.BluetoothConnectManager
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.manage.BitmapBleBmpManagement.*
import com.witstec.sz.badgeepaper.model.bean.GattType
import com.witstec.sz.badgeepaper.model.bean.Process
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.ui.activity.MainActivity
import com.witstec.sz.badgeepaper.ui.activity.device.add.AddSuccessActivity
import com.witstec.sz.badgeepaper.utils.*
import com.witstec.sz.badgeepaper.utils.LogHelper.i
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_loading.*
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.zip.CRC32

/**
 * 添加设备/OTA
 */
open class AddLoadingActivity : BleConnectionActivity() {

    private var isModeUpdate: Boolean = false
    private var otaBytes: ByteArray = byteArrayOf()
    private var bleType: String = "BER029B"
    private var otaVersion = "01"
    private var filePath = ""
    private var isConnectionOk = false
    private var isCard = false
    private var instructionStr = ""
    private var gattType: GattType = GattType.ADD_DEVICE
    private var isConnection = true
    private var mIndex = 0
    private var byteListSize = 0
    private var isCloseLoop = false
    private var sendContinuously = false

    override fun getContentViewLayout(): Int {
        return R.layout.activity_add_loading
    }

    override fun onStop() {
        super.onStop()
        if (timeout != null) {
            timeout!!.dispose()
        }
    }

    override fun onResume() {
        super.onResume()
        Ble.options().isAutoConnect = false
        Ble.getInstance<BleRssiDevice>().disconnectAll()
        SystemClock.sleep(400)
        connectionBle()
    }

    override fun initView() {
        gattType = intent.getSerializableExtra("ota_enum") as GattType
        if (gattType == GattType.ADD_DEVICE || gattType == GattType.IMAGE_COMMEND) {
            setToolbar(getString(R.string.add_device_loading_one) + App.device_name)
            if (gattType == GattType.IMAGE_COMMEND) {
                isModeUpdate = true
            }
            ll_add_loading_process.addProcessLayout(Process.ONE_ADD)
            mDeviceAddress = intent.getStringExtra("extra_address")
        } else if (gattType == GattType.QR_CODE) {
            setToolbar(getString(R.string.add_device_loading_one) + App.device_name)
            val code = intent.getStringExtra("extra_qrcode")
            if (!TextUtils.isEmpty(code)) {
                mDeviceAddress = code
            }
            gattType = GattType.ADD_DEVICE
        } else if (gattType == GattType.OTA_EMPTY_MEORY) {
            setToolbar(getString(R.string.ota_update))
            otaVersion = intent.getStringExtra("extra_version").toString()
            isCard = intent.getBooleanExtra("extra_isCard", false)
            bleType = intent.getStringExtra("extra_type").toString()
            mDeviceAddress = intent.getStringExtra("extra_address")
            filePath = intent.getStringExtra("file_path").toString()
            ll_add_loading_process.addProcessLayout(Process.OTA_1)
        }
        tv_mac.text = mDeviceAddress
        lottieAnimationView.playAnimation()
    }

    override fun listener() {
        btn_next_submit.setOnClickListener {
            if (connectManager == null) {
                connectManager = BluetoothConnectManager.getInstance(this)
            }
            isConnectionOk = false
            if (gattType == GattType.OTA_EMPTY_MEORY || gattType == GattType.OTA_DATA_SEND ||
                gattType == GattType.OTA_SAVE || gattType == GattType.OTA_CHECK
            ) {
                gattType = GattType.OTA_EMPTY_MEORY
                btn_next_submit.visibility = View.GONE
                ll_add_loading_process.addProcessLayout(Process.OTA_1)
                lottieAnimationView.playAnimation()
                connectManager!!.connect(mDeviceAddress)
                addLoadTimeOut()
            } else {
                btn_next_submit.visibility = View.GONE
                ll_add_loading_process.addProcessLayout(Process.ONE_ADD)
                lottieAnimationView.playAnimation()
                connectManager!!.connect(mDeviceAddress)
                addLoadTimeOut()
            }
        }
    }

    override fun onMtuChangedSuper(gatt: BluetoothGatt?, mtu: Int, status: Int) {

    }

    override fun onCharacteristicReadSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    ) {
        if (status == BluetoothGatt.GATT_SUCCESS && gattCharacteristicData.size != 0) {
            val byteValue = characteristic.value
            val uuid = characteristic.uuid
            i("gattService", "onCharacteristicRead GATT_SUCCESS successfully")
            if (UUID.fromString(gattCharacteristicData[1][0][LIST_UUID]) == uuid) {
                i(
                    "sendImage",
                    "型号 value=" + ByteUtils.hexStr2Str(
                        ByteUtils.byteArrayToHexStringLX(
                            characteristic.value
                        )
                    )
                )
//                bleType =
//                    ByteUtils.hexStr2Str(ByteUtils.byteArrayToHexStringLX(byteValue))
//                if (ByteUtils.byteArrayToHexStringLX(byteValue) == "FFFFFFFFFFFFFF") {
//                    dialogDismiss()
//                    runOnUiThread {
//                        ToastUtils.show(this@AddLoadingActivity, getString(R.string.no_model_type))
//                    }
//                    return
//                }
//                if (bleType.isNotEmpty()) {
//                    runOnUiThread {
//                        sendData(
//                            gattCharacteristicData[0][2][LIST_UUID]
//                                ?: error(""),
//                            GattType.VERSION_GET
//                        )
//                    }
//                }
//            } else if (UUID.fromString(gattCharacteristicData[2][0][LIST_UUID]) == uuid) {
//                i(
//                    "sendImage",
//                    "电量 value=" + Integer.valueOf(
//                        ByteUtils.byteArrayToHexStringLX(byteValue),
//                        16
//                    )
//                )
//                if (isModeUpdate) {
//                    electricity =
//                        Integer.valueOf(
//                            ByteUtils.byteArrayToHexStringLX(byteValue),
//                            16
//                        )
//                    if (electricity != -1) {
//                        sendData(
//                            gattCharacteristicData[1][0][LIST_UUID]
//                                ?: error(""),
//                            GattType.GET_TYPE_NAME
//                        )
//                    }
//                }
//            } else if (UUID.fromString(gattCharacteristicData[0][2][LIST_UUID]) == uuid
//                && gattType != GattType.OTA_DATA_SEND &&
//                gattType != GattType.OTA_SAVE && gattType != GattType.OTA_CHECK
//                && gattType != GattType.OTA_EMPTY_MEORY
//            ) {
//                i(
//                    "sendImage",
//                    "固件版本号 value=" +
//                            ("" + byteValue[0].toString() + "" + byteValue[1].toString()).replace(
//                                "-",
//                                ""
//                            )
//                )
//                otaVersion =
//                    ("" + byteValue[0].toString() + "" + byteValue[1].toString()).replace(
//                        "-",
//                        ""
//                    )
//                loadCloudConnection()
                if (gattType != GattType.OTA_DATA_SEND &&
                    gattType != GattType.OTA_SAVE && gattType != GattType.OTA_CHECK
                    && gattType != GattType.OTA_EMPTY_MEORY
                ) {
                    loadCloudConnection()
                }
            }
        } else {
            i(
                "gattService",
                "onCharacteristicRead value=" +
                        StringUtils.bytesToHexString(
                            characteristic.value
                        )
            )
        }
    }

    override fun onCharacteristicWriteSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    ) {
        if (status == BluetoothGatt.GATT_SUCCESS && gattType != GattType.OTA_DATA_SEND &&
            gattType != GattType.OTA_SAVE && gattType != GattType.OTA_CHECK
            && gattType != GattType.OTA_EMPTY_MEORY
        ) {
            if (characteristic.value[0] == 0.toByte() && characteristic.value[1] == 0.toByte()
                && characteristic.value[2] == 0.toByte()
            ) {
                PaperBaseActivity.start(
                    this@AddLoadingActivity,
                    mDeviceAddress!!,
                    otaVersion
                )
                if (timeout != null) {
                    timeout!!.dispose()
                }
                finish()
            }
            i("gattService", "onCharacteristicWrite GATT_SUCCESS successfully")
        }
    }

    override fun onCharacteristicChangedSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic
    ) {
        i(
            "sendImage",
            "onCharacteristicChanged value=" + ByteUtils.byteArrayToHexString(
                characteristic.value
            )
        )
        val returnBytes =
            ByteUtils.byteArrayToHexString(characteristic.value).split(",")
        when ((returnBytes[5] + returnBytes[6])) {
            "82AA" ->
                gattType = if (returnBytes[8] == "00") {
                    //内存不为空，继续清空
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                        GattType.OTA_EMPTY_MEORY
                    )
                    GattType.OTA_EMPTY_MEORY
                } else {
                    //内存为空，发送文件
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                        GattType.OTA_DATA_SEND
                    )
                    GattType.OTA_DATA_SEND
                }
            "0034", "0024" -> {
                if (returnBytes[8] == "00") {
                    LogHelper.i("ota", "收到的固件切换的命令")
                    sendContinuously = true
                    if (timeout != null) {
                        timeout!!.dispose()
                    }
                    timeout = Observable.timer(
                        if (isCard) 1000 * 6 else 1000 * 12, TimeUnit.MILLISECONDS
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            ll_add_loading_process.addProcessLayout(Process.OTA_4)
                            //更新完成
                            btn_next_submit.visibility = View.VISIBLE
                            gattType = GattType.OTA_EMPTY_MEORY
                            btn_next_submit.text =
                                getString(R.string.cary_out)
                            lottieAnimationView.cancelAnimation()
                            BleClear()
                            btn_next_submit.setOnClickListener {
                                ActivityManager.getInstance()
                                    .backTo(MainActivity::class.java)
                            }
                            if (timeout != null) {
                                timeout!!.dispose()
                            }
                        }
                    addDisposable(timeout!!)
                }
            }
            "0031", "0020" -> {
                if (returnBytes[8] == "00") {
                    sendContinuously = true
                    SystemClock.sleep(200)
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                        GattType.OTA_DATA_SEND
                    )
                    gattType = GattType.OTA_DATA_SEND
                }
            }
            "0032", "0022" -> {
                if (returnBytes[12] == "00") {
                    if (mIndex == byteListSize - 1) {
                        //退出发送图片循环
                        isCloseLoop = true
                        mIndex = 0
                        byteListSize = 0
                        sendData(
                            gattCharacteristicData[0][0][LIST_UUID]
                                ?: error(""),
                            GattType.OTA_CHECK
                        )
                        gattType = GattType.OTA_CHECK
                    }
                    isConnection = true
                    if (mIndex < byteListSize - 1) {
                        mIndex++
                    }
                    LogHelper.i(
                        "sendimg",
                        "if (mIndex ==byteListSize) {," + "byteList.size=" + byteListSize + ",mIndex=" + mIndex
                    )
                } else {
                    isConnection = true
                }

                runOnUiThread {
                    when (returnBytes[12]) {
                        "00" ->
                            i("otaState", "成功-可传输下一包")
                        "01" ->
                            i("otaState", "步骤错误")
                        "02" ->
                            i("otaState", "参数异常,Pack_Num 不连续")
                        "03" ->
                            i("otaState", " 参数异常,Pack_Num 超出 Pack_Sum")
                        "04" ->
                            i("otaState", " 参数错误,累计接收超过 Firmware_Size")
                        "05" ->
                            i("otaState", " 未知错误")
                    }
                }

            }
            "0021" -> {
                if (returnBytes[8] == "00") {
                    sendContinuously = true
                    SystemClock.sleep(200)
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                        GattType.OTA_DATA_SEND
                    )
                    gattType = GattType.OTA_DATA_SEND
                } else {
                    sendContinuously = true
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                        GattType.OTA_CHECK_EMPTY_MEORY
                    )
                    gattType = GattType.OTA_CHECK_EMPTY_MEORY
                }
            }
            "0030" -> {
                if (returnBytes[8] == "00") {
                    sendContinuously = true
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                        GattType.OTA_CHECK_EMPTY_MEORY
                    )
                    gattType = GattType.OTA_CHECK_EMPTY_MEORY
                } else {
                    ToastUtils.show(getString(R.string.error))
                    ll_add_loading_process.addProcessLayout(Process.OTA__START_ERROR)
                }
            }
            "0033", "0023" -> {
                i("sendImage", "校验状态=" + returnBytes[8])
                if (returnBytes[8] == "00" || returnBytes[8] == "02") {
                    sendContinuously = true
                    sendData(
                        gattCharacteristicData[0][0][LIST_UUID]
                            ?: error(""),
                        GattType.OTA_SAVE
                    )
                    gattType = GattType.OTA_SAVE
                }
            }
        }
    }

    override fun onConnectionStateChangeSuper(gatt: BluetoothGatt, status: Int, newState: Int) {
        i("sendImage", "蓝牙连接状态改变 状态=$newState")
        if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            if (connectManager != null) {
                connectManager!!.release()
                connectManager!!.closeAll()
            }
        }
    }

    override fun displayGattServicesInit() {
        when (gattType) {
            GattType.OTA_EMPTY_MEORY -> {
                SystemClock.sleep(100)
                //进入ota模式
                sendData(
                    gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                    GattType.OTA_EMPTY_MEORY
                )
            }

            GattType.ADD_DEVICE -> {
                sendData(
                    gattCharacteristicData[1][0][LIST_UUID] ?: error(""),
                    GattType.GET_TYPE_NAME
                )
            }

            GattType.IMAGE_COMMEND -> {
                sendData(
                    gattCharacteristicData[2][0][LIST_UUID] ?: error(""),
                    GattType.GET_ELECTRICITY
                )
            }

            else -> {

            }
        }
    }

    override fun sendDataInit(types: GattType) {
        when (types) {
            GattType.GET_TYPE_NAME, GattType.GET_ELECTRICITY, GattType.VERSION_GET -> {
                val disposable =
                    Observable.create(ObservableOnSubscribe<Boolean> { e ->
                        var isSendState4 = false
                        while (!isSendState4) {
                            var uuidStr: UUID
                            if (types == GattType.GET_TYPE_NAME) {
                                uuidStr =
                                    UUID.fromString(gattCharacteristicData[1][0][LIST_UUID])
                            } else if (types == GattType.GET_ELECTRICITY) {
                                uuidStr =
                                    UUID.fromString(gattCharacteristicData[2][0][LIST_UUID])
                            } else {
                                uuidStr =
                                    UUID.fromString(gattCharacteristicData[0][2][LIST_UUID])
                            }
                            gatt!!.readCharacteristic(characteristic)
                            val descriptor =
                                characteristic!!.getDescriptor(
                                    uuidStr
                                )
                            if (descriptor != null) {
                                descriptor.value =
                                    BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                                gatt!!.writeDescriptor(descriptor)
                            }
                            //这一步必须要有 否则收不到通知
                            isSendState4 =
                                gatt!!.setCharacteristicNotification(
                                    characteristic,
                                    true
                                )
                            if (isSendState4) {
                                e.onNext(true)
                                break
                            }
                        }
                    })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { isStatus ->
                        }
                addDisposable(disposable)
            }

            GattType.OTA_CHECK_EMPTY_MEORY -> {
                sendContinuously = false
                //清空内存
                val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
                    val byteListChange: ByteArray? = if (isCard) {
                        getBmpProtocolDataChange(49)
                    } else {
                        OTADataClear(32, otaBytes.size)
                    }
                    var isSendState3 = false
                    while (!isSendState3) {
                        if (sendContinuously) {
                            break
                        }
                        SystemClock.sleep(200)
                        instructionStr = if (isCard) "0031" else "0020"
                        characteristic!!.value = byteListChange
                        isSendState3 = gatt!!.writeCharacteristic(characteristic)
                        i(
                            "sendImage",
                            "底座 OTA 请求-区域擦除${byteListChange?.let { Utils.bytesToHexString(it) }}" + "isSendState3=" + isSendState3
                        )
                        if (isSendState3) {
                            e.onNext(true)
                        }
                    }
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isStatus ->
                        i(
                            "sendImage",
                            "发送清除OTA指令 等待回复"
                        )
                    }
                addDisposable(disposable)
            }

            GattType.OTA_EMPTY_MEORY -> {
                sendContinuously = false
                //读取.bin文件，转成二进制传给蓝牙
                val inputStream: InputStream = FileInputStream(File(filePath))
                otaBytes = FileUtils.toByteArray(inputStream)
                val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
                    val byteListChange: ByteArray? = if (isCard) {
                        OTADataClear(48, otaBytes.size)
                    } else {
                        getBmpProtocolDataChange(33)
                    }
                    var isSendState3 = false
                    while (!isSendState3) {
                        if (sendContinuously) {
                            break
                        }
                        SystemClock.sleep(300)
                        instructionStr = if (isCard) "0030" else "0021"
                        characteristic!!.value = byteListChange
                        isSendState3 = gatt!!.writeCharacteristic(characteristic)
                        i(
                            "sendImage",
                            "底座 OTA 请求-区域检查${byteListChange?.let { Utils.bytesToHexString(it) }}" + "isSendState3=" + isSendState3
                        )
                        if (isSendState3) {
                            e.onNext(true)
                        }
                    }
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isStatus ->

                    }
                addDisposable(disposable)

//                i("sendImage", "huidiao" + "RED_DATA")
//                val disposable2 =
//                    Observable.create(ObservableOnSubscribe<Boolean> { e ->
//                        var isDesc = false
//                        while (!isDesc) {
//                            if (isCloseLoop) {
//                                break
//                            }
//                            val isSendState4 =
//                                gatt!!.setCharacteristicNotification(
//                                    characteristic,
//                                    true
//                                )
//                            if (isSendState4) {
//                                val descriptor =
//                                    characteristic!!.getDescriptor(
//                                        characteristic!!.descriptors[0].uuid
//                                    )
//                                if (descriptor != null) {
//                                    descriptor.value =
//                                        BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
//                                    isDesc = gatt!!.writeDescriptor(descriptor)
//                                    if (isDesc) {
//                                        e.onNext(true)
//                                        break
//                                    }
//                                }
//                            }
//                        }
//                    })
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe { isStatus ->
//
//                        }
//                addDisposable(disposable2)
            }

            GattType.OTA_DATA_SEND -> {
                //发送OTA数据
                runOnUiThread {
                    ll_add_loading_process.addProcessLayout(Process.OTA_2)
                }
                mIndex = 0
                byteListSize = 0
                isConnection = true
                isCloseLoop = false
                loadFirmware(filePath)
            }

            GattType.OTA_CHECK -> {
                sendContinuously = false
//                //校验
//                runOnUiThread {
//                    ll_add_loading_process.addProcessLayout(Process.OTA_3)
//                }
                val crc32 = CRC32()
                crc32.update(otaBytes)
                val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
                    val byteListChange =
                        sendOTACheck(if (isCard) 51 else 35, crc32.value.toInt())
                    var isSendState3 = false
                    while (!isSendState3) {
                        if (sendContinuously) {
                            break
                        }
                        SystemClock.sleep(300)
                        instructionStr = if (isCard) "0033" else "0023"
                        characteristic!!.value = byteListChange
                        isSendState3 = gatt!!.writeCharacteristic(characteristic)
                        i(
                            "sendImage",
                            "底座 OTA 请求-CRC32 校验${Utils.bytesToHexString(byteListChange)}"
                        )
                        if (isSendState3) {
                            e.onNext(true)
                        }
                    }
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isStatus ->
                        //发送保存命令
                        gattType = GattType.OTA_CHECK
                    }
                addDisposable(disposable)
            }

            GattType.OTA_SAVE -> {
                sendContinuously = false
                //保存OTA
                val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
                    val byteListChange = getBmpProtocolDataChange(if (isCard) 52 else 36)
                    var isSendState3 = false
                    while (!isSendState3) {
                        if (sendContinuously) {
                            break
                        }
                        SystemClock.sleep(300)
                        instructionStr = if (isCard) "0034" else "0024"
                        characteristic!!.value = byteListChange
                        isSendState3 = gatt!!.writeCharacteristic(characteristic)
                        i(
                            "sendImage",
                            "底座 OTA 请求-固件切换${Utils.bytesToHexString(byteListChange)}"
                        )
                        if (isSendState3) {
                            e.onNext(true)
                        }
                    }
                })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isStatus ->
                        ll_add_loading_process.addProcessLayout(Process.OTA_3)
                    }
                addDisposable(disposable)
            }
        }
    }

    override fun onServicesDiscoveredSuper(gatt: BluetoothGatt) {
        i("sendImage", "连接成功")
        runOnUiThread {
            if (gattType == GattType.IMAGE_COMMEND || gattType == GattType.ADD_DEVICE) {
                ll_add_loading_process.addProcessLayout(Process.TWO_ADD)
            }
            isConnectionOk = true
            addDisposable(Observable.timer(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    displayGattServices(gatt.services)
                })
        }
    }

    override fun timeOut() {
        addLoadTimeOut()
    }

    override fun startSubscribeError() {


    }

    fun addLoadTimeOut() {
        if (timeout != null) {
            (timeout ?: return).dispose()
        }
        timeout = Observable.timer(
            if (gattType == GattType.OTA_EMPTY_MEORY || gattType == GattType.OTA_CHECK
                || gattType == GattType.OTA_CHECK_EMPTY_MEORY || gattType == GattType.OTA_DATA_SEND
            ) (1000 * 50).toLong() else
                1000 * 16, TimeUnit.MILLISECONDS
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                isCloseLoop = true
                timeOutView()
            }
        addDisposable(timeout!!)
    }

    fun timeOutView() {
        if (timeout != null) {
            (timeout ?: return).dispose()
        }
        if (gattType == GattType.OTA_EMPTY_MEORY || gattType == GattType.OTA_EMPTY_MEORY) {
            ll_add_loading_process.addProcessLayout(Process.OTA__START_ERROR)
            gattType = GattType.OTA_EMPTY_MEORY
        } else if (gattType == GattType.OTA_DATA_SEND) {
            ll_add_loading_process.addProcessLayout(Process.OTA_SEND_ERROR)
            gattType = GattType.OTA_EMPTY_MEORY
        } else if (gattType == GattType.OTA_CHECK || gattType == GattType.OTA_SAVE) {
            ll_add_loading_process.addProcessLayout(Process.OTA_CHECK_ERROR)
            gattType = GattType.OTA_EMPTY_MEORY
        } else {
            if (isConnectionOk) {
                ll_add_loading_process.addProcessLayout(Process.MSG_OUT_ADD)
            } else {
                ll_add_loading_process.addProcessLayout(Process.TIME_OUT_ADD)
            }
        }
        if (connectManager != null) {
            connectManager!!.disconnect(mDeviceAddress)
        }
        listener()
        btn_next_submit.visibility = View.VISIBLE
        lottieAnimationView.cancelAnimation()
    }


    private fun loadFirmware(filePath: String) {
        if (filePath.isEmpty()) {
            return
        }
        val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
            if (mIndex == 0) {
                val byteList = writeEntity(otaBytes, 140)
                byteListSize = byteList.size
                i("sendimg", "固件包大小=" + otaBytes.size)
                while (mIndex < byteListSize) {
                    if (isCloseLoop) {
                        i("sendimg", "关闭发送图片循环")
                        break
                    }
                    if (isConnection) {
                        val otaData = sendOtaData(
                            if (isCard) 50 else 34,
                            byteList[mIndex],
                            byteList.size,
                            mIndex + 1
                        )
//                        i(
//                            "sendImage",
//                            "底座 OTA 请求-固件传输 size=${byteList[mIndex].size},data=${Utils.bytesToHexString(
//                                otaData
//                            )}"
//                        )
                        LogHelper.i("", "正在发送OTA数据包" + mIndex)
                        SystemClock.sleep(8)
                        instructionStr = if (isCard) "0032" else "0022"
                        (characteristic ?: return@ObservableOnSubscribe).value = otaData
                        val isSendState =
                            (gatt ?: return@ObservableOnSubscribe).writeCharacteristic(
                                characteristic
                            )
                        if (isSendState) {
                            //阻塞，没有收到回复不发下一条
                            isConnection = false
                        }
                    }
                }
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isStatus ->

            }
        addDisposable(disposable)
    }

    private fun loadCloudConnection() {
        runOnUiThread {
            ll_add_loading_process.addProcessLayout(Process.THREE_ADD)
        }
        if (isModeUpdate) {
            PaperBaseActivity.start(
                this@AddLoadingActivity, mDeviceAddress ?: return, otaVersion
            )
            finish()
        } else {
            gettingInformation()
        }
    }

    private fun gettingInformation() {
        //获取分类信息
        runOnUiThread {
            ll_add_loading_process.addProcessLayout(Process.FOUR)
            btn_next_submit.visibility = View.GONE
            if (otaVersion.isNotEmpty()) {
                AddSuccessActivity.start(
                    this@AddLoadingActivity,
                    mDeviceAddress ?: return@runOnUiThread,
                    "",
                    otaVersion
                )
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        BleClear()
        lottieAnimationView.cancelAnimation()
        isCloseLoop = true
        sendContinuously = true
    }
}
