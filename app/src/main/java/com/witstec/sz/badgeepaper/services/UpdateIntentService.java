package com.witstec.sz.badgeepaper.services;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;
import com.witstec.sz.badgeepaper.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * apk更新
 */
public class UpdateIntentService extends IntentService {

    public static final String ACTION_UPDATE = "com.witstec.sz.wiseeslbht.action.UPDATE";
    private NotificationManager mNotificationManager;
    private RemoteViews mRemoteViews;
    private Notification mNotification;
    private Handler mUpdateHandler;
    private String mChannelId = "tomatoChannel";
    private String apkName;

    public UpdateIntentService() {
        super("MyIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case UpdateIntentService.ACTION_UPDATE:
                    apkName = intent.getStringExtra("appName");
                    handleActionUpdate(intent);
                    break;
                default:
                    break;
            }
        }
    }


    private void handleActionUpdate(Intent intent) {
        getUpdateHandler();
        beforeUpdateMessage();
        File file = updateIo(intent);
        finishUpdateMessage(file);
    }

    private void getUpdateHandler() {
        mUpdateHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.arg1) {
                    case 0:
                        createNotification();
                        //start
                        break;
                    case 1:
                        updateNotification(msg);
                        //updateingMessage
                        break;
                    case 2:
                        installApk(msg);
                        //finish
                    case 3:
                        //error
                        installApk(msg);
                        break;
                }
                return true;
            }
        });
    }


    private File updateIo(Intent intent) {
        File updateFile = getDiskCacheDir(getApplicationContext(),
                intent.getStringExtra("name") + System.currentTimeMillis() + ".apk");
        try {
            URL url = new URL(intent.getStringExtra("downUrl"));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(10000);
            conn.setRequestProperty("Accept-Encoding", "identity");
            conn.connect();
            int length = conn.getContentLength();
            InputStream inputStream = conn.getInputStream();
            FileOutputStream fos = new FileOutputStream(updateFile, true);
            int oldProgress = 0;
            byte buf[] = new byte[1024 * 8];
            int currentLength = 0;
            while (true) {
                int num = inputStream.read(buf);
                currentLength += num;
                // 计算进度条位置
                int progress = (int) ((currentLength / (float) length) * 100);
                if (progress > oldProgress) {
                    updatingMessage(progress);
                    oldProgress = progress;
                }
                if (num <= 0) {
                    break;
                }
                fos.write(buf, 0, num);
                fos.flush();
            }
            fos.flush();
            fos.close();
            inputStream.close();
        } catch (Exception e) {
            Log.i("updateException", e.toString());
            return null;
        }
        return updateFile;
    }


    private void beforeUpdateMessage() {
        Message message = mUpdateHandler.obtainMessage();
        message.arg1 = 0;
        mUpdateHandler.sendMessage(message);
    }

    private void updatingMessage(int progress) {
        Message message = mUpdateHandler.obtainMessage();
        message.arg1 = 1;
        message.obj = progress;
        mUpdateHandler.sendMessage(message);
    }

    private void finishUpdateMessage(File file) {
        Message message = mUpdateHandler.obtainMessage();
        message.arg1 = 2;
        message.obj = file;
        mUpdateHandler.sendMessage(message);
    }

    public void createNotification() {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = null;
        //notification channel work
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(getApplicationContext(), mChannelId);
            NotificationChannel channel = new NotificationChannel(mChannelId,
                    getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        } else {
            builder = new NotificationCompat.Builder(getApplicationContext());
        }
        builder.setSmallIcon(R.mipmap.ic_logo);
        builder.setTicker(getResources().getString(R.string.start_Download));
        mRemoteViews = new RemoteViews(getPackageName(), R.layout.notification_update_apk);
        mRemoteViews.setTextViewText(R.id.tv_apk_name, apkName);
        mRemoteViews.setProgressBar(R.id.notificationProgress, 100, 0, false);
        builder.setCustomContentView(mRemoteViews);
        mNotification = builder.build();
        //Notification.FLAG_ONLY_ALERT_ONCE 避免8.0在进度更新时候(notify)中多次响铃
        //Notification.FLAG_NO_CLEAR 下载过程中无法关闭通知,失败或者完成会切换到可以关闭
        mNotification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONLY_ALERT_ONCE;
        mNotification.icon = R.mipmap.ic_logo;
        mNotificationManager.notify(0, mNotification);
    }

    private void updateNotification(Message msg) {
        Integer aFloat = (Integer) msg.obj;
        mRemoteViews.setProgressBar(R.id.notificationProgress, 100, aFloat, false);
        mRemoteViews.setTextViewText(R.id.notification_note_tv, getString(R.string.download_in) + aFloat + "%");
        mNotificationManager.notify(0, mNotification);
    }

    // 下载完成后打开安装apk界面
    public void installApk(Message msg) {
        File file = (File) msg.obj;
        if (file == null || file.length() == 0) {
            mRemoteViews.setTextViewText(R.id.notification_note_tv, getString(R.string.download_error));
            //下载失败,flags设置为可关闭
            mNotification.flags = Notification.FLAG_AUTO_CANCEL;
            mNotificationManager.notify(0, mNotification);
            return;
        }
        //关闭之前的通知,为了兼容某些手机在mNotification.contentIntent后不更新的bug
        //,保证PendingIntent正确执行
        mNotificationManager.cancel(0);
        mRemoteViews.setProgressBar(R.id.notificationProgress, 100, 100, false);
        mRemoteViews.setTextViewText(R.id.notification_note_tv, getString(R.string.download_end));
        //下载完成,flags设置为可关闭
        mNotification.flags = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_ONLY_ALERT_ONCE;
        Intent openFile = openFile(file);
        mNotification.contentIntent = PendingIntent.getActivity(this, 0, openFile, 0);
        mNotificationManager.notify(1, mNotification);
        startActivity(openFile);
        stopSelf();
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        return new File(context.getExternalCacheDir() + File.separator + uniqueName);
    }

    //下载完成后调用安装
    private Intent openFile(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            String authority = getPackageName() + ".provider";
            Uri contentUri = FileProvider.getUriForFile(this, authority, file);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(Uri.parse("file://" + Uri.fromFile(file)),
                    "application/vnd.android.package-archive");
        }
        return intent;
    }
}
