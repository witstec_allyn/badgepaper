package com.witstec.sz.badgeepaper.network;

/**
 * api 接口处理失败返回 code != 0 时抛出
 */

public class ApiCodeException extends Exception {

    private int code;

    public ApiCodeException(int code) {
        this.code = code;
    }

    public ApiCodeException(int code, String message) {
        super(code + "," + message);

        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
