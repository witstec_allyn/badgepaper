package com.witstec.sz.badgeepaper.ui.adapter

import android.annotation.SuppressLint
import android.text.Html
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.witstec.sz.badgeepaper.Constants
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.BleTemplate
import com.witstec.sz.badgeepaper.utils.GlideUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import java.util.*

class OfficialTemplateAdapter(val click: onDownloadOfficialClick) :
    BaseQuickAdapter<BleTemplate.TemplatesBean, BaseViewHolder>(
        R.layout.item_download_image_data,
        ArrayList()
    ) {
    var searchKeyword: String? = null
    @SuppressLint("StringFormatInvalid")
    override fun convert(helper: BaseViewHolder, item: BleTemplate.TemplatesBean) {
        (helper.getView<View>(R.id.tv_be_applicable) as TextView).text =
            String.format(
                mContext.getString(
                    R.string.apply_type,
                    AppUtils.typeIdToTypeName(item.type)
                )
            )
        if (item.name != null) {
            (helper.getView<View>(R.id.tv_template_name) as TextView).text = item.name
        }

        (helper.getView<View>(R.id.layout_download_official) as LinearLayout).setOnClickListener {
            click.onClick(item)
            (helper.getView<View>(R.id.tv_template_name) as TextView).setText(
                helper.itemView.context.getString(
                    R.string.download_end
                )
            )
        }

        (helper.getView<View>(R.id.iv_tv_template_image) as AppCompatImageView).setImageBitmap(
            AppUtils.base64ToBitmap(item.img)
        )

        GlideUtils.loadImageView(
            helper.itemView.context,
            Constants.URL + item.img,
            (helper.getView<View>(R.id.iv_tv_template_image) as AppCompatImageView)
        )

        if (searchKeyword != null && searchKeyword!!.isNotEmpty())
            setChangeTextView(
                item.name,
                searchKeyword!!,
                (helper.getView<View>(R.id.tv_template_name) as TextView)
            )
    }

    interface onDownloadOfficialClick {
        fun onClick(item: BleTemplate.TemplatesBean)
    }

    //制定位置修改颜色
    private fun setChangeTextView(name: String?, changeStr: String, textView: TextView) {
        if (name != null && name.contains(changeStr)) {
            val index = name.indexOf(changeStr)
            val len = changeStr.length
            val temp = Html.fromHtml(
                name.substring(0, index) + "<font color=#E73324>"
                        + name.substring(index, index + len) + "</font>"
                        + name.substring(index + len, name.length)
            )
            textView.text = temp
        } else {
            textView.text = name
        }
    }
}
