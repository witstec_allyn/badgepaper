package com.witstec.sz.badgeepaper.draw.bean;

import android.graphics.Paint;
import android.graphics.Typeface;

import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.draw.Constant;
import com.witstec.sz.badgeepaper.draw.touch.DrawTouch;


public class Text {
    /**
     * 文本内容
     */
    private String content;
    /**
     * 文本画笔
     */
    private Paint paint;
    /**
     * 是否垂直显示
     */
    private boolean isVertical;

    public int textSize= Constants.PAINT_DEFAULT_TEXT_SIZE_55;


    public Text(String content, boolean isVertical,int textSize) {
        this.content = content;
        this.isVertical = isVertical;
        this.textSize = textSize;
        paint = new Paint();
        paint.setColor(DrawTouch.getCurPaint().getColor());
        paint.setTextSize(textSize);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    public String getContent() {
        return content;
    }


    public Paint getPaint() {
        return paint;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isVertical() {
        return isVertical;
    }

    public void setVertical(boolean vertical) {
        isVertical = vertical;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }
}
