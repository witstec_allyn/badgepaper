package com.witstec.sz.badgeepaper.manage;

import java.util.Arrays;

public class BitmapConvertor {

    private static byte[] dataByte;
    private static int total = 0;

    //传入15KByte数据
    public static byte[] BMGEnCode(byte BMGbuffer[]) {
        total = 0;
        int MaxSize = BMGbuffer.length * 8;//14字节最大数
        dataByte = new byte[MaxSize];
        long output = 0;
        int BMPcurrent = 0;
        byte[] buffer = new byte[MaxSize];/**//*缓冲区*/
       wbuffer(BMGbuffer, buffer, BMPcurrent, MaxSize);
       RLE(buffer, MaxSize, output);
        byte[] copyByte = Arrays.copyOf(dataByte, total);
        return copyByte;
    }

    //
    static int wbuffer(byte BMGbuffer[], byte buffer[], int BMPcurrent, int size) {
        //清空buffer
        byte temp;
        int i;
        int bufferIndex = 0;
        //int size_t = size;
        while (size > 0 && (BMGbuffer.length > BMPcurrent)) {
            temp = BMGbuffer[BMPcurrent];/**//*每次取8个像素*/
            if (BMPcurrent >= BMGbuffer.length) break;//BUG之所在,必须要再读一个字节,函数才能判断文件尾
            for (i = 7; i >= 0; --i) {
                buffer[bufferIndex] = (byte) ((temp >> i) & 1);/**//*写入一行转换为二进制,高位在行左边- 将一个字节拆分成8个字节存储*/
                bufferIndex++;
            }
            size -= 8;
            BMPcurrent += 1;
        }
        return BMPcurrent;

    }

    static long RLE(byte buffer[], int size, long output) {
//        LogHelper.INSTANCE.i("yasuoImage", "output=" + output);
        int current = 0;
        byte temp;
        short ounit = 0, count = 0;
        int i = 0;
        while ((current < (size)) && (buffer[current] != -1))
        {
            temp = buffer[current];
            ounit = count = 0;
//            System.out.print(" current:" + current + "  IsFeasible:" + IsFeasible(current, buffer, buffer.length) + "    ");
            if (IsFeasible(current, buffer, size) == 1) {
                //值得压缩
                count = 0;
                ounit |= 1 << 15;//压缩
                ounit |= temp << 14;
                while ((current < size) && (buffer[current] == temp)) {
                    if (count == size || buffer[current] != temp||count==16383) break;
                    ++count;
                    current++;
                }
                ounit |= count;
                dataByte[total] = (byte) ((ounit) & 0xff);
                total++;
                dataByte[total] = (byte) ((ounit>>8) & 0xff);
                total++;
//                System.out.print("  total:" + total + "  Encode:" + count + "  current:" + current + "\n");
            } else {
                //不值得压缩
                int k = 0;
                for (k = 13; k >= 0 && current <= (size); --k, ++current) {
                    if (current == size) break;
                    ounit |= buffer[current] << k;
                    ++count;
//                    System.out.print(" " + buffer[current]);
                }
                dataByte[total] = (byte) ((ounit) & 0xff);
                total++;
                dataByte[total] = (byte) ((ounit>>8) & 0xff);
                total++;
//                System.out.print(String.format("Uncode:%02x %02x\n", (byte) ((ounit) & 0xff), (byte) ((ounit >> 8) & 0xff)));
            }
            output += 2;
        }
        return output;
    }

    static int IsFeasible(int current, byte[] buffer, int size) {
        byte i, id;
        id = buffer[current];
        int distance = current;
        for (i = 0; i < 16; ++i, ++distance) {
            if (distance > size || buffer[current + i] != id || buffer[current + i] == -1)
                return 0;
        }
        return 1;
    }

}
