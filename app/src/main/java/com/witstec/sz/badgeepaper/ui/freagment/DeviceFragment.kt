package com.witstec.sz.badgeepaper.ui.freagment


import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import cn.com.heaton.blelibrary.ble.Ble
import com.blakequ.bluetooth_manager_lib.BleManager
import com.blakequ.bluetooth_manager_lib.scan.BluetoothScanManager
import com.blakequ.bluetooth_manager_lib.scan.bluetoothcompat.ScanCallbackCompat
import com.blakequ.bluetooth_manager_lib.scan.bluetoothcompat.ScanResultCompat
import com.github.nukc.stateview.StateView
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.BluetoothLeDeviceStore
import com.witstec.sz.badgeepaper.model.bean.DeviceBean
import com.witstec.sz.badgeepaper.model.bean.GattType
import com.witstec.sz.badgeepaper.model.db.LocalDataSource
import com.witstec.sz.badgeepaper.model.db.LocalDataSource.Companion.instance
import com.witstec.sz.badgeepaper.model.event.ChangeDeviceDataEvent
import com.witstec.sz.badgeepaper.myStartActivity
import com.witstec.sz.badgeepaper.ui.activity.device.add.QrcodeActivity
import com.witstec.sz.badgeepaper.ui.activity.device.add.ScanListActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.AddLoadingActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.DeviceManageActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.PaperBaseActivity
import com.witstec.sz.badgeepaper.ui.adapter.DeviceHomeAdapter
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.utils.*
import com.witstec.sz.badgeepaper.view.CustomPopupWindow
import com.witstec.sz.badgeepaper.view.IosDialog
import com.witstec.sz.badgeepaper.view.SearchRelativeLayout
import com.witstec.sz.badgeepaper.view.SimpleDividerDecoration
import com.yanzhenjie.permission.AndPermission
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_device.*
import kotlinx.android.synthetic.main.header_view_search_layout.view.*
import kotlinx.android.synthetic.main.layout_home_add_right.*
import java.util.*
import java.util.concurrent.TimeUnit

/**
 *
 */
class DeviceFragment : BaseFragment() {

    private var mAdapter: DeviceHomeAdapter? = null
    private var statView: StateView? = null

    // 标志位，标志已经初始化完成
    private var isPrepared: Boolean = false
    private var mBluetoothUtils: BluetoothUtils? = null
    private var scanManager: BluetoothScanManager? = null
    private var mDeviceStore: BluetoothLeDeviceStore? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_device, container, false)
        statView = StateView.inject(view!!)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        LogHelper.i("onScanResult", "onCreateView")
        mAdapter = DeviceHomeAdapter()
        mAdapter!!.setEnableLoadMore(false)
        statView!!.showLoading()
        recycler.adapter = mAdapter
        recycler.layoutManager = LinearLayoutManager(activity)
        recycler.addItemDecoration(SimpleDividerDecoration(activity))
        isPrepared = true

        mAdapter!!.sOnItemClick(object : DeviceHomeAdapter.OnItemClick {
            override fun onItemClick(position: Int) {
                if (scanManager != null) {
                    if ((scanManager ?: return).isScanning) {
                        (scanManager ?: return).stopCycleScan()
                        LogHelper.i("gattService", "停止扫描")
                        SystemClock.sleep(240)
                    }
                }
                App.device_name =
                    (mAdapter ?: return).data[position].deviceName
                App.template_id =
                    (mAdapter ?: return).data[position].id
                val intent = Intent()
                intent.setClass(activity ?: return, DeviceManageActivity::class.java)
                intent.putExtra("extra_mac", (mAdapter ?: return).data[position].mac)
                startActivity(intent)
            }

            override fun onItemLongClick(position: Int) {
                IosDialog(activity ?: return).init()
                    .setTitle(getString(R.string.delete_device))
                    .setMsg(getString(R.string.delete_device_yes_no))
                    .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                        instance.liteOrm.delete((mAdapter ?: return@OnClickListener).getItem(position))
                        (mAdapter ?: return@OnClickListener).remove(position)
                        (mAdapter ?: return@OnClickListener).notifyDataSetChanged()
                        ToastUtils.show("删除成功")
                    })
                    .setNegativeButton(getString(R.string.cancel), View.OnClickListener {

                    })
                    .show()
            }

            override fun onItemDeleteClick(position: Int) {

            }
        })

        addDisposable(RxBus.register(ChangeDeviceDataEvent::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                getBleData()
            })

        btn_home_add.setOnClickListener {
            val window = CustomPopupWindow((activity as Context?)!!)
            window.view(R.layout.popuwindow_add_menu)
            window.isOutsideTouchable = true
            window.showAsDropDown(btn_home_add)

            window.addViewOnclick(R.id.ll_search_device) {
                window.dismiss()
                SystemClock.sleep(200)
                myStartActivity(activity!!, ScanListActivity::class.java)
            }

            window.addViewOnclick(R.id.ll_scanning) {
                window.dismiss()
                myStartActivity(activity!!, QrcodeActivity::class.java)
            }
        }

        getBleData()

        relativeLayout.setOnRefreshListener {
            relativeLayout.isRefreshing = true
            getBleData()
        }

    }


    /**
     * 加载数据
     */
    private fun scanning() {
        if (NetworkUtils.isLocationEnabled()) {
            val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if (!mBluetoothAdapter.isEnabled) {
                //4、若未打开，则请求打开蓝牙
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, Ble.REQUEST_ENABLE_BT)
                LogHelper.i("onScanResult", "打开蓝牙")
            } else {
                LogHelper.i("onScanResult", "开始扫描")
                scanManager = BleManager.getScanManager(activity!!)
                mDeviceStore = BluetoothLeDeviceStore()
                mBluetoothUtils = BluetoothUtils(activity)
                mBluetoothUtils!!.askUserToEnableBluetoothIfNeeded()

                scanManager!!.setScanCallbackCompat(object : ScanCallbackCompat() {
                    override fun onBatchScanResults(results: List<ScanResultCompat>?) {
                        super.onBatchScanResults(results)
                        LogHelper.i("onScanResult", "onBatchScanResults")
                    }

                    override fun onScanResult(callbackType: Int, result: ScanResultCompat?) {
                        super.onScanResult(callbackType, result)
                        val deviceBean = result!!.leDevice
                        if (deviceBean.name != null) {
                            if (deviceBean.name == "WTCard") {
                                var isAdd = false
                                mDeviceStore!!.deviceList.forEach {
                                    if (it.address == deviceBean.address) {
                                        isAdd = true
                                    }
                                }
                                if (!isAdd) {
                                    mDeviceStore!!.addDevice(deviceBean)
                                    if (mAdapter!!.data.size != 0) {
                                        for (adapterItem in 0 until mAdapter!!.data.size) {
                                            for (devListItem in 0 until mDeviceStore!!.size()) {
                                                val itData = mAdapter!!.data[adapterItem]
                                                val itDevice =
                                                    mDeviceStore!!.deviceList[devListItem]
                                                if (itDevice.address == itData.mac && !itData.isOnline) {
                                                    itData.isOnline = true
                                                    val rssi =
                                                        itDevice.rssi.toString()
                                                            .replace("-", "").toInt()
                                                    when {
                                                        rssi <= 50 -> itData.signal =
                                                            getString(R.string.rssi_state_1)
                                                        rssi in 51..80 -> itData.signal =
                                                            getString(R.string.rssi_state_2)
                                                        rssi in 81..100 -> itData.signal =
                                                            getString(R.string.rssi_state_3)
                                                        rssi > 100 -> itData.signal =
                                                            getString(R.string.rssi_state_4)
                                                    }
                                                    activity!!.runOnUiThread {
                                                        mAdapter!!.notifyDataSetChanged()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
                scanManager!!.startScanNow()
            }
        } else {
            IosDialog(activity!!).init()
                .setTitle(getString(R.string.Need_location_information))
                .setMsg(getString(R.string.Need_location_information_msg))
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                    startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {
                    activity!!.finish()
                })
                .show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (scanManager != null) {
            if (scanManager!!.isScanning) {
                scanManager!!.stopCycleScan()
                scanManager = null
            }
        }
        if (mDeviceStore != null) {
            mDeviceStore!!.clear()
        }
    }

    fun getBleData() {
        relativeLayout.isRefreshing = false
        val qrCodeBleBeans: ArrayList<DeviceBean> = instance.queryAll(DeviceBean::class.java)
        if (qrCodeBleBeans.size == 0) {
            //添加设备
            recycler.visibility = View.GONE
            relativeLayout.visibility = View.GONE
            is_null_root.visibility = View.VISIBLE
            statView!!.showContent()
        } else {
            statView!!.showContent()
            recycler.visibility = View.VISIBLE
            relativeLayout.visibility = View.VISIBLE
            is_null_root.visibility = View.GONE
            //显示数据列表
            //扫描蓝牙
            mAdapter!!.replaceData(qrCodeBleBeans.reversed())
            val permission = arrayOf(
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            AndPermission.with(this)
                .runtime()
                .permission(permission)
                .onGranted {
                    scanning()
                }
                .onDenied {
                    ToastUtils.show(getString(R.string.no_permissions))
                }.start()

            if (mAdapter!!.data.size != 0) {
                if (mAdapter!!.headerLayoutCount == 0) {
                    val view =
                        View.inflate(activity, R.layout.header_view_search_layout, null)
                    view.searchLayout.setTextChangedListener(object :
                        SearchRelativeLayout.TextChangedListener {
                        override fun onTextChanged(text: String) {
                            if (text.isNotEmpty() || text != "") {
                                mAdapter!!.searchKeyword = text
                                //筛选
                                val deviceBean: ArrayList<DeviceBean> =
                                    LocalDataSource.instance.getQueryByWhere(
                                        DeviceBean::class.java, "deviceName",
                                        text
                                    )
                                mAdapter!!.replaceData(deviceBean)
                            } else {
                                mAdapter!!.searchKeyword = ""
                                getBleData()
                            }
                        }
                    })
                    mAdapter!!.addHeaderView(view)
                }
            } else {
                statView!!.showEmpty()
            }
        }
    }
}
