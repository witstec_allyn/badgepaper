package com.witstec.sz.badgeepaper.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.witstec.sz.badgeepaper.BuildConfig;

import java.util.Stack;

/**
 * Activity管理 allyn
 */

public class ActivityManager implements Application.ActivityLifecycleCallbacks {

    private Stack<Activity> mActivityStack = new Stack<>();

    private static ActivityManager INSTANCE;

    // 当前应用程序是否在前台
    private boolean isForeground;

    private ActivityManager() {
    }

    public static ActivityManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ActivityManager();
        }
        return INSTANCE;
    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        return mActivityStack.lastElement();
    }

    /**
     * 结束当前Activity（堆栈中最后一个压入的）
     */
    public void finishActivity() {
        finishActivity(currentActivity());
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            mActivityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        for (Activity activity : mActivityStack) {
            if (activity.getClass().equals(cls)) {
                finishActivity(activity);
            }
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = mActivityStack.size(); i < size; i++) {
            if (null != mActivityStack.get(i)) {
                mActivityStack.get(i).finish();
            }
        }
        mActivityStack.clear();
    }

    /**
     * 获取集合中上一个Activity
     *
     * @return
     */
    public static Activity getPreviousActivity() {
        ActivityManager adapter = getInstance();
        int count = adapter.mActivityStack.size();
        if (count < 2) {
            return null;
        }
        return adapter.mActivityStack.get(count - 2);
    }

    /**
     * 根据先进后出
     * 返回到指定的 activity，之间的 activity 都会 finish 掉
     *
     * @param cls 想要返回到的 activity，此 activity 应该在堆栈中
     */
    public void backTo(Class<?> cls) {
        int targetPosition = -1;
        for (int i = mActivityStack.size() - 1; i >= 0; i--) {
            Activity activity = mActivityStack.get(i);
            if (activity != null) {
                if (activity.getClass().equals(cls)) {
                    targetPosition = i;
                    break;
                } else {
                    activity.finish();
                }
            }
        }
        if (targetPosition == -1) {
            if (BuildConfig.DEBUG) { // 便利测试环境下及时发现问题
                throw new IllegalArgumentException(cls.getSimpleName() + " is not in the stack~");
            }
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        mActivityStack.add(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        isForeground = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isForeground = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        mActivityStack.remove(activity);
    }
}
