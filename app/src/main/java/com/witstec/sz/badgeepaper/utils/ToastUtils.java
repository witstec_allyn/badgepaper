package com.witstec.sz.badgeepaper.utils;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.StringRes;

import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.network.ExceptionUtils;

/**
 * Toast 工具类
 */

public class ToastUtils {

    static Toast mToast = null;


    /**
     * toast 显示 text
     *
     * @param text 显示的文字
     */
    public static void show(CharSequence text) {
        show(text, Toast.LENGTH_SHORT);
    }

    /**
     * toast 显示 text
     *
     * @param text     文字
     * @param duration {@link Toast#LENGTH_SHORT} {@link Toast#LENGTH_LONG}
     */
    public static void show(CharSequence text, int duration) {
        show(App.Companion.getInstance().getApplicationContext(), text, duration);
    }

    /**
     * toast 显示 text
     *
     * @param context  通常是 {@link android.app.Application or {@link android.app.Activity}
     * @param text     文字
     * @param duration {@link Toast#LENGTH_SHORT} {@link Toast#LENGTH_LONG}
     */
    public static void show(Context context, CharSequence text, int duration) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    public static void show(Context context, CharSequence text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    /**
     * @param object Object
     */
    public static void show(Object object) {
        show(String.valueOf(object));
    }

    /**
     * toast 显示 Throwable 错误信息
     *
     * @param throwable Throwable
     */
    public static void show(Throwable throwable) {
        show(ExceptionUtils.getDescription(throwable));
    }

    /**
     * @param resId 字符串资源 Id
     */
    public static void show(@StringRes int resId) {
        show(App.Companion.getInstance().getString(resId));
    }

    /**
     * @param context 通常是 {@link android.app.Application or {@link android.app.Activity}
     * @param resId   字符串资源 Id
     */
    public static void show(Context context, @StringRes int resId) {
        show(context.getString(resId));
    }

    /**
     * @param context  通常是 {@link android.app.Application or {@link android.app.Activity}
     * @param resId    字符串资源 Id
     * @param duration {@link Toast#LENGTH_SHORT} {@link Toast#LENGTH_LONG}
     */
    public static void show(Context context, @StringRes int resId, int duration) {
        show(context, context.getString(resId), duration);
    }
}
