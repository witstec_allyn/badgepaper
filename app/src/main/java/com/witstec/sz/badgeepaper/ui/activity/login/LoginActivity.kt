package com.witstec.sz.badgeepaper.ui.activity.login

import android.os.Bundle
import android.text.Selection
import android.text.Spannable
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.witstec.sz.badgeepaper.*
import com.witstec.sz.badgeepaper.model.event.ChangeUseEvent
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.ui.activity.MainActivity
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.activity.setting.HelpActivity
import com.witstec.sz.badgeepaper.utils.RxBus
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit


class LoginActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ed_input_user.setSelection(ed_input_user.text!!.length)
        listener()

        tv_is_agree.setOnClickListener {
            showLicenseDialog()
        }

        //用户协议同意处理
        if (getSaveBoolean(Constant.USER_LICENSE)) {
            check_is_agree.isChecked = true
            btn_submit_login.isEnabled = true
            btn_submit_login.setBackgroundResource(R.drawable.btn_radius)
        } else {
            check_is_agree.isChecked = false
            btn_submit_login.isEnabled = false
            btn_submit_login.setBackgroundResource(R.drawable.btn_radius_enobled_false)
            showLicenseDialog()
        }

        check_is_agree.setOnCheckedChangeListener { buttonView, isChecked ->
            btn_submit_login.isEnabled = isChecked
            saveBoolean(Constant.USER_LICENSE, isChecked)
            if (isChecked) {
                btn_submit_login.setBackgroundResource(R.drawable.btn_radius)
            } else {
                btn_submit_login.setBackgroundResource(R.drawable.btn_radius_enobled_false)
            }
        }
    }

    private fun showLicenseDialog() {
        val view = View.inflate(this, R.layout.dialog_privacy_olicy_layout, null)
        val dialog = MaterialDialog.Builder(this)
                .customView(view, false)
                .canceledOnTouchOutside(false)
                .show()
        val tv_in_service_agreement = view.findViewById<TextView>(R.id.tv_in_service_agreement)
        val tv_in_service_policy = view.findViewById<TextView>(R.id.tv_in_service_policy)
        val disagree = view.findViewById<TextView>(R.id.disagree)
        val agree = view.findViewById<TextView>(R.id.agree)
        tv_in_service_agreement.setOnClickListener {
            HelpActivity.start(
                this,
                if (AppUtils.getLocaleLanguage() == "zh_cn") "file:///android_asset/service_agreement_zh.html"
                else "file:///android_asset/service_agreement_en.html",
                getString(R.string.Service_Agreement)
            )
        }
        tv_in_service_policy.setOnClickListener {
            HelpActivity.start(
                this,
                if (AppUtils.getLocaleLanguage() == "zh_cn") "file:///android_asset/privacy_zh.html"
                else "file:///android_asset/privacy_en.html",
                getString(R.string.Service_Agreement)
            )
        }

        disagree.setOnClickListener {
            dialog.dismiss()
            saveBoolean(Constant.USER_LICENSE, false)
            btn_submit_login.isEnabled = false
            check_is_agree.isChecked = false
            btn_submit_login.setBackgroundResource(R.drawable.btn_radius_enobled_false)
        }

        agree.setOnClickListener {
            dialog.dismiss()
            //保存
            //关闭对话框
            saveBoolean(Constant.USER_LICENSE, true)
            btn_submit_login.isEnabled = true
            check_is_agree.isChecked = true
            btn_submit_login.setBackgroundResource(R.drawable.btn_radius)
        }
    }

    fun listener() {
        btn_facebook.setOnClickListener {
        }
        btn_instagram.setOnClickListener {
        }
        btn_register.setOnClickListener { myStartActivity(this, RegisterActivity::class.java) }
        btn_twitter.setOnClickListener {}
        btn_retrieve_password.setOnClickListener { myStartActivity(this, EmailRegisterActivity::class.java) }
        iv_pwd_visible.setOnCheckedChangeListener { _, isChecked ->
            ed_input_pwd.setSelection(ed_input_pwd.text!!.length)
            if (isChecked) {
                //如果选中，显示密码
                ed_input_pwd.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                //否则隐藏密码
                ed_input_pwd.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            //光标设置在末尾
            val charSequence = ed_input_pwd.text
            if (charSequence is Spannable) {
                val spanText = charSequence as Spannable
                Selection.setSelection(spanText, charSequence.length)
            }
        }

        btn_submit_login.setOnClickListener {
            if (TextUtils.isEmpty(ed_input_user.text) || TextUtils.isEmpty(ed_input_pwd.text)) {
                ToastUtils.show(getString(R.string.edit_is_null))
                return@setOnClickListener
            }
            showDialog()
            val disposable = ApiRepository.login(ed_input_user.text.toString(), ed_input_pwd.text.toString(),this)
                .subscribe({ userInfo ->
                    saveStringData(Constant.TOKEN, userInfo.token)
                    saveStringData(Constant.REFRESH_TOKEN, userInfo.refresh_token)
                    saveStringData(Constant.UID, userInfo.uid)
//                    saveStringData(Constant.AVATAR_USER_INFO, Constants.URL + userInfo.logo)
                    saveStringData(Constant.USER_NAME, ed_input_user.text.toString())
//                    saveStringData(Constant.USER_NAME_INFO, userInfo.name)
                    addDisposable(
                        Observable.timer(800, TimeUnit.MILLISECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                addDisposable(
                                    ApiRepository.uploadUserDeviceMsg(this)
                                        .subscribe({
                                            start()
                                        }, {
                                            start()
                                            it.message?.let { it1 -> ToastUtils.show(it1) }
                                        })
                                )
                            })
                }, {
                    dialogDismiss()
                    it.message?.let { it1 -> ToastUtils.show(it1) }
                })
            addDisposable(disposable)
        }
    }

    fun start() {
        dialogDismiss()
        myStartActivity(
            this@LoginActivity,
            MainActivity::class.java
        )
        finish()
        RxBus.post(ChangeUseEvent())
    }
}
