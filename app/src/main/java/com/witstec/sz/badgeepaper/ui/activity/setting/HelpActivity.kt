package com.witstec.sz.badgeepaper.ui.activity.setting

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import kotlinx.android.synthetic.main.activity_help.*


class HelpActivity : AppBaseActivity() {
    companion object {
        fun start(
            context: Context,
            url: String
        ) {
            val intent = Intent(context, HelpActivity::class.java)
            intent.putExtra("extra_url", url)
            context.startActivity(intent)
        }

        fun start(
            context: Context,
            url: String,
            title: String
        ) {
            val intent = Intent(context, HelpActivity::class.java)
            intent.putExtra("extra_url", url)
            intent.putExtra("extra_title", title)
            context.startActivity(intent)
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        val title = intent.getStringExtra("extra_title")
        if (title!=null){
            setToolbar(title)
        }else{
            setToolbar(getString(R.string.item_me_help))
        }
        webView.webViewClient = WebViewClient()
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        //支持javascript
        webSettings.setJavaScriptEnabled(true);
        // 设置可以支持缩放
        webSettings.setSupportZoom(true);
        // 设置出现缩放工具
        webSettings.setBuiltInZoomControls(true);
        //扩大比例的缩放
        webSettings.setUseWideViewPort(true);
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }
        }
        webView.loadUrl(intent.getStringExtra("extra_url"))
//        webView.loadDataWithBaseURL(null, intent.getStringExtra("extra_url"), "text/html", "utf-8", null)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }


}
