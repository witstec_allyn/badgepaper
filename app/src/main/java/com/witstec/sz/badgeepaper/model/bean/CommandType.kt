package com.witstec.sz.badgeepaper.model.bean

enum class CommandType {
    INSTRUCTION,
    BURDEN
}