package com.witstec.sz.badgeepaper.utils.language;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;

/**
 * 语言切换处理
 * by：Allyn
 */
public class LanguageUtil {

    private static final String TAG = "LanguageUtil";

    /**切换语言关键类
     * @param context
     * @param newLanguage
     */
    @SuppressWarnings("deprecation")
    public static void changeAppLanguage(Context context, String newLanguage) {
        if (TextUtils.isEmpty(newLanguage)) {
            return;
        }
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        //获取想要切换的语言类型
        Locale locale = getLocaleByLanguage();
        configuration.setLocale(locale);
        // updateConfiguration
        DisplayMetrics dm = resources.getDisplayMetrics();
        resources.updateConfiguration(configuration, dm);
    }

    public static Locale getLocaleByLanguage( ) {
        //默认英文
        //获取语言状态，得到语言类型，
        Locale locale = Locale.getDefault();
        String language = String.format("%s-%s", locale.getLanguage(), locale.getCountry());
        if (language.contains("zh-CN")) {
            locale = Locale.SIMPLIFIED_CHINESE;
        } else if (language.contains("en-")) {
            locale = Locale.ENGLISH;
        } else if (language.contains("ja")) {
            locale = Locale.JAPAN;
        } else if (language.contains("es")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                locale = Locale.forLanguageTag(language);
            }
        } else if (language.contains("zh-TW")) {
            locale = Locale.TRADITIONAL_CHINESE;
        } else {
            locale = Locale.ENGLISH;
        }
        return locale;
    }

    //判断系统版本，给出不同处理方式
    public static Context attachBaseContext(Context context, String language) {
        Log.d(TAG, "attachBaseContext: " + Build.VERSION.SDK_INT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResources(context, language);
        } else {
            return context;
        }
    }

    //加载资源文件
    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Resources resources = context.getResources();
        Locale locale = LanguageUtil.getLocaleByLanguage();

        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        configuration.setLocales(new LocaleList(locale));
        return context.createConfigurationContext(configuration);
    }
}
