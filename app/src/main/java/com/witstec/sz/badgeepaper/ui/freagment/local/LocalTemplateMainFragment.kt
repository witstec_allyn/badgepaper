package com.witstec.sz.badgeepaper.ui.freagment.local

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.github.nukc.stateview.StateView
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity
import com.witstec.sz.badgeepaper.model.bean.TemplateBean
import com.witstec.sz.badgeepaper.model.db.LocalDataSource
import com.witstec.sz.badgeepaper.model.event.ChangeTemplateListEvent
import com.witstec.sz.badgeepaper.ui.adapter.LocalTemplateAdapter
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.RxBus
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.view.IosDialog
import com.witstec.sz.badgeepaper.view.SearchRelativeLayout
import com.witstec.sz.badgeepaper.view.SimpleDividerDecoration
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_local_drawing.*
import kotlinx.android.synthetic.main.header_view_search_layout.view.*
import java.util.*

class LocalTemplateMainFragment : BaseFragment() {

    private var statView: StateView? = null
    private var mAdapter: LocalTemplateAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_local_drawing, container, false)
        statView = StateView.inject(view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter = LocalTemplateAdapter()
        mAdapter!!.setEnableLoadMore(false)
        statView!!.showLoading()
        recycler.adapter = mAdapter
        recycler.layoutManager = LinearLayoutManager(activity)
        recycler.addItemDecoration(SimpleDividerDecoration(activity))
        getBleData()

        floatingActionButton.setOnClickListener {
            MaterialDialog.Builder(activity!!)
                .title(getString(R.string.device_type))
                .items("2.13")
                .positiveText(getString(R.string.fixed))
                .negativeText(getString(R.string.cancel))
                .itemsCallbackSingleChoice(0) { _, _, which, text ->
                    var typeId = 0
                    if (which == 0) {
                        typeId = 2
                    } else if (which == 1) {
                        typeId = 3
                    }
                    DrawingMainActivity.start(
                        activity,
                        typeId,
                        true
                    )
                    true
                }
                .show()
//            DrawingMainActivity.start(
//                activity,
//                2,
//                true
//            )
        }

        mAdapter!!.setOnItemClickListener { adapter, view, position ->
            SystemClock.sleep(210)
            val templateBean = mAdapter!!.getItem(position) as TemplateBean
//            LogHelper.i("witstec", "xml文件路径=" + templateBean.templateXmlString)
            DrawingMainActivity.start(
                activity,
                templateBean.templateId,
                templateBean.templateName,
                templateBean.templateType.toInt(),
              "",
                true
            )
        }
        mAdapter!!.setOnItemLongClickListener { adapter, view, position ->
            IosDialog(activity!!).init()
                .setTitle(getString(R.string.template_selection_delete))
                .setMsg(getString(R.string.do_you_want_template_selection_delete))
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                    LocalDataSource.instance.liteOrm.delete(mAdapter!!.getItem(position))
                    mAdapter!!.remove(position)
                    ToastUtils.show(getString(R.string.delete_ok))
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {
                })
                .show()
            true
        }


        addDisposable(
            RxBus.register(ChangeTemplateListEvent::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    getBleData()
                })
    }

    fun getBleData() {
        val templateBean: ArrayList<TemplateBean> =
            LocalDataSource.instance.queryAll(TemplateBean::class.java)
        if (templateBean.size == 0) {
            //添加设备
            recycler.visibility = View.GONE
            is_null_root.visibility = View.VISIBLE
            statView!!.showContent()
        } else {
            statView!!.showContent()
            recycler.visibility = View.VISIBLE
            is_null_root.visibility = View.GONE
            mAdapter!!.replaceData(templateBean.reversed())
            if (mAdapter!!.data.size != 0) {
                if (mAdapter!!.headerLayoutCount == 0) {
                    val view =
                        View.inflate(activity, R.layout.header_view_search_layout, null)
                    view.searchLayout.setTextChangedListener(object :
                        SearchRelativeLayout.TextChangedListener {
                        override fun onTextChanged(text: String) {
                            if (text.isNotEmpty() || text != "") {
                                mAdapter!!.searchKeyword = text
                                //筛选
                                val templateBean: ArrayList<TemplateBean> =
                                    LocalDataSource.instance.getQueryByWhere(
                                        TemplateBean::class.java,
                                        "templateName",
                                        text
                                    )
                                mAdapter!!.replaceData(templateBean)
                            } else {
                                mAdapter!!.searchKeyword = ""
                                getBleData()
                            }
                        }
                    })
                    mAdapter!!.addHeaderView(view)
                }
            } else {
                statView!!.showEmpty()
            }
        }
    }
}
