package com.witstec.sz.badgeepaper.ui.activity.device.longConnection

import android.bluetooth.*
import android.content.Intent
import android.os.Build
import android.os.Bundle
import cn.com.heaton.blelibrary.ble.Ble
import com.blakequ.bluetooth_manager_lib.connect.BluetoothConnectManager
import com.blakequ.bluetooth_manager_lib.connect.BluetoothSubScribeData
import com.blakequ.bluetooth_manager_lib.device.resolvers.GattAttributeResolver
import com.blakequ.bluetooth_manager_lib.util.BluetoothUtils
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.GattType
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.utils.ByteUtils
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.LogHelper.i
import com.witstec.sz.badgeepaper.utils.StringUtils
import com.witstec.sz.badgeepaper.utils.ToastUtils
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * 连接设备父类
 */
abstract class BleConnectionActivity : AppBaseActivity() {

    var connectManager: BluetoothConnectManager? = null
    var mDeviceAddress: String? = ""
    var mGattCharacteristics: MutableList<List<BluetoothGattCharacteristic>>? =
        mutableListOf()
    val LIST_NAME = "NAME"
    val LIST_UUID = "UUID"
    var characteristic: BluetoothGattCharacteristic? = null
    var gatt: BluetoothGatt? = null
    val gattCharacteristicData = ArrayList<List<Map<String, String>>>()
    var timeout: Disposable? = null
    var isOpenBle: Boolean = true

    abstract fun getContentViewLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentViewLayout())
        initView()
        listener()
    }

    abstract fun initView()

    abstract fun listener()

    abstract fun startSubscribeError()

    abstract fun onMtuChangedSuper(gatt: BluetoothGatt?, mtu: Int, status: Int)

    abstract fun onCharacteristicReadSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    )

    abstract fun onCharacteristicWriteSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    )

    abstract fun onCharacteristicChangedSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic
    )

    abstract fun onConnectionStateChangeSuper(
        gatt: BluetoothGatt,
        status: Int,
        newState: Int
    )

    abstract fun displayGattServicesInit()

    abstract fun sendDataInit(gattType: GattType)

    abstract fun onServicesDiscoveredSuper(gatt: BluetoothGatt)

    fun connectionBle() {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (!mBluetoothAdapter.isEnabled) {
            if (isOpenBle) {
                //4、若未打开，则请求打开蓝牙
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, Ble.REQUEST_ENABLE_BT)
                isOpenBle = false
            }
        } else {
            //用监听器看电子价签什么时候发了广播，错过了多少次，
            if (connectManager == null) {
                connectManager = BluetoothConnectManager.getInstance(this)
            }
            if ((connectManager ?: return).isConnectDevice) {
                if ((connectManager
                        ?: return).getBluetoothGatt(mDeviceAddress) == null
                ) {
                    i("gattService", "没有服务，重连")
                    if (connectManager != null) {
                        (connectManager ?: return).disconnect(mDeviceAddress)
                    }
                    (connectManager ?: return).connect(mDeviceAddress)
                    return
                }
                if ((connectManager
                        ?: return).getBluetoothGatt(mDeviceAddress).services.size == 0
                ) {
                    i("gattService", "没有服务，重新连接了")
                    if (connectManager != null) {
                        (connectManager ?: return).disconnect(mDeviceAddress)
                    }
                    (connectManager ?: return).connect(mDeviceAddress)
                } else {
                    i("gattService", "不重新链接")
                    displayGattServices(
                        ((connectManager ?: return).getBluetoothGatt(mDeviceAddress)
                            ?: return).services
                    )
                }
            } else {
                i("gattService", "重新链接")
                if (StringUtils.checkBluetoothAddress(mDeviceAddress)) {
                    (connectManager ?: return).connect(mDeviceAddress)
                } else {
                    ToastUtils.show(getString(R.string.no_barCode))
                }
            }
            (connectManager ?: return).setBluetoothGattCallback(object :
                BluetoothGattCallback() {

                override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
                    super.onMtuChanged(gatt, mtu, status)
                    onMtuChangedSuper(gatt, mtu, status)
                    i("gattService", " onMtuChanged");
                    if (BluetoothGatt.GATT_SUCCESS == status) {
                        i("gattService", "onMtuChanged success MTU = $mtu");
                    } else {
                        i("gattService", "onMtuChanged fail ");
                    }
//                    if (BluetoothGatt.GATT_SUCCESS == status) {
//                        i("gattService", "onMtuChanged success MTU = $mtu");
//                        saveStringData(Constants.REQUEST_MTU_SIZE, mRequestMtuSize.toString())
//                        i("gattService", "mtu申请成功-=$mRequestMtuSize")
//                        isRequestMtuOk = true
//                    } else {
//                        i("gattService", "onMtuChanged fail ");
//                    }
                }

                override fun onCharacteristicRead(
                    gatt: BluetoothGatt,
                    characteristic: BluetoothGattCharacteristic,
                    status: Int
                ) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        i("gattService", "onCharacteristicRead GATT_SUCCESS successfully")
                    } else {
                        i("gattService", "fail to read characteristic")
                    }
                    i(
                        "sendImage",
                        "设备返回通知 ==" + ByteUtils.byteArrayToHexString(
                            characteristic.value
                        )
                    )

                    onCharacteristicReadSuper(gatt, characteristic, status)

                }

                override fun onCharacteristicWrite(
                    gatt: BluetoothGatt,
                    characteristic: BluetoothGattCharacteristic,
                    status: Int
                ) {
                    super.onCharacteristicWrite(gatt, characteristic, status)
                    LogHelper.i(
                        "receive",
                        "系统返回 ==" + ByteUtils.byteArrayToHexString(
                            characteristic.value
                        )
                    )
                    onCharacteristicWriteSuper(gatt, characteristic, status)
                }


                override fun onCharacteristicChanged(
                    gatt: BluetoothGatt,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    super.onCharacteristicChanged(gatt, characteristic)
                    i(
                        "sendImage",
                        "设备回复 ==" + ByteUtils.byteArrayToHexString(
                            characteristic.value
                        )
                    )
                    val returnBytes =
                        ByteUtils.byteArrayToHexString(characteristic.value).split(",")
                    val commandStr = (returnBytes[5] + returnBytes[6])
                    LogHelper.i("sendImage", "设备回复命令" + commandStr)
                    when (commandStr) {
                        "0015" -> {
                            if (timeout != null) {
                                (timeout ?: return).dispose()
                            }
                            runOnUiThread {
                                showDialogNo(getString(R.string.Screen_refresh_complete))
                            }
                        }
                    }
                    onCharacteristicChangedSuper(gatt, characteristic)
                }

                override fun onConnectionStateChange(
                    gatt: BluetoothGatt,
                    status: Int,
                    newState: Int
                ) {
                    super.onConnectionStateChange(gatt, status, newState)
                    i("sendImage", "状态改变了吗")
                    onConnectionStateChangeSuper(gatt, status, newState)
                    i("sendImage", "状态改变了吗2")
                    i("sendImage", "蓝牙连接状态改变 状态=$newState")
                    if (newState == BluetoothProfile.STATE_DISCONNECTED) {
//                        if (connectManager != null) {
//                            (connectManager ?: return).release()
//                            (connectManager ?: return).closeAll()
//                        }
                    }
                }

                override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                    super.onServicesDiscovered(gatt, status)
                    i("sendImage", "连接结果返回了  连接状态status=" + status)
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        i("sendImage", "连接成功")
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                    gatt.requestMtu(240)
//                                }
                        onServicesDiscoveredSuper(gatt)
                    } else {
                        i("sendImage", "连接失败")
                    }
                }
            })
            timeOut()
        }
    }

    abstract fun timeOut()

    override fun onRestart() {
        super.onRestart()
        listener()
    }

    fun displayGattServices(gattServices: List<BluetoothGattService>?) {
        if (gattServices == null) return
        var uuid: String? = null
        val unknownServiceString = resources.getString(R.string.unknown_service)
        val unknownCharaString = resources.getString(R.string.unknown_characteristic)
        val gattServiceData = ArrayList<Map<String, String>>()
        mGattCharacteristics = ArrayList()
        for (gattService in gattServices) {
            val currentServiceData = HashMap<String, String>()
            uuid = gattService.uuid.toString()
            currentServiceData[LIST_NAME] =
                GattAttributeResolver.getAttributeName(uuid, unknownServiceString)
            currentServiceData[LIST_UUID] = uuid
            gattServiceData.add(currentServiceData)
            val gattCharacteristicGroupData = ArrayList<Map<String, String>>()
            val gattCharacteristics = gattService.characteristics
            val charas = ArrayList<BluetoothGattCharacteristic>()
            for (gattCharacteristic in gattCharacteristics) {
                charas.add(gattCharacteristic)
                val currentCharaData = HashMap<String, String>()
                uuid = gattCharacteristic.uuid.toString()
                currentCharaData[LIST_NAME] =
                    GattAttributeResolver.getAttributeName(uuid, unknownCharaString)
                currentCharaData[LIST_UUID] = uuid
                gattCharacteristicGroupData.add(currentCharaData)
            }
            (mGattCharacteristics ?: return).add(charas)
            gattCharacteristicData.add(gattCharacteristicGroupData)
        }
        LogHelper.i("witstec", "服务数量" + gattCharacteristicData.size)
        if (gattCharacteristicData.size == 0) {
            startSubscribeError()
            return
        }
        //发送数据
        displayGattServicesInit()
    }

    fun sendData(uuid: String, type: GattType) {
        i("gattService", "sendData-uuid=$uuid")
        connectManager = BluetoothConnectManager.getInstance(this)
        var serverUUid: UUID? = null
        gatt = (connectManager ?: return).getBluetoothGatt(mDeviceAddress)
        if (gatt != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                (gatt ?: return).requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_BALANCED)
            }
            val list = (gatt ?: return).services
            if (list != null) {
                for (service in list) {
                    for (characteristics in service.characteristics) {
                        if (characteristics.uuid.toString() == uuid) {
                            characteristic = characteristics
                            serverUUid = service.uuid
                            break
                        }
                    }
                }
            }
            if (characteristic != null) {
                (characteristic ?: return).writeType =
                    BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
                (connectManager ?: return).setServiceUUID(serverUUid.toString())
                (connectManager ?: return).cleanSubscribeData()
                if (BluetoothUtils.isCharacteristicRead((characteristic ?: return).properties)) {
                    (connectManager ?: return).addBluetoothSubscribeData(
                        BluetoothSubScribeData.Builder()
                            .setCharacteristicRead((characteristic ?: return).uuid).build()
                    )
                }
                if (BluetoothUtils.isCharacteristicNotify((characteristic ?: return).properties)) {
                    (connectManager ?: return).addBluetoothSubscribeData(
                        BluetoothSubScribeData.Builder()
                            .setCharacteristicNotify((characteristic ?: return).uuid).build()
                    )
                }
                if (connectManager != null && gatt != null) {
                    if ((connectManager ?: return).isConnectDevice) {
                        try {
                            val isSuccess = (connectManager ?: return).startSubscribe(gatt)
                            if (isSuccess) {
                                i(
                                    "gattService",
                                    "开始发送命令"
                                )
                                val disposable =
                                    Observable.create(ObservableOnSubscribe<Boolean> { e ->
                                        var isSendState4 = false
                                        while (!isSendState4) {
                                            val descriptor =
                                                characteristic!!.getDescriptor(
                                                    UUID.fromString(
                                                        gattCharacteristicData[0][0][LIST_UUID]
                                                    )
                                                )
                                            if (descriptor != null) {
                                                descriptor.value =
                                                    BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                                                gatt!!.writeDescriptor(descriptor)
                                            }
                                            //这一步必须要有 否则收不到通知
                                            isSendState4 =
                                                gatt!!.setCharacteristicNotification(
                                                    characteristic,
                                                    true
                                                )
                                            if (isSendState4) {
                                                e.onNext(true)
                                                break
                                            }
                                        }
                                    })
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe { isStatus ->
                                            sendDataInit(type)
                                        }
                                addDisposable(disposable)
                            } else {
                                i("gattService", "启动失败")
                                startSubscribeError()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        } else {
            i("gattService", "gatt is null")
            connectionBle()
        }
    }

    fun BleClear() {
        i("witstec", "调用了关闭连接")
        if (connectManager != null) {
            (connectManager ?: return).disconnect(mDeviceAddress)
            (connectManager ?: return).release()
            (connectManager ?: return).closeAll()
            connectManager = null
        }
        if ((mGattCharacteristics ?: return).size != 0) {
            (mGattCharacteristics ?: return).clear()
        }
        if (gattCharacteristicData.size != 0) {
            gattCharacteristicData.clear()
        }
        refreshDeviceCache()
    }

    /**
     * 清除蓝牙缓存
     */
    private fun refreshDeviceCache(): Boolean {
        gatt?.let {
            try {
                val localMethod = it.javaClass.getMethod("refresh")
                return (localMethod.invoke(it) as Boolean)
            } catch (localException: Exception) {
                localException.printStackTrace()
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timeout != null) {
            (timeout ?: return).dispose()
        }
    }
}