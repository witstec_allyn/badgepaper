package com.witstec.sz.badgeepaper.model.bean

enum class TemplateSizeType {

    B015,
    B021,
    B029,
    B042,
    B075,
    OTHER
}