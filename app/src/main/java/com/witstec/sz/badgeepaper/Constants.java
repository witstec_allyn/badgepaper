package com.witstec.sz.badgeepaper;

import com.witstec.sz.badgeepaper.utils.Utils;

public class Constants {
    public static String URL = "https://wtcard-restapi.witstec.com";
    public static String APK_URL = "https://ea.witstec.com";
    public static String BASE64 = "data:image/png;base64,";
    public static String UID = "uid";
    public static String TOKEN = "token";
    public static String ADMIN_KEY = "admin_key";
    public static int image_effect_size = 26;
    public static int W_015 = 200;
    public static int H_015 = 200;
    public static int W_021 = 251;
    public static int H_021 = 122;

    public static int SEND_W_021 = 251;
    public static int SEND_H_021 = 128;
    public static int W_029 = 297;
    public static int H_029 = 128;
    public static int W_042 = 400;
    public static int H_042 = 300;
    public static int W_075 = 880;
    public static int H_075 = 528;
    public static int QR_W = Utils.Companion.dp2px(App.Companion.getInstance(), 120);
    public static int QR_H = Utils.Companion.dp2px(App.Companion.getInstance(), 120);
    public static int BAR_W = Utils.Companion.dp2px(App.Companion.getInstance(), 120);
    public static int BAR_H = Utils.Companion.dp2px(App.Companion.getInstance(), 38);
    public static int PAINT_WIDTH_S = Utils.Companion.dp2px(App.Companion.getInstance(), 7);
    public static int PAINT_WIDTH_M = Utils.Companion.dp2px(App.Companion.getInstance(), 14);
    public static int PAINT_WIDTH_L = Utils.Companion.dp2px(App.Companion.getInstance(), 28);
    public static final int PAINT_DEFAULT_TEXT_SIZE_45 = Utils.Companion.dp2px(App.Companion.getInstance(), 35);
    public static final int PAINT_DEFAULT_TEXT_SIZE_55 = Utils.Companion.dp2px(App.Companion.getInstance(), 45);
    public static final int PAINT_DEFAULT_TEXT_SIZE_65 = Utils.Companion.dp2px(App.Companion.getInstance(), 55);
    public static final int PAINT_DEFAULT_TEXT_SIZE_75 = Utils.Companion.dp2px(App.Companion.getInstance(), 65);
    public static final int PAINT_DEFAULT_TEXT_SIZE_85 = Utils.Companion.dp2px(App.Companion.getInstance(), 75);

}
