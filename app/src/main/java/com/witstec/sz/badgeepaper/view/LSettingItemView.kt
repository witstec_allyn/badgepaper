package com.witstec.sz.badgeepaper.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.SwitchCompat
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.witstec.sz.badgeepaper.R

/**
 * @author Allyn
 * @date 2019/03/05
 * GitHub：https://github.com/Allyns
 * description：
 */
class LSettingItemView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        RelativeLayout(context, attrs, defStyleAttr) {

    /*左侧显示文本*/
    private var mLeftText: String? = null
    /*左侧图标*/
    private var mLeftIcon: Drawable? = null
    /*右侧图标*/
    private var mRightIcon: Drawable? = null
    /*左侧显示文本大小*/
    private val mTextSize: Int = 0
    /*左侧显示文本颜色*/
    private var mTextColor: Int = 0
    /*右侧显示文本大小*/
    private var mRightTextSize: Float = 13.toFloat()
    /*右侧显示文本颜色*/
    private var mRightTextColor: Int = 0
    /*整体根布局view*/
    private var mView: View? = null
    /*左侧文本控件*/
    private var mTvLeftText: TextView? = null
    /*右侧文本控件*/
    var rightTextView: TextView? = null
        private set
    /*分割线*/
    private var mUnderLine: View? = null
    /*左侧图标控件*/
    private var mIvLeftIcon: ImageView? = null
    /*左侧图标大小*/
    private var mLeftIconSzie: Int = 0
    /*右侧图标控件区域,默认展示图标*/
    private var mRightLayout: FrameLayout? = null
    /*右侧图标控件,默认展示图标*/
    private var mIvRightIcon: ImageView? = null
    /*右侧图标控件,选择样式图标*/
    private var mRightIcon_check: AppCompatCheckBox? = null
    /*右侧图标控件,开关样式图标*/
    private var mRightIcon_switch: SwitchCompat? = null
    /*右侧图标展示风格*/
    private var mRightStyle = 0

    /**
     * 更改右侧文字
     */
    var rightText: String
        get() = rightTextView!!.text.toString()
        set(info) {
            rightTextView!!.text = info
        }

    init {
        initView(context)
        getCustomStyle(context, attrs)
        //获取到右侧展示风格，进行样式切换
        switchRightStyle(mRightStyle)
    }

    /**
     * 初始化自定义属性
     *
     * @param context
     * @param attrs
     */
    fun getCustomStyle(context: Context, attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.LSettingView)
        val n = a.indexCount
        for (i in 0 until n) {
            val attr = a.getIndex(i)
            if (attr == R.styleable.LSettingView_leftText) {
                mLeftText = a.getString(attr)
                mTvLeftText!!.text = mLeftText
            } else if (attr == R.styleable.LSettingView_leftIcon) {
                // 左侧图标
                mLeftIcon = a.getDrawable(attr)
                if (null != mLeftIcon) {
                    mIvLeftIcon!!.setImageDrawable(mLeftIcon)
                    mIvLeftIcon!!.visibility = View.VISIBLE
                }
            } else if (attr == R.styleable.LSettingView_leftIconSize) {
                mLeftIconSzie = a.getDimension(attr, 16f).toInt()
                val layoutParams = mIvLeftIcon!!.layoutParams as LayoutParams
                layoutParams.width = mLeftIconSzie
                layoutParams.height = mLeftIconSzie
                mIvLeftIcon!!.layoutParams = layoutParams
            } else if (attr == R.styleable.LSettingView_leftTextMarginLeft) {
                val leftMargin = a.getDimension(attr, 8f).toInt()
                val layoutParams = mTvLeftText!!.layoutParams as LayoutParams
                layoutParams.leftMargin = leftMargin
                mTvLeftText!!.layoutParams = layoutParams
            } else if (attr == R.styleable.LSettingView_rightIcon) {
                // 右侧图标
                mRightIcon = a.getDrawable(attr)
                mIvRightIcon!!.setImageDrawable(mRightIcon)
            } else if (attr == R.styleable.LSettingView_LtextSize) {
                // 默认设置为16sp
                val textSize = a.getFloat(attr, 16f)
                mTvLeftText!!.textSize = textSize
            } else if (attr == R.styleable.LSettingView_LtextColor) {
                //文字默认灰色
                mTextColor = a.getColor(attr, Color.LTGRAY)
                mTvLeftText!!.setTextColor(mTextColor)
            } else if (attr == R.styleable.LSettingView_rightStyle) {
                mRightStyle = a.getInt(attr, 0)
            } else if (attr == R.styleable.LSettingView_isShowUnderLine) {
                //默认显示分割线
                if (!a.getBoolean(attr, true)) {
                    mUnderLine!!.visibility = View.GONE
                }
            } else if (attr == R.styleable.LSettingView_isShowRightText) {
                //默认不显示右侧文字
                if (a.getBoolean(attr, false)) {
                    rightTextView!!.visibility = View.VISIBLE
                }
            } else if (attr == R.styleable.LSettingView_rightText) {
                rightTextView!!.text = a.getString(attr)
//            } else if (attr == R.styleable.LSettingView_rightTextSize) {
//                // 默认设置为16sp
//                mRightTextSize = a.getFloat(attr, 14f)
//                rightTextView!!.textSize = mRightTextSize
            } else if (attr == R.styleable.LSettingView_rightTextColor) {
                //文字默认灰色
                mRightTextColor = a.getColor(attr, Color.GRAY)
                rightTextView!!.setTextColor(mRightTextColor)
            } else if (attr == R.styleable.LSettingView_id) {
                id = a.getInteger(attr, -1)
            } else if (attr == R.styleable.LSettingView_isShowRightIcon) {
                if (a.getBoolean(attr, true)) {
                    mIvRightIcon!!.visibility = View.VISIBLE
                } else {
                    mIvRightIcon!!.visibility = View.GONE
                }
            }
        }
        a.recycle()
    }

    /**
     * 根据设定切换右侧展示样式，同时更新点击事件处理方式
     *
     * @param mRightStyle
     */
    private fun switchRightStyle(mRightStyle: Int) {
        when (mRightStyle) {
            0 -> {
                //默认展示样式，只展示一个图标
                mIvRightIcon!!.visibility = View.VISIBLE
                mRightIcon_check!!.visibility = View.GONE
                mRightIcon_switch!!.visibility = View.GONE
            }
            1 ->
                //隐藏右侧图标
                mRightLayout!!.visibility = View.INVISIBLE
            2 -> {
                //显示选择框样式
                mIvRightIcon!!.visibility = View.GONE
                mRightIcon_check!!.visibility = View.VISIBLE
                mRightIcon_switch!!.visibility = View.GONE
            }
            3 -> {
                //显示开关切换样式
                mIvRightIcon!!.visibility = View.GONE
                mRightIcon_check!!.visibility = View.GONE
                mRightIcon_switch!!.visibility = View.VISIBLE
            }
        }
    }

    fun getmRightIconSwitch(): SwitchCompat? {
        return mRightIcon_switch
    }

    fun setmRightIconSwitch(mRightIcon_switch: SwitchCompat) {
        this.mRightIcon_switch = mRightIcon_switch
    }


    private fun initView(context: Context) {
        mView = View.inflate(context, R.layout.settingitem_layout, this)
        mUnderLine = mView!!.findViewById(R.id.underline)
        mTvLeftText = mView!!.findViewById(R.id.tv_lefttext)
        rightTextView = mView!!.findViewById(R.id.tv_righttext)
        mIvLeftIcon = mView!!.findViewById(R.id.iv_lefticon)
        mIvRightIcon = mView!!.findViewById(R.id.iv_rightIcon)
        mRightLayout = mView!!.findViewById(R.id.rightlayout)
        mRightIcon_check = mView!!.findViewById(R.id.rightcheck)
        mRightIcon_switch = mView!!.findViewById(R.id.rightswitch)
    }

    /**
     * 更改左侧文字
     */
    fun setLeftText(info: String) {
        mTvLeftText!!.text = info
    }

    fun getTvRightText(): TextView? {
        return rightTextView
    }

    fun setmTvRightText(mTvRightText: TextView) {
        this.rightTextView = mTvRightText
    }

    fun getmIvRightIcon(): ImageView? {
        return mIvRightIcon
    }

    fun setmIvRightIcon(mIvRightIcon: ImageView) {
        this.mIvRightIcon = mIvRightIcon
    }
}

