package com.witstec.sz.badgeepaper.model.db

import com.litesuits.orm.LiteOrm
import com.litesuits.orm.db.DataBaseConfig
import com.litesuits.orm.db.assit.QueryBuilder
import com.litesuits.orm.db.assit.SQLiteHelper
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.BuildConfig
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.schedulers.Schedulers

import java.util.ArrayList

class LocalDataSource private constructor() {

    val liteOrm: LiteOrm

    init {
        val config = DataBaseConfig(
            App.instance.applicationContext, "explorer.db"
        )
        config.debugged = BuildConfig.DEBUG
        config.dbVersion = 2 // set database version
        config.onUpdateListener = SQLiteHelper.OnUpdateListener { db, oldVersion, newVersion -> }
        liteOrm = LiteOrm.newCascadeInstance(config)
    }

    private object Inner {
        val INSTANCE = LocalDataSource()
    }

    /**
     * 查询
     * @param tClass class
     * @param <T>    类型
     * @return Observable
    </T> */
    fun <T> query(tClass: Class<T>): Observable<List<T>> {
        return Observable.create(ObservableOnSubscribe<List<T>> { e ->
            val list = liteOrm.query(QueryBuilder(tClass).orderBy("id DESC"))
            e.onNext(list)
        }).subscribeOn(Schedulers.io())
    }

    /**
     * 根据id查询
     */
    fun <T> query(id: Long, clazz: Class<T>): T {
        return liteOrm.queryById(id, clazz)
    }

    /**
     * @return 用于级联操作的 LiteOrm
     */
    fun cascade(): LiteOrm {
        return liteOrm.cascade()
    }

    /**
     * 保存或更新
     */
    fun save(`object`: Any) {
        liteOrm.save(`object`)
    }

    /**
     * 更新
     */
    fun renewSave(`object`: Any) {
        liteOrm.delete(`object`)
        liteOrm.save(`object`)
    }

    /**
     * 查询所有
     */
    fun <T> queryAll(classs: Class<T>): ArrayList<T> {
        return liteOrm.query(classs)
    }


    /**
     * 查询  某字段 等于 Value的值
     *
     * @return
     */
    fun <T> getQueryByWhere(cla: Class<T>, key: String, value: String): ArrayList<T> {
        return liteOrm.query(QueryBuilder(cla).where("$key=?", value))
    }

    /**
	 * 模糊查询
	 * @param cla
	 * @param field
	 * @param value
	 * @param <T>
	 * @return
	 */
    fun <T> getQueryByWhereLike(cla: Class<T>, key: String, value: String): ArrayList<T> {
        return liteOrm.query(QueryBuilder(cla).where("$key LIKE ?",value));
    }
    /**
     * 删除
     */
    fun delete(`object`: Any) {
        liteOrm.delete(`object`)
    }

    /**
     * 删除全部
     */
    fun delete(Class: Class<*>) {
        liteOrm.delete(Class)
    }

    /**
     * 更新
     */
    fun update(`object`: Any) {
        liteOrm.update(`object`)
    }

    companion object {

        val instance: LocalDataSource
            get() = Inner.INSTANCE
    }


}
