package com.witstec.sz.badgeepaper.model.bean;

public class ImageBean {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
