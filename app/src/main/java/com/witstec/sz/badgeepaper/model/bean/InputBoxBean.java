package com.witstec.sz.badgeepaper.model.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

public class InputBoxBean implements MultiItemEntity{

    public static final int TYPE_TEXT = 0x01;
    public static final int TYPE_BARCODE = 0x02;
    public static final int TYPE_QRCODE = 0x03;
    public static final int TYPE_IMAGE = 0x04;

    public InputBoxBean(int type, String text, int itemType) {
        this.type = type;
        this.text = text;
        this.itemType = itemType;
    }
    public InputBoxBean(int type, String image, int imageEffectSize, int itemType) {
        this.type = type;
        this.image = image;
        this.imageEffectSize = imageEffectSize;
        this.itemType = itemType;
    }

    private int type;
    private String text;
    private String image;
    private int imageEffectSize;
    private int itemType;

    public int getImageEffectSize() {
        return imageEffectSize;
    }

    public void setImageEffectSize(int imageEffectSize) {
        this.imageEffectSize = imageEffectSize;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }
}
