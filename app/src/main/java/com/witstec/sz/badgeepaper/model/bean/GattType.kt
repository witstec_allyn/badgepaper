package com.witstec.sz.badgeepaper.model.bean

enum class GattType {

    /**
     * 发送图片数据，黑色
     */
    IMAGE_FILE,
    /**
     * 发送图片数据2，红色
     */
    IMAGE_FILE_DOUBLE,
    /**
     * 是否为发送图片模式
     */
    IMAGE_SEND_REQUEST,
    /**
     * 是否为发送图片模式
     */
    IMAGE_COMMEND,

    /**
     * 图片发送完成刷新
     */
    IMAGE_CHANGE,

    /**
     * 通知电子价签保存的为黑色数据
     */
    IMAGE_UPDATE_BLACK,
    /**
     * 通知电子价签保存的为红色数据
     */
    IMAGE_UPDATE_RED,
    /**
     * 清空OTA内存
     */
    OTA_EMPTY_MEORY,
    /**
     * 检查OTA内存
     */
    OTA_CHECK_EMPTY_MEORY,

    /**
     * OTA数据发送
     */
    OTA_DATA_SEND,
    /**
     * OTA计算校验
     */
    OTA_CHECK,
    /**
     * OTA保存，完成
     */
    OTA_SAVE,

    /**
     * 读取模板信息命令
     */
    RED_DEVICE_MSG,

    /**
     * 打开接收通知开关
     */
    RED_DATA,

    /**
     * 添加设备模式
     */
    ADD_DEVICE,

    /**
     * 进入添加设备界面类型为二维码
     */
    QR_CODE,

    /**
     * 获取电量
     */
    GET_ELECTRICITY,

    /**
     * 获取型号
     */
    GET_TYPE_NAME,

    /**
     * 获取版本号
     */
    VERSION_GET,

    /**
     * 设置固件版本号
     */
    VERSION_SET,

    /**
     * 初始化铭牌
     */
    INIT_BASE_PAPER,

    /**
     *获取铭牌数据
     */
    GET_BASE_PAPER,

    /**
     * 设置铭牌数据
     */
    SET_BASE_PAPER,
    /**
     * 设置铭牌数据
     */
    UPDATE_DEVICE_NAME

}