package com.witstec.sz.badgeepaper.model.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

public class ImageBleMsg implements MultiItemEntity {

    public static final int TYPE_TEXT = 0x01;
    public static final int TYPE_BARCODE = 0x02;
    public static final int TYPE_QRCODE = 0x03;
    public static final int TYPE_IMAGE = 0x04;

    public String id;
    public Style style;
    public String vertex = "";
    public String parent = "";
    public String as = "";
    public String label = "";
    public String code = "";
    public int x = 0;
    public int y = 0;
    public int width = 0;
    public int height = 0;
    public ValueFont valueFont;
    public Blockquote blockquote;
    public Line line;
    public String valueText = "";
    public int itemType = 0;
    public TextViewAttributes tx;
    public int downslash = -1;

    public TextViewAttributes getTx() {
        return tx;
    }

    public int getDownslash() {
        return downslash;
    }

    public void setDownslash(int downslash) {
        this.downslash = downslash;
    }

    public void setTx(TextViewAttributes tx) {
        this.tx = tx;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    public ValueFont getValueFont() {
        return valueFont;
    }

    public void setValueFont(ValueFont valueFont) {
        this.valueFont = valueFont;
    }

    public Blockquote getBlockquote() {
        return blockquote;
    }

    public void setBlockquote(Blockquote blockquote) {
        this.blockquote = blockquote;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getVertex() {
        return vertex;
    }

    public void setVertex(String vertex) {
        this.vertex = vertex;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getAs() {
        return as;
    }

    public void setAs(String as) {
        this.as = as;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public static class Line {
        private int sourcePointX = 0;
        private int sourcePointY = 0;
        private int targetPointX = 0;
        private int targetPointY = 0;

        public int getSourcePointX() {
            return sourcePointX;
        }

        public void setSourcePointX(int sourcePointX) {
            this.sourcePointX = sourcePointX;
        }

        public int getSourcePointY() {
            return sourcePointY;
        }

        public void setSourcePointY(int sourcePointY) {
            this.sourcePointY = sourcePointY;
        }

        public int getTargetPointX() {
            return targetPointX;
        }

        public void setTargetPointX(int targetPointX) {
            this.targetPointX = targetPointX;
        }

        public int getTargetPointY() {
            return targetPointY;
        }

        public void setTargetPointY(int targetPointY) {
            this.targetPointY = targetPointY;
        }
    }

    public static class TextViewAttributes {

        private String textSpecial = "";
        private String text= "";
        private String textJuntos= "";
        private int size = 12;
        private int sizeSpecial = 12;
        private String color = "";
        private String colorSpecial = "";
        private String verticalAlign = "";
        private String verticalAlignSpecial = "";

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getTextJuntos() {
            return textJuntos;
        }

        public void setTextJuntos(String textJuntos) {
            this.textJuntos = textJuntos;
        }

        public String getVerticalAlignSpecial() {
            return verticalAlignSpecial;
        }

        public void setVerticalAlignSpecial(String verticalAlignSpecial) {
            this.verticalAlignSpecial = verticalAlignSpecial;
        }

        public String getVerticalAlign() {
            return verticalAlign;
        }

        public void setVerticalAlign(String verticalAlign) {
            this.verticalAlign = verticalAlign;
        }

        public String getTextSpecial() {
            return textSpecial;
        }

        public void setTextSpecial(String textSpecial) {
            this.textSpecial = textSpecial;
        }

        public int getSizeSpecial() {
            return sizeSpecial;
        }

        public void setSizeSpecial(int sizeSpecial) {
            this.sizeSpecial = sizeSpecial;
        }

        public String getColorSpecial() {
            return colorSpecial;
        }

        public void setColorSpecial(String colorSpecial) {
            this.colorSpecial = colorSpecial;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }

    public static class Blockquote {
        private String margin = "";
        private String border = "";
        private String padding = "0";
        private List<ValueFont> valueFontList;

        public String getMargin() {
            return margin;
        }

        public void setMargin(String margin) {
            this.margin = margin;
        }

        public String getBorder() {
            return border;
        }

        public void setBorder(String border) {
            this.border = border;
        }

        public String getPadding() {
            return padding;
        }

        public void setPadding(String padding) {
            this.padding = padding;
        }

        public List<ValueFont> getValueFontList() {
            return valueFontList;
        }

        public void setValueFontList(List<ValueFont> valueFontList) {
            this.valueFontList = valueFontList;
        }
    }

    public static class ValueFont {

        private String color = "@000000";
        private FontStyle fontStyle;
        private String text = "";

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public FontStyle getStyle() {
            return fontStyle;
        }

        public void setStyle(FontStyle style) {
            this.fontStyle = style;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }

    public static class FontStyle {
        private String fontSize = "0";

        public String getFontSize() {
            return fontSize;
        }

        public void setFontSize(String fontSize) {
            this.fontSize = fontSize;
        }
    }

    public static class Style {

        private int rounded = -1;
        private String whiteSpace = "";
        private String html = "";
        private String strokeColor;
        private String fillColor = "";
        private int horizontal = -1;
        private String align = "";
        private String verticalAlign = "";
        private int fontSize = 0;
        private String shape = "";
        private String imageAspect = "";
        private String aspect = "";
        private String verticalLabelPosition = "";
        private String image = "";
        private String type = "";
        private String fontColor = "";
        private int dashed = 0;

        public int getHorizontal() {
            return horizontal;
        }

        public void setHorizontal(int horizontal) {
            this.horizontal = horizontal;
        }

        public int getDashed() {
            return dashed;
        }

        public void setDashed(int dashed) {
            this.dashed = dashed;
        }

        public String getFontColor() {
            return fontColor;
        }

        public void setFontColor(String fontColor) {
            this.fontColor = fontColor;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getRounded() {
            return rounded;
        }

        public void setRounded(int rounded) {
            this.rounded = rounded;
        }

        public String getWhiteSpace() {
            return whiteSpace;
        }

        public void setWhiteSpace(String whiteSpace) {
            this.whiteSpace = whiteSpace;
        }

        public String getHtml() {
            return html;
        }

        public void setHtml(String html) {
            this.html = html;
        }

        public String getStrokeColor() {
            return strokeColor;
        }

        public void setStrokeColor(String strokeColor) {
            this.strokeColor = strokeColor;
        }

        public String getFillColor() {
            return fillColor;
        }

        public void setFillColor(String fillColor) {
            this.fillColor = fillColor;
        }

        public String getAlign() {
            return align;
        }

        public void setAlign(String align) {
            this.align = align;
        }

        public String getVerticalAlign() {
            return verticalAlign;
        }

        public void setVerticalAlign(String verticalAlign) {
            this.verticalAlign = verticalAlign;
        }

        public int getFontSize() {
            return fontSize;
        }

        public void setFontSize(int fontSize) {
            this.fontSize = fontSize;
        }

        public String getShape() {
            return shape;
        }

        public void setShape(String shape) {
            this.shape = shape;
        }

        public String getImageAspect() {
            return imageAspect;
        }

        public void setImageAspect(String imageAspect) {
            this.imageAspect = imageAspect;
        }

        public String getAspect() {
            return aspect;
        }

        public void setAspect(String aspect) {
            this.aspect = aspect;
        }

        public String getVerticalLabelPosition() {
            return verticalLabelPosition;
        }

        public void setVerticalLabelPosition(String verticalLabelPosition) {
            this.verticalLabelPosition = verticalLabelPosition;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
