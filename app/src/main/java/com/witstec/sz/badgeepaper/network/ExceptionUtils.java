package com.witstec.sz.badgeepaper.network;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.os.NetworkOnMainThreadException;

import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.BuildConfig;
import com.witstec.sz.badgeepaper.R;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.exceptions.OnErrorNotImplementedException;


public class ExceptionUtils {

    /**
     * 抛出 ApiCodeException
     *
     * @throws ApiCodeException api code exception
     *                          40000	系统错误
     *                          40001	参数缺失!
     *                          40002	手机号码已存在！
     *                          40003	验证码信息不存在！
     *                          40004	验证码已过期
     *                          40005	验证码错误！
     *                          40006	邮箱已存在
     *                          40007	邮件信息生成失败
     *                          40008	邮件发送错误
     *                          40009	摄像头绑定场景
     *                          40010	未知类型
     *                          40011	手机号码格式不符合规范！
     *                          40012	用户邮箱不符合规范
     *                          40013	用户账号不符合规范
     *                          40014	密码格式不符合规范！
     *                          40015	获取验证码过于频繁
     *                          40016	手机号码已注册
     *                          40017	验证码保存失败
     *                          40018	短信发送失败
     *                          40019	用户名或密码错误！
     *                          40020	登录错误
     *                          40021	账号不存在或被禁用
     *                          40022	手机用户不存在
     *                          40023	邮箱不存在！
     *                          40024	长度超过32位！
     *                          40025	帐号未激活
     *                          40026	网关设备离线
     *                          40100	未知错误！
     *                          40101	APP信息缺失！
     *                          40102	不存在此APP！
     *                          40103	用户token已过期！
     *                          40104	mac地址错误或设备已被禁用！
     *                          40105	无权绑定设备！
     *                          40106	用户token错误
     *                          40107	获取数据点信息失败
     *                          40108	获取设备数据失败
     *                          40201	设备尚未连接到云服务器！
     *                          40202	产品不存在或被禁用！
     *                          40203	长度不得超过30位
     *                          40204	长度不得超过100位
     *                          40205	设备已绑定
     *                          40206	设备未绑定
     *                          40301	群组名为空
     *                          40302	群组已存在
     *                          40303	群组中已存在本设备
     *                          40304	群组不存在
     *                          40305	群组非空
     *                          404	not found
     *                          9001	PRODUCT_KEY_NOT_EXIST
     *                          9002	PRODUCT_QUOTA_FULL
     *                          9003	REGISTER_SAVE_FAILED
     *                          9004	DEVICE_ID_IS_NULL
     *                          9005	PARENT_DEVICE_NOT_EXIST
     *                          9006	DEVICE_NOT_EXIST
     *                          9007	DEVICE_ACTIVE_FAILED
     *                          9008	SUB_DEVICE_NOT_EXIST
     *                          9009	DEVICE_BIND_FAILED
     *                          9010	DEVICE_LOGIN_FAILED
     *                          9011	DEVICE_HAS_BE_REGISTED_UNDER_ANOTHER_PRODUCT
     */
    public static void throwApiCodeException(int errcode, Context context) throws ApiCodeException {
            String errorMsg = "";
        switch (errcode) {
                case 40000:
                    errorMsg = context.getString(R.string.error_40000);
                    break;
                case 40001:
                    errorMsg = context.getString(R.string.error_40001);
                    break;
                case 40002:
                    errorMsg = context.getString(R.string.error_40002);
                    break;
                case 40003:
                    errorMsg = context.getString(R.string.error_40003);
                    break;
                case 40004:
                    errorMsg = context.getString(R.string.error_40004);
                    break;
                case 40005:
                    errorMsg = context.getString(R.string.error_40005);
                    break;
                case 40006:
                    errorMsg = context.getString(R.string.error_40006);
                    break;
                case 40007:
                    errorMsg = context.getString(R.string.error_40007);
                    break;
                case 40008:
                    errorMsg = context.getString(R.string.error_40008);
                    break;
                case 40009:
                    errorMsg = context.getString(R.string.error_40009);
                    break;
                case 40010:
                    errorMsg = context.getString(R.string.error_40010);
                    break;
                case 40011:
                    errorMsg = context.getString(R.string.error_40011);
                    break;
                case 40012:
                    errorMsg = context.getString(R.string.error_40012);
                    break;
                case 40013:
                    errorMsg = context.getString(R.string.error_40013);
                    break;
                case 40014:
                    errorMsg = context.getString(R.string.error_40014);
                    break;
                case 40015:
                    errorMsg = context.getString(R.string.error_40015);
                    break;
                case 40016:
                    errorMsg = context.getString(R.string.error_40016);
                    break;
                case 40017:
                    errorMsg = context.getString(R.string.error_40017);
                    break;
                case 40018:
                    errorMsg = context.getString(R.string.error_40018);
                    break;
                case 40019:
                    errorMsg = context.getString(R.string.error_40019);
                    break;
                case 400020:
                    errorMsg = context.getString(R.string.error_40020);
                    break;
                case 40021:
                    errorMsg = context.getString(R.string.error_40021);
                    break;
                case 40022:
                    errorMsg = context.getString(R.string.error_40022);
                    break;
                case 40023:
                    errorMsg = context.getString(R.string.error_40023);
                    break;
                case 40024:
                    errorMsg = context.getString(R.string.error_40024);
                    break;
                case 40025:
                    errorMsg = context.getString(R.string.error_40025);
                    break;
                case 40026:
                    errorMsg = context.getString(R.string.error_40026);
                    break;
                case 40100:
                    errorMsg = context.getString(R.string.error_40100);
                    break;
                case 40101:
                    errorMsg = context.getString(R.string.error_40101);
                    break;
                case 40102:
                    errorMsg = context.getString(R.string.error_40102);
                    break;
                case 40103:
                    errorMsg = context.getString(R.string.error_40103);
                    break;
                case 40104:
                    errorMsg = context.getString(R.string.error_40104);
                    break;
                case 40105:
                    errorMsg = context.getString(R.string.error_40105);
                    break;
                case 40106:
                    errorMsg = context.getString(R.string.error_40106);
                    break;
                case 40107:
                    errorMsg = context.getString(R.string.error_40107);
                    break;
                case 40108:
                    errorMsg = context.getString(R.string.error_40108);
                    break;
                case 40201:
                    errorMsg = context.getString(R.string.error_40201);
                    break;
                case 40202:
                    errorMsg = context.getString(R.string.error_40202);
                    break;
                case 40203:
                    errorMsg = context.getString(R.string.error_40203);
                    break;
                case 40204:
                    errorMsg = context.getString(R.string.error_40204);
                    break;
                case 40205:
                    errorMsg = context.getString(R.string.error_40205);
                    break;
                case 40206:
                    errorMsg = context.getString(R.string.error_40206);
                    break;
                case 40301:
                    errorMsg = context.getString(R.string.error_40301);
                    break;
                case 40302:
                    errorMsg = context.getString(R.string.error_40302);
                    break;
                case 40303:
                    errorMsg = context.getString(R.string.error_40303);
                    break;
                case 40304:
                    errorMsg = context.getString(R.string.error_40304);
                    break;
                case 40305:
                    errorMsg = context.getString(R.string.error_40305);
                    break;
                case 404:
                    errorMsg = context.getString(R.string.error_404);
                    break;
               case 40501:
                    errorMsg = context.getString(R.string.error_40501);
                    break;
                default:
                    errorMsg = "errorCode=" + errcode;
                    break;
            }
            //上次错误
            throw new ApiCodeException(errcode,
                    errorMsg);
    }

    public static String getDescription(Throwable throwable) {
        if (BuildConfig.DEBUG) {
            return getDescriptionForDebug(throwable);
        } else {
            return "";
        }
    }

    private static String getDescriptionForDebug(Throwable throwable) {
        if (throwable == null) {
            return "错误";
        }

        if (throwable instanceof ApiCodeException) {
            ApiCodeException apiCodeException = ((ApiCodeException) throwable);
            return "code=" + apiCodeException.getCode() + "," + apiCodeException.getMessage();
        }

        if (!NetworkUtils.isNetworkConnected(App.Companion.getInstance())) {
            return "网络未连接，请打开网络连接后重试~";
        }

        if (throwable instanceof UnknownHostException) {
            return "连接服务器失败~";
        }

        if (throwable instanceof SocketTimeoutException) {
            return "网络超时~";
        }

        if (throwable instanceof SocketException || throwable instanceof NetworkErrorException) {
            return "网络异常~";
        }

        if (throwable instanceof HttpException) {
            return "网络响应异常~";
        }

        if (throwable instanceof JsonSyntaxException || throwable instanceof JsonParseException) {
            return "数据解析错误~";
        }

        if (throwable instanceof NetworkOnMainThreadException) {
            return "出错，在主线程进行了网络请求~";
        }

        return "错误~";
    }

    private static String getDescriptionForRelease(Throwable throwable) {
        if (throwable == null) {
            return "错误";
        }

        if (throwable instanceof ApiCodeException) {
            ApiCodeException apiCodeException = ((ApiCodeException) throwable);
            return apiCodeException.getMessage();
        }

        if (!NetworkUtils.isNetworkConnected(App.Companion.getInstance())) {
            return "网络未连接，请打开网络连接后重试~";
        }

        if (throwable instanceof UnknownHostException) {
            return "连接服务器失败~";
        }
        if (throwable instanceof OnErrorNotImplementedException) {
            return "必须在主线程中执行~";
        }

        if (throwable instanceof SocketTimeoutException) {
            return "网络超时~";
        }

        if (throwable instanceof SocketException || throwable instanceof NetworkErrorException) {
            return "网络异常~";
        }

        if (throwable instanceof HttpException) {
            return "网络响应异常~";
        }

        if (throwable instanceof JsonSyntaxException || throwable instanceof JsonParseException) {
            return "数据解析错误~";
        }

        if (throwable instanceof NetworkOnMainThreadException) {
            return "网络请求出现问题~";
        }

        return "错误~";
    }
//    40000	系统错误
//40001	参数缺失!
//            40002	手机号码已存在！
//            40003	验证码信息不存在！
//            40004	验证码已过期
//40005	验证码错误！
//            40006	邮箱已存在
//40007	邮件信息生成失败
//40008	邮件发送错误
//40009	摄像头绑定场景
//40010	未知类型
//40011	手机号码格式不符合规范！
//            40012	用户邮箱不符合规范
//40013	用户账号不符合规范
//40014	密码格式不符合规范！
//            40015	获取验证码过于频繁
//40016	手机号码已注册
//40017	验证码保存失败
//40018	短信发送失败
//40019	用户名或密码错误！
//            40020	登录错误
//40021	账号不存在或被禁用
//40022	手机用户不存在
//40023	邮箱不存在！
//            40024	长度超过32位！
//            40025	帐号未激活
//40026	网关设备离线
//40100	未知错误！
//            40101	APP信息缺失！
//            40102	不存在此APP！
//            40103	用户token已过期！
//            40104	mac地址错误或设备已被禁用！
//            40105	无权绑定设备！
//            40106	用户token错误
//40107	获取数据点信息失败
//40108	获取设备数据失败
//40201	设备尚未连接到云服务器！
//            40202	产品不存在或被禁用！
//            40203	长度不得超过30位
//40204	长度不得超过100位
//40205	设备已绑定
//40206	设备未绑定
//40301	群组名为空
//40302	群组已存在
//40303	群组中已存在本设备
//40304	群组不存在
//404	not found
//9001	PRODUCT_KEY_NOT_EXIST
//9002	PRODUCT_QUOTA_FULL
//9003	REGISTER_SAVE_FAILED
//9004	DEVICE_ID_IS_NULL
//9005	PARENT_DEVICE_NOT_EXIST
//9006	DEVICE_NOT_EXIST
//9007	DEVICE_ACTIVE_FAILED
//9008	SUB_DEVICE_NOT_EXIST
//9009	DEVICE_BIND_FAILED
//9010	DEVICE_LOGIN_FAILED
//9011	DEVICE_HAS_BE_REGISTED_UNDER_ANOTHER_PRODUCT
}
