package com.witstec.sz.badgeepaper.utils

import android.content.Context
import android.graphics.Bitmap
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.model.bean.TemplateSizeType


/**
 *
 */
class Utils {

    companion object {

        /**
         * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
         */
        fun dp2px(context: Context, dpValue: Float): Int {
            val scale = context.resources.displayMetrics.density
            var dix = (dpValue * scale + 0.5f).toInt()
            val sizeType = App.BLE_TYPE
            when {
                sizeType === TemplateSizeType.B075 -> {
                    dix = (dix / 2.434).toInt()
                }
                sizeType === TemplateSizeType.B029 -> {
                    dix = (dix * 1.227).toInt()
                }
                sizeType === TemplateSizeType.B015 -> {
                    dix = (dix * 1.788).toInt()
                }
                sizeType === TemplateSizeType.B021 -> {
                    dix = (dix * 1.360).toInt()
                }
                sizeType === TemplateSizeType.B042 -> {
                    dix = (dix / 1.100).toInt()
                }
                else -> {
                    return dix
                }
            }
            return dix
        }

        /**
         * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
         */
        fun px2dp(context: Context, pxValue: Float): Int {
            val scale = context.resources.displayMetrics.density
            var dix = (pxValue / scale + 0.5f).toInt()
            val sizeType = App.BLE_TYPE
            when {
                sizeType === TemplateSizeType.B075 -> {
                    dix = (dix * 2.434).toInt()
                }
                sizeType === TemplateSizeType.B029 -> {
                    dix = (dix / 1.227).toInt()
                }
                sizeType === TemplateSizeType.B015 -> {
                    dix = (dix / 1.788).toInt()
                }
                sizeType === TemplateSizeType.B021 -> {
                    dix = (dix / 1.360).toInt()
                }
                sizeType === TemplateSizeType.B042 -> {
                    dix = (dix * 1.100).toInt()
                }
                else -> {
                    return dix
                }
            }
            return dix
        }

        /**
         * 对图片进行二值化处理
         *
         * @param bm
         * 原始图片
         * @return 二值化处理后的图片
         */
        fun getBinaryzationBitmap(bm: Bitmap): Bitmap {
            var bitmap: Bitmap? = null
            // 获取图片的宽和高
            val width = bm.width
            val height = bm.height
            // 创建二值化图像
            bitmap = bm.copy(Bitmap.Config.RGB_565, true)
            // 遍历原始图像像素,并进行二值化处理
            for (i in 0 until width) {
                for (j in 0 until height) {
                    // 得到当前的像素值
                    val pixel = bitmap!!.getPixel(i, j)
                    // 得到Alpha通道的值
                    val alpha = pixel and -0x1000000
                    // 得到Red的值
                    val red = pixel and 0x00FF0000 shr 16
                    // 得到Green的值
                    val green = pixel and 0x0000FF00 shr 8
                    // 得到Blue的值
                    val blue = pixel and 0x000000FF
                    // 通过加权平均算法,计算出最佳像素值
                    var gray =
                        (red.toFloat() * 0.3 + green.toFloat() * 0.59 + blue.toFloat() * 0.11).toInt()
                    // 对图像设置黑白图
                    if (gray <= 95) {
                        gray = 0
                    } else {
                        gray = 255
                    }
                    // 得到新的像素值
                    val newPiexl = alpha or (gray shl 16) or (gray shl 8) or gray
                    // 赋予新图像的像素
                    bitmap.setPixel(i, j, newPiexl)
                }
            }
            return bitmap
        }

        fun bytesToHexString(bArr: ByteArray): String {
            val sb = StringBuffer(bArr.size)
            var sTmp: String
            sb.append("[")
            var index = 0
            for (i in bArr.indices) {
                sTmp = Integer.toHexString(0xFF and bArr[i].toInt())
                if (sTmp.length < 2)
                    sb.append(0)
                sb.append(sTmp.toUpperCase())
                sb.append(",")
//                if (index % 2 == 1) {
//                    sb.append("  ")
//                }
                index++
            }
            sb.append("]")
            return sb.toString()
        }
    }

}
