package com.witstec.sz.badgeepaper.model.bean;

public class TokenBean {


    /**
     * token : string
     * refresh_token : string
     * expire_at : 0
     */

    private String token;
    private String refresh_token;
    private int expire_at;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public int getExpire_at() {
        return expire_at;
    }

    public void setExpire_at(int expire_at) {
        this.expire_at = expire_at;
    }
}
