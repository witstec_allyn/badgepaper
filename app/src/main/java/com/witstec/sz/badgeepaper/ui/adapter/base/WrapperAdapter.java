package com.witstec.sz.badgeepaper.ui.adapter.base;

import androidx.recyclerview.widget.RecyclerView;
import com.github.nukc.LoadMoreWrapper.LoadMoreWrapper;
import com.github.nukc.recycleradapter.RecyclerAdapter;

import java.util.List;

/**
 * 封装 LoadMoreWrapper 和 RecyclerAdapter
 */

public abstract class WrapperAdapter<T> extends RecyclerAdapter<T> {

    private LoadMoreWrapper mWrapper;

    public WrapperAdapter() {}

    public WrapperAdapter(List<T> dataList) {
        super(dataList);
    }

    public WrapperAdapter(Object listener) {
        super(listener);
    }

    public WrapperAdapter(List<T> dataList, Object listener) {
        super(dataList, listener);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mWrapper = LoadMoreWrapper.with(this)
                .setListener(enabled -> WrapperAdapter.this.onLoadMore())
                .setShowNoMoreEnabled(false);
        mWrapper.into(recyclerView);
    }

    public abstract void onLoadMore();

    public void setLoadMoreEnabled(boolean enabled) {
        mWrapper.setLoadMoreEnabled(enabled);
    }

    public void isLoadFailed() {
        mWrapper.setLoadFailed(true);
    }

    public int getCount() {
        return mWrapper.getOriginalAdapter().getItemCount();
    }

}
