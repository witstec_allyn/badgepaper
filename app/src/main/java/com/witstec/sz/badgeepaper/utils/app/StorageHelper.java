package com.witstec.sz.badgeepaper.utils.app;

import com.witstec.sz.badgeepaper.utils.PreferencesHelper;
import com.witstec.sz.badgeepaper.utils.StringUtils;

public class StorageHelper {


    /**
     * 判断是否为同一天
     */
    public static void saveTheSameDay() {
        String time = StringUtils.getNowadays();
        PreferencesHelper.getInstance().putString("theSameDay", time);
    }

    /**
     * 判断是否为同一天
     *
     * @return
     */
    public static String getTheSameDay() {
        return PreferencesHelper.getInstance().getString("theSameDay", "2000-00-00");
    }

    /**
     * 判断是否为同一天  日志上传
     */
    public static void saveTheSameDayLogs() {
        String time = StringUtils.getNowadays();
        PreferencesHelper.getInstance().putString("theSameDayLogs", time);
    }

    /**
     * 判断是否为同一天   日志上传
     *
     * @return
     */
    public static String getTheSameDayLogs() {
        return PreferencesHelper.getInstance().getString("theSameDayLogs", "2000-00-00");
    }

    /**
     * 保存语言信息
     *
     * @param witch
     */
    public static void saveLanguage(int witch) {
        PreferencesHelper.getInstance().putInt("languageIndex", witch);
    }

    /**
     * 获取语言信息
     *
     * @param
     */
    public static int getLanguage() {
        return PreferencesHelper.getInstance().getInt("languageIndex", 1);
    }

}
