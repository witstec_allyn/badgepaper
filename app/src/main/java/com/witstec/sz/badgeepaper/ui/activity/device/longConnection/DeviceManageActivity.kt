package com.witstec.sz.badgeepaper.ui.activity.device.longConnection

import android.annotation.SuppressLint
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.SystemClock
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import cn.com.heaton.blelibrary.ble.Ble
import cn.com.heaton.blelibrary.ble.BleLog
import cn.com.heaton.blelibrary.ble.callback.BleConnectCallback
import cn.com.heaton.blelibrary.ble.callback.BleNotifyCallback
import cn.com.heaton.blelibrary.ble.callback.BleReadCallback
import cn.com.heaton.blelibrary.ble.callback.BleWriteCallback
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.litesuits.orm.db.assit.WhereBuilder
import com.litesuits.orm.db.model.ColumnsValue
import com.litesuits.orm.db.model.ConflictAlgorithm
import com.witstec.sz.badgeepaper.*
import com.witstec.sz.badgeepaper.draw.Constant
import com.witstec.sz.badgeepaper.draw.bean.Pel
import com.witstec.sz.badgeepaper.draw.bean.Picture
import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity
import com.witstec.sz.badgeepaper.draw.ui.view.CanvasView
import com.witstec.sz.badgeepaper.manage.BitmapBleBmpManagement
import com.witstec.sz.badgeepaper.manage.BitmapBleBmpManagement.*
import com.witstec.sz.badgeepaper.model.bean.*
import com.witstec.sz.badgeepaper.model.db.LocalDataSource.Companion.instance
import com.witstec.sz.badgeepaper.model.event.DeviceMsgUpdateEvent
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.activity.device.DeviceManageMoreActivity
import com.witstec.sz.badgeepaper.ui.activity.device.FilterTemplateActivity
import com.witstec.sz.badgeepaper.ui.adapter.DeviceMsgTypeAdapter
import com.witstec.sz.badgeepaper.utils.*
import com.witstec.sz.badgeepaper.utils.LogHelper.i
import com.witstec.sz.badgeepaper.utils.Utils.Companion.px2dp
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.utils.app.CRC16CheckUtil
import com.witstec.sz.badgeepaper.utils.app.CacheActivity
import com.witstec.sz.badgeepaper.view.IosDialog
import com.witstec.sz.badgeepaper.view.cop.CropImageView
import com.wuhenzhizao.titlebar.widget.CommonTitleBar
import com.zhihu.matisse.Matisse
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_device_manage.*
import kotlinx.android.synthetic.main.layout_device_manage_right.*
import top.zibin.luban.Luban
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.nio.ByteBuffer
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * 模板编辑，发送模板
 */
class DeviceManageActivity : AppBaseActivity() {
    private val TAG = "DeviceManageActivity"
    private var madapter: DeviceMsgTypeAdapter? = null
    private var bitmapBleImage: Bitmap? = null
    private var isConfirmSend: Boolean = false
    private var dataList: ArrayList<InputBoxBean> = arrayListOf()
    private var bleType: String = ""
    private var templateId: String = ""
    private var sizeType: TemplateSizeType = TemplateSizeType.B029
    private var mDeviceAddress = ""
    private var mIndex = 0
    private var isCloseLoop = false
    private var canSendClick = false
    private var template_W = 0
    private var template_H = 0
    private var image_effect_size = Constants.image_effect_size
    private var image_type_position = 0
    private var timeout: Disposable? = null
    private var mCardVersion = ""
    private var mBaseVersion = ""
    private var gattServices: java.util.ArrayList<BluetoothGattService>? = java.util.ArrayList()
    private var isShowIng = false
    private var isCardExit = false

    companion object {
        var isUpdateModeAdapter: Boolean = true
        fun start(
            context: Context, mac: String
        ) {
            val intent = Intent(context, DeviceManageActivity::class.java)
            intent.putExtra("extra_mac", mac)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_manage)
        if (CacheActivity.activityList.contains(this)) {
            CacheActivity.addActivity(this);
        }
        setToolbar(App.device_name)
        mDeviceAddress = intent.getStringExtra("extra_mac").toString()
        //清除事件
        iv_image_template.setOnTouchListener { v, event ->
            true
        }
        //定义尺寸
        template_W = Constants.W_021
        template_H = Constants.H_021
        tv_mode_type.text = this.getString(R.string.ble_mode_text_type_021)
        sizeType = TemplateSizeType.B021
        App.BLE_TYPE = sizeType
        iv_image_template.setCanvasSize(Constants.W_021, Constants.H_021)
        madapter = DeviceMsgTypeAdapter(dataList)
        recycler.adapter = madapter
        recycler.layoutManager = LinearLayoutManager(this)
        //s实时更新
        val disposable = RxBus.register(DeviceMsgUpdateEvent::class.java)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                image_type_position = it.position
                refreshView(madapter!!.data)
            }
        addDisposable(disposable)
        connectBle()
        listener()
    }

    fun connectBle() {
        //开始连接
        tv_connect_status.text = getString(R.string.add_device_loading_one)
        Log.e(
            TAG,
            "执行了初始化界面: "
        )
        connectHint()
        showNUllMode()
        if (App.isBleInitSuccess) {
            Ble.getInstance<BleRssiDevice>().disconnectAll()
            timeout = Observable.timer(
                2600, TimeUnit.MILLISECONDS
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Ble.options().isAutoConnect = true
                    Ble.getInstance<BleRssiDevice>().connect(mDeviceAddress, connectCallback)
                    Log.e(
                        TAG,
                        "没有执行初始界面222: "
                    )

                    timeout = Observable.timer(
                        1000 * 12, TimeUnit.MILLISECONDS
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            tv_connect_status.text =
                                getString(R.string.The_base_cannot_be_connected)
                            connectNull()
                        }
                    addDisposable(disposable = timeout ?: return@subscribe)
                }
            addDisposable(timeout!!)
        } else {
            ToastUtils.show("初始化蓝牙失败")
        }
    }

    fun listener() {
        btn_update_template.setOnClickListener {
            SystemClock.sleep(250)
            val intent = Intent(this, FilterTemplateActivity::class.java)
            intent.putExtra("extra_size_type", "2")
            startActivityForResult(intent, FilterTemplateActivity.EXTRO_GROUP_SELECT_CODE)
        }

        btn_save_edit.setOnClickListener {
            mIndex = 0
            if (timeout != null) {
                (timeout ?: return@setOnClickListener).dispose()
            }
            isConfirmSend = true
            showDialog()
            initImage()
            timeOut()
        }

        titlebar.setListener { v, action, _ ->
            if (action == CommonTitleBar.ACTION_LEFT_BUTTON || action == CommonTitleBar.ACTION_LEFT_TEXT) {
                onBackPressed()
            }
        }

        btn_device_manage.setOnClickListener {
            if (mBaseVersion.isEmpty()) {
                ToastUtils.show(getString(R.string.get_card_msg_wait))
                return@setOnClickListener
            }
            if (isCardExit) {
                if (mCardVersion.isEmpty()) {
                    ToastUtils.show(getString(R.string.get_card_msg_wait))
                    return@setOnClickListener
                }
            }
            DeviceManageMoreActivity.start(
                this, mDeviceAddress, bleType, mBaseVersion, mCardVersion
            )
        }
    }

    fun initImage() {
        i("sendImage", "huidiao" + "SET_BASE_PAPER")
        val pelArrayList = ArrayList<Pel>()
        val pelArrayListOld = ArrayList<Pel>()
        pelArrayList.clear()
        pelArrayListOld.clear()
        if (bitmapBleImage != null) {
            bitmapBleImage!!.recycle()
            bitmapBleImage = null
        }
        val pelListOld = CanvasView.getPelList()
        for (i in 0 until pelListOld.size) {
            val pel = pelListOld[i]
            if (pel.type == 30) {
                val pod = Pel()
                pod.region.set(
                    pel.region.bounds.left,
                    pel.region.bounds.top,
                    pel.region.bounds.right,
                    pel.region.bounds.bottom
                )
                pod.type = pel.type
                pod.picture = pel.picture
                pod.image_effect_size = pel.image_effect_size
                pelArrayListOld.add(pod)
                val left =
                    (pel.centerPoint.x + pel.transDx) - ((pel.centerPoint.x - pel.region.bounds.left) * pel.scale)
                val right =
                    (pel.centerPoint.x + pel.transDx) + ((pel.region.bounds.right - pel.centerPoint.x) * pel.scale)
                val top =
                    (pel.centerPoint.y + pel.transDy) - ((pel.centerPoint.y - pel.region.bounds.top) * pel.scale)
                val bottom =
                    (pel.centerPoint.x + pel.transDx) + ((pel.region.bounds.bottom - pel.centerPoint.y) * pel.scale)

                pel.region.set(
                    pxToDp(left).toInt(),
                    pxToDp(top).toInt(),
                    pxToDp(right).toInt(),
                    pxToDp(bottom).toInt()
                )
                pel.isOriginalImage = true
                pelArrayList.add(pel)
            }
        }
        iv_image_template.updateSavedBitmap()
        if (sizeType == TemplateSizeType.B021) {
            bitmapBleImage = convertGreyImgByFloydSendImg(
                AppUtils.combineBitmap(
                    AppUtils.zoomImg(CanvasView.getSavedBitmap(), template_W, template_H)
                ), pelArrayList
            )
        } else {
            bitmapBleImage = convertGreyImgByFloyd(
                AppUtils.zoomImg(
                    CanvasView.getSavedBitmap(),
                    template_W,
                    template_H
                )
            )
        }
        val pelList = CanvasView.getPelList()
        var index = 0
        for (i in 0 until pelList.size) {
            if (pelList[i].type == 30) {
                pelList[i].region.set(
                    pelArrayListOld[index].region.bounds.left,
                    pelArrayListOld[index].region.bounds.top,
                    pelArrayListOld[index].region.bounds.right,
                    pelArrayListOld[index].region.bounds.bottom
                )
                pelList[i].picture = pelArrayListOld[index].picture
                pelList[i].image_effect_size =
                    pelArrayListOld[index].image_effect_size
                pelList[i].isOriginalImage = false
                index++
            }
        }

        iv_image_template.updateSavedBitmap()
        tv_device_no_template.visibility = View.GONE
        iv_image_template.visibility = View.VISIBLE
        if (sizeType == TemplateSizeType.B029 || sizeType == TemplateSizeType.B021) {
            bitmapBleImage = adjustPhotoRotation(bitmapBleImage, 270)
        }
        sendImage01()
    }

    fun sendImage01() {
        //压缩总数
        val byteDataBlack = BitmapBleBmpManagement.getBmpProtocolData(bitmapBleImage, false)
        val byteDataRed = BitmapBleBmpManagement.getBmpProtocolData(bitmapBleImage, true)
        val loadData = ByteBuffer.allocate(byteDataBlack.size + byteDataRed.size)
        loadData.put(byteDataBlack)
        loadData.put(byteDataRed)
        loadData.flip()
        val totalLoadData = ByteArray(loadData.remaining())
        loadData[totalLoadData]

        //不压缩总数
        val byteDataBlack_no =
            BitmapBleBmpManagement.getBmpProtocolDataNoCompression(bitmapBleImage, false)
        val byteDataRed_no =
            BitmapBleBmpManagement.getBmpProtocolDataNoCompression(bitmapBleImage, true)
        val loadData_no = ByteBuffer.allocate(byteDataBlack_no.size + byteDataRed_no.size)
        loadData_no.put(byteDataBlack_no)
        loadData_no.put(byteDataRed_no)
        loadData_no.flip()
        val totalLoadData_no = ByteArray(loadData_no.remaining())
        loadData_no[totalLoadData_no]

        val crcData = ByteBuffer.allocate(10)
        crcData.put(IntSplitToByteReverseA(totalLoadData_no.size, 4))
        crcData.put(IntSplitToByteReverseA(totalLoadData.size, 4))
        crcData.put(IntSplitToByteReverseA(CRC16CheckUtil.getCRC3(totalLoadData), 2))
        crcData.flip()
        val crcDatas = ByteArray(crcData.remaining())
        crcData[crcDatas]

        val sendData = setOteVersion(18, crcDatas)
        val characteristic = gattServices?.get(0)?.characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.isNotEmpty()) {
            val bleDevice = connetedDevices[0]
            Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, sendData, suid, cuid,
                object :
                    BleWriteCallback<BleRssiDevice?>() {
                    override fun onWriteSuccessCharacteristicChanged(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        LogHelper.i(
                            "writeByUuid",
                            "onWriteSuccessCharacteristicChanged value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                        val returnBytes =
                            ByteUtils.byteArrayToHexString(
                                characteristic.value
                            ).split(",")
                        val commandStr = (returnBytes[5] + returnBytes[6])
                        if (commandStr == "0012") {
                            runOnUiThread {
                                showDialog(getString(R.string.sms_code_send))
                            }
                            sendImage02(false)
                        }
                    }

                    override fun onWriteSuccess(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        LogHelper.i(
                            "writeByUuid",
                            "onWriteSuccess value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                    }
                })
        }
    }

    fun sendImage02(isRed: Boolean) {
        val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
            val characteristic = gattServices?.get(0)?.characteristics?.get(0)
            val suid = characteristic?.service?.uuid
            val cuid = characteristic?.uuid
            val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
            var mIndex = 0;
            var isWaiting = true
            isCloseLoop = false
            var onWriteSuccess: Boolean
            var onChanged: Boolean
            if (connetedDevices.isNotEmpty()) {
                val bleDevice = connetedDevices[0]
                if (mIndex != 0 && bitmapBleImage == null) return@ObservableOnSubscribe
                val byteDataBlack = BitmapBleBmpManagement.getBmpProtocolData(bitmapBleImage, isRed)
                val byteList = BitmapBleBmpManagement.writeEntity(byteDataBlack, 140)
                val byteListSize = byteList.size
                while (mIndex < byteListSize) {
                    if (isCloseLoop) {
                        break
                    }
                    if (isWaiting) {
                        SystemClock.sleep(10)
                        if (mIndex == byteListSize) {
                            i("sendimg", "达到了数量 关闭循环")
                            break
                        }
                        onWriteSuccess = false
                        onChanged = false
                        i(
                            "sendimg",
                            "发送数据包数量：byteList.size=" + byteListSize + ",mIndex=" + mIndex
                        )
                        val sendData = getBmpProtocolEach(
                            byteList[mIndex],
                            byteListSize,
                            byteList[mIndex].size,
                            mIndex
                        )
//                        i(
//                            "sendImage", "发送数据包数量内容 ==" + ByteUtils.byteArrayToHexString(
//                                sendData
//                            )
//                        )
                        isWaiting = false

                        Ble.getInstance<BleRssiDevice>()
                            .writeByUuid(bleDevice, sendData, suid, cuid,
                                object :
                                    BleWriteCallback<BleRssiDevice?>() {
                                    override fun onWriteSuccessCharacteristicChanged(
                                        device: BleRssiDevice?,
                                        characteristic: BluetoothGattCharacteristic
                                    ) {
                                        val returnBytes =
                                            ByteUtils.byteArrayToHexString(
                                                characteristic.value
                                            ).split(",")
                                        val commandStr = (returnBytes[5] + returnBytes[6])
                                        if (commandStr == "0013") {
                                            LogHelper.i(
                                                "writeByUuid",
                                                "发送图片设备回复 value=" +
                                                        ByteUtils.byteArrayToHexString(
                                                            characteristic.value
                                                        )
                                            )
                                            if (returnBytes[returnBytes.size - 2] == "00") {
                                                onChanged = true
                                                if (mIndex == byteListSize - 1 && onWriteSuccess && onChanged) {
                                                    isCloseLoop = true
                                                    i("sendimg", "达到了数量 下一步操作")
                                                    if (isRed) {
                                                        SystemClock.sleep(80)
                                                        refreshImage()
                                                        LogHelper.i("sendimage", "刷新屏幕");
                                                    } else {
                                                        LogHelper.i("sendimage", "开始刷新红色图层数据");
                                                        SystemClock.sleep(50)
                                                        sendImage02(true)
                                                    }
                                                }
                                                if (mIndex < byteListSize && onWriteSuccess && onChanged) {
                                                    i("sendimg", "发下一条数据包")
                                                    mIndex++
                                                    isWaiting = true
                                                }
                                            }
                                        }
                                    }

                                    override fun onWriteSuccess(
                                        device: BleRssiDevice?,
                                        characteristic: BluetoothGattCharacteristic
                                    ) {

                                        val returnBytes =
                                            ByteUtils.byteArrayToHexString(
                                                characteristic.value
                                            ).split(",")
                                        val commandStr = (returnBytes[5] + returnBytes[6])
                                        if (commandStr == "0013") {
                                            LogHelper.i(
                                                "writeByUuid",
                                                "发送图片写入回调=" +
                                                        ByteUtils.byteArrayToHexString(
                                                            characteristic.value
                                                        )
                                            )
                                            onWriteSuccess = true
                                            if (mIndex == byteListSize - 1 && onWriteSuccess && onChanged) {
                                                isCloseLoop = true
                                                i("sendimg", "达到了数量 下一步操作")
                                                if (isRed) {
                                                    SystemClock.sleep(100)
                                                    refreshImage()
                                                    LogHelper.i("sendimage", "刷新屏幕");
                                                } else {
                                                    LogHelper.i("sendimage", "开始刷新红色图层数据");
                                                    sendImage02(true)
                                                }
                                            }
                                            if (mIndex < byteListSize && onWriteSuccess && onChanged) {
                                                i("sendimg", "发下一条数据包")
                                                mIndex++
                                                isWaiting = true
                                            }
                                        }
                                    }
                                })
                    }
                }
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isStatus ->
            }
        addDisposable(disposable)

    }

    fun refreshImage() {
        val byteListChange = BitmapBleBmpManagement.getBmpProtocolDataChange(20)
        val characteristic = gattServices?.get(0)?.characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.isNotEmpty()) {
            val bleDevice = connetedDevices[0]
            LogHelper.i("sendimage", "发送了刷新屏幕");
            i(
                "sendImage", "刷新命令数据 ==" + ByteUtils.byteArrayToHexString(
                    byteListChange
                )
            )
            Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, byteListChange, suid, cuid,
                object :
                    BleWriteCallback<BleRssiDevice?>() {
                    override fun onWriteSuccessCharacteristicChanged(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        LogHelper.i(
                            "writeByUuid",
                            "onWriteSuccessCharacteristicChanged value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                        val returnBytes =
                            ByteUtils.byteArrayToHexString(
                                characteristic.value
                            ).split(",")
                        val commandStr = (returnBytes[5] + returnBytes[6])
                        if (commandStr == "0014") {
                            if (returnBytes[returnBytes.size - 2] == "00") {
                                runOnUiThread {
                                    if (timeout != null) {
                                        (timeout ?: return@runOnUiThread).dispose()
                                    }
                                    timeout = Observable.timer(1000 * 20, TimeUnit.MILLISECONDS)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe {
                                            showDialogNo(getString(R.string.Screen_refresh_complete))
                                            saveData()
                                            if (timeout != null) {
                                                (timeout ?: return@subscribe).dispose()
                                            }
                                        }
                                    addDisposable(timeout ?: return@runOnUiThread)
                                    showDialog(getString(R.string.Refreshing_screen_please_wait))
                                }
                            } else {
                                runOnUiThread {
                                    if (timeout != null) {
                                        (timeout ?: return@runOnUiThread).dispose()
                                    }
                                    dialogDismiss()
                                    ToastUtils.show(getString(R.string.Screen_refresh_failure))
                                }
                            }
                        }
                        if (commandStr == "0015") {
                            if (timeout != null) {
                                (timeout ?: return).dispose()
                            }
                            runOnUiThread {
                                showDialogNo(getString(R.string.Screen_refresh_complete))
                                saveData()
                            }
                        }
                    }

                    override fun onWriteSuccess(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        LogHelper.i(
                            "writeByUuid",
                            "onWriteSuccess value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                    }
                })
        }
    }

    fun saveData() {
        val dataList = madapter!!.data
        if (dataList.isNotEmpty()) {
            val jsonArray = JsonArray()
            dataList.forEach {
                if (it.itemType == InputBoxBean.TYPE_TEXT || it.itemType == InputBoxBean.TYPE_QRCODE ||
                    it.itemType == InputBoxBean.TYPE_BARCODE
                ) {
                    if (it.text.isNotEmpty()) {
                        val jsonObject = JsonObject()
                        jsonObject.addProperty("text", it.text)
                        jsonObject.addProperty("type", it.type.toString())
                        jsonArray.add(jsonObject)
                    }
                } else if (it.itemType == InputBoxBean.TYPE_IMAGE) {
                    val jsonObject = JsonObject()
                    jsonObject.addProperty("image", it.image)
                    jsonObject.addProperty("imageEffectSize", it.imageEffectSize)
                    jsonObject.addProperty("type", it.type.toString())
                    jsonArray.add(jsonObject)
                }
            }
            //修改模板
            val columnsValue = ColumnsValue(
                arrayOf(
                    "inputDataJson"
                ), arrayOf(jsonArray.toString())
            )
            instance
                .liteOrm
                .update(
                    WhereBuilder.create(TemplateBean::class.java)
                        .where("templateId=?", templateId),
                    columnsValue, ConflictAlgorithm.None
                )
            LogHelper.i("witstec", "jsonArray=" + jsonArray.toString())
        }
        val currentDate = StringUtils.toByte(templateId)
        val byteListChange =
            BitmapBleBmpManagement.getBmpDatacolEach(currentDate)
        i("witstec", "bytes=" + currentDate.size)
        LogHelper.i("witstec", "currentDate=" + templateId)
        val characteristic = gattServices?.get(0)?.characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.isNotEmpty()) {
            val bleDevice = connetedDevices[0]
            Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, byteListChange, suid, cuid,
                object :
                    BleWriteCallback<BleRssiDevice?>() {
                    override fun onWriteSuccessCharacteristicChanged(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        LogHelper.i(
                            "writeByUuid",
                            "onWriteSuccessCharacteristicChanged value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                        val returnBytes =
                            ByteUtils.byteArrayToHexString(
                                characteristic.value
                            ).split(",")
                        val commandStr = (returnBytes[5] + returnBytes[6])
                        if (commandStr == "0017") {
                            runOnUiThread {
//                                ToastUtils.show(getString(R.string.Template_saved_successfully))
                                LogHelper.i(
                                    "writeByUuid",
                                    getString(R.string.Template_saved_successfully)
                                )
                            }
                        }
                    }

                    override fun onWriteSuccess(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        LogHelper.i(
                            "writeByUuid",
                            "onWriteSuccess value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                    }
                })
        }
    }

    //输入框的数据改变后刷新
    private fun refreshView(inputList: List<InputBoxBean>) {
        if ((madapter ?: return).data.size != 0) {
            val pelList = CanvasView.getPelList()
            var index = 0
            for (modeItem in 0 until pelList.size) {
                if (pelList[modeItem].type == inputList[index].type) {
                    when (inputList[index].type) {
                        31 -> {
                            val pell = pelList[modeItem]
                            pell.text.content = inputList[index].text
                            pelList[modeItem] = pell
                            if (inputList.size - 1 > index) {
                                index++
                            }
                        }
                        32 -> {
                            val pell = pelList[modeItem]
                            pell.text.content = inputList[index].text
                            pelList[modeItem] = pell
                            if (inputList.size - 1 > index) {
                                index++
                            }
                        }
                        20 -> {
                            val pell = pelList[modeItem]
                            pell.text.content = inputList[index].text
                            pelList[modeItem] = pell
                            if (inputList.size - 1 > index) {
                                index++
                            }
                        }
                        30 -> {
                            val pell = pelList[modeItem]
                            val picture = Picture(AppUtils.base64ToBitmap(inputList[index].image))
                            pell.picture = picture
                            pell.image_effect_size = inputList[index].imageEffectSize
                            pelList[modeItem] = pell
                            if (inputList.size - 1 > index) {
                                index++
                            }
                        }
                    }
                }
            }
            iv_image_template.updateSavedBitmap()
            canvasViewListToAdapterData()
        }
    }

    fun canSent() {
        runOnUiThread {
            if (!canSendClick) return@runOnUiThread
            dialogDismiss()
            tv_device_no_template.visibility = View.GONE
            iv_image_template.visibility = View.VISIBLE
            btn_update_template.visibility = View.VISIBLE
            btn_save_edit.background =
                ContextCompat.getDrawable(this, R.drawable.btn_radius)
            btn_save_edit.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.white
                )
            )
            btn_save_edit.isEnabled = true
        }
    }

    private fun getSendData() {
        if (timeout != null) {
            (timeout ?: return).dispose()
        }
        runOnUiThread {
            tv_connect_status.text =
                getString(R.string.nameplate_is_connected)
            canSendClick = true
            connectView()
            btn_update_template.visibility = View.VISIBLE
        }
        if (templateId.isEmpty()) {
            showNUllMode()
            return
        }
        val templateBean: java.util.ArrayList<TemplateBean> =
            instance.getQueryByWhere(
                TemplateBean::class.java, "templateId",
                templateId
            )
        if (templateBean.size == 0) {
            //添加设备
            showNUllMode()
        } else {
            LogHelper.i("xmlstring", templateBean[0].templateXmlString)
            iv_image_template.loadFileDatXml(templateBean[0].templateXmlString, true)
            iv_image_template.setOnTouchListener { v, event ->
                true
            }
            canSent()
            val inputList =
                Gson().fromJson<List<BleTemplateBase>>(
                    templateBean[0].inputDataJson,
                    object : TypeToken<List<BleTemplateBase?>>() {}.type
                )
            if (templateBean[0].inputDataJson.isNotEmpty()) {
                runOnUiThread {
                    inputDataRefreshView(inputList)
                }
            }
        }
    }

    //刚进入界面时给输入框的的数据赋值
    private fun inputDataRefreshView(inputList: List<BleTemplateBase>) {
        val pelList = CanvasView.getPelList()
        var index = 0
        for (modeItem in 0 until pelList.size) {
            LogHelper.i("witstec", "inputList=" + inputList[index])
            if (pelList[modeItem].type == inputList[index].type.toInt()) {
                when (inputList[index].type.toInt()) {
                    31 -> {
                        val pell = pelList[modeItem]
                        pell.text.content = inputList[index].text
                        pelList[modeItem] = pell
                        if (inputList.size - 1 > index) {
                            index++
                        }
                    }
                    32 -> {
                        val pell = pelList[modeItem]
                        pell.text.content = inputList[index].text
                        pelList[modeItem] = pell
                        if (inputList.size - 1 > index) {
                            index++
                        }
                    }
                    20 -> {
                        val pell = pelList[modeItem]
                        pell.text.content = inputList[index].text
                        pelList[modeItem] = pell
                        if (inputList.size - 1 > index) {
                            index++
                        }
                    }
                    30 -> {
                        val pell = pelList[modeItem]
                        val picture = Picture(AppUtils.base64ToBitmap(inputList[index].image))
                        pell.picture = picture
                        pell.image_effect_size = inputList[index].imageEffectSize
                        pelList[modeItem] = pell
                        if (inputList.size - 1 > index) {
                            index++
                        }
                    }
                }
            }
        }
        iv_image_template.updateSavedBitmap()
        canvasViewListToAdapterData()
    }

    override fun onStop() {
        super.onStop()
        if (timeout != null) {
            timeout!!.dispose()
        }
    }

    fun timeOut() {
        if (!isConfirmSend) {
            return
        }
        if (mIndex == 0) {
            timeout = Observable.timer(1000 * 18, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mIndex = 0
                    isConfirmSend = false
                    isCloseLoop = false
                    if (timeout != null) {
                        (timeout ?: return@subscribe).dispose()
                    }
                    ToastUtils.show(getString(R.string.connext_Timeout_retry))
                    dialogDismiss()
                }
            addDisposable(timeout!!)
        }
    }

    fun pxToDp(px: Float): Float {
        val dp = px2dp(this, px)
        i("witstec", "canvas dp=$dp")
        return dp.toFloat()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //下载的xml处理
        if (resultCode == FilterTemplateActivity.EXTRO_GROUP_SELECT_CODE) {
            isUpdateModeAdapter = false
            var templateXmlString = data!!.getStringExtra("templateXmlString")
            val isLocal = data.getBooleanExtra("is_local", false)
            templateId = data.getStringExtra("template_id").toString()
            if (templateXmlString != null) {
                canSent()
                if (templateId.isNotEmpty()) {
                    if (isLocal) {
                        val templateBeans = instance.getQueryByWhere(
                            TemplateBean::class.java,
                            "templateId",
                            templateId
                        )
                        templateXmlString = templateBeans[0].templateXmlString
                    }
                    iv_image_template.loadFileDatXml(templateXmlString, isLocal)
                    tv_device_no_template?.visibility = View.GONE
                    iv_image_template.visibility = View.VISIBLE
                    btn_save_edit?.visibility = View.VISIBLE
                    tv_hint?.text = getString(R.string.input_need_update_content)
                    btn_update_template?.text = getString(R.string.update_template)
                    canvasViewListToAdapterData()
                    addDisposable(Observable.timer(500, TimeUnit.MILLISECONDS).subscribe {
                        isUpdateModeAdapter = true
                    })
                } else {
                    showNUllMode()
                }
            }
        }
        //图片返回处理
        if (requestCode == Constant.REQUEST_CODE_CHOOSE) {
            try {
                if (data == null) {
                    return
                }
                //获取选择器返回的数据
                //获取选择器返回的数据
                val selected = Matisse.obtainPathResult(data)!!
                val path = Luban.with(this)
                    .load(selected[0])
                    .ignoreBy(50).get()[0]
                if (path.path.isEmpty()) {
                    return
                }
                val viewcop =
                    View.inflate(this, R.layout.activity_crop, null)
                val cropImageView: CropImageView = viewcop.findViewById(R.id.cropImageView)
                MaterialDialog.Builder(this)
                    .positiveText(getString(R.string.next))
                    .negativeText(getString(R.string.cancel))
                    .title(getString(R.string.corp))
                    .customView(viewcop, true)
                    .cancelable(false)
                    .onPositive { dialog: MaterialDialog, which: DialogAction? ->
                        val cBitmap = cropImageView.croppedImage
                        madapter!!.data[image_type_position].imageEffectSize =
                            image_effect_size
                        madapter!!.data[image_type_position].image =
                            AppUtils.bitmapToBase64Tep(cBitmap)
                        refreshView(madapter!!.data)
                        canSent()

//                        val view = View.inflate(this, R.layout.layout_image_effect_drawing, null)
//                        val imageView = view.findViewById<ImageView>(R.id.iv_drawing_effect)
//                        val seekBar = view.findViewById<SeekBar>(R.id.seekBar_drawing_effect)
//                        seekBar.progress = image_effect_size
//                        val whs = AppUtils.bitmapShrink(cBitmap.getWidth(), cBitmap.getHeight())
//                        i(
//                            "witstec", "bitmapEffect=" +
//                                    cBitmap.getWidth() + "," + cBitmap.getHeight()
//                        )
//                        i("witstec", whs[0].toString() + "," + whs[1])
//                        val newBitmap = AppUtils.zoomImg(cBitmap, whs[0], whs[1])
//                        bitmapBleImage = convertGreyImgByFloyd(newBitmap, image_effect_size)
//                        imageView.setImageBitmap(bitmapBleImage)
//                        seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
//                            override fun onProgressChanged(
//                                seekBar: SeekBar,
//                                progress: Int,
//                                fromUser: Boolean
//                            ) {
//
//                            }
//
//                            override fun onStartTrackingTouch(seekBar: SeekBar) {}
//                            override fun onStopTrackingTouch(seekBar: SeekBar) {
//                                image_effect_size = (seekBar.progress * 0.1).toInt()
//                                bitmapBleImage = convertGreyImgByFloyd(
//                                    newBitmap,
//                                    image_effect_size
//                                )
//                                imageView.setImageBitmap(bitmapBleImage)
//                            }
//                        })
//                        MaterialDialog.Builder(this)
//                            .customView(view, true)
//                            .positiveText(getString(R.string.fixed))
//                            .negativeText(getString(R.string.cancel))
//                            .onPositive { dialog, which ->
//                                madapter!!.data[image_type_position].imageEffectSize =
//                                    image_effect_size
//                                madapter!!.data[image_type_position].image =
//                                    AppUtils.bitmapToBase64Tep(bitmapBleImage)
//                                refreshView(madapter!!.data)
//                                canSent()
//                            }
//                            .onNegative { dialog, which -> dialog.dismiss() }
//                            .show()
                    }
                    .showListener { dialogInterface: DialogInterface? ->
                        var bitmap: Bitmap? = null
                        try {
                            bitmap = BitmapFactory.decodeStream(
                                FileInputStream(path)
                            )
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }
                        cropImageView.setImageBitmap(bitmap)
                    }
                    .onNegative { dialog: MaterialDialog, which: DialogAction? -> dialog.dismiss() }
                    .show()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    //修改输入框列表数据
    private fun canvasViewListToAdapterData() {
        dataList.clear()
        val pelList = CanvasView.getPelList()
        pelList.forEach {
            if (it.type == 31) {
                if (it.text != null) {
                    dataList.add(InputBoxBean(it.type, it.text.content, InputBoxBean.TYPE_BARCODE))
                }
            } else if (it.type == 20) {
                if (it.text != null) {
                    dataList.add(InputBoxBean(it.type, it.text.content, InputBoxBean.TYPE_TEXT))
                }
            } else if (it.type == 32) {
                if (it.text != null) {
                    dataList.add(InputBoxBean(it.type, it.text.content, InputBoxBean.TYPE_QRCODE))
                }
            } else if (it.type == 30) {
                if (it.picture != null) {
                    val inputBean = InputBoxBean(
                        it.type, AppUtils.bitmapToBase64Tep(it.picture.createContent()),
                        it.image_effect_size, InputBoxBean.TYPE_IMAGE
                    )
                    dataList.add(inputBean)
                }
            }
        }
        madapter!!.replaceData(dataList)
    }

    fun showNUllMode() {
        isConfirmSend = false
        i("downFile", "没有模板")
        runOnUiThread {

            dialogDismiss()
            root_view_mode.visibility = View.VISIBLE
            tv_device_no_template.visibility = View.VISIBLE
            iv_image_template.visibility = View.GONE
            tv_hint.text = getString(R.string.null_mode)
            btn_update_template.text = getString(R.string.select_mode)
            btn_save_edit.background =
                ContextCompat.getDrawable(this, R.drawable.div_sms_code_bg_gray)
            btn_save_edit.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.black_sub
                )
            )
            btn_save_edit.isEnabled = false
            if (madapter != null) {
                if (madapter!!.data.size != 0) {
                    madapter!!.data.clear()
                    madapter!!.notifyDataSetChanged()
                }
            }
        }
    }


    private val connectCallback: BleConnectCallback<BleRssiDevice> =
        object :
            BleConnectCallback<BleRssiDevice>() {
            override fun onConnectionChanged(device: BleRssiDevice?) {
                Log.e(
                    TAG,
                    "onConnectionChanged: " + device!!.connectionState
                )
                tv_connect_status.post {
                    if (device.isConnected) {
                        tv_connect_status.text = getString(R.string.add_device_loading_one)
                        connectHint()
                    } else if (device.isConnecting) {
                        tv_connect_status.setText(getString(R.string.reconnecting_the_base))
                        connectHint()
                    }
                }
            }

            override fun onConnectFailed(
                device: BleRssiDevice?,
                errorCode: Int
            ) {
                super.onConnectFailed(device, errorCode)
                tv_connect_status.post {
                    tv_connect_status.text = getString(R.string.The_base_cannot_be_connected)
                    connectNull()
                }
            }

            override fun onConnectCancel(device: BleRssiDevice) {
                super.onConnectCancel(device)
                Log.e(
                    TAG,
                    "onConnectCancel: " + device.bleName
                )
            }

            override fun onServicesDiscovered(
                device: BleRssiDevice?,
                gatt: BluetoothGatt
            ) {
                super.onServicesDiscovered(device, gatt)
                gattServices?.addAll(gatt.services)
                tv_connect_status.post {
                    tv_connect_status.setText(getString(R.string.Get_nameplate_connection_status))
                    connectHint()
                    if (timeout != null) {
                        (timeout ?: return@post).dispose()
                    }
                    timeout = Observable.timer(
                        1000 * 8, TimeUnit.MILLISECONDS
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            tv_connect_status.text =
                                getString(R.string.No_nameplate_inspected)
                            connectNull()
                        }
                    addDisposable(timeout ?: return@post)
                }
            }

            override fun onReady(device: BleRssiDevice?) {
                super.onReady(device)
                if (gattServices?.size == 0) return
                SystemClock.sleep(150)
                getCardVer()
            }
        }

    //接受通知
    fun getCardVer() {
        val characteristic =
            gattServices?.get(0)?.characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices =
            Ble.getInstance<BleRssiDevice>().connectedDevices
        //连接成功后，设置通知
        if (connetedDevices.size == 0) return
        SystemClock.sleep(200)
        Ble.getInstance<BleRssiDevice>().enableNotifyByUuid(
            connetedDevices[0],
            true,
            suid,
            cuid,
            object : BleNotifyCallback<BleRssiDevice>() {
                override fun onChanged(
                    device: BleRssiDevice,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    val byteDatas =
                        cn.com.heaton.blelibrary.ble.utils.ByteUtils.toHexString(
                            characteristic.value
                        )
                    BleLog.e(TAG, "onChanged==data:$byteDatas")
                    val returnBytes =
                        ByteUtils.byteArrayToHexString(characteristic!!.value).split(",")
                    val byteValue = characteristic!!.value
                    val commandStr = (returnBytes[5] + returnBytes[6])
                    when (commandStr) {
                        "0010" -> {
                            tv_connect_status.post {
                                if (returnBytes[8] == "01") {
                                    if (returnBytes.size > 17) {
                                        val model =
                                            returnBytes[9] + returnBytes[10] + returnBytes[11] + returnBytes[12] + returnBytes[13] + returnBytes[14] + returnBytes[15]

                                        val version =
                                            ("" + byteValue[16].toString() + "." + byteValue[17].toString()).replace(
                                                "-",
                                                ""
                                            )
                                        LogHelper.i(
                                            "witstec", "型号=" + ByteUtils.hexStr2Str(
                                                model
                                            )
                                        )
                                        LogHelper.i(
                                            "witstec",
                                            "版本=" + ("" + byteValue[16].toString() + "" + byteValue[17].toString()).replace(
                                                "-",
                                                ""
                                            )
                                        )
                                        bleType = ByteUtils.hexStr2Str(
                                            model
                                        )
                                        mCardVersion = version
                                        isCardExit = true
                                    }
                                    getSendData()
                                } else {
                                    isCardExit = false
                                    tv_connect_status.text =
                                        getString(R.string.No_nameplate_inspected)
                                    connectNull()
                                }
                                readVer()
                            }
                        }
                    }
                }

                override fun onNotifySuccess(device: BleRssiDevice) {
                    super.onNotifySuccess(device)
                    BleLog.e(
                        TAG,
                        "onNotifySuccess: " + device.bleName
                    )
                    SystemClock.sleep(100)
                    getCarMsg()
                }
            })
    }

    fun getCarMsg() {
        val characteristic =
            gattServices?.get(0)?.characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices =
            Ble.getInstance<BleRssiDevice>().connectedDevices
        val byteListChange =
            BitmapBleBmpManagement.getBmpProtocolDataChange(22)
        if (connetedDevices.size == 0) return
        LogHelper.i("witstec", "开始主动获取铭牌信息回复")
        Ble.getInstance<BleRssiDevice>().writeByUuid(
            connetedDevices[0],
            byteListChange,
            suid,
            cuid,
            object :
                BleWriteCallback<BleRssiDevice?>() {
                override fun onWriteSuccessCharacteristicChanged(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    LogHelper.i(
                        "writeByUuid",
                        "主动获取铭牌信息回复 onWriteSuccess value=" +
                                ByteUtils.byteArrayToHexString(
                                    characteristic.value
                                )
                    )
                    val returnBytes =
                        ByteUtils.byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    val commandStr =
                        (returnBytes[5] + returnBytes[6])
                    if (commandStr == "0016") {
                        var tyteId = ""
                        if (returnBytes[8] == "00") {
                            val strbug = StringBuffer()
                            for (i in returnBytes.indices) {
                                if (i > 10 && i < returnBytes.size - 1) {
                                    strbug.append(
                                        returnBytes[i]
                                    )
                                }
                            }
                            tyteId =
                                ByteUtils.hexStr2Str(strbug.toString())
                            LogHelper.i(
                                "witstec",
                                "模板id=" + tyteId
                            )
                            LogHelper.i(
                                "witstec",
                                "模板id=" + strbug.toString()
                            )
                            LogHelper.i("witstec", "获取铭牌数据成功")
                            templateId = tyteId
                            isCardExit = true
                            getSendData()
                        } else {
                            isCardExit = false
                            showNUllMode()
                        }
                    }
                }

                override fun onWriteSuccess(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    LogHelper.i(
                        "writeByUuid",
                        "主动获取铭牌信息写入回掉 onWriteSuccess value=" +
                                ByteUtils.byteArrayToHexString(
                                    characteristic.value
                                )
                    )
                }
            })
    }

    fun readVer() {
        val characteristic2 = gattServices?.get(0)?.characteristics?.get(2)
        val connetedDevices2 = Ble.getInstance<BleRssiDevice>().connectedDevices
        val serviceUuid2: UUID? = characteristic2?.getService()?.getUuid()
        val characteristicUuid2: UUID? = characteristic2?.getUuid()
        if (connetedDevices2.size == 0) return
        Ble.getInstance<BleRssiDevice>()
            .readByUuid(
                connetedDevices2[0],
                serviceUuid2,
                characteristicUuid2,
                object :
                    BleReadCallback<BleRssiDevice?>() {
                    @SuppressLint("SyntheticAccessor")
                    override fun onReadSuccess(
                        device: BleRssiDevice?,
                        characteristic: BluetoothGattCharacteristic
                    ) {
                        super.onReadSuccess(device, characteristic)
                        if (TextUtils.isEmpty(characteristic.uuid.toString())) return
                        LogHelper.i(
                            "readDesByUuid",
                            "onDescReadSuccess value=" +
                                    ByteUtils.byteArrayToHexString(
                                        characteristic.value
                                    )
                        )
                        val byteValue = characteristic.value
                        mBaseVersion =
                            ("" + byteValue[0].toString() + "." + byteValue[1].toString()).replace(
                                "-",
                                ""
                            )
                        LogHelper.i(
                            "sendImage",
                            "底座固件版本号 value=" + mBaseVersion
                        )
                        runOnUiThread {
                            if (isShowIng) return@runOnUiThread
                            addDisposable(
                                ApiRepository.otaCheck(
                                    this@DeviceManageActivity,
                                    "WTCard"
                                )
                                    .subscribe({ data ->
                                        val dataItem =
                                            Gson().fromJson<OTADataMsg>(
                                                data.string(),
                                                OTADataMsg::class.java
                                            )
                                        val newVersion = dataItem.version
                                        val isUpgrade =
                                            AppUtils.isUpgrade(
                                                mBaseVersion,
                                                newVersion
                                            )
                                        if (isUpgrade) {
                                            if (dataItem.url.isNotEmpty()) {
                                                isShowIng = true
                                                IosDialog(this@DeviceManageActivity).init()
                                                    .setTitle(getString(R.string.New_firmware_detected))
                                                    .setMsg(getString(R.string.New_firmware_detected_go))
                                                    .setPositiveButton(
                                                        getString(R.string.fixed),
                                                        View.OnClickListener {
                                                            isShowIng = false
                                                            DeviceManageMoreActivity.start(
                                                                this@DeviceManageActivity,
                                                                mDeviceAddress,
                                                                bleType,
                                                                mBaseVersion,
                                                                mCardVersion
                                                            )
                                                        })
                                                    .setNegativeButton(
                                                        getString(R.string.cancel),
                                                        View.OnClickListener {
                                                            isShowIng = false
                                                        }).show()
                                            }
                                        }
                                    }, {
                                        dialogDismiss()
                                    })
                            )
                            if (!isShowIng) {
                                if (isShowIng) return@runOnUiThread
                                addDisposable(
                                    ApiRepository.otaCheck(
                                        this@DeviceManageActivity,
                                        "wtc021a"
                                    )
                                        .subscribe({ data ->
                                            val dataItem =
                                                Gson().fromJson<OTADataMsg>(
                                                    data.string(),
                                                    OTADataMsg::class.java
                                                )
                                            val newVersion = dataItem.version
                                            val isUpgrade = AppUtils.isUpgrade(
                                                mCardVersion,
                                                newVersion
                                            )
                                            if (isUpgrade) {
                                                if (dataItem.url.isNotEmpty()) {
                                                    isShowIng = true
                                                    try {
                                                        IosDialog(this@DeviceManageActivity).init()
                                                            .setTitle(getString(R.string.New_firmware_detected))
                                                            .setMsg(getString(R.string.New_firmware_detected_go))
                                                            .setPositiveButton(
                                                                getString(R.string.fixed),
                                                                View.OnClickListener {
                                                                    isShowIng = false
                                                                    DeviceManageMoreActivity.start(
                                                                        this@DeviceManageActivity,
                                                                        mDeviceAddress,
                                                                        bleType,
                                                                        mBaseVersion,
                                                                        mCardVersion
                                                                    )
                                                                })
                                                            .setNegativeButton(
                                                                getString(R.string.cancel),
                                                                View.OnClickListener {
                                                                    isShowIng = false
                                                                }).show()
                                                    } catch (e: IOException) {
                                                        e.printStackTrace()
                                                    }
                                                }
                                            }
                                        }, {
                                            dialogDismiss()
                                        })
                                )
                            }
                        }
                    }

                    override fun onReadFailed(
                        device: BleRssiDevice?,
                        failedCode: Int
                    ) {
                        super.onReadFailed(device, failedCode)
                        tv_hint.post {
                            tv_hint.setText("读取描述失败:$failedCode")
                        }
                        SystemClock.sleep(100)
                        getCardVer()
                    }
                })
    }

    fun connectNull() {
        canSendClick = false
        runOnUiThread {
            dialogDismiss()
            btn_save_edit.background =
                ContextCompat.getDrawable(this, R.drawable.div_sms_code_bg_gray)
            btn_save_edit.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.black_sub
                )
            )
            btn_save_edit.isEnabled = false
            tv_connect_status.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
            tv_device_no_template.visibility = View.VISIBLE
            iv_image_template.visibility = View.GONE
            btn_update_template.visibility = View.GONE
            CanvasView.getPelList().clear()
            iv_image_template.updateSavedBitmap()
            madapter!!.data.clear()
            madapter!!.notifyDataSetChanged()
        }
    }

    fun connectHint() {
        canSendClick = false
        runOnUiThread {
            dialogDismiss()
            btn_update_template.visibility = View.GONE
            btn_save_edit.background =
                ContextCompat.getDrawable(this, R.drawable.div_sms_code_bg_gray)
            btn_save_edit.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.black_sub
                )
            )
            btn_save_edit.isEnabled = false
            tv_connect_status.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.orange
                )
            )
        }
    }

    fun connectView() {
        runOnUiThread {
            dialogDismiss()
            tv_connect_status.setBackgroundColor(
                ContextCompat.getColor(
                    this@DeviceManageActivity,
                    R.color.colorPrimary
                )
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timeout != null) {
            timeout!!.dispose()
        }
    }

    @SuppressLint("CheckResult")
    override fun onBackPressed() {
        disconnection()
        if (timeout != null) {
            timeout!!.dispose()
        }
        runOnUiThread {
            connectHint()
            tv_connect_status.text = getString(R.string.Disconnecting_from_the_base_in)
        }
        showDialog(getString(R.string.Disconnecting_from_the_base_in))
        LogHelper.i(TAG, "退出")
        timeout = Observable.timer(
            1600, TimeUnit.MILLISECONDS
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                dialogDismiss()
                super.onBackPressed()
            }
        addDisposable(timeout!!)
    }

    fun disconnection() {
        Ble.options().isAutoConnect = false
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.size != 0) {
            val bleDevice = connetedDevices[0]
            Ble.getInstance<BleRssiDevice>().disconnect(bleDevice)
        }
    }
}
