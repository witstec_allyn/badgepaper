package com.witstec.sz.badgeepaper.ui.freagment.local

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nukc.stateview.StateView
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.TemplateBean
import com.witstec.sz.badgeepaper.model.bean.TemplateOfficialBean
import com.witstec.sz.badgeepaper.model.db.LocalDataSource
import com.witstec.sz.badgeepaper.model.event.ChangeTemplateListEvent
import com.witstec.sz.badgeepaper.ui.activity.device.FilterTemplateActivity
import com.witstec.sz.badgeepaper.ui.activity.device.FilterTemplateActivity.Companion.EXTRO_GROUP_SELECT_CODE
import com.witstec.sz.badgeepaper.ui.adapter.FeliOfficialTemplateAdapter
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.utils.RxBus
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.view.IosDialog
import com.witstec.sz.badgeepaper.view.SearchRelativeLayout
import com.witstec.sz.badgeepaper.view.SimpleDividerDecoration
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_local.*
import kotlinx.android.synthetic.main.header_view_search_layout.view.*
import java.util.*

class OfficialLocalFragment : BaseFragment() {

    private var statView: StateView? = null
    private var mAdapter: FeliOfficialTemplateAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_official_local, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tv_filter.text = getString(R.string.filter) + ":" + AppUtils.typeIdToTypeName(
            FilterTemplateActivity.BLE_TYPE
        )
        mAdapter = FeliOfficialTemplateAdapter()
        mAdapter!!.setEnableLoadMore(false)
        statView = StateView.inject(recycler)
        statView!!.showLoading()
        recycler.adapter = mAdapter
        recycler.layoutManager = LinearLayoutManager(activity!!)
        recycler.addItemDecoration(SimpleDividerDecoration(activity!!))

        getBleData()

        mAdapter!!.setOnItemClickListener { adapter, view, position ->
            SystemClock.sleep(280)
            val templateBean = mAdapter!!.getItem(position) as TemplateOfficialBean
            val intent = Intent()
            intent.putExtra("templateXmlString", templateBean.templateFileUrl)
            intent.putExtra("template_id", templateBean.templateId)
            intent.putExtra("is_local", false)
            activity!!.setResult(EXTRO_GROUP_SELECT_CODE, intent)
            activity!!.finish()
        }
        mAdapter!!.setOnItemLongClickListener { adapter, view, position ->
            IosDialog(activity!!).init()
                .setTitle(getString(R.string.template_selection_delete))
                .setMsg(getString(R.string.do_you_want_template_selection_delete))
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                    LocalDataSource.instance.liteOrm.delete(mAdapter!!.getItem(position))
                    mAdapter!!.remove(position)
                    ToastUtils.show(getString(R.string.delete_ok))
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {
                })
                .show()
            true
        }

        addDisposable(
            RxBus.register(ChangeTemplateListEvent::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    getBleData()
                })
    }

    private fun getBleData() {
        val templateBean: ArrayList<TemplateOfficialBean> =
            LocalDataSource.instance.getQueryByWhere(
                TemplateOfficialBean::class.java, "templateType",
                FilterTemplateActivity.BLE_TYPE
            )
        if (templateBean.size == 0) {
            //添加设备
            recycler.visibility = View.GONE
            statView!!.showContent()
            statView!!.showEmpty()
        } else {
            statView!!.showContent()
            recycler.visibility = View.VISIBLE
            mAdapter!!.replaceData(templateBean.reversed())
            if (mAdapter!!.data.size != 0) {
                if (mAdapter!!.headerLayoutCount == 0) {
                    val view =
                        View.inflate(activity, R.layout.header_view_search_layout, null)
                    view.searchLayout.setTextChangedListener(object :
                        SearchRelativeLayout.TextChangedListener {
                        override fun onTextChanged(text: String) {
                            if (text.isNotEmpty() || text != "") {
                                mAdapter!!.searchKeyword = text
                                //筛选
                                val templateBean: ArrayList<TemplateOfficialBean> =
                                    LocalDataSource.instance.getQueryByWhere(
                                        TemplateOfficialBean::class.java,
                                        "templateName",
                                        text
                                    )
                                mAdapter!!.replaceData(templateBean)
                            } else {
                                mAdapter!!.searchKeyword = ""
                                getBleData()
                            }
                        }
                    })
                    mAdapter!!.addHeaderView(view)
                }
            } else {
                statView!!.showEmpty()
            }
        }
    }

}
