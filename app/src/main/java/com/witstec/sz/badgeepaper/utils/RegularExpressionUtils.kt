package com.witstec.sz.badgeepaper.utils

import java.util.regex.Pattern

/**
 * author : Allyn
 * e-mail : 1839565349@qq.com
 * date   : 2019/4/2310:42
 * desc   :
 */
object RegularExpressionUtils {

    fun isPhone(phone: String): Boolean {
        val regex = "^(((13[0-9]{1})|(14[57]{1})|(15[^4]{1})|(18[0-9]{1})|(17[^12459]{1}))+\\d{8})?$"//正则匹配电话号码
        return Pattern.matches(regex, phone)
    }

    fun isEmail(email: String): Boolean {
        val regex = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$"//正则匹配邮箱
        return Pattern.matches(regex, email)
    }

    fun isIp(email: String): Boolean {
        val regex = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)"//正则匹配ip
        return Pattern.matches(regex, email)
    }

    fun isPassword(password: String): Boolean {
        val regex = "^[A-Za-z][A-Za-z0-9]{5,17}$"//正则匹配密码
        return Pattern.matches(regex, password)
    }

}
