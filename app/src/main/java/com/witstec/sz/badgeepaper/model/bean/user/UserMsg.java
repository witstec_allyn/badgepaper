package com.witstec.sz.badgeepaper.model.bean.user;

public class UserMsg {

    /**
     * id : string
     * nickname : string
     * email : string
     * phone : string
     * phone_zone : string
     * lang : en
     * create_date : int(10)
     * avatar : string
     * country : string
     * province : string
     * city : string
     * gender : string
     * age : 18
     * birthday : string
     * remark : string
     */

    private String id;
    private String nickname;
    private String email;
    private String phone;
    private String phone_zone;
    private String lang;
    private String create_date;
    private String avatar;
    private String country;
    private String province;
    private String city;
    private String gender;
    private int age;
    private String birthday;
    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone_zone() {
        return phone_zone;
    }

    public void setPhone_zone(String phone_zone) {
        this.phone_zone = phone_zone;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
