package com.witstec.sz.badgeepaper.ui.freagment


import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.meituan.android.walle.WalleChannelReader
import com.witstec.sz.badgeepaper.*
import com.witstec.sz.badgeepaper.model.bean.CheckAPP
import com.witstec.sz.badgeepaper.model.event.ChangeTemplateListEvent
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.services.UpdateIntentService
import com.witstec.sz.badgeepaper.ui.activity.setting.ContactActivity
import com.witstec.sz.badgeepaper.ui.activity.setting.HelpActivity
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.RxBus
import com.witstec.sz.badgeepaper.utils.StringUtils
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.utils.app.StorageHelper
import com.witstec.sz.badgeepaper.view.IosDialog
import com.yanzhenjie.permission.AndPermission
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_user.*
import q.rorbin.badgeview.Badge
import q.rorbin.badgeview.QBadgeView


class UserFragment : BaseFragment() {


    val counts: Int = 5
    val duration: Long = 3 * 1000
    val mHits = LongArray(counts)
    var isClick = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tv_version.text = "V" + BuildConfig.VERSION_NAME
        item_contact_us.setOnClickListener {
            myStartActivity(
                activity!!,
                ContactActivity::class.java
            )
        }

        item_User_Agreement.setOnClickListener {
            HelpActivity.start(
                activity!!,
                if (AppUtils.getLocaleLanguage() == "cn") "file:///android_asset/service_agreement_zh.html"
                else "file:///android_asset/service_agreement_en.html",
                getString(R.string.Service_Agreement)
            )
        }

        privacy_Policy.setOnClickListener {
            HelpActivity.start(
                activity!!,
                if (AppUtils.getLocaleLanguage() == "cn") "file:///android_asset/privacy_zh.html"
                else "file:///android_asset/privacy_en.html",
                getString(R.string.Service_Agreement)
            )
        }

        check_App.setOnClickListener {
            isClick = true
            checkAppUpdate()
        }

        start_admin?.setOnClickListener {
            System.arraycopy(mHits, 1, mHits, 0, mHits.size - 1)
            mHits[mHits.size - 1] = SystemClock.uptimeMillis()
            if (mHits[0] >= (SystemClock.uptimeMillis() - duration)) {
                IosDialog(activity!!).init()
                    .setTitle("秘钥")
                    .setEditTextContext(
                        getStringData(Constants.ADMIN_KEY),
                        "请输入秘钥"
                    )
                    .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                    }, object : IosDialog.EditOnClickListener {
                        override fun editContext(context: EditText) {
                            val key = context.text.toString()
                            if (key.equals("VdQ6DtB2g1YFk4lE")) {
                                saveStringData(Constants.ADMIN_KEY, key)
                                RxBus.post(ChangeTemplateListEvent())
                                dialogDismiss()
                            } else {
                                dialogDismiss()
                            }
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), View.OnClickListener {

                    })
                    .show()
            }
        }

        about_text?.setOnClickListener {
            System.arraycopy(mHits, 1, mHits, 0, mHits.size - 1)
            mHits[mHits.size - 1] = SystemClock.uptimeMillis()
            if (mHits[0] >= (SystemClock.uptimeMillis() - duration)) {
                saveStringData(Constants.ADMIN_KEY, "")
                RxBus.post(ChangeTemplateListEvent())
            }
        }
        checkAppUpdate()
    }

    @SuppressLint("SetTextI18n", "WrongConstant")
    fun checkAppUpdate() {
        addDisposable(
            ApiRepository.appCheck(activity)
                .subscribe({ appStr ->
                    val version = appStr.v.ver
                    LogHelper.i("checkapp", "" + version)
                    if (version > BuildConfig.VERSION_CODE) {
                        check_App.rightTextView!!.text = getString(R.string.new_version)
                        check_App.rightTextView!!.setTextColor(
                            ContextCompat.getColor(
                                activity!!,
                                R.color.red
                            )
                        )
                        addBadgeAt(3, 1)
                        val isNowDay = StringUtils.IsToday(StorageHelper.getTheSameDay())
                        if (!isNowDay || isClick) {
                            isClick = false
                            val channel =
                                WalleChannelReader.getChannel(activity!!.applicationContext)
                            when (channel) {
                                "google" -> {
                                    MaterialDialog.Builder(activity!!)
                                        .title(getString(R.string.latest_version) + appStr.v.name)
                                        .content(appStr.v.desc.replace("，", "\n"))
                                        .contentColor(Color.BLACK)
                                        .positiveText(R.string.google_play_download)
                                        .onPositive { dialog, which ->
                                            dialog.dismiss()
                                            val intent = Intent()
                                            intent.action = "android.intent.action.VIEW"
                                            intent.data =
                                                Uri.parse("https://play.google.com/store/apps/details?id=com.witstec.badgeepaper")
                                            startActivity(intent)
                                        }
                                        .negativeText(R.string.not_update)
                                        .dismissListener {
                                            StorageHelper.saveTheSameDay()
                                        }
                                        .show()
                                }
                                else -> {
                                    MaterialDialog.Builder(activity!!)
                                        .title(getString(R.string.latest_version) + appStr.v.name)
                                        .content(appStr.v.desc.replace("，", "\n"))
                                        .contentColor(Color.BLACK)
                                        .positiveText(R.string.update_now)
                                        .onPositive { dialog, which ->
                                            dialog.dismiss()
                                            AndPermission.with(this)
                                                .runtime()
                                                .permission(
                                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                    Manifest.permission.READ_EXTERNAL_STORAGE
                                                )
                                                .onGranted {
                                                    val updateIntent =
                                                        Intent(
                                                            activity!!,
                                                            UpdateIntentService::class.java
                                                        )
                                                    updateIntent.action =
                                                        UpdateIntentService.ACTION_UPDATE
                                                    updateIntent.putExtra(
                                                        "appName",
                                                        "WtCard.apk"
                                                    )
                                                    updateIntent.putExtra(
                                                        "downUrl", Constants.APK_URL + appStr.v.url
                                                    )
//                                    updateIntent.putExtra(
//                                        "downUrl", "http://d5.kunyouxi.cn/down/xhdvr_7_5.apk"
//                                    )
                                                    activity!!.startService(updateIntent)
                                                }
                                                .onDenied {
                                                    val packageURI =
                                                        Uri.parse("package:" + activity!!.packageName)
                                                    val intent = Intent(
                                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                        packageURI
                                                    )
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                                    startActivity(intent)
                                                    Toast.makeText(
                                                        activity!!,
                                                        getString(R.string.no_permissions),
                                                        Toast.LENGTH_LONG
                                                    )
                                                        .show()
                                                }.start()
                                        }
                                        .onNegative { dialog, which ->
                                            StorageHelper.saveTheSameDay()
                                        }
                                        .negativeText(R.string.not_update)
                                        .show()
                                }
                            }
                        }
                    } else {
                        if (isClick) {
                            ToastUtils.show(getString(R.string.Is_latest_version))
                        }
                    }
                }, {
//                    ToastUtils.show(it.message)
                })
        )
    }

    private fun addBadgeAt(position: Int, number: Int): Badge {
        return QBadgeView(activity!!)
            .setBadgeNumber(number)
            .setGravityOffset(28F, 0F, true)
            .bindTarget(activity!!.bottom_Navigation.getBottomNavigationItemView(position))
            .setOnDragStateChangedListener { dragState, _, _ ->
                if (Badge.OnDragStateChangedListener.STATE_SUCCEED === dragState) {
                    checkAppUpdate()
                }
            }
    }

}
