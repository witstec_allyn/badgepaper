package com.witstec.sz.badgeepaper.ui.adapter

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.TemplateTypeBean
import java.util.*

class SelectTemplateTypeAdapter : BaseQuickAdapter<TemplateTypeBean, BaseViewHolder>(R.layout.item_template_type_select, ArrayList()) {

    override fun convert(helper: BaseViewHolder, item: TemplateTypeBean) {
       val textView= (helper.getView<View>(R.id.tv_type_name) as TextView)
        textView.text = item.name
        if (item.isSelect){
            textView.setTextColor(ContextCompat.getColor(helper.itemView.context,R.color.white))
            textView.setBackgroundResource(R.drawable.btn_radius)
        }else{
            textView.setBackgroundResource(R.drawable.div_green_bg)
            textView.setTextColor(ContextCompat.getColor(helper.itemView.context,R.color.black_sub))
        }
    }

}
