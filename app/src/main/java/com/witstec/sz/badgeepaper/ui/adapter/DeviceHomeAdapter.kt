package com.witstec.sz.badgeepaper.ui.adapter

import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.DeviceBean
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import java.util.*

class DeviceHomeAdapter :
    BaseQuickAdapter<DeviceBean, BaseViewHolder>(R.layout.item_device_msg, ArrayList()) {

    var searchKeyword: String? = null
    lateinit var click: OnItemClick

    override fun convert(helper: BaseViewHolder, item: DeviceBean) {
        (helper.getView<View>(R.id.tv_device_name) as TextView).text = item.deviceName
        (helper.getView<View>(R.id.tv_epd_id) as TextView).text = item.mac
        if (item.isOnline) {
            (helper.getView<View>(R.id.tv_onile_state) as TextView).text =
                mContext.getString(R.string.ble_state_online)
            (helper.getView<View>(R.id.tv_onile_state) as TextView).setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.green
                )
            )
            (helper.getView<View>(R.id.tv_signal) as TextView).text =
                mContext.getString(R.string.Signal_strength) + item.signal
        } else {
            (helper.getView<View>(R.id.tv_onile_state) as TextView).text =
                mContext.getString(R.string.ble_state_Offline)
            (helper.getView<View>(R.id.tv_signal) as TextView).text = ""
            (helper.getView<View>(R.id.tv_onile_state) as TextView).setTextColor(
                ContextCompat.getColor(
                    mContext,
                    R.color.gray
                )
            )
        }

        if (searchKeyword != null && searchKeyword!!.isNotEmpty())
            setChangeTextView(
                item.deviceName,
                searchKeyword!!,
                (helper.getView<View>(R.id.tv_device_name) as TextView)
            )
        if (searchKeyword != null && searchKeyword!!.isNotEmpty())
            setChangeTextView(
                item.mac,
                searchKeyword!!,
                (helper.getView<View>(R.id.tv_epd_id) as TextView)
            )

        (helper.getView<View>(R.id.item_in_device) as ConstraintLayout).setOnClickListener {
            click.onItemClick(helper.adapterPosition - 1)
        }

        (helper.getView<View>(R.id.item_in_device) as
                ConstraintLayout).setOnLongClickListener() {
            click.onItemLongClick(helper.adapterPosition - 1)
            true
        }
    }

    //制定位置修改颜色
    private fun setChangeTextView(name: String?, changeStr: String, textView: TextView) {
        if (name != null && name.contains(changeStr)) {
            val index = name.indexOf(changeStr)
            val len = changeStr.length
            val temp = Html.fromHtml(
                name.substring(0, index)
                        + "<font color=#E73324>"
                        + name.substring(index, index + len) + "</font>"
                        + name.substring(index + len, name.length)
            )
            textView.text = temp
        } else {
            textView.text = name
        }
    }

    fun sOnItemClick(click: OnItemClick) {
        this.click = click
    }

    interface OnItemClick {

        fun onItemClick(position: Int)
        fun onItemLongClick(position: Int)
        fun onItemDeleteClick(position: Int)

    }
}
