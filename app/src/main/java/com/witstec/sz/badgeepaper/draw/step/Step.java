package com.witstec.sz.badgeepaper.draw.step;


import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity;
import com.witstec.sz.badgeepaper.draw.bean.Pel;
import com.witstec.sz.badgeepaper.draw.ui.view.CanvasView;


public abstract class Step {
    /**
     * 通知重绘用
     */
    protected static CanvasView canvasVi = DrawingMainActivity.getCanvasView();
    /**
     * 最早放入undo的图元
     */
    protected Pel curPel;

    public Step(Pel pel)
    {
        this.curPel = pel;
    }

    /**
     * 进undo栈时对List中图元的更新（子类覆写）
     */
    public abstract void toUndoUpdate();

    /**
     * 进redo栈时对List中图元的反悔（子类覆写）
     */
    public abstract void toRedoUpdate();

    /**
     * 进undo栈时对List中图元的更新（子类覆写）
     * @param pel
     */
    public abstract void setToUndoPel(Pel pel);
}
