package com.witstec.sz.badgeepaper.model.bean;

public class LoginRequest {


    /**
     * uid : 623de94e62c79c9e
     * token : 1008aa4cf2db84a41169d77824d2800b68d2b609
     * refresh_token : d15e71a5c730ae32dbebfab886c25c04b4fa7a87
     * expire_at : 1594620659
     */

    private String uid;
    private String token;
    private String refresh_token;
    private int expire_at;
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public int getExpire_at() {
        return expire_at;
    }

    public void setExpire_at(int expire_at) {
        this.expire_at = expire_at;
    }
}
