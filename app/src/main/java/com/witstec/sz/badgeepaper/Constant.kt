package com.witstec.sz.badgeepaper

object Constant {
    internal val UID = "uid"
    internal val TOKEN = "token"
    internal val REFRESH_TOKEN = "refresh_token"
    internal val USER_NAME = "user_name"
    internal val USER_NAME_INFO = "user_name_info"
    internal var AVATAR_USER_INFO = "avatar_user_info"
    internal var USER_LICENSE = "user_license"
    internal val REQUEST_CODE_CHOOSE = 123
}