package com.witstec.sz.badgeepaper.ui.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.witstec.sz.badgeepaper.ExtKt;
import com.witstec.sz.badgeepaper.R;
import com.witstec.sz.badgeepaper.model.bean.ImageBleMsg;
import com.witstec.sz.badgeepaper.model.bean.InputBoxBean;
import com.witstec.sz.badgeepaper.model.event.DeviceMsgUpdateEvent;
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.DeviceManageActivity;
import com.witstec.sz.badgeepaper.utils.RxBus;
import com.witstec.sz.badgeepaper.utils.app.AppUtils;

import java.util.List;
import java.util.Objects;

public class DeviceMsgTypeAdapter extends BaseMultiItemQuickAdapter<InputBoxBean, BaseViewHolder> {


    public DeviceMsgTypeAdapter(List<InputBoxBean> data) {
        super(data);
        addItemType(InputBoxBean.TYPE_TEXT, R.layout.layout_device_template_text);
        addItemType(InputBoxBean.TYPE_BARCODE, R.layout.layout_device_template_bar_code);
        addItemType(InputBoxBean.TYPE_QRCODE, R.layout.layout_device_template_qr_code);
        addItemType(InputBoxBean.TYPE_IMAGE, R.layout.layout_device_template_image);
    }

    @Override
    protected void convert(BaseViewHolder helper, InputBoxBean item) {
        helper.setIsRecyclable(false);
        switch (helper.getItemViewType()) {
            case ImageBleMsg.TYPE_QRCODE:
            case ImageBleMsg.TYPE_BARCODE:
            case ImageBleMsg.TYPE_TEXT:
                AppCompatEditText textView = helper.getView(R.id.ed_device_text);
                ImageView iv_preview = helper.getView(R.id.iv_preview);
                textView.setOnFocusChangeListener((v, hasFocus) -> {
                    if (hasFocus) {
                        // 此处为得到焦点时的处理内容
                        iv_preview.setVisibility(View.VISIBLE);
                    } else {
                        iv_preview.setVisibility(View.GONE);
                    }
                });
                iv_preview.setOnClickListener(v -> RxBus.Companion.post(new DeviceMsgUpdateEvent(getData())));
                textView.setText(item.getText());
                textView.setSelection(Objects.requireNonNull(textView.getText()).toString().length());
                textView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (DeviceManageActivity.Companion.isUpdateModeAdapter()) {
                            String s1 = s.toString();
                            item.setText(s1);
                            getData().set(helper.getAdapterPosition(), item);
                        }
                    }
                });
                break;

            case ImageBleMsg.TYPE_IMAGE:
                ImageView iv_template_image = helper.getView(R.id.iv_template_image);
                AppCompatButton image_update = helper.getView(R.id.btn_template_image_update);
                image_update.setOnClickListener(v -> {
                    RxBus.Companion.post(new DeviceMsgUpdateEvent(getData(), helper.getAdapterPosition()));
                    ExtKt.selectImage(helper.itemView.getContext(), 1);
                });
                if (item.getImage()!=null) {
                    iv_template_image.setImageBitmap(AppUtils.base64ToBitmap(item.getImage()));
                }
                break;
        }
    }
}