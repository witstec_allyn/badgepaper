package com.witstec.sz.badgeepaper.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class BleTemplateBase implements Parcelable {

    /**
     * image :
     * imageEffectSize : 88
     * text : 体育婆婆
     * type : 32
     */

    private String image="";
    private int imageEffectSize=88;
    private String text="";
    private String type="";

    public BleTemplateBase(String text, String type) {
        this.text = text;
        this.type = type;
    }

    public BleTemplateBase(String image, int imageEffectSize, String type) {
        this.image = image;
        this.imageEffectSize = imageEffectSize;
        this.type = type;
    }

    public BleTemplateBase() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageEffectSize() {
        return imageEffectSize;
    }

    public void setImageEffectSize(int imageEffectSize) {
        this.imageEffectSize = imageEffectSize;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.image);
        dest.writeInt(this.imageEffectSize);
        dest.writeString(this.text);
        dest.writeString(this.type);
    }

    protected BleTemplateBase(Parcel in) {
        this.image = in.readString();
        this.imageEffectSize = in.readInt();
        this.text = in.readString();
        this.type = in.readString();
    }

    public static final Creator<BleTemplateBase> CREATOR = new Creator<BleTemplateBase>() {
        @Override
        public BleTemplateBase createFromParcel(Parcel source) {
            return new BleTemplateBase(source);
        }

        @Override
        public BleTemplateBase[] newArray(int size) {
            return new BleTemplateBase[size];
        }
    };
}
