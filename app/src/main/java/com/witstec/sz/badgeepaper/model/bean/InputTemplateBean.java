package com.witstec.sz.badgeepaper.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class InputTemplateBean implements Parcelable {

    /**
     * content : string
     */

    private String template_id;
    private String content;

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.template_id);
        dest.writeString(this.content);
    }

    public InputTemplateBean() {
    }

    protected InputTemplateBean(Parcel in) {
        this.template_id = in.readString();
        this.content = in.readString();
    }

    public static final Parcelable.Creator<InputTemplateBean> CREATOR = new Parcelable.Creator<InputTemplateBean>() {
        @Override
        public InputTemplateBean createFromParcel(Parcel source) {
            return new InputTemplateBean(source);
        }

        @Override
        public InputTemplateBean[] newArray(int size) {
            return new InputTemplateBean[size];
        }
    };
}
