package com.witstec.sz.badgeepaper

import android.app.Application
import android.content.Context
import android.os.Build
import cn.com.heaton.blelibrary.ble.Ble
import cn.com.heaton.blelibrary.ble.BleLog
import cn.com.heaton.blelibrary.ble.model.BleFactory
import com.witstec.sz.badgeepaper.model.bean.TemplateSizeType
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.newble.MyBleWrapperCallback
import com.witstec.sz.badgeepaper.utils.ActivityManager
import com.witstec.sz.badgeepaper.utils.language.LanguageUtil
import com.witstec.sz.badgeepaper.utils.language.SpUtil
import java.util.*
import kotlin.properties.Delegates

/**
 *
 */
class App : Application() {

    companion object {
        var instance: Context by Delegates.notNull()
        var device_name = ""
        var BLE_TYPE: TemplateSizeType = TemplateSizeType.OTHER
        var template_id = 0
        var isBleInitSuccess = false
    }

    override fun onCreate() {
        super.onCreate()
        instance = applicationContext
        bleInit()
        // 注册Activity生命周期回调监听
        registerActivityLifecycleCallbacks(ActivityManager.getInstance())
        /**
         * 对于7.0以下，需要在Application创建的时候进行语言切换
         */
        val language = SpUtil.getInstance(this).getString(SpUtil.LANGUAGE)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            LanguageUtil.changeAppLanguage(instance, language)
        }
    }

    fun bleInit() {
        Ble.options()
            .setLogBleEnable(true) //设置是否输出打印蓝牙日志
            .setThrowBleException(true) //设置是否抛出蓝牙异常
            .setLogTAG("BadgePaper") //设置全局蓝牙操作日志TAG
            .setAutoConnect(false) //设置是否自动连接
            .setIgnoreRepeat(false) //设置是否过滤扫描到的设备(已扫描到的不会再次扫描)
            .setConnectFailedRetryCount(5) //连接异常时（如蓝牙协议栈错误）,重新连接次数
            .setConnectTimeout(15 * 1000) //设置连接超时时长
            .setScanPeriod(15 * 1000) //设置扫描时长
            .setMaxConnectNum(3) //最大连接数量
            //                .setScanFilter(scanFilter)
            .setUuidService(UUID.fromString("fdebff7c-ee07-471a-8eb1-a488158e3c1b")) //设置主服务的uuid
            .setUuidWriteCha(UUID.fromString("0000fa02-0000-1000-8000-00805f9b34fb")) //设置可写特征的uuid
            .setUuidReadCha(UUID.fromString("0000fa02-0000-1000-8000-00805f9b34fb")) //设置可读特征的uuid （选填）
            .setUuidNotifyCha(UUID.fromString("0000fa02-0000-1000-8000-00805f9b34fb")) //设置可通知特征的uuid （选填，库中默认已匹配可通知特征的uuid）
            .setUuidNotifyDesc(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"))
            .setFactory(object : BleFactory<BleRssiDevice?>() {
                //实现自定义BleDevice时必须设置
                override fun create(address: String, name: String): BleRssiDevice {
                    return BleRssiDevice(address, name) //自定义BleDevice的子类
                }
            })
            .setBleWrapperCallback(MyBleWrapperCallback())
            .create<BleRssiDevice>(this, object : Ble.InitCallback {
                override fun failed(failedCode: Int) {
                    BleLog.e("MainApplication", "初始化失败：$failedCode")
                    isBleInitSuccess = false
                }

                override fun success() {
                    BleLog.e("MainApplication", "初始化成功")
                    isBleInitSuccess = true
                }
            })
    }

}
