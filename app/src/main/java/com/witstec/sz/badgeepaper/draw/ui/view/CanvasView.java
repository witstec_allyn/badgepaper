package com.witstec.sz.badgeepaper.draw.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.R;
import com.witstec.sz.badgeepaper.draw.bean.Pel;
import com.witstec.sz.badgeepaper.draw.bean.Picture;
import com.witstec.sz.badgeepaper.draw.bean.Text;
import com.witstec.sz.badgeepaper.draw.step.DeletePelStep;
import com.witstec.sz.badgeepaper.draw.step.DrawPelStep;
import com.witstec.sz.badgeepaper.draw.step.Step;
import com.witstec.sz.badgeepaper.draw.touch.DrawBesselTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawBrokenLineTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawFreehandTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawLineTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawOvalTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawPolygonTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawRectTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawTouch;
import com.witstec.sz.badgeepaper.draw.touch.Touch;
import com.witstec.sz.badgeepaper.draw.touch.TransformTouch;
import com.witstec.sz.badgeepaper.utils.LogHelper;
import com.witstec.sz.badgeepaper.utils.Utils;
import com.witstec.sz.badgeepaper.utils.app.AppUtils;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Stack;

import static com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity.clearRedoStack;

public class CanvasView extends View {
    /**
     * 动画画笔（变换相位用）
     */
    private float phase;
    /**
     * 动画效果画笔
     */
    public static Paint animPelPaint;
    /**
     * 画画用的画笔
     */
    public static Paint drawPelPaint;
    /**
     * 画图片画笔
     */
    public static Paint drawPicturePaint;
    /**
     * 画文字画笔
     */
    private Paint drawTextPaint;
    /**
     * 画布宽
     */
    public static int CANVAS_WIDTH;
    /**
     * 画布高
     */
    public static int CANVAS_HEIGHT;
    /**
     * undo栈
     */
    public static Stack<Step> undoStack;
    /**
     * redo栈
     */
    public static Stack<Step> redoStack;
    /**
     * 图元链表
     */
    public static ArrayList<Pel> pelList;
    /**
     * 画布裁剪区域
     */
    public static Region clipRegion;
    /**
     * 当前被选中的图元
     */
    public static Pel selectedPel = null;
    /**
     * 重绘位图
     */
    public static Bitmap savedBitmap;
    /**
     * 重绘画布
     */
    private Canvas savedCanvas;
    /**
     * 缓存画布
     */
    private Canvas cacheCanvas;
    public static Bitmap backgroundBitmap;
    /**
     * 原图片副本，清空或还原时用
     */
    public static Bitmap copyOfBackgroundBitmap;
    public static Bitmap originalBackgroundBitmap;

    private PaintFlagsDrawFilter pfd;
    /**
     * 触摸操作父类
     */
    public static Touch mChildTouch;

    public static void setCanvasWidth(int canvasWidth) {
        CANVAS_WIDTH = canvasWidth;
    }

    public static void setCanvasHeight(int canvasHeight) {
        CANVAS_HEIGHT = canvasHeight;
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrs
     */
    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        pfd = new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        //初始化画布宽高为屏幕宽高
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        assert wm != null;
        //初始化undo redo栈
        undoStack = new Stack<Step>();
        redoStack = new Stack<Step>();
        pelList = new ArrayList<>();
        // 图元总链表
        savedCanvas = new Canvas();
        savedCanvas.setDrawFilter(pfd);
        //获取画布裁剪区域
        clipRegion = new Region();
        //初始化为自由手绘操作
        mChildTouch = new DrawFreehandTouch();
        drawPelPaint = DrawFreehandTouch.getCurPaint();

        animPelPaint = new Paint(drawPelPaint);
        animPelPaint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        animPelPaint.setStyle(Paint.Style.STROKE);

        drawTextPaint = new Paint();
        drawTextPaint.setColor(DrawTouch.getCurPaint().getColor());
        drawTextPaint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_55);
        drawTextPaint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        drawTextPaint.setAntiAlias(true);
        drawTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        drawPicturePaint = new Paint();
        drawPicturePaint.setColor(Color.BLACK);
        //针对绘制bitmap添加抗锯齿
        drawPicturePaint.setFilterBitmap(true);
        drawPicturePaint.setAntiAlias(true);

    }

    /**
     * 绘制
     *
     * @param canvas
     */
    protected void onDraw(Canvas canvas) {
        try {
            if (savedBitmap.isRecycled()) return;
            canvas.setDrawFilter(pfd);
            // 画其余图元
            canvas.drawBitmap(savedBitmap, 0, 0, new Paint());
            if (selectedPel != null) {
                if (mChildTouch instanceof TransformTouch) //选中状态才产生动态画笔效果
                {
                    setAnimPaint();
                    selectedPel.drawObject(canvas);
                    //动画矩阵虚线
                    @SuppressLint("DrawAllocation") Path path = new Path();
                    if (selectedPel.leftBottomPoint != null) {
                        path.moveTo(selectedPel.leftTopPoint.x, selectedPel.leftTopPoint.y);
                        path.lineTo(selectedPel.rightTopPoint.x, selectedPel.rightTopPoint.y);
                        path.lineTo(selectedPel.rightBottomPoint.x, selectedPel.rightBottomPoint.y);
                        path.lineTo(selectedPel.leftBottomPoint.x, selectedPel.leftBottomPoint.y);
                        path.lineTo(selectedPel.leftTopPoint.x, selectedPel.leftTopPoint.y);
                        canvas.drawPath(path, animPelPaint);
                        //拖曳图标
                        canvas.drawBitmap(selectedPel.dragBitmap, selectedPel.dragBtnRect.left, selectedPel.dragBtnRect.top, drawPicturePaint);
                        invalidate();
                    }
                } else //画图状态不产生动态画笔效果
                {
                    canvas.drawPath(selectedPel.path, drawPelPaint);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 两次点击时间间隔，单位毫秒
     */
    private final int interval = 300;
    private int count = 0;
    private long firClick = 0;
    private long secClick = 0;
    //双击
    int textSize = 2;
    boolean isVertical = false;
    int textColor = Color.BLACK;
    String textConext = "abc";

    /**
     * 触摸监听
     *
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //第一只手指坐标
        mChildTouch.setCurPoint(new PointF(event.getX(0), event.getY(0)));

        //第二只手指坐标（可能在第二只手指还没按下时发生异常）
        try {
            mChildTouch.setSecPoint(new PointF(event.getX(1), event.getY(1)));
        } catch (Exception e) {
            mChildTouch.setSecPoint(new PointF(1, 1));
        }
        Pel pel = getSelectedPel();
        if (pel != null) {
            if (MotionEvent.ACTION_DOWN == event.getAction()) {
                count++;
                if (1 == count) {
                    firClick = System.currentTimeMillis();
                } else if (2 == count) {
                    secClick = System.currentTimeMillis();
                    if (secClick - firClick < interval) {
                        count = 0;
                        firClick = 0;

                        textSize = 2;
                        isVertical = false;
                        textColor = Color.BLACK;
                        textConext = "abc";
                        if (pel.type == 20) {
                            View view1 = inflate(getContext(), R.layout.dialog_drawing_text_input, null);

                            final AppCompatEditText text_input = view1.findViewById(R.id.text_input);
                            final ImageButton iv_text_horizontal = view1.findViewById(R.id.iv_text_horizontal);
                            final ImageButton iv_text_vertical = view1.findViewById(R.id.iv_text_vertical);

                            final ImageButton iv_text_red = view1.findViewById(R.id.iv_text_red);
                            final ImageButton iv_text_black = view1.findViewById(R.id.iv_text_black);
                            final ImageButton iv_text_white = view1.findViewById(R.id.iv_text_white);

                            final ImageButton text_size1 = view1.findViewById(R.id.text_size1);
                            final ImageButton text_size2 = view1.findViewById(R.id.text_size2);
                            final ImageButton text_size3 = view1.findViewById(R.id.text_size3);
                            final ImageButton text_size4 = view1.findViewById(R.id.text_size4);
                            final ImageButton text_size5 = view1.findViewById(R.id.text_size5);

                            text_size1.setOnClickListener(v15 -> {
                                textSize = 1;
                                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });
                            text_size2.setOnClickListener(v15 -> {
                                textSize = 2;
                                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });
                            text_size3.setOnClickListener(v15 -> {
                                textSize = 3;
                                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });
                            text_size4.setOnClickListener(v15 -> {
                                textSize = 4;
                                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });
                            text_size5.setOnClickListener(v15 -> {
                                textSize = 5;
                                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                            });


                            iv_text_horizontal.setOnClickListener(v1 -> {
                                isVertical = false;
                                iv_text_horizontal.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                iv_text_vertical.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });

                            iv_text_vertical.setOnClickListener(v12 -> {
                                isVertical = true;
                                iv_text_horizontal.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                iv_text_vertical.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                            });


                            iv_text_red.setOnClickListener(v13 -> {
                                textColor = Color.RED;
                                iv_text_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                iv_text_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });
                            iv_text_black.setOnClickListener(v14 -> {
                                textColor = Color.BLACK;
                                iv_text_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                iv_text_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                                iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                            });

                            iv_text_white.setOnClickListener(v14 -> {
                                textColor = Color.WHITE;
                                iv_text_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                iv_text_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                                iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                            });
                            text_input.setText(pel.text.getContent());
                            text_input.requestFocus();
                            text_input.setSelection(Objects.requireNonNull(text_input.getText()).length());
                            text_input.post(() -> {
                                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                assert imm != null;
                                imm.showSoftInput(text_input, InputMethodManager.SHOW_FORCED);
                            });
                            new MaterialDialog.Builder(getContext())
                                    .title(getContext().getString(R.string.text_content))
                                    .customView(view1, true)
                                    .positiveText(getContext().getString(R.string.fixed))
                                    .negativeText(getContext().getString(R.string.cancel))
                                    .onPositive((dialog, which) -> {
                                        textConext = Objects.requireNonNull(text_input.getText()).toString();
                                        Rect rect = new Rect();
                                        getDrawTextPaint().getTextBounds(textConext, 0, textConext.length(), rect);

                                        PointF beginPoint = new PointF(pel.centerPoint.x - (rect.width() >> 1),
                                                pel.centerPoint.y - (rect.height() >> 1));

                                        //文本开始坐标
                                        //文本区域
                                        Region region = new Region();
                                        //横向显示
                                        if (!isVertical) {
                                            region.set((int) (pel.centerPoint.x - (rect.width() / 3)),
                                                    (int) pel.centerPoint.y - (rect.height() / 3),
                                                    (int) (pel.centerPoint.x + (rect.width() / 3)),
                                                    (int) pel.centerPoint.y + (rect.height() / 3));
                                        }
                                        //竖向显示
                                        else {
                                            if (AppUtils.isContainsLetter(selectedPel.text.getContent())) {
                                                region.set((int)pel. centerPoint.x - (rect.height() / 2),
                                                        ((int) pel.centerPoint.y - (rect.width() / 2) + 6),
                                                        (int) (pel.centerPoint.x + (rect.height() / 2)),
                                                        ((int) pel.centerPoint.y + (rect.width() / 2) + 6));
                                            } else {
                                                region.set((int) pel.centerPoint.x - (rect.height() / 2),
                                                        ((int)pel. centerPoint.y - (rect.width() / 4)),
                                                        (int) (pel.centerPoint.x + (rect.height() / 2)),
                                                        ((int)pel. centerPoint.y + (rect.width() / 4)));
                                            }
                                        }
                                        Pel newPel = new Pel();
                                        switch (textSize) {
                                            case 1:
                                                newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_45);
                                                break;
                                            case 2:
                                                newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_55);
                                                break;
                                            case 3:
                                                newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_65);
                                                break;
                                            case 4:
                                                newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_75);
                                                break;
                                            case 5:
                                                newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_85);
                                                break;
                                        }
                                        newPel.textSize = textSize;
                                        Text text = new Text(textConext, isVertical, newPel.textSize);
                                        newPel.type = pel.type;
                                        newPel.text = text;
                                        newPel.angle = pel.angle;
                                        newPel.scale = pel.scale;
                                        newPel.transDx = pel.transDx;
                                        newPel.transDy = pel.transDy;
                                        newPel.region = region;
                                        newPel.beginPoint = beginPoint;
                                        newPel.centerPoint = pel.centerPoint;
                                        newPel.bottomRightPointF.set(pel.region.getBounds().right, pel.region.getBounds().bottom);
                                        newPel.paint.setColor(textColor);

                                        if (textColor == Color.RED) {
                                            textColor = 1;
                                        } else if (textColor == Color.BLACK) {
                                            textColor = 0;
                                        } else {
                                            textColor = 2;
                                        }
                                        newPel.paintColor = textColor;
                                        //添加至文本总链表
                                        pelList.remove(pel);
                                        undoStack.push(new DeletePelStep(pel));
                                        setSelectedPel(null);
                                        updateSavedBitmap();
                                        pelList.add(newPel);
                                        //记录栈中信息
                                        undoStack.push(new DrawPelStep(newPel));//将该“步”压入undo栈
                                        //可拖动
                                        clearRedoStack();
                                        CanvasView.setmChildTouch(new TransformTouch(getContext()));
                                        //更新画布
                                        updateSavedBitmap();
                                    })
                                    .onNegative((dialog, which) -> dialog.dismiss())
                                    .show();

                            textColor = Color.BLACK;
                            isVertical = false;
                            textSize = 2;
                        }

                    } else {
                        firClick = secClick;
                        count = 1;
                    }
                    secClick = 0;
                }
            }
        }
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:// 第一只手指按下
            {
                mChildTouch.down1();
            }
            break;
            case MotionEvent.ACTION_POINTER_DOWN:// 第二个手指按下
                mChildTouch.down2();
                break;
            case MotionEvent.ACTION_MOVE:
//                if (DrawMainActivity.topToolbarSclVi.getVisibility() == View.VISIBLE) {
////                    DrawMainActivity.closeTools();
////                }
                mChildTouch.move();
                break;
            case MotionEvent.ACTION_UP:// 第一只手指抬起
            case MotionEvent.ACTION_POINTER_UP://第二只手抬起
                if (mChildTouch != null)
                    mChildTouch.up();
                break;
        }
        invalidate();

        return true;
    }


    //确保未画完的图元能够真正敲定
    private void ensurePelFinished() {
        Touch touch = CanvasView.getmChildTouch();
        selectedPel = CanvasView.getSelectedPel();

        if (selectedPel != null) {
            //使人为敲定图元的操作(贝塞尔、折线、多边形)
            if (touch instanceof DrawBesselTouch) {
                touch.control = true;
                touch.up();
            } else if (touch instanceof DrawBrokenLineTouch) {
                touch.hasFinished = true;
                touch.up();
            } else if (touch instanceof DrawPolygonTouch) {
                (touch.curPoint).set(touch.beginPoint);
                touch.up();
            } else //单纯选中
            {
                CanvasView.setSelectedPel(null);//失去焦点
                updateSavedBitmap();//重绘位图
            }
        }
    }

    /**
     * 初始化画布
     */
    public void initBitmap() {
        clipRegion.set(new Rect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT));
        BitmapDrawable backgroundDrawable = (BitmapDrawable) this.getResources().getDrawable(R.mipmap.ic_bg);
        Bitmap scaledBitmap = AppUtils.zoomImg(backgroundDrawable.getBitmap(), CANVAS_WIDTH, CANVAS_HEIGHT);

        ensureBitmapRecycled(backgroundBitmap);
        backgroundBitmap = scaledBitmap.copy(Bitmap.Config.RGB_565, true);
        ensureBitmapRecycled(scaledBitmap);

        ensureBitmapRecycled(copyOfBackgroundBitmap);
        copyOfBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        ensureBitmapRecycled(originalBackgroundBitmap);
        originalBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        cacheCanvas = new Canvas();
        savedCanvas.setDrawFilter(pfd);
        cacheCanvas.setBitmap(backgroundBitmap);
    }

    /*
     * 自定义成员函数
     */
    public void updateSavedBitmap() //更新重绘背景位图用（当且仅当选择的图元有变化的时候才调用）
    {
        //创建缓冲位图
        ensureBitmapRecycled(savedBitmap);
        savedBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);//由画布背景创建缓冲位图
        savedCanvas.setBitmap(savedBitmap);

        //画除selectedPel外的所有图元
        drawPels();

        invalidate();
    }

    /**
     * 放大画布
     */
    private float scale = 1f;

    public void zoomUp() {
        //创建缓冲位图
        ensureBitmapRecycled(savedBitmap);
        savedBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);//由画布背景创建缓冲位图
        savedCanvas.setBitmap(savedBitmap);
        //画除selectedPel外的所有图元
        drawPels();
        if (scale < 2) {
            scale += 0.2f;
        }
        invalidate();
    }

    /**
     * 缩小
     */
    public void zoomDown() {
        //创建缓冲位图
        ensureBitmapRecycled(savedBitmap);
        savedBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);//由画布背景创建缓冲位图
        savedCanvas.setBitmap(savedBitmap);
        //画除selectedPel外的所有图元
        drawPels();
        if (scale > 1) {
            scale -= 0.2f;
        }
        invalidate();
    }

    public void drawPels() {
        ListIterator<Pel> pelIterator = pelList.listIterator();// 获取pelList对应的迭代器头结点
        while (pelIterator.hasNext()) {
            Pel pel = pelIterator.next();
            if (!pel.equals(selectedPel)) {
                pel.drawObject(savedCanvas);
            }
        }
    }


    public static void ensureBitmapRecycled(Bitmap bitmap) //确保传入位图已经回收
    {
        if (bitmap != null && !bitmap.isRecycled())
            bitmap.recycle();
    }

    // 动画画笔更新
    private void setAnimPaint() {
        phase++; // 变相位

        Path p = new Path();
        p.addRect(new RectF(0, 0, 6, 3), Path.Direction.CCW); // 路径单元是矩形（也可以为椭圆）
        PathDashPathEffect effect = new PathDashPathEffect(p, 12, phase, // 设置路径效果
                PathDashPathEffect.Style.ROTATE);
        animPelPaint.setPathEffect(effect);
    }

    /**
     * ---------------------get set-------------------
     **/
    public static int getCanvasWidth() {
        return CANVAS_WIDTH;
    }

    public static int getCanvasHeight() {
        return CANVAS_HEIGHT;
    }

    public static Region getClipRegion() {
        return clipRegion;
    }

    public static ArrayList<Pel> getPelList() {
        return pelList;
    }

    public static Pel getSelectedPel() {
        return selectedPel;
    }

    public static Bitmap getSavedBitmapT() {
        Matrix matrix = new Matrix();
        matrix.setScale(0.5f, 0.5f);
        return Bitmap.createBitmap(savedBitmap, 0, 0, savedBitmap.getWidth(),
                savedBitmap.getHeight(), matrix, true);
    }

    public static Bitmap getSavedBitmap() {
        return savedBitmap;
    }

    public static Bitmap getBackgroundBitmap() {
        return backgroundBitmap;
    }

    public static Bitmap getCopyOfBackgroundBitmap() {
        return copyOfBackgroundBitmap;
    }

    public static Bitmap getOriginalBackgroundBitmap() {
        return originalBackgroundBitmap;
    }

    public static Touch getmChildTouch() {
        return mChildTouch;
    }

    public static Stack<Step> getUndoStack() {
        return undoStack;
    }

    public static Stack<Step> getRedoStack() {
        return redoStack;
    }

    /*
     * set()方法:设置CanvasView下指定成员
     */
    public static void setSelectedPel(Pel pel) {
        selectedPel = pel;
    }

    public static void setmChildTouch(Touch childTouch) {
        mChildTouch = childTouch;
    }

    public void setCanvasSize(int width, int height) {
        CANVAS_WIDTH = (Utils.Companion.dp2px(getContext(), width));
        CANVAS_HEIGHT = (Utils.Companion.dp2px(getContext(), height));
        LogHelper.INSTANCE.i("witstec", "CANVAS_WIDTH=" + CANVAS_WIDTH);
        LogHelper.INSTANCE.i("witstec", "CANVAS_HEIGHT=" + CANVAS_HEIGHT);
        RelativeLayout.LayoutParams linearParams = (RelativeLayout.LayoutParams) getLayoutParams(); //取控件textView当前的布局参数
        linearParams.height = CANVAS_HEIGHT;// 控件的高强制设成20
        linearParams.width = CANVAS_WIDTH;// 控件的宽强制设成30
        setLayoutParams(linearParams);
        initBitmap();
        updateSavedBitmap();
    }

    public static void setSavedBitmap(Bitmap bitmap) {
        savedBitmap = bitmap;
    }

    public void setBackgroundBitmap(int id) //以已提供选择的背景图片换画布
    {
        BitmapDrawable backgroundDrawable = (BitmapDrawable) this.getResources().getDrawable(id);
        Bitmap offeredBitmap = backgroundDrawable.getBitmap();

        ensureBitmapRecycled(backgroundBitmap);
        backgroundBitmap = AppUtils.zoomImg(offeredBitmap, CANVAS_WIDTH, CANVAS_HEIGHT);

        ensureBitmapRecycled(copyOfBackgroundBitmap);
        copyOfBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        ensureBitmapRecycled(originalBackgroundBitmap);
        originalBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        updateSavedBitmap();
    }

    public void setBackgroundBitmap(Bitmap photo)//以图库或拍照得到的背景图片换画布
    {
        ensureBitmapRecycled(backgroundBitmap);
        backgroundBitmap = AppUtils.zoomImg(photo, CANVAS_WIDTH, CANVAS_HEIGHT);

        ensureBitmapRecycled(copyOfBackgroundBitmap);
        copyOfBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        ensureBitmapRecycled(originalBackgroundBitmap);
        originalBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        updateSavedBitmap();
    }

    public void setProcessedBitmap(Bitmap imgPro)//设置处理后的图片作为背景
    {
        ensureBitmapRecycled(backgroundBitmap);
        backgroundBitmap = AppUtils.zoomImg(imgPro, CANVAS_WIDTH, CANVAS_HEIGHT);

        ensureBitmapRecycled(copyOfBackgroundBitmap);
        copyOfBackgroundBitmap = backgroundBitmap.copy(Bitmap.Config.ARGB_8888, true);

        updateSavedBitmap();
    }

    public void setBackgroundBitmap() //清空画布时将之前保存的副本背景作为重绘（去掉填充）
    {
        ensureBitmapRecycled(backgroundBitmap);
        backgroundBitmap = copyOfBackgroundBitmap.copy(Bitmap.Config.RGB_565, true);

        updateSavedBitmap();
    }

    public Paint getDrawTextPaint() {
        return drawTextPaint;
    }

    //生成xml文件
    public synchronized String saveFileDataXml(String path) throws Exception {
        XmlSerializer serializer = Xml.newSerializer();
        FileOutputStream fos = null;
        StringWriter stringWriter = null;
        //1.保存背景
        //2.保存各图元数据
        ListIterator<Pel> pelIterator = pelList.listIterator();// 获取pelList对应的迭代器头结点
        if (!TextUtils.isEmpty(path)) {
            fos = new FileOutputStream(path);
            serializer.setOutput(fos, "utf-8");
        } else {
            stringWriter = new StringWriter();
            serializer.setOutput(stringWriter);
        }
        //开始写xml文档的开头
        serializer.startDocument("utf-8", true);//文档的编码格式
        //写xml的根节点，namespace 命名空间
        serializer.startTag(null, "Drawing");
        serializer.attribute(null, "childSize", "" + pelList.size());
        serializer.attribute(null, "platform", "android");
        while (pelIterator.hasNext()) {
            Pel pel = pelIterator.next();
            //根目录
            serializer.startTag(null, "Module");
            // 根目录属性=类型
            serializer.attribute(null, "type", "" + pel.type);
            //属性开始
            serializer.startTag(null, "Attributes");
            //缩放倍数
            serializer.attribute(null, "scale", "" + pel.scale);
            //画笔颜色
            serializer.attribute(null, "paintColor", "" + pel.paintColor);
            //旋转角度
            serializer.attribute(null, "angle", "" + (int) pel.angle);
            //是否填充
            serializer.attribute(null, "fillColor", "" + pel.fillColor);
            //图片效果 取值范围1-200
            serializer.attribute(null, "image_effect_size", "" + pel.image_effect_size);
            //是否为虚线 1-虚线  0=实线
            serializer.attribute(null, "dotted_line", "" + pel.dotted_line);
            ///画笔宽度
            serializer.attribute(null, "paintStrokeWidth", "" + pel.pnWidth);
            //属性结束
            serializer.endTag(null, "Attributes");
            //位置开支
            serializer.startTag(null, "Occupy");
            Rect rect = pel.region.getBounds();
            //距离左边
            serializer.attribute(null, "left", "" + Utils.Companion.px2dp(getContext(), rect.left));
            //距离上面
            serializer.attribute(null, "top", "" + Utils.Companion.px2dp(getContext(), rect.top));
            //距离右边
            serializer.attribute(null, "right", "" + Utils.Companion.px2dp(getContext(), rect.right));
            //距离下边
            serializer.attribute(null, "bottom", "" + Utils.Companion.px2dp(getContext(), rect.bottom));

            //中心点x
            serializer.attribute(null, "centerPointX", "" + Utils.Companion.px2dp(getContext(), pel.centerPoint.x));
            //中心点y
            serializer.attribute(null, "centerPointY", "" + Utils.Companion.px2dp(getContext(), pel.centerPoint.y));
            //开始点x
            serializer.attribute(null, "beginPointX", "" + Utils.Companion.px2dp(getContext(), pel.beginPoint.x));
            //开始点y
            serializer.attribute(null, "beginPointY", "" + Utils.Companion.px2dp(getContext(), pel.beginPoint.y));
            //区域右下角点x
            serializer.attribute(null, "bottomRightPointFX", "" + Utils.Companion.px2dp(getContext(), pel.bottomRightPointF.x));
            ///区域右下角点y
            serializer.attribute(null, "bottomRightPointFY", "" + Utils.Companion.px2dp(getContext(), pel.bottomRightPointF.y));
            //平移x
            serializer.attribute(null, "transDx", "" + Utils.Companion.px2dp(getContext(), pel.transDx));
            //平移y
            serializer.attribute(null, "transDy", "" + Utils.Companion.px2dp(getContext(), pel.transDy));
            //位置结束
            serializer.endTag(null, "Occupy");
            /**
             * 类型
             * path类型：10:自由线 11：矩型 12：塞尔曲线 13：圆 14：直线 15：多重折线 16：多边形
             * 文本类型：20
             * 照片类型：30
             * 条形码 31
             * 二维码 32
             */
            switch (pel.type) {
                //path类型
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                    serializer.startTag(null, "pathType");
                    //点总数
                    serializer.attribute(null, "TotalPoints", pel.pathPointFList != null ? "" + pel.pathPointFList.size() : "" + 0);
                    //点坐标
                    if (pel.pathPointFList != null) {
                        for (PointF pointF : pel.pathPointFList) {
                            serializer.startTag(null, "dot");
                            serializer.attribute(null, "x", "" + Utils.Companion.px2dp(getContext(), pointF.x));
                            serializer.attribute(null, "y", "" + Utils.Companion.px2dp(getContext(), pointF.y));
                            serializer.endTag(null, "dot");
                        }
                    }
                    serializer.endTag(null, "pathType");
                    break;
                //文本
                case 20:
                    //条形码
                case 31:
                    //二维码
                case 32:
                    if (pel.text != null) {
                        serializer.startTag(null, "text");
                        serializer.attribute(null, "isVertical", "" + pel.text.isVertical());
                        serializer.attribute(null, "content", pel.text.getContent());
                        serializer.attribute(null, "textSize", "" + pel.textSize);
                        serializer.endTag(null, "text");
                    }
                    break;
                //照片
                case 30:
                    if (pel.picture != null) {
                        serializer.startTag(null, "image");
                        serializer.attribute(null, "data", AppUtils.bitmapToBase64Tep(pel.picture.createContent()));
                        serializer.endTag(null, "image");
                    }
                    break;
            }
            serializer.endTag(null, "Module");
        }
        serializer.endTag(null, "Drawing");
        serializer.endDocument();//写文档的结尾（写一个开头就写一个结尾，不会出错）
        if (!TextUtils.isEmpty(path)) {
            assert fos != null;
            fos.close();//关闭流
            return path;
        } else {
            return String.valueOf(stringWriter);
        }
    }

    //解析xml文件绘制控件
    public synchronized void loadFileDatXml(String str, boolean isLocal) throws Exception {
        setBackgroundBitmap();
        CanvasView.pelList.clear();
        SAXReader reader = new SAXReader();
        Document document;
        if (isLocal) {
            InputSource inputSource = new InputSource(new StringReader(str));
            document = reader.read(inputSource);
        } else {
            FileInputStream fileInputStream = new FileInputStream(new File(str));
            document = reader.read(fileInputStream);
        }
        Element imageBleMsg = document.getRootElement();
        String platform = imageBleMsg.attributeValue("platform");
        boolean isIos;
        if (platform.equals("ios")) {
            isIos = true;
        } else {
            isIos = false;
        }
        Iterator module = imageBleMsg.elementIterator("Module");
        Pel pel = null;
        while (module.hasNext()) {
            Element Moduleelement = (Element) module.next();
            int type = Integer.parseInt(Moduleelement.attributeValue("type"));
            Element pathType = Moduleelement.element("pathType");
            /**
             * 类型
             * path类型：10:自由线 11：矩型 12：塞尔曲线 13：圆 14：直线 15：多重折线 16：多边形
             * 文本类型：20
             * 照片类型：30
             */
            switch (type) {
                //path类型
                //自由线
                case 10:
                    pel = DrawFreehandTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //矩型
                case 11:
                    pel = DrawRectTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //塞尔曲线
                case 12:
                    pel = DrawBesselTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //圆
                case 13:
                    pel = DrawOvalTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //直线
                case 14:
                    pel = DrawLineTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //多重折线
                case 15:
                    pel = DrawBrokenLineTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //多边形
                case 16:
                    pel = DrawPolygonTouch.loadPelXml(getContext(), pathType, isIos);
                    break;
                //文本
                case 20:
                    Element textElement = Moduleelement.element("text");
                    String isVertical = textElement.attributeValue("isVertical");
                    String textContext = textElement.attributeValue("content");
                    String textSize = textElement.attributeValue("textSize");
                    pel = new Pel();
                    if (textSize != null)
                    pel.textSize = Integer.parseInt(textSize);
                    Text text = new Text(textContext, Boolean.parseBoolean(isVertical), pel.textSize);
                    pel.type = 20;
                    if (textSize != null)
                        pel.text = text;
                    break;
                //照片
                case 30:
                    //这里要做改动
                    Element image = Moduleelement.element("image");
                    Picture picture = new Picture(AppUtils.base64ToBitmap(image.attributeValue("data")));
                    pel = new Pel();
                    pel.type = 30;
                    pel.picture = picture;
                    break;

                //条形码
                case 31:
                    Element textElementBar = Moduleelement.element("text");
                    String isVerticalBar = textElementBar.attributeValue("isVertical");
                    String textContextBar = textElementBar.attributeValue("content");
                    Text textBar = new Text(textContextBar, Boolean.parseBoolean(isVerticalBar), Constants.PAINT_DEFAULT_TEXT_SIZE_55);
                    pel = new Pel();
                    pel.type = 31;
                    pel.text = textBar;
                    break;

                //二维码
                case 32:
                    Element textElementQr = Moduleelement.element("text");
                    String isVerticalQr = textElementQr.attributeValue("isVertical");
                    String textContextQr = textElementQr.attributeValue("content");
                    Text textQr = new Text(textContextQr, Boolean.parseBoolean(isVerticalQr), Constants.PAINT_DEFAULT_TEXT_SIZE_55);
                    pel = new Pel();
                    pel.type = 32;
                    pel.text = textQr;
                    break;
            }

            if (pel != null) {
                Element occupy = Moduleelement.element("Occupy");
                float beginPointX = Float.parseFloat(occupy.attributeValue("beginPointX"));
                float beginPointY = Float.parseFloat(occupy.attributeValue("beginPointY"));
                float bottomRightPointFX = Float.parseFloat(occupy.attributeValue("bottomRightPointFX"));
                float bottomRightPointFY = Float.parseFloat(occupy.attributeValue("bottomRightPointFY"));
                float centerPointX = Float.parseFloat(occupy.attributeValue("centerPointX"));
                float centerPointY = Float.parseFloat(occupy.attributeValue("centerPointY"));
                float bottom = Float.parseFloat(occupy.attributeValue("bottom"));
                float left = Float.parseFloat(occupy.attributeValue("left"));
                float right = Float.parseFloat(occupy.attributeValue("right"));
                float top = Float.parseFloat(occupy.attributeValue("top"));
                float transDx = Float.parseFloat(occupy.attributeValue("transDx"));
                float transDy = Float.parseFloat(occupy.attributeValue("transDy"));


                Element attributes = Moduleelement.element("Attributes");
                int paintColor = Integer.parseInt(attributes.attributeValue("paintColor"));
                float angle = Integer.parseInt(attributes.attributeValue("angle"));
                int fillColor = Integer.parseInt(attributes.attributeValue("fillColor"));
                int image_effect_size = Integer.parseInt(attributes.attributeValue("image_effect_size"));
                int dotted_line = Integer.parseInt(attributes.attributeValue("dotted_line"));
                float scale = Float.parseFloat(attributes.attributeValue("scale"));
                //中心点
                pel.centerPoint = new PointF(Utils.Companion.dp2px(getContext(), centerPointX), Utils.Companion.dp2px(getContext(), centerPointY));
                //开始点x
                pel.beginPoint = new PointF(Utils.Companion.dp2px(getContext(), beginPointX), Utils.Companion.dp2px(getContext(), beginPointY));
                //区域右下角点x
                pel.bottomRightPointF = new PointF(Utils.Companion.dp2px(getContext(), bottomRightPointFX), Utils.Companion.dp2px(getContext(), bottomRightPointFY));
                //矩阵
                pel.region.set((int) Utils.Companion.dp2px(getContext(), left), (int) Utils.Companion.dp2px(getContext(), top), (int) Utils.Companion.dp2px(getContext(), right), (int) Utils.Companion.dp2px(getContext(), bottom));
                //平移x
                pel.transDx = Utils.Companion.dp2px(getContext(), transDx);
                //平移y
                pel.transDy = Utils.Companion.dp2px(getContext(), transDy);
                int color;
                if (paintColor == 1) {
                    color = Color.RED;
                } else if (paintColor == 0) {
                    color = Color.BLACK;
                } else {
                    color = Color.WHITE;
                }
                ///画笔颜色
                pel.paint.setColor(color);
                pel.paintColor = paintColor;
                pel.angle = angle;
                pel.fillColor = fillColor;
                pel.image_effect_size = image_effect_size;
                pel.dotted_line = dotted_line;
                int paintStrokeWidth = Integer.parseInt(attributes.attributeValue("paintStrokeWidth"));
                ///画笔宽度
                if (paintStrokeWidth == 1) {
                    pel.paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
                } else if (paintStrokeWidth == 2) {
                    pel.paint.setStrokeWidth(Constants.PAINT_WIDTH_M);
                } else if (paintStrokeWidth == 3) {
                    pel.paint.setStrokeWidth(Constants.PAINT_WIDTH_L);
                }
                pel.pnWidth = paintStrokeWidth;
                //缩放比
                pel.scale = scale;
                pel.isIos = isIos;
            }
            CanvasView.pelList.add(pel);
        }
        //更新
        updateSavedBitmap();
    }

}
