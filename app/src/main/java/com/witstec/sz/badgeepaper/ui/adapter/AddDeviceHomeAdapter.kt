package com.witstec.sz.badgeepaper.ui.adapter

import android.os.Build
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.utils.ByteUtils
import java.util.*

class AddDeviceHomeAdapter : BaseQuickAdapter<BleRssiDevice, BaseViewHolder>(R.layout.item_add_device_msg, ArrayList()) {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun convert(helper: BaseViewHolder, item: BleRssiDevice) {

        val scanRecordByteData = item.scanRecord.bytes
        val totalLoadData = ByteArray(7)
        totalLoadData[0] = scanRecordByteData[13]
        totalLoadData[1] = scanRecordByteData[14]
        totalLoadData[2] = scanRecordByteData[15]
        totalLoadData[3] = scanRecordByteData[16]
        totalLoadData[4] = scanRecordByteData[17]
        totalLoadData[5] = scanRecordByteData[18]
        totalLoadData[6] = scanRecordByteData[19]
        val name = ByteUtils.hexStr2Str(ByteUtils.byteArrayToHexStringLX(totalLoadData))
        (helper.getView<View>(R.id.tv_device_name) as TextView).text = name
        (helper.getView<View>(R.id.tv_epd_id) as TextView).text = item.bleAddress
        (helper.getView<View>(R.id.tv_signal) as TextView).text = "RSSI:" + "-" + item.rssi + "db"

    }
}
