package com.witstec.sz.badgeepaper.utils;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.witstec.sz.badgeepaper.R;

/**
 * 图片加载器
 */

public class GlideUtils {

    /**
     * 设置加载中以及加载失败图片
     *
     * @param mContext
     * @param path
     * @param mImageView
     */
    public static void loadImageView(Context mContext, Object path, ImageView mImageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.ic_def_iamge)
                .error(R.mipmap.ic_def_iamge);
        Glide.with(mContext)
                .load(path)
                .apply(options)
                .skipMemoryCache(true) // 不使用内存缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                .into(mImageView);
    }

    public static void loadImageViewSetDefImage(Context mContext, String path, ImageView mImageView,int defImage) {
        RequestOptions options = new RequestOptions()
                .placeholder(defImage)
                .error(defImage);
        Glide.with(mContext)
                .load(path)
                .apply(options)
                .skipMemoryCache(true) // 不使用内存缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                .into(mImageView);
    }

    public static void loadImageViewDef(Context mContext, Object path, ImageView mImageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.ic_def_iamge)
                .error(R.mipmap.ic_def_iamge);
        Glide.with(mContext)
                .load(path)
                .skipMemoryCache(true) // 不使用内存缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                .apply(options)
                .into(mImageView);
    }

    /**
     * 加载gif图片
     *
     * @param mContext
     * @param path
     * @param mImageView
     */
    public static void loadImageViewGif(Context mContext, String path, ImageView mImageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.ic_def_iamge)
                .error(R.mipmap.ic_def_iamge);
        Glide.with(mContext)
                .asGif()
                .load(path)
                .apply(options)
                .skipMemoryCache(true) // 不使用内存缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                .into(mImageView);
    }

    /**
     * 圆形图标
     *
     * @param mContext
     * @param path
     * @param mImageView
     */
    public static void cropCircleLoadImageView(Context mContext, String path, ImageView mImageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.mipmap.ic_user_def)
                .error(R.mipmap.ic_user_def)
                .skipMemoryCache(true) // 不使用内存缓存
                .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                .circleCrop();
        Glide.with(mContext)
                .load(path)
                .apply(options)
                .into(mImageView);
    }

    /**
     * 模糊图片
     *
     * @param mContext
     * @param path
     * @param mImageView
     */
//    public static void blurTransImageView(Context mContext, String path, ImageView mImageView) {
//        RequestOptions options = new RequestOptions()
//                .placeholder(R.drawable.placeholder)
//                .error(R.drawable.placeholder)
//                .transform(new BlurTransformation(70, 12));
//        Glide.with(mContext)
//                .load(path)
//                .apply(options)
//                .into(mImageView);
//    }

//    public static void saveImage(Context context, String url) {
//        Glide.with(context)
//                .asBitmap()
//                .load(url)
//                .listener(new RequestListener<Bitmap>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
//                                                Target<Bitmap> target,
//                                                boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Bitmap resource, Object model,
//                                                   Target<Bitmap> target,
//                                                   DataSource dataSource,
//                                                   boolean isFirstResource) {
//                        if (resource == null) {
//                            return true;
//                        }
//                        Observable.just(Utils.saveImageToGallery(
//                                context, resource))
//                                .subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .subscribe(path -> {
//                                    if (!path.equals("")) {
//                                        ToastUtils.show("已保存到图库");
//                                    } else {
//                                        ToastUtils.show("保存失败");
//                                    }
//                                }, throwable -> LogHelper.e(throwable.getMessage()));
//                        return false;
//                    }
//                }).submit();
//    }


}
