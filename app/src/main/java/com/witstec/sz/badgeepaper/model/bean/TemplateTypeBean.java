package com.witstec.sz.badgeepaper.model.bean;

public class TemplateTypeBean {

    private String name;
    private int typeId;
    private boolean isSelect;

    public TemplateTypeBean(String name, int typeId, boolean isSelect) {
        this.name = name;
        this.typeId = typeId;
        this.isSelect = isSelect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
