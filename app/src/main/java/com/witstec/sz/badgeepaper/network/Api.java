package com.witstec.sz.badgeepaper.network;

import com.google.gson.JsonObject;
import com.witstec.sz.badgeepaper.model.bean.AppUpdate;
import com.witstec.sz.badgeepaper.model.bean.BleTemplate;
import com.witstec.sz.badgeepaper.model.bean.BleTemplateType;
import com.witstec.sz.badgeepaper.model.bean.DeviceBean;
import com.witstec.sz.badgeepaper.model.bean.ImageBean;
import com.witstec.sz.badgeepaper.model.bean.InputTemplateBean;
import com.witstec.sz.badgeepaper.model.bean.LoginRequest;
import com.witstec.sz.badgeepaper.model.bean.ModelTypeBean;
import com.witstec.sz.badgeepaper.model.bean.RegisterRequest;
import com.witstec.sz.badgeepaper.model.bean.TemplateBean;
import com.witstec.sz.badgeepaper.model.bean.TokenBean;
import com.witstec.sz.badgeepaper.model.bean.user.UserMsg;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    /**
     * 用户注册
     * @return
     */
    @POST("/v1/user_register")
    Observable<Response<RegisterRequest>> register(@Body JsonObject json);

    /**
     * 获取模板类型列表
     *
     * @param
     * @return
     */
    @GET("/v1/templates/tags")
    Observable<Response<BleTemplateType>> getTags();


    /**
     * 获取设备类型
     *
     * @return
     */
    @GET("/v1/devices/size")
    Observable<Response<ModelTypeBean>> getDeviceTypes();


    /**
     * 用户登录
     *
     * @return
     */
    @POST("/v1/user_auth")
    Observable<Response<LoginRequest>> login(@Body JsonObject json);

    /**
     * 刷新Token
     * @param json
     * @return
     */
    @POST("/v1/user/token/refresh")
    Observable<Response<TokenBean>> tokenRefresh(@Body JsonObject json);


    /**
     * 获取设备列表
     * @return
     */
    @GET("/v1/bindings")
    Observable<Response<DeviceBean>> getBindList(@Query("tags") String tags, @Query("search") String search, @Query("page") String page);

    /**
     * 用户绑定设备---mac
     * @return
     */
    @POST("/v1/bind_mac")
    Observable<Response<ResponseBody>> addDevice(@Body JsonObject json);

    /**
     * 创建模板
     * @param json
     * @return
     */
    @POST("/V1/admin_tems")
    Observable<Response<ResponseBody>> createTemplate(@Body JsonObject json);

    /**
     * 模板删除
     *
     * @param
     * @return
     */
    @DELETE("/V1/admin_tems/{temp_id}")
    Observable<Response<ResponseBody>> modalDel(@Path("temp_id") String temp_id,@Query("key") String key);
    /**
     * 上传用户设备信息
     *
     * @param json
     * @return
     */
    @POST("/v1/user/{user_id}/client_info")
    Observable<Response<ResponseBody>> uploadUserDeviceMsg(@Path("user_id") String user_id, @Body JsonObject json);


    /**
     * 修改模板
     */
    @PUT("/V1/admin_tems/{temp_id}")
    Observable<Response<ResponseBody>> updateTemplate(@Path("temp_id") String temp_id,@Body JsonObject json);

    /**
     * 获取模板列表
     *
     * @return
     */
    @GET("/V1/admin_tems/tags/{tags}")
    Observable<Response<BleTemplate>> getTemplateList(@Path("tags") int tags, @Query("page") int page,
                                                      @Query("search") String search, @Query("lang") String lang);

    /**
     * 获取模板列表，筛选
     * @param tags
     * @param page
     * @return
     */
    @GET("/V1/admin_tems/tags/{tags}")
    Observable<Response<BleTemplate>> getTemplateListFilter(@Path("tags") int tags, @Query("page") int page);

    /**
     * 获取模板插入数据
     *
     * @param device_id
     * @return
     */
    @GET("/v1/devices/{device_id}/template")
    Observable<Response<InputTemplateBean>> getTemplateInputData(@Path("device_id") String device_id);

    /**
     * 获取单个模板信息
     * @param temp_id
     * @return
     */
    @GET("/v1/templates/{temp_id}")
    Observable<Response<TemplateBean>> getTemplate(@Path("temp_id") String temp_id);

    /**
     * 保存模板输入框数据
     *
     * @param device_id
     * @param json
     * @return
     */
    @POST("/v1/devices/{device_id}/template")
    Observable<Response<ResponseBody>> saveTemplateInputData(@Path("device_id") String device_id, @Body JsonObject json);


    /**
     * 获取验证码
     * @return
     */
    @POST("/v1/user/password/forgot")
    Observable<Response<ResponseBody>> getNewPwdCode(@Body JsonObject jsonObject);


    /**
     * 找回密码
     *
     * @param jsonObject
     * @return
     */
    @POST("/v1/user/password/foundback")
    Observable<Response<ResponseBody>> foundback(@Body JsonObject jsonObject);

    /**
     * 用户退出
     * @return
     */
    @DELETE("/v1/user_cancelled")
    Observable<Response<ResponseBody>> logout();

    /**
     * 用户修改密码
     *
     */
    @PUT("/v1/user/password/reset")
    Observable<Response<ResponseBody>> changePwd(@Body JsonObject jsonObject);

    /**
     * 获取用户个人信息
     *
     * @param
     * @return
     */
    @GET("/v1/user/{user_id}")
    Observable<Response<UserMsg>> getUserInfo(@Path("user_id") String user_id);


    /**
     * 修改用户个人信息
     *
     * @param
     * @return
     */
    @PUT("/v1/user/{user_id}")
    Observable<Response<ResponseBody>> updUserInfo(
            @Path("user_id") String user_id, @Body JsonObject jsonObject);

    /**
     * app用户反馈
     *
     * @return
     */
    @POST("/v1/feedback")
    Observable<Response<ResponseBody>> setFeedback(
            @Body JsonObject jsonObject);

    /**
     * 修改头像
     *
     * @param
     */
    @POST("/v1/user/avatar/upload")
    Observable<Response<ResponseBody>> uploadAvatar(
            @Body JsonObject jsonObject);

    /**
     * 上传日志
     *
     * @param
     * @return
     */
    @POST("/v1/logs")
    Observable<Response<ImageBean>> uploadLogs(@Body JsonObject jsonObject);

    /**
     * 检查版本更新
     *
     * @return
     */
    @FormUrlEncoded
    @POST("https://ea.witstec.com/Card/checkApp")
    Observable<Response<AppUpdate>> appCheck(@Field("type") int type,@Field("lang") String lang);

    /**
     * ota检查更新
     *
     * @param
     * @return
     */
    @GET("/v1/ota/check/{device_id}/type/{type}")
    Observable<Response<ResponseBody>> otaCheck(@Path("device_id") String device_id, @Path("type") String type);

    /**
     * app检查更新
     *
     * @param
     * @param lang
     * @return
     */
    @GET("/v1/upgrade/check")
    Observable<Response<AppUpdate>> appUpgrade(@Query("lang") String lang);

    /**
     * 修改设备信息
     *
     * @param
     * @return
     */
    @PUT("/v1/bindings/{device_id}")
    Observable<Response<ResponseBody>> updDeviceInfo(
            @Path("device_id") String device_id,
            @Body JsonObject json);

    /**
     * 固件版本升级结果返回
     *
     * @param
     * @return
     */
    @POST("/v1/ota/update_result/{device_id}")
    Observable<Response<ResponseBody>> updateOta(
            @Path("device_id") String device_id,
            @Body JsonObject json);


    /**
     * 用户解除绑定设备
     *
     * @param
     * @param device_id 设备mac地址    12位
     * @return
     */
    @DELETE("/v1/bindings/{device_id}")
    Observable<Response<ResponseBody>> unbind(
            @Path("device_id") String device_id);


}
