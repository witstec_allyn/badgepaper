package com.witstec.sz.badgeepaper.draw.bean;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Region;

import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.ExtKt;
import com.witstec.sz.badgeepaper.draw.ui.view.CanvasView;
import com.witstec.sz.badgeepaper.utils.ZXingUtils;

import java.util.ArrayList;
import java.util.List;


public class Pel extends BasePel {
    /**
     * 区域
     */
    public Region region;
    /**
     * /画笔
     */
    public Paint paint;
    /**
     * 路径
     */
    public Path path;
    /**
     * 类型
     * path类型：10:自由线 11：矩型 12：塞尔曲线 13：圆 14：直线 15：多重折线 16：多边形
     * 文本类型：20
     * 照片类型：30
     */
    public int type;
    /**
     * 组成path的所有点
     */
    public List<PointF> pathPointFList;
    /**
     * 文本
     */
    public Text text;
    /**
     * 插画
     */
    public Picture picture;

    /**
     * 是否封闭
     */
    public boolean closure;

    //构造（实际使用时应该把Pel构造成Pel(path region paint name)的形式，形参均在外部都已经定义好了的）
    public Pel() {
        pathPointFList = new ArrayList<>();
        path = new Path();
        region = new Region();
        paint = new Paint(Paint.DITHER_FLAG);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_55);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        centerPoint = new PointF();
        beginPoint = new PointF();
        bottomRightPointF = new PointF();
        text = null;
        picture = null;
    }

    //深拷贝
    public Pel clone() {
        Pel pel = new Pel();
        (pel.path).set(path);
        (pel.region).set(new Region(region));
        //敲定画笔粗细
        if (pnWidth == 1) {
            paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        } else if (pnWidth == 2) {
            paint.setStrokeWidth(Constants.PAINT_WIDTH_M);
        } else if (pnWidth == 3) {
            paint.setStrokeWidth(Constants.PAINT_WIDTH_L);
        } else {
            paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        }
        (pel.paint).set(new Paint(paint));
        if (text != null) {
            paint.setTextSize(text.textSize);
            pel.textSize = text.textSize;
            pel.text = new Text(text.getContent(), text.isVertical(), text.textSize);
        }
        if (picture != null) {
            pel.picture = new Picture(picture.createContent());
            pel.picture.createContent();
        }
        pel.type = type;
        pel.pathPointFList = pathPointFList;
        pel.bottomRightPointF = bottomRightPointF;
        pel.centerPoint = new PointF(centerPoint.x, centerPoint.y);
        pel.beginPoint = new PointF(beginPoint.x, beginPoint.y);
        pel.transDx = transDx;
        pel.transDy = transDy;
        pel.scale = scale;
        pel.angle = angle;
        int color;
        if (paintColor == Color.RED) {
            color = 1;
        } else if (paintColor == Color.WHITE) {
            color = 2;
        } else {
            color = 0;
        }
        pel.paintColor = color;
        pel.pnWidth = pnWidth;
        pel.fillColor = fillColor;
        pel.image_effect_size = image_effect_size;
        pel.dotted_line = dotted_line;
        pel.closure = closure;
        return pel;
    }

    /**
     * 在画布上作画
     *
     * @param canvas
     */
    public void drawObject(Canvas canvas) {
        //文本图元
        if (text != null) {
            if (type == 31) {
                String content = text.getContent();
                if (!content.isEmpty()) {
                    Bitmap bitmap = ZXingUtils.creatBarcode(App.Companion.getInstance(), content,
                            Constants.BAR_W, Constants.BAR_H, false);
                    canvas.save();
                    canvas.translate(transDx, transDy);
                    canvas.scale(scale, scale, centerPoint.x, centerPoint.y);
                    canvas.rotate(angle, centerPoint.x, centerPoint.y);
                    Rect rect = new Rect((int) beginPoint.x, (int) beginPoint.y, (int) bottomRightPointF.x, (int) bottomRightPointF.y);
                    canvas.drawBitmap(bitmap, null, rect, CanvasView.drawPicturePaint);
                    canvas.restore();
                }
            } else if (type == 32) {
                String content = text.getContent();
                if (!content.isEmpty()) {
                    Bitmap bitmap = ZXingUtils.createQRImage(content, Constants.QR_W, Constants.QR_H);
                    canvas.save();
                    canvas.translate(transDx, transDy);
                    canvas.scale(scale, scale, centerPoint.x, centerPoint.y);
                    canvas.rotate(angle, centerPoint.x, centerPoint.y);
                    assert bitmap != null;
                    Rect rect = new Rect((int) beginPoint.x, (int) beginPoint.y, (int) bottomRightPointF.x, (int) bottomRightPointF.y);
                    canvas.drawBitmap(bitmap, null, rect, CanvasView.drawPicturePaint);
                    canvas.restore();
                }
            } else {
                canvas.save();
                canvas.translate(transDx, transDy);
                canvas.scale(scale, scale, centerPoint.x, centerPoint.y);
                canvas.rotate(angle, centerPoint.x, centerPoint.y);
                Paint textPaint = text.getPaint();
                if (paintColor == 1) {
                    textPaint.setColor(Color.RED);
                } else if (paintColor == 0) {
                    textPaint.setColor(Color.BLACK);
                } else {
                    textPaint.setColor(Color.WHITE);
                }
                switch (textSize) {
                    case 1:
                        textPaint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_45 * 0.5f);
                        break;
                    case 2:
                        textPaint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_55 * 0.5f);
                        break;
                    case 3:
                        textPaint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_65 * 0.5f);
                        break;
                    case 4:
                        textPaint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_75 * 0.5f);
                        break;
                    case 5:
                        textPaint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_85 * 0.5f);
                        break;
                }

                textPaint.setTextAlign(Paint.Align.CENTER);
                if (!text.isVertical()) {
                    Paint.FontMetrics fontMetrics = textPaint.getFontMetrics();
                    float distance = (fontMetrics.bottom - fontMetrics.top) / 2 - fontMetrics.bottom;
                    float baseline = centerPoint.y + distance;
                    canvas.drawText(text.getContent(), centerPoint.x, baseline, textPaint);
                } else {
                    int heightPer = ((region.getBounds().bottom - region.getBounds().top)) / text.getContent().length();
                    char[] chars = text.getContent().toCharArray();
                    for (int i = chars.length - 1; i >= 0; i--) {
                        canvas.drawText(chars[i] + "", centerPoint.x, region.getBounds().bottom - heightPer * (chars.length - 1 - i), textPaint);
                    }
                }
                canvas.restore();
            }
        }

        //图标图元
        else if (picture != null) {
            canvas.save();
            canvas.translate(transDx, transDy);
            canvas.scale(scale, scale, centerPoint.x, centerPoint.y);
            canvas.rotate(angle, centerPoint.x, centerPoint.y);
            Rect rect = new Rect((int) beginPoint.x, (int) beginPoint.y, (int) bottomRightPointF.x, (int) bottomRightPointF.y);
//            if (isOriginalImage) {
//                canvas.drawBitmap(picture.createContent(),
//                        null, rect, CanvasView.drawPicturePaint);
//            } else {
//
//            }
            canvas.drawBitmap(picture.createContent(),
                    null, rect, CanvasView.drawPicturePaint);
            canvas.restore();
        }
        //路径
        else {
            if (dotted_line == 1) {
                paint.setPathEffect(new DashPathEffect(new float[]{30, 30}, 1));
            } else {
                paint.setPathEffect(new CornerPathEffect(10));
            }
            if (fillColor == 1) {
                paint.setColor(Color.RED);
                paint.setStyle(Paint.Style.FILL);
            } else if (fillColor == 0) {
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.FILL);
            } else if (fillColor == 2) {
                paint.setColor(Color.WHITE);
                paint.setStyle(Paint.Style.FILL);
            } else {
                paint.setStyle(Paint.Style.STROKE);
            }

            //敲定画笔粗细
            if (pnWidth == 1) {
                paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
            } else if (pnWidth == 2) {
                paint.setStrokeWidth(Constants.PAINT_WIDTH_M);
            } else if (pnWidth == 3) {
                paint.setStrokeWidth(Constants.PAINT_WIDTH_L);
            } else {
                paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
            }
            canvas.save();
            canvas.translate(transDx, transDy);
            canvas.scale(scale, scale, centerPoint.x, centerPoint.y);
            canvas.rotate(angle, centerPoint.x, centerPoint.y);
            canvas.drawPath(path, paint);
            canvas.restore();
        }

    }
}
