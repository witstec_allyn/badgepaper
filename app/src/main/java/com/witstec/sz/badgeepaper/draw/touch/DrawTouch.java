package com.witstec.sz.badgeepaper.draw.touch;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;

import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.draw.bean.Pel;
import com.witstec.sz.badgeepaper.draw.step.DrawPelStep;
import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity;
import com.witstec.sz.badgeepaper.draw.ui.view.CanvasView;


/**
 * 画图元触摸类
 */
public class DrawTouch extends Touch {

    protected PointF downPoint;
    protected PointF movePoint;
    protected Pel newPel;
    protected static Paint paint;
    /**
     * 旋转角度
     */
    protected static int angle=0;
    /**
     * 是否为虚线 1-虚线  0=实线
     */
    protected static int dotted_line=0;

    /**
     * 是否填充  不填充=-1，填充红色=1，填充黑色=0
     */
    protected static int fillColor=-1;
    /**
     * 画笔粗细  1-2-3
     */
    protected static int pnWidth = 1;

    public static int getPnWidth() {
        return pnWidth;
    }

    public static void setPnWidth(int pnWidth) {
        DrawTouch.pnWidth = pnWidth;
    }

    public static int getAngle() {
        return angle;
    }

    public static void setAngle(int angle) {
        DrawTouch.angle = angle;
    }

    public static int getDotted_line() {
        return dotted_line;
    }

    public static void setDotted_line(int dotted_line) {
        DrawTouch.dotted_line = dotted_line;
    }

    public static int getFillColor() {
        return fillColor;
    }

    public static void setFillColor(int fillColor) {
        DrawTouch.fillColor = fillColor;
    }

    static {
        // 第一个对象创建时构造初始画笔
        paint = new Paint(Paint.DITHER_FLAG);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_55);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    protected DrawTouch() {
        super();

        downPoint = new PointF();
        movePoint = new PointF();
    }

    @Override
    public void down1() {
        super.down1();
        downPoint.set(curPoint);
    }

    public void move() {
        super.move();
    }

    @Override
    public void up() {
        //轻敲屏幕不算
        if (!isNeedToOpenTools() && newPel != null) {
            Pel pel = newPel.clone();
            newPel = null;
            //敲定该图元的路径
            (pel.region).setPath(pel.path, clipRegion);
            Rect boundRect = pel.region.getBounds();
            //敲定区域中心点
            (pel.centerPoint).set((boundRect.right + boundRect.left) / 2, (boundRect.bottom + boundRect.top) / 2);
            //敲定区域右下角
            (pel.bottomRightPointF).set(boundRect.right, boundRect.bottom);
            //敲定画笔
            (pel.paint).set(paint);
            int color;
            if (paint.getColor() == Color.RED) {
                color = 1;
            } else if (paint.getColor() == Color.BLACK) {
                color = 0;
            } else {
                color = 2;
            }
            //敲定画笔颜色
            (pel.paintColor) = color;

            (pel.angle) = angle;

            (pel.dotted_line) = dotted_line;

            (pel.fillColor) = fillColor;
            //敲定画笔粗细
            pel.pnWidth = pnWidth;

            /**
             * 更新操作
             */
            //1.将新画好的图元存入图元链表中
            CanvasView.getPelList().add(pel);

            //2.包装好当前步骤 内的操作
            CanvasView.getUndoStack().push(new DrawPelStep(pel));//将该“步”压入undo栈

            //3.更新重绘位图
            CanvasView.setSelectedPel(selectedPel = null);//刚才画的图元失去焦点
            updateSavedBitmap();//重绘位图
        }
    }

    public boolean isNeedToOpenTools() {
        if (dis < 10f) {
            dis = 0;
            DrawingMainActivity.openOrCloseTools();
            control = true;
            return true;
        } else {
            dis = 0;
            return false;
        }
    }

    //获得当前画笔
    public static Paint getCurPaint() {
        return paint;
    }


}
