package com.witstec.sz.badgeepaper.model.bean;

import com.blakequ.bluetooth_manager_lib.device.BluetoothLeDevice;

import java.util.*;

public class BluetoothLeDeviceStore {

    private final Map<String, BluetoothLeDevice> mDeviceMap;


    public BluetoothLeDeviceStore() {
        mDeviceMap = new HashMap<>();
    }

    public void addDevice(final BluetoothLeDevice device) {
        //if (mDeviceMap.containsKey(device.getAddress())) {
        //    mDeviceMap.get(device.getAddress()).updateRssiReading(device.getTimestamp(), device.getRssi());
        //} else {
        //    mDeviceMap.put(device.getAddress(), device);
        //}
        mDeviceMap.put(device.getAddress(), device);
    }

    public void clear() {
        mDeviceMap.clear();
    }


    public List<BluetoothLeDevice> getDeviceList() {
        final List<BluetoothLeDevice> methodResult = new ArrayList<>(mDeviceMap.values());

        Collections.sort(methodResult, new Comparator<BluetoothLeDevice>() {

            @Override
            public int compare(final BluetoothLeDevice arg0, final BluetoothLeDevice arg1) {
                return arg0.getAddress().compareToIgnoreCase(arg1.getAddress());
            }
        });

        return methodResult;
    }

    public int size(){
        return mDeviceMap.size();
    }

}
