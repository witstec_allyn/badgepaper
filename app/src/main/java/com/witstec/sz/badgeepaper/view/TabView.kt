package com.witstec.sz.badgeepaper.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.utils.Utils

class TabView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : View(context, attrs, defStyleAttr) {

    private var mTabs: ArrayList<String> = arrayListOf("")
    private var selectedPosition = 0
    private var mWidth = 0
    private var mHeight = 0
    private var corner = Utils.dp2px(context, 20f).toFloat()
    private val mPaint = Paint()

    //字号
    private var textSize = Utils.dp2px(context, 15f).toFloat()
    //主色调
    private var mainColor = context.resources.getColor(R.color.colorPrimary)
    //字的颜色
    private var textColor = context.resources.getColor(R.color.white)
    //选中的颜色
    private var selectColor = Color.parseColor("#ccffffff")

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context) : this(context, null)

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TabView)
        mainColor =
            typedArray.getColor(R.styleable.TabView_tabMainColor, context.resources.getColor(R.color.colorPrimary))
        textColor = typedArray.getColor(R.styleable.TabView_tabTextColor, context.resources.getColor(R.color.white))
        selectColor = typedArray.getColor(R.styleable.TabView_tabSelectColor, Color.parseColor("#ccffffff"))
        textSize = typedArray.getDimension(R.styleable.TabView_tabTextSize, Utils.dp2px(context, 15f).toFloat())
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mWidth = MeasureSpec.getSize(widthMeasureSpec)
        mHeight = MeasureSpec.getSize(heightMeasureSpec)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val rectF = RectF(0f, 0f, mWidth.toFloat(), mHeight.toFloat())
        //画背景
        mPaint.isAntiAlias = true
        mPaint.color = mainColor
        mPaint.style = Paint.Style.FILL
        canvas!!.drawRoundRect(rectF, corner, corner, mPaint)
        //画圆角矩形边框
        mPaint.reset()
        mPaint.color = textColor
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeWidth = Utils.dp2px(context, 1f).toFloat()
        mPaint.isAntiAlias = true
        canvas.drawRoundRect(rectF, corner, corner, mPaint)

        //画每一个字
        mPaint.reset()
        mPaint.color = textColor
        mPaint.isAntiAlias = true
        mPaint.textSize = textSize

        val count = mTabs.size
        if (count == 0) {
            return
        } else {
            //画字
            for (i in mTabs.indices) {
                drawText(i, canvas, mPaint)
            }
        }
        //每个item的宽度
        val itemWidth = mWidth / mTabs.size.toFloat()
        //画分隔线
        mPaint.strokeWidth = Utils.dp2px(context, 0.5f).toFloat()
        mTabs.indices
            .filter { it != 0 }
            .forEach {
                canvas.drawLine(it * itemWidth, 0f, it * itemWidth, mHeight.toFloat(), mPaint)
            }

        mPaint.reset()
        mPaint.color = selectColor
//        mPaint.alpha = 0xcc
        mPaint.style = Paint.Style.FILL

        //画选中状态
        if (count == 0 || count == 1) {
            return
        } else {
            if (selectedPosition == 0) {
                //第一个被选中，把左半边画上圆角
                canvas.save()
                //只画左半边
                canvas.clipRect(RectF(0f, 0f, itemWidth / 2, mHeight.toFloat()))
                //圆角矩形
                val rectF = RectF(0f, 0f, itemWidth, mHeight.toFloat())
                canvas.drawRoundRect(rectF, corner, corner, mPaint)
                canvas.restore()

                canvas.drawRect(RectF(itemWidth / 2, 0f, itemWidth, mHeight.toFloat()), mPaint)
            } else if (selectedPosition == count - 1) {
                //最后一个被选中，右半边画上圆角
                canvas.save()
                //只画右半边
                canvas.clipRect(RectF((selectedPosition) * itemWidth, 0f, count * itemWidth, mHeight.toFloat()))
                //圆角矩形
                val rectF = RectF(selectedPosition * itemWidth, 0f, count * itemWidth, mHeight.toFloat())
                canvas.drawRoundRect(rectF, corner, corner, mPaint)
                canvas.restore()
                canvas.drawRect(
                    RectF(
                        selectedPosition * itemWidth,
                        0f,
                        (selectedPosition + 0.5f) * itemWidth,
                        mHeight.toFloat()
                    ), mPaint
                )
            } else {
//                //选中中间的
                val rect = Rect(
                    (itemWidth * selectedPosition).toInt()
                    , 0
                    , (itemWidth * (selectedPosition + 1)).toInt()
                    , mHeight
                )
                canvas.drawRect(rect, mPaint)
            }
        }

        //画选中的条目的文字
        mPaint.reset()
        mPaint.color = mainColor
        mPaint.isAntiAlias = true
        mPaint.textSize = textSize
        drawText(selectedPosition, canvas, mPaint)

    }

    private fun drawText(position: Int, canvas: Canvas, paint: Paint) {
        val text = mTabs[position]
        val textWidth = paint.measureText(text)
        val itemWidth = mWidth / mTabs.size
        //X起始坐标，X轴居中
        val startX = (itemWidth - textWidth) / 2 + itemWidth * position
        //Y起始坐标，Y轴居中
        val startY = mHeight / 2 + (Math.abs(mPaint.fontMetrics.ascent) - mPaint.fontMetrics.descent) / 2

        canvas.drawText(text, startX, startY, paint)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event!!.action == MotionEvent.ACTION_DOWN) {
            if (mTabs.isNotEmpty()) {
                selectedPosition = (event.x / (mWidth / mTabs.size)).toInt()
                invalidate()
                if (listener != null)
                    listener!!.onChange(selectedPosition)
            }
        }
        return super.onTouchEvent(event)
    }

    fun setSelectedPosition(selectedPosition: Int) {
        this.selectedPosition = selectedPosition
        invalidate()
    }

    private var listener: OnSelectedItemChange? = null

    //设置切换监听
    fun setOnSelectedItemChange(listener: OnSelectedItemChange) {
        this.listener = listener
    }

    fun setTabs(tabs: ArrayList<String>) {
        mTabs = tabs
        invalidate()
    }

    interface OnSelectedItemChange {
        fun onChange(select: Int)
    }
}