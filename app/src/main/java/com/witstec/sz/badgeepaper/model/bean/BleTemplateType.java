package com.witstec.sz.badgeepaper.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class BleTemplateType implements Parcelable {

    private ArrayList<TagsBean> tags;

    public ArrayList<TagsBean> getTags() {
        return tags;
    }

    public void setTags(ArrayList<TagsBean> tags) {
        this.tags = tags;
    }

    public static class TagsBean implements Parcelable {
        /**
         * id : 1
         * name : ESL
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.name);
        }

        public TagsBean() {
        }

        protected TagsBean(Parcel in) {
            this.id = in.readString();
            this.name = in.readString();
        }

        public static final Creator<TagsBean> CREATOR = new Creator<TagsBean>() {
            @Override
            public TagsBean createFromParcel(Parcel source) {
                return new TagsBean(source);
            }

            @Override
            public TagsBean[] newArray(int size) {
                return new TagsBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.tags);
    }

    public BleTemplateType() {
    }

    protected BleTemplateType(Parcel in) {
        this.tags = new ArrayList<TagsBean>();
        in.readList(this.tags, TagsBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<BleTemplateType> CREATOR = new Parcelable.Creator<BleTemplateType>() {
        @Override
        public BleTemplateType createFromParcel(Parcel source) {
            return new BleTemplateType(source);
        }

        @Override
        public BleTemplateType[] newArray(int size) {
            return new BleTemplateType[size];
        }
    };
}
