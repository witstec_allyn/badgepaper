package com.witstec.sz.badgeepaper.ui.adapter


import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

import java.util.ArrayList

class ViewPagerAdapter(private val views: ArrayList<View>) : PagerAdapter() {

    override fun getCount(): Int {
        return views.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        container.addView(views[position])
        return views[position]
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(views[position])
    }
}