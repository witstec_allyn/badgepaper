package com.witstec.sz.badgeepaper.manage;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Integer.toHexString;

public class BitmapBleBmpManagement {

    /**
     * 发送图片
     *
     * @param bitmap 要转换的bitmap
     */
    public static byte[] getBmpProtocolData(Bitmap bitmap, boolean isRed) {
        int w = bitmap.getWidth(), h = bitmap.getHeight();
        int[] pixels = new int[w * h];
        bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
        return addBMP_RGB_888(pixels, w, h, isRed);
    }

    public static byte[] getBmpProtocolDataNoCompression(Bitmap bitmap, boolean isRed) {
        int w = bitmap.getWidth(), h = bitmap.getHeight();
        int[] pixels = new int[w * h];
        bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
        return addBMP_RGB_888_No_Compression(pixels, w, h, isRed);
    }

    /**
     * 发送每一张图片
     */
    public static byte[] getBmpProtocolEach(byte[] eachByte, int packageSize, int toLean, int index) {
        ByteBuffer loadData = ByteBuffer.allocate(eachByte.length + 6);
        loadData.put(IntSplitToByteReverseA(packageSize, 2));
        loadData.put(IntSplitToByteReverseA(index + 1, 2));
        loadData.put(IntSplitToByteReverseA(toLean, 2));
        loadData.put(eachByte);
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        int instruction = 19;
        byte[] verification = IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length + totalLoadData.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadData, 0, buffer, 7 + verification.length, totalLoadData.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 发送保存数据
     */
    @NonNull
    public static byte[] getBmpDatacolEach(@NonNull byte[] eachByte) {
        ByteBuffer loadData = ByteBuffer.allocate(eachByte.length + 2);
        loadData.put(IntSplitToByteReverseA(eachByte.length, 2));
        loadData.put(eachByte);
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        int instruction = 23;
        byte[] verification = IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length + totalLoadData.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadData, 0, buffer, 7 + verification.length, totalLoadData.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * OTA数据擦除
     */
    @NonNull
    public static byte[] OTADataClear(int instruction, @NonNull int size) {
        ByteBuffer loadData = ByteBuffer.allocate(4);
        loadData.put(IntSplitToByteReverseA(size, 4));
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        byte[] verification = IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length + totalLoadData.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadData, 0, buffer, 7 + verification.length, totalLoadData.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 发送OTA数据
     *
     * @param eachByte
     * @param packageSize
     * @param index
     * @return
     */
    public static byte[] sendOtaData(int instruction, byte[] eachByte, int packageSize, int index) {
        ByteBuffer loadData = ByteBuffer.allocate(eachByte.length + 6);
        loadData.put(IntSplitToByteReverseA(packageSize, 2));
        loadData.put(IntSplitToByteReverseA(index+1, 2));
        loadData.put(IntSplitToByteReverseA(eachByte.length, 2));
        loadData.put(eachByte);
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        byte[] verification = IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length + totalLoadData.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadData, 0, buffer, 7 + verification.length, totalLoadData.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    public static byte[] sendOTACheck(int instruction, int crc32) {
        ByteBuffer loadData = ByteBuffer.allocate(4);
        loadData.put(IntSplitToByteReverseA(crc32, 4));
        loadData.flip();
        byte[] totalLoadData = new byte[loadData.remaining()];
        loadData.get(totalLoadData);
        byte[] verification = IntSplitToByte(IntSplit(totalLoadData, totalLoadData.length, (short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length + totalLoadData.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(totalLoadData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(totalLoadData, 0, buffer, 7 + verification.length, totalLoadData.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }


    /**
     * 发送命令不带负载
     *
     * @param instruction
     * @return
     */
    public static byte[] getBmpProtocolDataChange(int instruction) {
        byte[] verification = IntSplitToByte(IntSplitChange((short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(0, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, 7 + verification.length, 1);
        return buffer;
    }

    /**
     * 一条命令加数据
     *
     * @param instruction
     * @param byteData
     * @return
     */
    public static byte[] setOteVersion(int instruction, byte[] byteData) {
        byte[] verification = IntSplitToByte(IntSplit(byteData, byteData.length, (short) instruction), 1);
        byte[] buffer = new byte[8 + verification.length + byteData.length];
        System.arraycopy(IntSplitToByte(133, 1), 0, buffer, 0, 1);
        System.arraycopy(IntSplitToByte(byteData.length, 4), 0, buffer, 1, 4);
        System.arraycopy(IntSplitToByte(instruction, 2), 0, buffer, 5, 2);
        System.arraycopy(verification, 0, buffer, 7, verification.length);
        System.arraycopy(byteData, 0, buffer, 7 + verification.length, byteData.length);
        System.arraycopy(IntSplitToByte(149, 1), 0, buffer, buffer.length - 1, 1);
        return buffer;
    }

    /**
     * 负载字节相加
     *
     * @param escape
     * @return
     */
    private static short LiabilitySuperimposed(byte[] escape) {
        short resultInt = 0;
        for (byte b : escape) {
            resultInt += b;
            resultInt &= 0xFF;
        }
        return resultInt;
    }

    /**
     * 字符串拆分
     *
     * @return
     */
    private static int IntSplit(int len, short command) {
//        short cks = LiabilitySuperimposed(escape);
        short cks = 0;
        cks += (command & 0xff);
        cks &= 0xFF;

        cks += ((command & 0xff00) >> 8);
        cks &= 0xFF;

        cks += (len & 0xff);
        cks &= 0xFF;

        cks += ((len & 0xff00) >> 8);
        cks &= 0xFF;

        cks += ((len & 0xff0000) >> 16);
        cks &= 0xFF;

        cks += ((len & 0xff000000) >> 24);
        cks &= 0xFF;
        return cks % 256;
    }

    private static int IntSplit(byte[] byteData, int len, short command) {
        short cks = LiabilitySuperimposed(byteData);
        cks += (command & 0xff);
        cks &= 0xFF;

        cks += ((command & 0xff00) >> 8);
        cks &= 0xFF;

        cks += (len & 0xff);
        cks &= 0xFF;

        cks += ((len & 0xff00) >> 8);
        cks &= 0xFF;

        cks += ((len & 0xff0000) >> 16);
        cks &= 0xFF;

        cks += ((len & 0xff000000) >> 24);
        cks &= 0xFF;
        return cks % 256;
    }

    private static int IntSplitChange(short command) {
        short cks = 0;
        cks += (command & 0xff);
        cks &= 0xFF;

        cks += ((command & 0xff00) >> 8);
        cks &= 0xFF;

        int quyu = cks % 256;

        return quyu;
    }

    /**
     * 转成一位图的bmp，返回字节数组
     *
     * @param data
     * @param w
     * @param h
     * @return
     */
    private static byte[] addBMP_RGB_888(int[] data, int w, int h, boolean isRed) {
        int len = w * h;
        int bufflen = 0;
        byte[] tmp = new byte[3];
        int index = 0, bitindex = 1, indexZy = 0;

        if (w * h % 8 != 0)//将8字节变成1个字节,不足补0
        {
            bufflen = w * h / 8 + 1;
        } else {
            bufflen = w * h / 8;
        }
        if (bufflen % 4 != 0)//BMP图像数据大小，必须是4的倍数，图像数据大小不是4的倍数时用0填充补足
        {
            bufflen = bufflen + bufflen % 4;
        }

        byte[] buffer = new byte[bufflen];
        for (int i = len - 1; i >= w; i -= w) {
            // DIB文件格式最后一行为第一行，每行按从左到右顺序
            int end = i, start = i - w + 1;
            for (int j = start; j <= end; j++) {
                tmp[0] = (byte) (data[j]);
                tmp[1] = (byte) (data[j] >> 8);
                tmp[2] = (byte) (data[j] >> 16);

                String hex = "";
                for (int g = 0; g < tmp.length; g++) {
                    String temp = toHexString(tmp[g] & 0xFF);
                    if (temp.length() == 1) {
                        temp = "0" + temp;
                    }
                    hex = hex + temp;
                }

                if (bitindex > 8) {
                    index += 1;
                    bitindex = 1;
                }
                int i1 = buffer[index] | (0x01 << 8 - bitindex);
                if (isRed) {
                    if (hex.equals("0000ff") || hex.equals("ff0000")) {
                        buffer[index] = (byte) (i1);
                    } else {
                        buffer[index] = (byte) (buffer[index] | (0));
                    }
                } else {
                    if (hex.equals("000000")) {
                        buffer[index] = (byte) (buffer[index] | (0));
                    } else {
                        buffer[index] = (byte) i1;
                    }
                }
                bitindex++;
            }
        }
        return BitmapConvertor.BMGEnCode(buffer);
//        return buffer;
    }


    /**
     * 转成一位图的bmp，返回字节数组
     *
     * @param data
     * @param w
     * @param h
     * @return
     */
    private static byte[] addBMP_RGB_888_No_Compression(int[] data, int w, int h, boolean isRed) {
        int len = w * h;
        int bufflen = 0;
        byte[] tmp = new byte[3];
        int index = 0, bitindex = 1, indexZy = 0;

        if (w * h % 8 != 0)//将8字节变成1个字节,不足补0
        {
            bufflen = w * h / 8 + 1;
        } else {
            bufflen = w * h / 8;
        }
        if (bufflen % 4 != 0)//BMP图像数据大小，必须是4的倍数，图像数据大小不是4的倍数时用0填充补足
        {
            bufflen = bufflen + bufflen % 4;
        }

        byte[] buffer = new byte[bufflen];
        for (int i = len - 1; i >= w; i -= w) {
            // DIB文件格式最后一行为第一行，每行按从左到右顺序
            int end = i, start = i - w + 1;
            for (int j = start; j <= end; j++) {
                tmp[0] = (byte) (data[j]);
                tmp[1] = (byte) (data[j] >> 8);
                tmp[2] = (byte) (data[j] >> 16);

                String hex = "";
                for (int g = 0; g < tmp.length; g++) {
                    String temp = toHexString(tmp[g] & 0xFF);
                    if (temp.length() == 1) {
                        temp = "0" + temp;
                    }
                    hex = hex + temp;
                }

                if (bitindex > 8) {
                    index += 1;
                    bitindex = 1;
                }
                int i1 = buffer[index] | (0x01 << 8 - bitindex);
                if (isRed) {
                    if (hex.equals("0000ff") || hex.equals("ff0000")) {
                        buffer[index] = (byte) (i1);
                    } else {
                        buffer[index] = (byte) (buffer[index] | (0));
                    }
                } else {
                    if (hex.equals("000000")) {
                        buffer[index] = (byte) (buffer[index] | (0));
                    } else {
                        buffer[index] = (byte) i1;
                    }
                }
                bitindex++;
            }
        }
//        return BitmapConvertor.BMGEnCode(buffer);
        return buffer;
    }

    /**
     * 添加转义符
     *
     * @param buffer
     * @return
     */
//    private static byte[] addBMP_RGB_888_Escape(byte[] buffer) {
//        int indexZy = 0;
//        for (int i = 0; i < buffer.length; i++) {
//            if (buffer[i] == (byte) 0x85 || buffer[i] == (byte) 0x90 || buffer[i] == (byte) 0x95) {
//                indexZy++;
//            }
//        }
//        ByteBuffer bufferzy = ByteBuffer.allocate(buffer.length + indexZy);
//        int indexxb = 0;
//        while (indexxb < buffer.length) {
//            if (buffer[indexxb] == (byte) 0x85 || buffer[indexxb] == (byte) 0x90 || buffer[indexxb] == (byte) 0x95) {
//                bufferzy.put((byte) 0x90);
//                bufferzy.put(buffer[indexxb]);
//            } else {
//                bufferzy.put(buffer[indexxb]);
//            }
//            indexxb++;
//        }
//        bufferzy.flip();
//        byte[] valueArray = new byte[bufferzy.remaining()];
//        bufferzy.get(valueArray);
//        return valueArray;
//    }

    /**
     * int转byte，指定字节数
     *
     * @param intData
     * @param strLength
     * @return
     */
    public static byte[] IntSplitToByte(int intData, int strLength) {
        String str = Integer.toHexString(intData);
        int strLen = str.length();
        StringBuffer sb;
        while (strLen < strLength * 2) {
            sb = new StringBuffer();
            sb.append("0").append(str);// 左补0
            str = sb.toString();
            strLen = str.length();
        }
        byte[] buffer = new byte[strLength];
        int index = 0;
        for (int i = 0; i < strLength; i++) {
            byte[] bytes = StringToBytes(str.substring(index, index + 2));
            System.arraycopy(bytes, 0, buffer, i, bytes.length);
            index = index + 2;
        }
        return buffer;
    }

    public static byte[] IntSplitToByteReverseA(int intData, int strLength) {
        String str = Integer.toHexString(intData);
        int strLen = str.length();
        StringBuffer sb;
        while (strLen < strLength * 2) {
            sb = new StringBuffer();
            sb.append("0").append(str);// 左补0
            str = sb.toString();
            strLen = str.length();
        }
        byte[] buffer = new byte[strLength];
        int index = 0;
        for (int i = 0; i < strLength; i++) {
            byte[] bytes = StringToBytes(str.substring(index, index + 2));
            System.arraycopy(bytes, 0, buffer, i, bytes.length);
            index = index + 2;
        }
        return reverseA(buffer);
    }

    public static byte[] reverseA(byte[] arry) {
        int length = arry.length;
        byte[] re = new byte[length];
        for (int i = 0; i < length; i++) {
            re[length - 1 - i] = arry[i];
        }
        arry = re;
        return arry;
    }

    public static byte[] toStrByte(int intData) {
        byte[] buffer = new byte[4];
        buffer[3] = (byte) (intData >> 24 & 0xFF);
        buffer[2] = (byte) (intData >> 16 & 0xFF);
        buffer[1] = (byte) (intData >> 8 & 0xFF);
        buffer[0] = (byte) (intData & 0xFF);
        return buffer;
    }

    public static String IntSplitToString(String intData, int strLength) {
        int strLen = intData.length();
        StringBuffer sb = null;
        while (strLen < strLength) {
            sb = new StringBuffer();
            sb.append("0").append(intData);// 左补0
            intData = sb.toString();
            strLen = intData.length();
        }
        assert sb != null;
        String strBuff = sb.toString();
        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0; i < strBuff.length(); i++) {
            stringBuffer.append(strBuff.charAt(i));
            if (i < strBuff.length() - 1) {
                stringBuffer.append(",");
            }
        }
        return stringBuffer.toString();
    }

    /**
     * 将16进制字符串转换为byte[]
     *
     * @param str
     * @return
     */
    public static byte[] StringToBytes(String str) {
        if (str == null || str.trim().equals("")) {
            return new byte[0];
        }
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < str.length() / 2; i++) {
            String subStr = str.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }
        return bytes;
    }


    /**
     * 分包发送数据
     *
     * @param data 发送数据
     */
    @NonNull
    public static ArrayList<byte[]> writeEntity(@NonNull byte[] data, int size) {
        if (data == null) {
            return null;
        }
        ArrayList packDataList = new ArrayList();
        int index = 0;
        int runSize = 0;
        int lastDataSize;
        int length = data.length;
        while (index < length) {
            byte[] txBuffer = new byte[size];
            for (int i = 0; i < size; i++) {
                if (index < length) {
                    txBuffer[i] = data[index++];
                }
                runSize++;
            }
            if (length == index) {
                lastDataSize = size - (runSize - index);
                byte[] lastBuffer = new byte[lastDataSize];
                System.arraycopy(txBuffer, 0, lastBuffer, 0, lastDataSize);
                packDataList.add(lastBuffer);
            } else {
                packDataList.add(txBuffer);
            }
        }
        return packDataList;
    }

    public static List<byte[]> writeEntityOta(byte[] data, int size) {
        if (data == null) {
            return null;
        }
        List<byte[]> packDataList = new LinkedList<>();
        int index = 0;
        int runSize = 0;
        int lastDataSize;
        int length = data.length;
        while (index < length) {
            byte[] txBuffer = new byte[size];
            for (int i = 0; i < size; i++) {
                if (index < length) {
                    txBuffer[i] = data[index++];
                }
                runSize++;
            }
            if (length == index) {
                lastDataSize = size - (runSize - index);
                byte[] lastBuffer;
                if (lastDataSize < size) {
                    lastBuffer = new byte[size];
                } else {
                    lastBuffer = new byte[lastDataSize];
                }
                System.arraycopy(txBuffer, 0, lastBuffer, 0, lastDataSize);
                //自动补FF
//                if (String.valueOf(lastBuffer[lastDataSize - 1]).equals("80")) {
//                    lastBuffer[lastDataSize - 2] = (byte) 255;
//                    LogHelper.INSTANCE.i("witstec", "转换80 1");
//                }
//                if (String.valueOf(lastBuffer[lastDataSize - 2]).equals("80")) {
//                    lastBuffer[lastDataSize - 2] = (byte) 255;
//                    LogHelper.INSTANCE.i("witstec", "转换80 2");
//                }
                if (lastDataSize < size) {
                    for (int i = lastDataSize; i < size; i++) {
                        lastBuffer[i] = (byte) 0x00;
                    }
                }
                packDataList.add(lastBuffer);
            } else {
                packDataList.add(txBuffer);
            }
        }
        return packDataList;
    }


    /**
     * @param orientationDegree 0 - 360 范围
     * @return
     */
    public static Bitmap adjustPhotoRotation(Bitmap bm, final int orientationDegree) {

        Matrix m = new Matrix();
        m.setRotate(orientationDegree, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);

        try {
            Bitmap bm1 = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), m, true);

            return bm1;

        } catch (OutOfMemoryError ex) {
        }
        return null;
    }
}
