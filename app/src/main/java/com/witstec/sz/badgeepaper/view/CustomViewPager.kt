package com.witstec.sz.badgeepaper.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * author : Allyn
 * e-mail : 1839565349@qq.com
 * date   : 2019/4/815:53
 * desc   :
 */
class CustomViewPager : ViewPager {
    var isSlide = false

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun onTouchEvent(event: MotionEvent): Boolean {
        // 触摸事件不触发
        return if (this.isSlide) {
            super.onTouchEvent(event)
        } else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        // 不处理触摸拦截事件
        return if (this.isSlide) {
            super.onInterceptTouchEvent(event)
        } else false
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        // 分发事件，这个是必须要的，如果把这个方法覆盖了，那么ViewPager的子View就接收不到事件了
        return if (this.isSlide) {
            super.dispatchTouchEvent(event)
        } else super.dispatchTouchEvent(event)
    }

    fun setPagingEnabled(enabled: Boolean) {
        this.isSlide = enabled
    }
}