package com.witstec.sz.badgeepaper.model.bean;

public class TemplateBeanOld {


    public TemplateBeanOld(String email) {
        this.email = email;
    }

    /**
     * id : KjXn0FuoCiXBpKBR
     * type : BR029A
     * name : Drawing1.xml
     * base64 : /Public/Tem/publiced/KjXn0FuoCiXBpKBR.jpg
     * url : /Public/Tem/publiced/KjXn0FuoCiXBpKBR_Drawing1.xml
     * email : ta612@qq.com
     * logo :
     * i : 0
     */

    private String id;
    private String type;
    private String name;
    private String base64;
    private String url;
    private String email;
    private String logo;
    private int i;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
