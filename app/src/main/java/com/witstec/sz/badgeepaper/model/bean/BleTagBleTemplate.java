package com.witstec.sz.badgeepaper.model.bean;

public class BleTagBleTemplate {

    /**
     * id : 3
     * name : 医疗保健
     */

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
