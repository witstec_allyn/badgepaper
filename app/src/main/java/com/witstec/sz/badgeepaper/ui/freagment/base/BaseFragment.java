package com.witstec.sz.badgeepaper.ui.freagment.base;

import android.content.Intent;
import android.os.Build;

import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.R;
import com.witstec.sz.badgeepaper.ui.activity.MainActivity;
import com.witstec.sz.badgeepaper.utils.language.LanguageUtil;
import com.witstec.sz.badgeepaper.utils.language.SpUtil;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import java.util.Objects;

public class BaseFragment extends Fragment {

    private CompositeDisposable mCompositeDisposable;

    protected boolean isVisible;
    private MaterialDialog dialog = null;

    public CompositeDisposable getCompositeDisposable() {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = new CompositeDisposable();
        }

        return this.mCompositeDisposable;
    }

    public void addDisposable(Disposable disposable) {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = new CompositeDisposable();
        }

        this.mCompositeDisposable.add(disposable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (this.mCompositeDisposable != null) {
            this.mCompositeDisposable.dispose();
        }
    }

    public void showDialog() {
        dialog = new MaterialDialog.Builder(Objects.requireNonNull(getActivity()))
                .content(getString(R.string.loading_text))
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    public void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * 如果是7.0以下，我们需要调用changeAppLanguage方法，
     * 如果是7.0及以上系统，直接把我们想要切换的语言类型保存在SharedPreferences中即可
     * 然后重新启动MainActivity
     *
     * @param language
     */
    public void changeLanguage(String language) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            LanguageUtil.changeAppLanguage(App.Companion.getInstance(), language);
        }
        SpUtil.getInstance(getActivity()).putString(SpUtil.LANGUAGE, language);
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }

    /**
     * 在这里实现Fragment数据的缓加载.
     *
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            lazyLoad();
        } else {
            isVisible = false;
        }
    }

    protected void lazyLoad() {
    }

}
