package com.witstec.sz.badgeepaper.ui.adapter.holder;

import android.view.View;
import androidx.appcompat.widget.AppCompatEditText;
import com.github.nukc.recycleradapter.RecyclerHolder;
import com.witstec.sz.badgeepaper.R;
import com.witstec.sz.badgeepaper.model.bean.ImageBleMsg;

public class DeviceTextHolder extends RecyclerHolder<ImageBleMsg> {

    private AppCompatEditText device_barCode;

    public DeviceTextHolder(View itemView) {
        super(itemView);
        device_barCode = itemView.findViewById(R.id.ed_device_text);
    }

    @Override
    public void onBindView(final ImageBleMsg postsBean) {

    }

}