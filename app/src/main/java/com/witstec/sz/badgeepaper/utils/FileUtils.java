package com.witstec.sz.badgeepaper.utils;

import android.os.Environment;
import android.util.Log;

import java.io.*;

public class FileUtils {

    // 将字符串写入到文本文件中
    public static void writeTxtToFile(byte[] strcontent, String filePath) {
        //生成文件夹之后，再生成文件，不然会出错
        makeFilePath(filePath);

        String strFilePath = filePath;
        // 每次写入时，都换行写
        try {
            File file = new File(strFilePath);
            if (!file.exists()) {
                Log.d("TestFile", "Create the file:" + strFilePath);
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            RandomAccessFile raf = new RandomAccessFile(file, "rwd");
            raf.seek(file.length());
            raf.write(strcontent);
            raf.close();
        } catch (Exception e) {
            Log.e("TestFile", "Error on write File:" + e);
        }
    }

    //生成文件
    public static File makeFilePath(String filePath) {
        File file = null;
        makeRootDirectory(filePath);
        try {
            file = new File(filePath);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    //生成文件夹
    public static void makeRootDirectory(String filePath) {
        File file = null;
        try {
            file = new File(filePath);
            if (!file.exists()) {
                file.mkdir();
            }
        } catch (Exception e) {
            Log.i("error:", e + "");
        }
    }

    //读取指定目录下的所有TXT文件的文件内容
    public static String getFileContent(File file) {
        String content = "";
        if (!file.isDirectory()) {  //检查此路径名的文件是否是一个目录(文件夹)
            if (file.getName().endsWith("txt")) {//文件格式为""文件
                try {
                    InputStream instream = new FileInputStream(file);
                    if (instream != null) {
                        InputStreamReader inputreader
                                = new InputStreamReader(instream, "UTF-8");
                        BufferedReader buffreader = new BufferedReader(inputreader);
                        String line = "";
                        //分行读取
                        while ((line = buffreader.readLine()) != null) {
                            content += line + "\n";
                        }
                        instream.close();//关闭输入流
                    }
                } catch (java.io.FileNotFoundException e) {
                    Log.d("TestFile", "The File doesn't not exist.");
                } catch (IOException e) {
                    Log.d("TestFile", e.getMessage());
                }
            }
        }
        return content;
    }

    public static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }

    /*
     * 定义文件保存的方法，写入到文件中，所以是输出流
     * */
    public static void save(String fileName, String content) {
        FileOutputStream fos = null;
        try {

            /* 判断sd的外部设置状态是否可以读写 */
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

                File file = new File(Environment.getExternalStorageDirectory(), fileName + ".txt");

                // 先清空内容再写入
                fos = new FileOutputStream(file);

                byte[] buffer = content.getBytes();
                fos.write(buffer);
                fos.close();
            }

        } catch (Exception ex) {

            ex.printStackTrace();

            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // 写入文件
    public static void writeFile(byte[] date) {

        // 外部存储私有路径：Android文件夹
//        String privatePath = getExternalFilesDir(null).getPath();// 私有路径不分类为null
//        String filePath = privatePath + "/abc/";

        // 外部存储公共路径：DICM，DOWNLOAD，MUSIC等系统提供的文件夹
//        String publicPath = Environment
//                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//                .getPath();
//        String filePath = publicPath + "/abc/";

        // 自定义文件路径
        String rootPath = Environment.getExternalStorageDirectory().getPath();// 外部存储路径（根目录）
        String filePath = rootPath + "/abc/";

        String fileName = "imagetext.txt";
        FileOutputStream fos = null;
        try {

            File file = new File(filePath, fileName);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            fos.write(date);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("1507", "error: " + e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
