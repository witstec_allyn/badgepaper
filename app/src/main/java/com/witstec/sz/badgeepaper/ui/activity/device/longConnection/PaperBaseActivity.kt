package com.witstec.sz.badgeepaper.ui.activity.device.longConnection

import android.annotation.SuppressLint
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.KeyEvent
import android.view.View
import androidx.core.content.ContextCompat
import cn.com.heaton.blelibrary.ble.Ble
import cn.com.heaton.blelibrary.ble.BleLog
import cn.com.heaton.blelibrary.ble.callback.BleConnectCallback
import cn.com.heaton.blelibrary.ble.callback.BleNotifyCallback
import cn.com.heaton.blelibrary.ble.callback.BleWriteCallback
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.manage.BitmapBleBmpManagement
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.utils.ByteUtils
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.ToastUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_paper_base.*
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * 电子价签连接状态监听，获取铭牌信息
 */
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PaperBaseActivity : AppBaseActivity() {
    private val TAG = "PaperBaseActivity"
    private var bleType: String = ""
    private var timeout: Disposable? = null
    private var mVersion = ""
    private var mDeviceAddress = ""
    private var gattServices: ArrayList<BluetoothGattService>? = ArrayList()

    companion object {
        fun start(
            context: Context, mac: String,
            version: String
        ) {
            val intent = Intent(context, PaperBaseActivity::class.java)
            intent.putExtra("extra_mac", mac)
            intent.putExtra("extra_version", version)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paper_base)
        mDeviceAddress = intent.getStringExtra("extra_mac").toString()
        setToolbar(getString(R.string.Nameplate_Base))
        tv_connect_status.text = getString(R.string.add_device_loading_one)
        Log.e(
            TAG,
            "执行了初始化界面: "
        )
        connectHint()
        Ble.options().isAutoConnect = true
        if (App.isBleInitSuccess) {
            Ble.getInstance<BleRssiDevice>().disconnectAll()
            Ble.getInstance<BleRssiDevice>().connect(mDeviceAddress, connectCallback)
            Log.e(
                TAG,
                "没有执行初始界面222: "
            )
        } else {
            ToastUtils.show("初始化蓝牙失败")
        }

    }

    private val connectCallback: BleConnectCallback<BleRssiDevice> =
        object :
            BleConnectCallback<BleRssiDevice>() {
            override fun onConnectionChanged(device: BleRssiDevice?) {
                Log.e(
                    TAG,
                    "onConnectionChanged: " + device!!.connectionState
                )
                tv_connect_status.post {
                    if (device.isConnected) {
                        tv_connect_status.text = getString(R.string.add_device_loading_one)
                        connectHint()
                    } else if (device.isConnecting) {
                        tv_connect_status.setText(getString(R.string.reconnecting_the_base))
                        connectHint()
                    }
                }
            }

            override fun onConnectFailed(
                device: BleRssiDevice?,
                errorCode: Int
            ) {
                super.onConnectFailed(device, errorCode)
                tv_connect_status.post {
                    tv_connect_status.text = getString(R.string.The_base_cannot_be_connected)
                    connectNull()
                }
            }

            override fun onConnectCancel(device: BleRssiDevice) {
                super.onConnectCancel(device)
                Log.e(
                    TAG,
                    "onConnectCancel: " + device.bleName
                )
            }

            override fun onServicesDiscovered(
                device: BleRssiDevice?,
                gatt: BluetoothGatt
            ) {
                super.onServicesDiscovered(device, gatt)
                gattServices?.addAll(gatt.services)
                tv_connect_status.post {
                    tv_connect_status.setText(getString(R.string.Get_nameplate_connection_status))
                    connectHint()
                    if (timeout != null) {
                        (timeout ?: return@post).dispose()
                    }
                    timeout = Observable.timer(
                        1000 * 8, TimeUnit.MILLISECONDS
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            tv_connect_status.text =
                                getString(R.string.No_nameplate_inspected)
                            connectNull()
                        }
                    addDisposable(timeout ?: return@post)
                }
            }

            override fun onReady(device: BleRssiDevice?) {
                super.onReady(device)
                if (gattServices?.size == 0) return
                //连接成功后，设置通知
                val characteristic = gattServices?.get(0)?.characteristics?.get(0)
                val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
                val serviceUuid: UUID? = characteristic?.getService()?.getUuid()
                val characteristicUuid: UUID? = characteristic?.getUuid()
                Ble.getInstance<BleRssiDevice>().enableNotifyByUuid(
                    connetedDevices[0],
                    true,
                    serviceUuid,
                    characteristicUuid,
                    object : BleNotifyCallback<BleRssiDevice>() {
                        override fun onChanged(
                            device: BleRssiDevice,
                            characteristic: BluetoothGattCharacteristic
                        ) {
                            val byteDatas =
                                cn.com.heaton.blelibrary.ble.utils.ByteUtils.toHexString(
                                    characteristic.value
                                )
                            BleLog.e(TAG, "onChanged==data:$byteDatas")
                            val returnBytes =
                                ByteUtils.byteArrayToHexString(characteristic.value).split(",")
                            val byteValue = characteristic.value
                            val commandStr = (returnBytes[5] + returnBytes[6])
                            when (commandStr) {
                                "0010" -> {
                                    tv_connect_status.post {
                                        if (returnBytes[8] == "01") {
                                            if (returnBytes.size > 17) {
                                                val model =
                                                    returnBytes[9] + returnBytes[10] + returnBytes[11] + returnBytes[12] + returnBytes[13] + returnBytes[14] + returnBytes[15]

                                                val version =
                                                    ("" + byteValue[16].toString() + "" + byteValue[17].toString()).replace(
                                                        "-",
                                                        ""
                                                    )
                                                LogHelper.i(
                                                    "witstec", "型号=" + ByteUtils.hexStr2Str(
                                                        model
                                                    )
                                                )
                                                LogHelper.i(
                                                    "witstec",
                                                    "版本=" + ("" + byteValue[16].toString() + "" + byteValue[17].toString()).replace(
                                                        "-",
                                                        ""
                                                    )
                                                )
                                                bleType = ByteUtils.hexStr2Str(
                                                    model
                                                )
                                                mVersion = version
                                            }
                                            tv_connect_status.text =
                                                getString(R.string.nameplate_is_connected)
                                            connectView()
                                            listener()
                                            if (timeout != null) {
                                                (timeout ?: return@post).dispose()
                                            }
                                        } else {
                                            tv_connect_status.text =
                                                getString(R.string.No_nameplate_inspected)
                                            connectNull()
                                        }
                                    }
                                }
                            }
                        }

                        override fun onNotifySuccess(device: BleRssiDevice) {
                            super.onNotifySuccess(device)
                            BleLog.e(
                                TAG,
                                "onNotifySuccess: " + device.bleName
                            )
                        }
                    })
            }
        }

    fun listener() {
        btn_to_telepate_manage.setOnClickListener {
            val characteristic = gattServices?.get(0)?.characteristics?.get(0)
            val suid = characteristic?.service?.uuid
            val cuid = characteristic?.uuid
            val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
            val byteListChange =
                BitmapBleBmpManagement.getBmpProtocolDataChange(22)
            if (connetedDevices.isNotEmpty()) {
                val bleDevice = connetedDevices[0]
                Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, byteListChange, suid, cuid,
                    object :
                        BleWriteCallback<BleRssiDevice?>() {
                        override fun onWriteSuccessCharacteristicChanged(
                            device: BleRssiDevice?,
                            characteristic: BluetoothGattCharacteristic
                        ) {
                            LogHelper.i(
                                "writeByUuid",
                                "onWriteSuccess value=" +
                                        ByteUtils.byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            val returnBytes =
                                ByteUtils.byteArrayToHexString(
                                    characteristic.value
                                ).split(",")
                            val commandStr = (returnBytes[5] + returnBytes[6])
                            if (commandStr == "0016") {
                                if (bleType.isEmpty()) {
                                    return
                                }
//                                if (returnBytes[8] == "01") {
//                                    return
//                                }
                                var tyteId = ""
                                if (returnBytes.size > 14) {
                                    val strbug = StringBuffer()
                                    for (i in returnBytes.indices) {
                                        if (i > 10 && i < returnBytes.size - 1) {
                                            strbug.append(returnBytes[i])
                                        }
                                    }
                                    tyteId = ByteUtils.hexStr2Str(strbug.toString())
                                    LogHelper.i("witstec", "模板id=" + tyteId)
                                    LogHelper.i("witstec", "模板id=" + strbug.toString())
                                }
//                                if (returnBytes.size < 26) {
                                LogHelper.i("witstec", "获取铭牌数据成功")
//                                    val connetedDevices =
//                                        Ble.getInstance<BleRssiDevice>().connectedDevices
//                                    if (connetedDevices.size != 0) {
//                                        val bleDevice = connetedDevices[0]
//                                        Ble.getInstance<BleRssiDevice>().disconnect(bleDevice)
//                                    }
//                                    Ble.options().isAutoConnect = false
//                                    if (timeout != null) {
//                                        (timeout ?: return).dispose()
//                                    }
//                                    runOnUiThread {
//                                        showDialog()
//                                        SystemClock.sleep(2000)
//                                        dialogDismiss()
//                                    }
                                SystemClock.sleep(240)
//                                DeviceManageActivity.start(
//                                    this@PaperBaseActivity,
//                                    mDeviceAddress ?: return,
//                                    bleType,
//                                    mVersion,
//                                    tyteId
//                                )

//                                }
                            }
                        }

                        override fun onWriteSuccess(
                            device: BleRssiDevice?,
                            characteristic: BluetoothGattCharacteristic
                        ) {
                            LogHelper.i(
                                "writeByUuid",
                                "onWriteSuccess value=" +
                                        ByteUtils.byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                        }
                    })
            }
        }
    }

    internal fun connectNull() {
        runOnUiThread {
            btn_to_telepate_manage.background =
                ContextCompat.getDrawable(this, R.drawable.div_sms_code_bg_gray)
            btn_to_telepate_manage.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.black_sub
                )
            )
            btn_to_telepate_manage.isEnabled = false
            tv_connect_status.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
            iv_paper_connected_true.visibility = View.GONE
            iv_paper_connected_false.visibility = View.VISIBLE
        }
    }

    internal fun connectHint() {
        runOnUiThread {
            btn_to_telepate_manage.background =
                ContextCompat.getDrawable(this, R.drawable.div_sms_code_bg_gray)
            btn_to_telepate_manage.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.black_sub
                )
            )
            btn_to_telepate_manage.isEnabled = false
            tv_connect_status.setBackgroundColor(ContextCompat.getColor(this, R.color.orange))
            iv_paper_connected_true.visibility = View.GONE
            iv_paper_connected_false.visibility = View.VISIBLE
        }
    }

    internal fun connectView() {
        runOnUiThread {
            btn_to_telepate_manage.background =
                ContextCompat.getDrawable(this, R.drawable.div_sms_code_bg)
            btn_to_telepate_manage.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorPrimary
                )
            )
            btn_to_telepate_manage.isEnabled = true
            tv_connect_status.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorPrimary
                )
            )
            iv_paper_connected_true.visibility = View.VISIBLE
            iv_paper_connected_false.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timeout != null) {
            timeout!!.dispose()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        return super.onKeyDown(keyCode, event)
    }

    @SuppressLint("CheckResult")
    override fun onBackPressed() {
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.size != 0) {
            val bleDevice = connetedDevices[0]
            Ble.getInstance<BleRssiDevice>().disconnect(bleDevice)
        }
        Ble.options().isAutoConnect = false
        if (timeout != null) {
            timeout!!.dispose()
        }
        runOnUiThread {
            connectHint()
            tv_connect_status.text = getString(R.string.Disconnecting_from_the_base_in)
        }
        LogHelper.i(TAG, "退出")
        timeout = Observable.timer(
            700, TimeUnit.MILLISECONDS
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                super.onBackPressed()
            }
        addDisposable(timeout!!)
    }
}
