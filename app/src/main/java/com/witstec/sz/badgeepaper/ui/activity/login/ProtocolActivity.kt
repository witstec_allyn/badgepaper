package com.witstec.sz.badgeepaper.ui.activity.login

import android.os.Bundle
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.wuhenzhizao.titlebar.widget.CommonTitleBar
import kotlinx.android.synthetic.main.activity_protocol.*

class ProtocolActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_protocol)

        /**
         *
         *
         *
         *
         *
         *
         *
         * 此页面需要显示字体的不同样式以及换行，做成html页面本地加载，或者加载网页，
         *
         *
         *
         *
         *
         *
         *
         *
         */

        titlebar.setListener { v, action, extra ->
            if (action == CommonTitleBar.ACTION_LEFT_BUTTON || action == CommonTitleBar.ACTION_LEFT_TEXT) {
                onBackPressed()
            }
        }
    }
}
