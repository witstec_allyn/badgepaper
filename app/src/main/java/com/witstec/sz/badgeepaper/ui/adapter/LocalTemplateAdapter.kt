package com.witstec.sz.badgeepaper.ui.adapter

import android.annotation.SuppressLint
import android.text.Html
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.TemplateBean
import com.witstec.sz.badgeepaper.utils.GlideUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import java.util.*

class LocalTemplateAdapter :
    BaseQuickAdapter<TemplateBean, BaseViewHolder>(
        R.layout.item_draw_image_data,
        ArrayList()
    ) {
    var searchKeyword: String? = null

    @SuppressLint("StringFormatInvalid")
    override fun convert(helper: BaseViewHolder, item: TemplateBean) {
        (helper.getView<View>(R.id.tv_be_applicable) as TextView).text =
            String.format(
                mContext.getString(
                    R.string.apply_type,
                    AppUtils.typeIdToTypeName(item.templateType)
                )
            )
        if (item.templateName != null) {
            (helper.getView<View>(R.id.tv_template_name) as TextView).text = item.templateName
        }
        (helper.getView<View>(R.id.tv_create_time) as TextView).text =item.templateCreateTime

        (helper.getView<View>(R.id.iv_tv_template_image) as AppCompatImageView).setImageBitmap(AppUtils.base64ToBitmap(item.templateImage))

        if (searchKeyword != null && searchKeyword!!.isNotEmpty())
            setChangeTextView(
                item.templateName,
                searchKeyword!!,
                (helper.getView<View>(R.id.tv_template_name) as TextView)
            )
    }


    //制定位置修改颜色
    private fun setChangeTextView(name: String?, changeStr: String, textView: TextView) {
        if (name != null && name.contains(changeStr)) {
            val index = name.indexOf(changeStr)
            val len = changeStr.length
            val temp = Html.fromHtml(name.substring(0, index) + "<font color=#E73324>"
                        + name.substring(index, index + len) + "</font>"
                        + name.substring(index + len, name.length)
            )
            textView.text = temp
        } else {
            textView.text = name
        }
    }
}
