package com.witstec.sz.badgeepaper.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.witstec.sz.badgeepaper.R;
import com.witstec.sz.badgeepaper.model.bean.Process;

public class AddDeviceLoadingLayout extends LinearLayout {

    private Process process = Process.ONE;

    public void setProcess(Process process) {
        this.process = process;
    }

    public Process getProcess() {
        return process;
    }

    public AddDeviceLoadingLayout(Context context) {
        super(context);
    }

    public AddDeviceLoadingLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AddDeviceLoadingLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView() {
        addView(inflate(getContext(), R.layout.view_add_loading_one, null));
    }

    public void addProcessLayout(Process process) {
        switch (process) {
            case ONE:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_one, null));
                setProcess(Process.ONE);
                break;
            case TWO:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_two, null));
                setProcess(Process.TWO);
                break;
            case THREE:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_three, null));
                setProcess(Process.THREE);
                break;
            case ONE_ADD:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_one_add, null));
                setProcess(Process.ONE_ADD);
                break;
            case TWO_ADD:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_two_add, null));
                setProcess(Process.TWO_ADD);
                break;
            case THREE_ADD:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_three_add, null));
                setProcess(Process.THREE_ADD);
                break;
            case THREE_ERROR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_three_add_error, null));
                setProcess(Process.THREE_ERROR);
                break;
            case FOUR_ERROR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_four_error, null));
                setProcess(Process.FOUR_ERROR);
                break;
            case FOUR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_four, null));
                setProcess(Process.FOUR);
                break;
            case FIVES:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_four_ok, null));
                setProcess(Process.FIVES);
                break;
            case TIME_OUT:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_time_out, null));
                setProcess(Process.TIME_OUT);
                break;
            case TIME_OUT_ADD:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_time_out_add, null));
                setProcess(Process.TIME_OUT_ADD);
                break;
            case OTA_1:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_1, null));
                setProcess(Process.OTA_1);
                break;
            case OTA_2:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_2, null));
                setProcess(Process.OTA_2);
                break;
            case OTA_3:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_3, null));
                setProcess(Process.OTA_3);
                break;
            case OTA_4:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_4, null));
                setProcess(Process.OTA_4);
                break;
            case OTA__START_ERROR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_error_1, null));
                setProcess(Process.OTA__START_ERROR);
                break;
            case OTA_SEND_ERROR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_error_2, null));
                setProcess(Process.OTA_SEND_ERROR);
                break;
            case OTA_CHECK_ERROR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_ota_error_3, null));
                setProcess(Process.OTA_CHECK_ERROR);
                break;
            case ERROR:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_time_error, null));
                setProcess(Process.ERROR);
                break;
            case MSG_OUT:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_get_msg_out, null));
                setProcess(Process.MSG_OUT);
                break;
            case MSG_OUT_ADD:
                removeAllViews();
                addView(inflate(getContext(), R.layout.view_add_loading_get_msg_out_add, null));
                setProcess(Process.MSG_OUT_ADD);
                break;
        }

    }

}
