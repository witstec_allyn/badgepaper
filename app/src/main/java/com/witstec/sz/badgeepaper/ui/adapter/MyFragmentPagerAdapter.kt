package com.witstec.sz.badgeepaper.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

import java.util.ArrayList


class MyFragmentPagerAdapter(fm: FragmentManager, private val tab_title_list: ArrayList<String>//存放标签页标题
                             , private val fragment_list: ArrayList<Fragment>//存放ViewPager下的Fragment
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return fragment_list[position]
    }

    override fun getCount(): Int {
        return fragment_list.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tab_title_list[position]
    }
}
