package com.witstec.sz.badgeepaper.model.bean;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.NotNull;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

@Table("TemplateBean")
public class TemplateBean {

    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @NotNull
    @Column("templateId")
    private String templateId;
    @NotNull
    @Column("templateName")
    private String templateName;
    @NotNull
    @Column("templateType")
    private String templateType;
    @NotNull
    @Column("templateImage")
    private String templateImage;
    @NotNull
    @Column("templateXmlString")
    private String templateXmlString;
    @NotNull
    @Column("templateCreateTime")
    private String templateCreateTime;
    @Column("inputDataJson")
    private String inputDataJson = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TemplateBean(String templateId, String templateName, String templateType, String templateImageUrl, String templateFileUrl, String templateCreateTime) {
        this.templateId = templateId;
        this.templateName = templateName;
        this.templateType = templateType;
        this.templateImage = templateImageUrl;
        this.templateXmlString = templateFileUrl;
        this.templateCreateTime = templateCreateTime;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getTemplateImage() {
        return templateImage;
    }

    public void setTemplateImage(String templateImage) {
        this.templateImage = templateImage;
    }

    public String getTemplateXmlString() {
        return templateXmlString;
    }

    public void setTemplateXmlString(String templateXmlString) {
        this.templateXmlString = templateXmlString;
    }

    public String getTemplateCreateTime() {
        return templateCreateTime;
    }

    public void setTemplateCreateTime(String templateCreateTime) {
        this.templateCreateTime = templateCreateTime;
    }


    public String getInputDataJson() {
        return inputDataJson;
    }

    public void setInputDataJson(String inputDataJson) {
        this.inputDataJson = inputDataJson;
    }

}
