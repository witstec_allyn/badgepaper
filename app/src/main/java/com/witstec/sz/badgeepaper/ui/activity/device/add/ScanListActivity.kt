package com.witstec.sz.badgeepaper.ui.activity.device.add

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import cn.com.heaton.blelibrary.ble.Ble
import cn.com.heaton.blelibrary.ble.callback.BleScanCallback
import cn.com.heaton.blelibrary.ble.model.ScanRecord
import com.github.nukc.stateview.StateView
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.GattType
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.AddLoadingActivity
import com.witstec.sz.badgeepaper.ui.adapter.AddDeviceHomeAdapter
import com.witstec.sz.badgeepaper.utils.*
import com.witstec.sz.badgeepaper.view.SimpleDividerDecoration
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_scan_list.*
import java.util.*
import java.util.concurrent.TimeUnit

/**
 *
 */
open class ScanListActivity : AppBaseActivity() {

    private var mScanListAdapter: AddDeviceHomeAdapter? = null
    private val REQUESTCODE: Int = 0x01
    private var statView: StateView? = null
    private val ble: Ble<BleRssiDevice> = Ble.getInstance<BleRssiDevice>()
    private var bleList: ArrayList<BleRssiDevice> = arrayListOf()
    private var searchKeyword: String = ""
    private var timeout: Disposable? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_list)

        setToolbar(getString(R.string.device_search_add_loading_text))

        statView = StateView.inject(recycler)
        (statView ?: return).showLoading()
        mScanListAdapter = AddDeviceHomeAdapter()
        recycler.adapter = mScanListAdapter
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerDecoration(this))

        requestBLEPermission()

        (mScanListAdapter ?: return).setOnItemClickListener { adapter, view, position ->
            val item = (mScanListAdapter ?: return@setOnItemClickListener).getItem(position)
            val scanRecordByteData = (item ?: return@setOnItemClickListener).scanRecord.bytes
            val totalLoadData = ByteArray(7)
            totalLoadData[0] = scanRecordByteData[13]
            totalLoadData[1] = scanRecordByteData[14]
            totalLoadData[2] = scanRecordByteData[15]
            totalLoadData[3] = scanRecordByteData[16]
            totalLoadData[4] = scanRecordByteData[17]
            totalLoadData[5] = scanRecordByteData[18]
            totalLoadData[6] = scanRecordByteData[19]
            val name = ByteUtils.hexStr2Str(
                ByteUtils.byteArrayToHexStringLX(
                    totalLoadData
                )
            )
            App.device_name = name
            val intent = Intent()
            intent.setClass(this, AddLoadingActivity::class.java)
            intent.putExtra("extra_name", name)
            intent.putExtra("extra_address", item.bleAddress)
            intent.putExtra("ota_enum", GattType.ADD_DEVICE)
            startActivity(intent)
            finish()
        }

        searchLayout.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                searchKeyword = s.toString().toUpperCase()
                if (searchKeyword.isEmpty()) {
                    mScanListAdapter!!.addData(bleList)
                } else {
                    mScanListAdapter!!.data.clear()
                    bleList.forEach {
                        if (StringUtils.eslIdMacStr(it.bleAddress).contains(searchKeyword)) {
                            mScanListAdapter!!.addData(it)
                        }
                    }
                }
            }
        })
    }

    private fun requestBLEPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            REQUESTCODE
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUESTCODE) {
            if (grantResults.isNotEmpty()) {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    // 判断用户是否 点击了不再提醒。(检测该权限是否还可以申请)
                    val b = shouldShowRequestPermissionRationale(permissions[0])
                    if (!b) {
                        // 用户还是想用我的 APP 的
                        // 提示用户去应用设置界面手动开启权限
                    } else
                        finish()
                } else {
                    //权限申请成功
                    initData()
                }
            } else {
                initData()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUESTCODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val i = ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
                if (i != PackageManager.PERMISSION_GRANTED) {
                    // 提示用户应该去应用设置界面手动开启权限
                } else {
                    //权限申请成功
                    initData()
                }
            }
        } else if (requestCode == Ble.REQUEST_ENABLE_BT) {
            initData()
        }
    }

    private val scanCallback: BleScanCallback<BleRssiDevice?> =
        object : BleScanCallback<BleRssiDevice?>() {
            @SuppressLint("SyntheticAccessor")
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onLeScan(device: BleRssiDevice?, rssi: Int, scanRecord: ByteArray?) {
                if (device != null) {
                    if (device.bleName.contains("Card") && StringUtils.eslIdMacStr(device.bleAddress)
                            .contains(searchKeyword)
                    ) {
                        for (i in bleList.indices) {
                            val rssiDevice: BleRssiDevice = bleList[i]
                            if (TextUtils.equals(rssiDevice.bleAddress, device.bleAddress)) {
                                if (rssiDevice.rssi !== rssi && System.currentTimeMillis() - rssiDevice.rssiUpdateTime > 1000L) {
                                    rssiDevice.rssiUpdateTime = System.currentTimeMillis()
                                    rssiDevice.rssi = rssi
                                    mScanListAdapter!!.notifyItemChanged(i)
                                }
                                return
                            }
                        }
                        device.scanRecord = ScanRecord.parseFromBytes(
                            scanRecord
                        )
                        device.rssi = rssi
                        (statView ?: return).showContent()
                        bleList.add(device)
                        (mScanListAdapter ?: return).addData(device)
                        if (timeout != null) {
                            timeout!!.dispose()
                        }
                    }
                }
            }

            override fun onStart() {
                super.onStart()
//                startBannerLoadingAnim()
            }

            override fun onStop() {
                super.onStop()
//                stopBannerLoadingAnim()
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
                Log.e("scan", "onScanFailed: $errorCode")
            }
        }

    /**
     * 加载数据
     */
    private fun initData() {
        if (NetworkUtils.isLocationEnabled()) {
            val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            if (!mBluetoothAdapter.isEnabled) {
                //4、若未打开，则请求打开蓝牙
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, Ble.REQUEST_ENABLE_BT)
            } else {
                ble.startScan(scanCallback)
                timeout = Observable.timer(
                    1000 * 12, TimeUnit.MILLISECONDS
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        statView!!.showEmpty()
                    }
                addDisposable(timeout!!)
            }
        } else {
            Toast.makeText(this, "请打开位置信息开关", Toast.LENGTH_LONG).show()
            startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
        }
    }

    override fun onStop() {
        super.onStop()
        ble.stopScan()
    }

    override fun onRestart() {
        super.onRestart()
        ble.startScan(scanCallback)
    }

    public override fun onDestroy() {
        super.onDestroy()
        ble.stopScan()
    }
}
