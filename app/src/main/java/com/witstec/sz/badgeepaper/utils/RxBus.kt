package com.witstec.sz.badgeepaper.utils

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBus {

    private val mSubject = PublishSubject.create<Any>()

    private object HolderClass {
        val INSTANCE = RxBus()
    }

    private fun <T> toObservable(eventClass: Class<T>): Observable<T> {
        return mSubject
            .onErrorReturn { throwable ->
                null
            }
            .filter { o -> eventClass.isInstance(o) }.cast(eventClass)
    }

    companion object {

        fun post(o: Any) {
            HolderClass.INSTANCE.mSubject.onNext(o)
        }

        fun <T> register(eventClass: Class<T>): Observable<T> {
            return HolderClass.INSTANCE.toObservable(eventClass)
        }
    }

}