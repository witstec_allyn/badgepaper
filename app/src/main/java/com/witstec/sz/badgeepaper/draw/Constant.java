package com.witstec.sz.badgeepaper.draw;

import android.content.ContextWrapper;
import android.os.Environment;

import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.utils.FileManager;


public class Constant {
    /**
     * 文件保存路径
     */
    public static final String SAVE_PATH =  new ContextWrapper(App.Companion.getInstance()).getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath()+ "/DrawTools";
    /**
     * 保存画图图片文件后缀名
     */
    public static final String SAVE_IMAGE_FILE_SUFFIX = ".png";
    /**
     * 保存画图数据文件后缀名
     */
    public static final String SAVE_DATA_FILE_SUFFIX_XML = ".xml";
    /**
     * 默认画笔颜色
     */
    public static final String PAINT_DEFAULT_COLOR= "#000000";
    /**
     * 默认画笔粗细大小
     */
    public static final int PAINT_DEFAULT_STROKE_WIDTH= 6;
    /**
     * 默认画笔textsize大小
     */
    public static final int PAINT_DEFAULT_TEXT_SIZE = 55;

    public static final int REQUEST_CODE_CHOOSE = 123;

    public static final int REQUEST_CODE_CHOOSE_IMAGE = 110;


}
