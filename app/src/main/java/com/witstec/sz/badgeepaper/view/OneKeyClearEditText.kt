package com.witstec.sz.badgeepaper.view


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.witstec.sz.badgeepaper.R

/**
 * 自定义EditText控件之一键删除的按钮  2017/4/10.
 */

class OneKeyClearEditText @SuppressLint("InlinedApi")
constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : AppCompatEditText(context, attrs, defStyleAttr), View.OnFocusChangeListener, TextWatcher {

    private var mClearDrawable: Drawable? = null// 一键删除的按钮
    private var hasFocus: Boolean = false// 控件是否有焦点

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null) : this(context, attrs, R.attr.editTextStyle)

    init {
        initClearDrawable(context)
    }

    @SuppressLint("NewApi")
    private fun initClearDrawable(context: Context) {
        // 获取EditText的DrawableRight,假如没有设置我们就使用默认的图片
        mClearDrawable = compoundDrawables[2]
        if (mClearDrawable == null) {
            mClearDrawable = ContextCompat.getDrawable(getContext(), R.mipmap.ic_delete)
        }
        // 设置删除按钮的颜色和TextColor的颜色一致
//        mClearDrawable = setTint(getContext(), mClearDrawable!!, R.color.black_sub)
        // 设置Drawable的宽高和TextSize的大小加20
        mClearDrawable!!.setBounds(0, 0, textSize.toInt() + 20, textSize.toInt() + 20)
        setClearIconVisible(false)
        // 设置焦点改变的监听
        onFocusChangeListener = this
        // 设置输入框里面内容发生改变的监听
        addTextChangedListener(this)
    }

    /**
     * 设置清除图标的显示与隐藏，调用setCompoundDrawables为EditText绘制上去
     *
     * @param visible
     */
    private fun setClearIconVisible(visible: Boolean) {
        val right = if (visible) mClearDrawable else null
        setCompoundDrawables(compoundDrawables[0], compoundDrawables[1],
                right, compoundDrawables[3])
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (mClearDrawable != null && event.action == MotionEvent.ACTION_UP) {
            val x = event.x.toInt()
            // 判断触摸点是否在水平范围内
            val isInnerWidth = x > width - totalPaddingRight && x < width - paddingRight
            // 获取删除图标的边界，返回一个Rect对象
            val rect = mClearDrawable!!.bounds
            // 获取删除图标的高度
            val height = rect.height()
            val y = event.y.toInt()
            // 计算图标底部到控件底部的距离
            val distance = (getHeight() - height) / 2
            // 判断触摸点是否在竖直范围内(可能会有点误差)
            // 触摸点的纵坐标在distance到（distance+图标自身的高度）之内，则视为点中删除图标
            val isInnerHeight = y > distance && y < distance + height
            if (isInnerHeight && isInnerWidth) {
                this.setText("")
            }
        }
        return super.onTouchEvent(event)
    }

    /**
     * 当输入框里面内容发生变化的时候回调的方法
     */
    override fun onTextChanged(text: CharSequence, start: Int, lengthBefore: Int, lengthAfter: Int) {
        if (hasFocus) {
            setClearIconVisible(text.isNotEmpty())
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun afterTextChanged(s: Editable) {}

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        this.hasFocus = hasFocus
        if (hasFocus) {
            setClearIconVisible(text!!.isNotEmpty())
        } else {
            setClearIconVisible(false)
        }

    }

    companion object {

        fun setTint(context: Context, drawable: Drawable, @ColorRes id: Int): Drawable {
            val tint = ContextCompat.getColor(context, id)
            return setTint(drawable, tint)
        }

        fun setTint(drawable: Drawable, @ColorInt tint: Int): Drawable {
            var drawable = drawable
            drawable = DrawableCompat.wrap(drawable)
            drawable.mutate()
            DrawableCompat.setTint(drawable, tint)
            return drawable
        }
    }

}