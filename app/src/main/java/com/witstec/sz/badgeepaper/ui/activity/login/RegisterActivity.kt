package com.witstec.sz.badgeepaper.ui.activity.login

import android.os.Bundle
import android.text.TextUtils
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.witstec.sz.badgeepaper.Constants
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.getStringData
import com.witstec.sz.badgeepaper.myStartActivity
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setToolbar(getString(R.string.email_register))
        btn_register_protocol.setOnClickListener {
            myStartActivity(this@RegisterActivity, ProtocolActivity::class.java)
        }

        //点击注册
        btn_submit_register.setOnClickListener {
            if (ed_input_register_pwd.text.toString() != ed_input_register_pwd_to.text.toString()) {
                ToastUtils.show(getString(R.string.pwd_is_not_the_same))
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(ed_input_register_email.text.toString())
                || TextUtils.isEmpty(ed_input_register_pwd.text.toString()) || TextUtils.isEmpty(
                    ed_input_register_pwd_to.text.toString()
                ) || TextUtils.isEmpty(
                    ed_input_register_user_name.text.toString()
                )
            ) {
                ToastUtils.show(getString(R.string.edit_is_null))
                return@setOnClickListener
            }
            //显示进度对话框
            val dialog = MaterialDialog.Builder(this)
                .content(getString(R.string.loading_text))
                .cancelable(false)
                .progress(true, 0)
                .show()
            LogHelper.i("token", getStringData(Constants.TOKEN))
            addDisposable(ApiRepository.register(
                ed_input_register_user_name.text.toString(),
                ed_input_register_email.text.toString(),
                ed_input_register_pwd_to.text.toString(),
                AppUtils.getLocaleLanguage(),this
            )
                .subscribe({
                    dialog.dismiss()
                    MaterialDialog.Builder(this)
                        .content(getString(R.string.registered_ok))
                        .positiveText(getString(R.string.fixed))
                        .onPositive { dialog, which ->
                            this@RegisterActivity.finish()
                        }
                        .show()
                }, {
                    dialog.dismiss()
                    it.message?.let { it1 -> ToastUtils.show(it1) }
                }))

        }

        btn_to_login.setOnClickListener {
            this@RegisterActivity.finish()
        }
        check_register_protocol.setOnCheckedChangeListener { _, isChecked ->
            btn_submit_register.isEnabled = isChecked
            if (!isChecked) {
                btn_submit_register.background =
                    ContextCompat.getDrawable(
                        this@RegisterActivity,
                        R.drawable.btn_radius_enobled_false
                    )
            } else {
                btn_submit_register.background =
                    ContextCompat.getDrawable(this@RegisterActivity, R.drawable.btn_radius)
            }
        }
    }
}
