package com.witstec.sz.badgeepaper.network;

import android.accounts.NetworkErrorException;

import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.ExtKt;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class CommonParamsInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (NetworkUtils.isNetworkConnected(App.Companion.getInstance())) {
            Request request = chain.request();
            Request httpUrl = request.newBuilder()
//                    .header("X-WITSTEC-USER-TOKEN", ExtKt.getStringData(Constants.TOKEN))
                    .header("X-WITSTEC-APPLICATION-ID", "1db96a2d763689d0")
                    .build();
            return chain.proceed(httpUrl);
        } else {
            try {
                throw new NetworkErrorException("No internet");
            } catch (NetworkErrorException e) {
                e.printStackTrace();
            }
            Request request = chain.request();
            return chain.proceed(request);
        }
    }
}
