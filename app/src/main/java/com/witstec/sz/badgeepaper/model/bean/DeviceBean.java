package com.witstec.sz.badgeepaper.model.bean;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.NotNull;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

@Table("student")
public class DeviceBean {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @NotNull
    @Column("deviceName")
    private String deviceName;
    @NotNull
    @Column("mac")
    private String mac;
    @NotNull
    @Column("deviceType")
    private String deviceType;
    @NotNull
    @Column("deviceVersion")
    private String deviceVersion;
    private boolean isOnline;
    private String signal = "";

    public DeviceBean(String deviceName, String mac, String deviceType, String deviceVersion) {
        this.deviceName = deviceName;
        this.mac = mac;
        this.deviceType = deviceType;
        this.deviceVersion = deviceVersion;
    }

    public DeviceBean(int id, String deviceName, String mac, String deviceType, String deviceVersion, boolean isOnline, String signal) {
        this.id = id;
        this.deviceName = deviceName;
        this.mac = mac;
        this.deviceType = deviceType;
        this.deviceVersion = deviceVersion;
        this.isOnline = isOnline;
        this.signal = signal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }
}
