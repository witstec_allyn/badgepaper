package com.witstec.sz.badgeepaper.ui.activity.base

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.ui.activity.MainActivity
import com.witstec.sz.badgeepaper.utils.SystemBarTintManager
import com.witstec.sz.badgeepaper.utils.language.LanguageUtil
import com.witstec.sz.badgeepaper.utils.language.SpUtil
import com.wuhenzhizao.titlebar.widget.CommonTitleBar
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.view_toolbar.*

open class AppBaseActivity : AppCompatActivity() {

    private var mCompositeDisposable: CompositeDisposable? = null
    private var dialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * 此方法先于 onCreate()方法执行
     * @param newBase
     */
    override fun attachBaseContext(newBase: Context) {
        //获取我们存储的语言环境 比如 "en","zh",等等
        val language = SpUtil.getInstance(App.instance).getString(SpUtil.LANGUAGE)
        /**
         * attach对应语言环境下的context
         */
        super.attachBaseContext(LanguageUtil.attachBaseContext(newBase, language))
    }

    /**
     * 如果是7.0以下，我们需要调用changeAppLanguage方法，
     * 如果是7.0及以上系统，直接把我们想要切换的语言类型保存在SharedPreferences中即可
     * 然后重新启动MainActivity
     * @param language
     */
    public fun changeLanguage(language: String?) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            LanguageUtil.changeAppLanguage(App.instance, language)
        }
        SpUtil.getInstance(this).putString(SpUtil.LANGUAGE, language)
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    protected fun setToolbar(title: String) {
        if (titlebar != null) {
            titlebar.setCenterText(title)
            titlebar.setListener { v, action, _ ->
                if (action == CommonTitleBar.ACTION_LEFT_BUTTON || action == CommonTitleBar.ACTION_LEFT_TEXT) {
                    onBackPressed()
                }
            }
        }
    }

    protected fun setStatusBarColor(color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            setTranslucentStatus(true)
            val tintManager = SystemBarTintManager(this)
            tintManager.isStatusBarTintEnabled = true
            tintManager.setStatusBarTintResource(color)//通知栏所需颜色
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT//6.0的真机上反而是用这句生效
            window.statusBarColor = resources.getColor(color)
        }
    }

    fun showDialog() {
        dialogDismiss()
        dialog = MaterialDialog.Builder(this)
            .content(getString(R.string.loading_text))
            .cancelable(false)
            .progress(true, 0)
            .show()
    }

    fun showDialog(title: String) {
        dialogDismiss()
        dialog = MaterialDialog.Builder(this)
            .content(title)
            .cancelable(false)
            .progress(true, 0)
            .show()
    }

    fun showDialogNo(title: String) {
        dialogDismiss()
        dialog = MaterialDialog.Builder(this)
            .content(title)
            .cancelable(true)
            .show()
    }

    fun dialogDismiss() {
        if (dialog != null) {
            dialog!!.dismiss()
        }
    }

    /**
     * 沉浸式状态栏设置
     */
    @TargetApi(19)
    private fun setTranslucentStatus(on: Boolean) {
        val win = window
        val winParams = win.attributes
        val bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    fun getCompositeDisposable(): CompositeDisposable {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = CompositeDisposable()
        }

        return this.mCompositeDisposable!!
    }

    fun addDisposable(disposable: Disposable) {
        if (this.mCompositeDisposable == null) {
            this.mCompositeDisposable = CompositeDisposable()
        }

        this.mCompositeDisposable!!.add(disposable)
    }

    override fun onDestroy() {
        if (this.mCompositeDisposable != null) {
            this.mCompositeDisposable!!.dispose()
        }
        super.onDestroy()
    }
}
