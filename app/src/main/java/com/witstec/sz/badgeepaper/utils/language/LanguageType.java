package com.witstec.sz.badgeepaper.utils.language;

/**
 * 语言切换类型
 * by:Allyn
 */
public enum LanguageType {

    CHINESE("ch"),
    ENGLISH("en"),
    THAILAND("tw"),
    SPAIN("es"),
    JAPAN("ja");

    private String language;

    LanguageType(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language == null ? "" : language;
    }
}
