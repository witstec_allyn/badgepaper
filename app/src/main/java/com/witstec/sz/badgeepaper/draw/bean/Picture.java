package com.witstec.sz.badgeepaper.draw.bean;


import android.graphics.Bitmap;

public class Picture {

    /**
     * 图片bitmap
     */
    private Bitmap content;

    public Picture(Bitmap bitmap) {
        this.content = bitmap;
    }

    public Bitmap createContent() {
        return content;
    }

    public Bitmap getContent() {
        return content;
    }

    public Bitmap setContent(Bitmap content) {
        this.content = content;
        return  this.content;
    }
}

