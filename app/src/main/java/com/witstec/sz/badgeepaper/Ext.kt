package com.witstec.sz.badgeepaper

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.view.View
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import com.witstec.sz.badgeepaper.draw.Constant
import com.witstec.sz.badgeepaper.draw.bean.Pel
import com.witstec.sz.badgeepaper.utils.Glide4Engine
import com.witstec.sz.badgeepaper.utils.PreferencesHelper
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.yanzhenjie.permission.AndPermission
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlin.math.pow

fun View.dp2px(dp: Int): Int {
    val scale = this.resources.displayMetrics.density
    return (dp * scale + 0.5f).toInt()
}

fun View.px2dp(px: Int): Int {
    val scale = this.resources.displayMetrics.density
    return (px / scale + 0.5f).toInt()
}

fun myStartActivity(content: Context, activity: Class<*>) {
    val intent = Intent(content, activity)
    content.startActivity(intent)
}


fun Context.openBrowser(url: String) {
    Intent(Intent.ACTION_VIEW, Uri.parse(url)).run { startActivity(this) }
}

//存储key对应的数据
fun saveStringData(key: String, info: String) {
    PreferencesHelper.getInstance().putString(key, info)
}

//取key对应的数据
fun getStringData(key: String): String {
    return PreferencesHelper.getInstance().getString(key, "")
}

fun saveBoolean(key: String, isBoolean: Boolean) {
    PreferencesHelper.getInstance().putBoolean(key, isBoolean)
}

fun getSaveBoolean(key: String): Boolean {
    return PreferencesHelper.getInstance().getBoolean(key, false)
}

//清空缓存对应key的数据
fun clearData(key: String) {
    App.instance.getSharedPreferences(key, MODE_PRIVATE).edit().clear().apply()
}

fun selectImage(content: Context, selectSize: Int) {
    val permission = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    AndPermission.with(content)
        .runtime()
        .permission(
            permission
        )
        .onGranted { permissions ->
            val authority: String = content.packageName + ".provider"
            Matisse.from(content as Activity)
                .choose(MimeType.ofImage())
                .countable(true)
                .capture(false)
                .captureStrategy(
                    CaptureStrategy(true, authority)
                )
                .maxSelectable(selectSize)
                .gridExpectedSize(App.instance.resources.getDimensionPixelSize(R.dimen.grid_expected_size))
                .theme(R.style.Matisse_Dracula)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(Glide4Engine())    // for glide-V4
                .setOnSelectedListener { uriList, pathList ->
                }
                .originalEnable(true)
                .maxOriginalSize(8)
                .setOnCheckedListener { isChecked ->
                }
                .forResult(Constant.REQUEST_CODE_CHOOSE)
        }
        .onDenied { permissions ->
            ToastUtils.show(content.getString(R.string.permission_no))
        }
        .start()
}

//抖动算法来对图像进行二值化处理
 fun convertGreyImgByFloyd(img: Bitmap, amount: Int=16): Bitmap {

    val width = img.width         //获取位图的宽
    val height = img.height       //获取位图的高
    val pixels = IntArray(width * height) //通过位图的大小创建像素点数组
    img.getPixels(pixels, 0, width, 0, 0, width, height)
    //调色板
    val t_white = Color.rgb(255, 255, 255)
    val t_black = Color.rgb(0, 0, 0)
    val t_red = Color.rgb(255, 0, 0)
    val t_rgb: IntArray = intArrayOf(t_black, t_red, t_white)
    for (r in pixels.indices) {
        val raw: Int = r / width
        val column: Int = r % width
        val ocr= pixels[width * raw + column]
        val old_color: IntArray =   intArrayOf(ocr.red, ocr.green, ocr.blue)
//            //当前颜色值
//            var m_color = 0
//            //mi值
//            var m_minr = 0.0
//            t_rgb.forEach {
//                val min = (old_color[0] - it.red).toDouble().pow(2.toDouble()) +
//                        (old_color[1] - it.green).toDouble().pow(2.toDouble()) +
//                        (old_color[2] - it.blue).toDouble().pow(2.toDouble())
//                if (m_minr != 0.0) {
//                    //相似值
//                    if (min < m_minr) {
//                        m_color = it
//                        m_minr = min
//                        mew_color_array = intArrayOf(it.red, it.green, it.blue)
//                    }
//
//                } else {
//                    m_color = it
//                    m_minr = min
//                    mew_color_array = intArrayOf(it.red, it.green, it.blue)
//                }
//            }

        var m_color  : IntArray = intArrayOf(0,0,0,0)
        val nearColor=  getNearstColor(old_color[0].toDouble(), old_color[1].toDouble(), old_color[2].toDouble())
        if (nearColor == 0) {
            pixels[width * raw + column]=Color.rgb(0,0,0)
            m_color[0] = old_color[0]
            m_color[1] =  old_color[1]
            m_color[2] =  old_color[2]
        } else if (nearColor == 1){
            pixels[width * raw + column]=Color.rgb(255,255,255)
            m_color[0] =  old_color[0]-255
            m_color[1] =  old_color[1]-255
            m_color[2] =  old_color[2]-255
        }else{
            pixels[width * raw + column]=Color.rgb(255,0,0)
            m_color[0] =  old_color[0]-255
            m_color[1] =  old_color[1]
            m_color[2] =  old_color[2]
        }
        //残差 16分之 7、5、3、1，可设置变量
        val rate1 =7/amount.toFloat()
        val rate2 = 5/amount.toFloat()
        val rate3 =3/amount.toFloat()
        val nc1=   getPixel(pixels, width, height, raw, column + 1, rate1, m_color)
        val nc2=   getPixel(pixels, width, height, raw + 1, column, rate2, m_color)
        val nc3=   getPixel(pixels, width, height, raw + 1, column - 1, rate3, m_color)
        setPixel(pixels, width, height, raw, column + 1, nc1)
        setPixel(pixels, width, height, raw + 1, column, nc2)
        setPixel(pixels, width, height, raw + 1, column - 1, nc3)
    }

    val mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    mBitmap.setPixels(pixels, 0, width, 0, 0, width, height)
    return mBitmap
}

private fun getNearstColor(r: Double, g: Double, b: Double): Int {
    val distance0Squared: Int = (Math.pow(r, 2.0) + Math.pow(g, 2.0) + Math.pow(b, 2.0)).toInt()

    val distance255Squared: Int = (Math.pow(255 - r, 2.0) + Math.pow(
        255 - g,
        2.0
    ) + Math.pow(255 - b, 2.0)).toInt()

    val distanceredSquared: Int = (Math.pow(255 - r, 2.0) + Math.pow(g, 2.0) + Math.pow(b, 2.0)).toInt()

    return if (distance0Squared < distance255Squared && distanceredSquared < distance255Squared && distance0Squared < distanceredSquared) {
        0
    } else if (distanceredSquared in (distance255Squared + 1) until distance0Squared){
        1
    } else if (distance255Squared < distanceredSquared && distance255Squared < distance0Squared) {
        1
    } else if (distance0Squared < distance255Squared && distance0Squared < distanceredSquared) {
        0
    } else {
        2
    }
}


private fun getPixel(
    oldColor: IntArray,
    width: Int,
    height: Int,
    raw: Int,
    column: Int,
    rate: Float,
    newColor: IntArray
): Int {
    if (raw < 0 || raw >= height || column < 0 || column >= width) {
        return Color.rgb(255, 255, 255)
    }
    val index: Int = raw * width + column
    var o_r=oldColor[index].red
    var o_g= oldColor[index].green
    var o_b= oldColor[index].blue
    val ne_r=  newColor[0]
    val ne_g=   newColor[1]
    val ne_b=  newColor[2]
    //三目运算获取实际像素点颜色赋值
    o_r = clamp(o_r + (rate * ne_r).toInt())
    o_g = clamp(o_g + (rate * ne_g).toInt())
    o_b = clamp(o_b + (rate * ne_b).toInt())
    return  Color.rgb(o_r, o_g, o_b)
}

private fun setPixel(
    pixels: IntArray,
    width: Int,
    height: Int,
    raw: Int,
    column: Int,
    value: Int
) {
    if (raw < 0 || raw >= height || column < 0 || column >= width) {
        return
    }
    val index: Int = raw * width + column
    pixels[index]= Color.rgb(value.red, value.green, value.blue)
}

fun clamp(value: Int): Int {
    return if (value > 255) 255 else if (value < 0) 0 else value
}

fun convertGreyImgByFloydSendImg(img: Bitmap, pellList: ArrayList<Pel>): Bitmap {
    val width = img.width
    val height = img.height
    val pixels = IntArray(width * height)
    img.getPixels(pixels, 0, width, 0, 0, width, height)
    val gray = IntArray(height * width)
    for (i in 0 until height) {
        for (j in 0 until width) {
            val green = Color.green(pixels[width * i + j])
            gray[width * i + j] = green
        }
    }
    var e = 0
    for (i in 0 until height) {
        for (j in 0 until width) {
            val g = gray[width * i + j]
            val red = Color.red(pixels[width * i + j])
            val green = Color.green(pixels[width * i + j])
            val blue = Color.blue(pixels[width * i + j])
            if (pellList.size != 0) {
                pellList.forEach { pell ->
                    if (pell.region.bounds.left-20 <= j && j <= pell.region.bounds.right+30 &&
                        pell.region.bounds.top-20 <= i && i <= pell.region.bounds.bottom+30
                    ) {
                        val ocr= pixels[width * i + j]
                        val old_color: IntArray =   intArrayOf(ocr.red, ocr.green, ocr.blue)
                        var m_color  : IntArray = intArrayOf(0,0,0,0)
                        val nearColor=  getNearstColor(old_color[0].toDouble(), old_color[1].toDouble(), old_color[2].toDouble())
                        if (nearColor == 0) {
                            pixels[width * i + j]=Color.rgb(0,0,0)
                            m_color[0] = old_color[0]
                            m_color[1] =  old_color[1]
                            m_color[2] =  old_color[2]
                        } else if (nearColor == 1){
                            pixels[width * i + j]=Color.rgb(255,255,255)
                            m_color[0] =  old_color[0]-255
                            m_color[1] =  old_color[1]-255
                            m_color[2] =  old_color[2]-255
                        }else{
                            pixels[width * i + j]=Color.rgb(255,0,0)
                            m_color[0] =  old_color[0]-255
                            m_color[1] =  old_color[1]
                            m_color[2] =  old_color[2]
                        }
                        //残差 16分之 7、5、3、1，可设置变量

                        val rate1 =7/pell.image_effect_size.toFloat()
                        val rate2 = 5/pell.image_effect_size.toFloat()
                        val rate3 =3/pell.image_effect_size.toFloat()
                        val nc1=   getPixel(pixels, width, height, i, j + 1, rate1, m_color)
                        val nc2=   getPixel(pixels, width, height, i + 1, j, rate2, m_color)
                        val nc3=   getPixel(pixels, width, height, i + 1, j - 1, rate3, m_color)
                        setPixel(pixels, width, height, i, j + 1, nc1)
                        setPixel(pixels, width, height, i + 1, j, nc2)
                        setPixel(pixels, width, height, i + 1, j - 1, nc3)
                    } else {
                        if (g >= 128) {
                            pixels[width * i + j] = 0xffffff
                            e = g - 255
                        } else {
                            if (red - green > 128 && red - blue > 128) {
                                pixels[width * i + j] = 0xff0000
                            } else {
                                pixels[width * i + j] = 0x000000
                            }
                            e = g - 0
                        }
                    }
                }
            } else {
                if (g >= 128) {
                    pixels[width * i + j] = 0xffffff
                    e = g - 255
                } else {
                    if (red - green > 128 && red - blue > 128) {
                        pixels[width * i + j] = 0xff0000
                    } else {
                        pixels[width * i + j] = 0x000000
                    }
                    e = g - 0
                }
            }
        }
    }
    val mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
    mBitmap.setPixels(pixels, 0, width, 0, 0, width, height)
    return mBitmap;
}

fun convertGreyImgByFloydSend(bitmap: Bitmap, pixelsSize: Int = 0): Bitmap {
    val width = bitmap.width
    val height = bitmap.height
    val pixels = IntArray(width * height)
    bitmap.getPixels(pixels, 0, width, 0, 0, width, height)
    val gray = IntArray(height * width)
    for (i in 0 until height) {
        for (j in 0 until width) {
            val green = Color.green(pixels[width * i + j])
            gray[width * i + j] = green
        }
    }
    var e = 0
    for (i in 0 until height) {
        for (j in 0 until width) {
            val g = gray[width * i + j]
            val red = Color.red(pixels[width * i + j])
            val green = Color.green(pixels[width * i + j])
            val blue = Color.blue(pixels[width * i + j])
            if (g >= 128) {
                pixels[width * i + j] = 0xffffff
                e = g - 255
            } else {
                if (red - green > pixelsSize && red - blue > pixelsSize) {
                    pixels[width * i + j] = 0xff0000
                } else {
                    pixels[width * i + j] = 0x000000
                }
                e = g - 0
            }
        }
    }
    val mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
    mBitmap.setPixels(pixels, 0, width, 0, 0, width, height)
    return mBitmap
}

