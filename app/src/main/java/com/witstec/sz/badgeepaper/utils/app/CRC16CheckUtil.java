package com.witstec.sz.badgeepaper.utils.app;

public class CRC16CheckUtil {


    public static String getCRC2(byte[] bytes) {
        int CRC = 0xffff;
        int POLYNOMIAL = 0X1021;
        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC = CRC ^ ((int) bytes[i] << 8);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0X8000) != 0) {
                    CRC = (CRC << 1) ^ POLYNOMIAL;
                } else {
                    CRC <<= 1;
                }
            }
            CRC &= 0XFFFF;
        }
        return Integer.toHexString(CRC);
    }


    public static int getCRC3(byte[] bytes) {
        int CRC = 0xffff;
        int POLYNOMIAL = 0X1021;
        int i, j;
        for (i = 0; i < bytes.length; i++) {
            CRC = CRC ^ ((int) bytes[i] << 8);
            for (j = 0; j < 8; j++) {
                if ((CRC & 0X8000) != 0) {
                    CRC = (CRC << 1) ^ POLYNOMIAL;
                } else {
                    CRC <<= 1;
                }
            }
            CRC &= 0XFFFF;
        }
        return CRC;
    }
}
