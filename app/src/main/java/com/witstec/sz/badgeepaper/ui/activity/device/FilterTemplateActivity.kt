package com.witstec.sz.badgeepaper.ui.activity.device

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.adapter.MyFragmentPagerAdapter
import com.witstec.sz.badgeepaper.ui.freagment.local.OfficialLocalFragment
import com.witstec.sz.badgeepaper.ui.freagment.local.UserLocalFragment
import com.witstec.sz.badgeepaper.view.TabView
import com.wuhenzhizao.titlebar.widget.CommonTitleBar
import kotlinx.android.synthetic.main.activity_filter_template.*
import java.util.*

class FilterTemplateActivity : AppBaseActivity() {

    private val fragment_list = ArrayList<Fragment>()
    private var adapter: MyFragmentPagerAdapter? = null

    companion object {
        var BLE_TYPE: String = "2"
        val EXTRO_GROUP_SELECT_CODE = 0x03
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_template)
        BLE_TYPE = intent.getStringExtra("extra_size_type").toString()
        val ios_tab = titlebar.centerCustomView.findViewById<TabView>(R.id.tab_list)
        val tabs =
            arrayListOf(getString(R.string.my_template), getString(R.string.download_template))
        ios_tab.setTabs(tabs)
        fragment_list.add(UserLocalFragment())
        fragment_list.add(OfficialLocalFragment())
        adapter = MyFragmentPagerAdapter(supportFragmentManager, tabs, fragment_list)
        viewPager.adapter = adapter
        viewPager.currentItem = 0
        ios_tab.setOnSelectedItemChange(object : TabView.OnSelectedItemChange {
            override fun onChange(select: Int) {
                viewPager.currentItem = select
            }
        })

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                ios_tab.setSelectedPosition(position)
            }
        })

        titlebar.setListener { v, action, _ ->
            if (action == CommonTitleBar.ACTION_LEFT_BUTTON || action == CommonTitleBar.ACTION_LEFT_TEXT) {
                onBackPressed()
            }
        }
    }

}
