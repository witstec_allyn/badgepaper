package com.witstec.sz.badgeepaper.network;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.ExtKt;
import com.witstec.sz.badgeepaper.model.bean.AppUpdate;
import com.witstec.sz.badgeepaper.model.bean.BleTemplate;
import com.witstec.sz.badgeepaper.model.bean.BleTemplateType;
import com.witstec.sz.badgeepaper.model.bean.DeviceBean;
import com.witstec.sz.badgeepaper.model.bean.ImageBean;
import com.witstec.sz.badgeepaper.model.bean.InputTemplateBean;
import com.witstec.sz.badgeepaper.model.bean.LoginRequest;
import com.witstec.sz.badgeepaper.model.bean.ModelTypeBean;
import com.witstec.sz.badgeepaper.model.bean.RegisterRequest;
import com.witstec.sz.badgeepaper.model.bean.TemplateBean;
import com.witstec.sz.badgeepaper.model.bean.TokenBean;
import com.witstec.sz.badgeepaper.model.bean.user.UserMsg;
import com.witstec.sz.badgeepaper.utils.LogHelper;
import com.witstec.sz.badgeepaper.utils.app.AppUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class ApiRepository {

    /**
     * 登录
     *
     * @param email
     * @param pwd
     * @return
     */
    public static Observable<LoginRequest> login(String email, String pwd, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("phone/email", email);
        jsonObject.addProperty("password", pwd);
        jsonObject.addProperty("lang", AppUtils.getLocaleLanguage());
        return RetrofitManager.getWordPressService()
                .login(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 刷新Token
     *
     * @return
     */
    public static Observable<TokenBean> tokenRefresh(String refresh_token, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("refresh_token", refresh_token);
        return RetrofitManager.getWordPressService()
                .tokenRefresh(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取验证码
     *
     * @param email
     * @return
     */
    public static Observable<ResponseBody> getNewPwdCode(String email, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("phone/email", email);
        return RetrofitManager.getWordPressService()
                .getNewPwdCode(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 找回密码
     *
     * @param email
     * @param code
     * @param newPwd
     * @param context
     * @return
     */
    public static Observable<ResponseBody> foundback(String email, String code, String newPwd, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("phone/email", email);
        jsonObject.addProperty("verifycode", code);
        jsonObject.addProperty("new_password", newPwd);
        return RetrofitManager.getWordPressService()
                .foundback(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 登出
     *
     * @return
     */
    public static Observable<ResponseBody> logout(Context context) {
        return RetrofitManager.getWordPressService()
                .logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 注册
     *
     * @param email
     * @param pwd
     * @return
     */
    public static Observable<RegisterRequest> register(String userName, String email, String pwd, String language_type, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("nickname", userName);
        jsonObject.addProperty("password", pwd);
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("lang", language_type);
        return RetrofitManager.getWordPressService()
                .register(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 修改密码
     *
     * @param oldPassowrd
     * @param newPassowrd
     * @return
     */
    public static Observable<ResponseBody> changePwd(String oldPassowrd, String newPassowrd, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("old_password", oldPassowrd);
        jsonObject.addProperty("new_password", newPassowrd);
        return RetrofitManager.getWordPressService()
                .changePwd(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 获取设备型号
     */
    public static Observable<ModelTypeBean> getDeviceTypes(Context context) {
        return RetrofitManager.getWordPressService()
                .getDeviceTypes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取设备列表
     *
     * @param tags
     * @param page
     * @param context
     * @return
     */
    public static Observable<DeviceBean> getBindList(String search, String tags, String page, Context context) {
        return RetrofitManager.getWordPressService()
                .getBindList(tags, search, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 绑定设备
     *
     * @param mac
     * @param color
     * @param devicename
     * @param type
     * @param name
     * @param ver
     * @param template_type_id
     * @param context
     * @return
     */
    public static Observable<ResponseBody> addDevice(String mac, String color, String devicename, String type, String name
            , String ver, int template_type_id, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("mac", mac);
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("type", type);
        jsonObject.addProperty("color", color);
        jsonObject.addProperty("devicename", devicename);
        jsonObject.addProperty("ver", ver);
        jsonObject.addProperty("tags", template_type_id);
        return RetrofitManager.getWordPressService()
                .addDevice(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 创建模板
     *
     * @param name
     * @param type_id
     * @param size
     * @param img
     * @param xmlString
     * @param context
     * @return
     */
    public static Observable<ResponseBody> createTemplate(String name, int type_id, String size, String img, String xmlString, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("key", ExtKt.getStringData(Constants.ADMIN_KEY));
        jsonObject.addProperty("id", AppUtils.getRandomString(16));
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("type", type_id);
        jsonObject.addProperty("size", size);
        jsonObject.addProperty("img", img);
        jsonObject.addProperty("xml_file", xmlString);
        LogHelper.INSTANCE.i("jsonObj", jsonObject.toString());
        return RetrofitManager.getWordPressService()
                .createTemplate(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 上传用户设备信息
     *
     * @return
     */
    public static Observable<ResponseBody> uploadUserDeviceMsg(Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("language", AppUtils.getLocaleLanguage());
        jsonObject.addProperty("operate_system", "android");
        jsonObject.addProperty("machine_type", AppUtils.getPhoneModel());
        return RetrofitManager.getWordPressService()
                .uploadUserDeviceMsg(ExtKt.getStringData(Constants.UID), jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 修改模板
     */
    public static Observable<ResponseBody> updateTemplate(String tempId, String name, String img, String xmlString, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("key", ExtKt.getStringData(Constants.ADMIN_KEY));
        jsonObject.addProperty("id", tempId);
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("img", img);
        jsonObject.addProperty("xml_file", xmlString);
        return RetrofitManager.getWordPressService()
                .updateTemplate(tempId, jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取模板类型列表
     *
     * @return
     */
    public static Observable<BleTemplateType> getTags(Context context) {
        //AppUtils.getLocaleLanguage()
        return RetrofitManager.getWordPressService()
                .getTags()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取模板列表
     *
     * @return
     */
    public static Observable<BleTemplate> getTemplateList(String search, int tagId, int page, Context context) {
        return RetrofitManager.getWordPressService()
                .getTemplateList(tagId, page, search, AppUtils.getLocaleLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取模板列表，筛选
     */
    public static Observable<BleTemplate> getTemplateListFilter(int tagId, int page, Context context) {
        return RetrofitManager.getWordPressService()
                .getTemplateListFilter(tagId, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取模板插入数据
     *
     * @param device_id
     * @return
     */
    public static Observable<InputTemplateBean> getTemplateInputData(String device_id, Context context) {
        return RetrofitManager.getWordPressService()
                .getTemplateInputData(device_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 获取单个模板信息
     *
     * @param temp_id
     * @param context
     * @return
     */
    public static Observable<TemplateBean> getTemplate(String temp_id, Context context) {
        return RetrofitManager.getWordPressService()
                .getTemplate(temp_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 保存模板输入框数据
     */
    public static Observable<ResponseBody> saveTemplateInputData(String device_id, String template_id, JsonArray jsonArrayData, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("template_id", template_id);
        jsonObject.add("content", jsonArrayData);
        LogHelper.INSTANCE.i("jsonData", jsonObject.toString());
        return RetrofitManager.getWordPressService()
                .saveTemplateInputData(device_id, jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 模板删除
     *
     * @param modalId
     * @return
     */
    public static Observable<ResponseBody> modalDel(String modalId, Context context) {
        return RetrofitManager.getWordPressService()
                .modalDel(modalId, ExtKt.getStringData(Constants.ADMIN_KEY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 删除设备
     *
     * @param mac
     * @return
     */
    public static Observable<ResponseBody> unbind(String mac, Context context) {
        return RetrofitManager.getWordPressService()
                .unbind(mac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 修改设备信息
     *
     * @param mac
     * @param newName
     * @param note
     * @return
     */
    public static Observable<ResponseBody> updDeviceInfo(String mac, String newName, String note, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("devicename", newName);
        jsonObject.addProperty("remark", note);
        return RetrofitManager.getWordPressService()
                .updDeviceInfo(mac, jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 固件版本升级结果返回
     *
     * @param mac
     * @return
     */
    public static Observable<ResponseBody> updateOta(String mac, String ver, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ver", ver);
        return RetrofitManager.getWordPressService()
                .updateOta(mac, jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * 修改用户个人信息
     *
     * @return
     */
    public static Observable<ResponseBody> updUserInfo(String name, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("nickname", name);
        return RetrofitManager.getWordPressService()
                .updUserInfo(ExtKt.getStringData(Constants.UID), jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * app用户反馈
     *
     * @return
     */
    public static Observable<ResponseBody> setFeedback(String ftype, String ucontent, List<String> pictures, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("ftype", ftype);
        jsonObject.addProperty("ucontent", ucontent);
        JsonArray jsonArray = new JsonArray();
        for (int i = 0; i < pictures.size(); i++) {
            jsonArray.add(pictures.get(i));
        }
        jsonObject.add("picture", jsonArray);
        LogHelper.INSTANCE.i("witstec", jsonObject.toString());
        return RetrofitManager.getWordPressService()
                .setFeedback(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 修改头像
     */
    public static Observable<ResponseBody> uploadAvatar(File file, Context context) {
        String base64 = AppUtils.imageToBase64(file.getPath());
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("img", base64);
        return RetrofitManager.getWordPressService()
                .uploadAvatar(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 上传日志
     *
     * @param file
     * @param context
     * @return
     */
    public static Observable<ImageBean> uploadLogs(File file, Context context) throws FileNotFoundException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("logs", AppUtils.xmlToString(new FileInputStream(file)));
        return RetrofitManager.getWordPressService()
                .uploadLogs(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * 检车更新
     *
     * @param context
     * @return
     */
    public static Observable<AppUpdate> appCheck(Context context) {
        return RetrofitManager.getWordPressService()
                .appCheck(1, AppUtils.getLocaleLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }


    /**
     * ota检查更新
     *
     * @param context
     * @return
     */
    public static Observable<ResponseBody> otaCheck(Context context, String type) {
        return RetrofitManager.getWordPressService()
                .otaCheck("12:9A:25:00:00:00", type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.<ResponseBody>unWrap(context));
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    public static Observable<UserMsg> getUserInfo(Context context) {
        return RetrofitManager.getWordPressService()
                .getUserInfo(ExtKt.getStringData(Constants.UID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.unWrap(context));
    }

    /**
     * app检查更新
     *
     * @return
     */
    public static Observable<AppUpdate> appUpgrade(Context context) {
        return RetrofitManager.getWordPressService()
                .appUpgrade(AppUtils.getLocaleLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxRepositoryResponse.<AppUpdate>unWrap(context));
    }


}
