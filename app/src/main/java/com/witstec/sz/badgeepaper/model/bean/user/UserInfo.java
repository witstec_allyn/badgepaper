package com.witstec.sz.badgeepaper.model.bean.user;

public class UserInfo {

    /**
     * uid : 42f46e89ec11878f
     * name : null
     * logo :
     * token : 15b724e4cf74e7df584ecd8f58001be41e6b129e
     * expire_at : 1569148993
     */

    private String uid;
    private String name;
    private String logo;
    private String token;
    private int expire_at;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getExpire_at() {
        return expire_at;
    }

    public void setExpire_at(int expire_at) {
        this.expire_at = expire_at;
    }
}
