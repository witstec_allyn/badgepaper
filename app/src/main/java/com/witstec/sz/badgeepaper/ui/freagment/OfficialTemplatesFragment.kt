package com.witstec.sz.badgeepaper.ui.freagment


import android.Manifest
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.github.nukc.stateview.StateView
import com.witstec.sz.badgeepaper.Constants
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.draw.Constant
import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity
import com.witstec.sz.badgeepaper.getStringData
import com.witstec.sz.badgeepaper.model.bean.BleTemplate
import com.witstec.sz.badgeepaper.model.bean.TemplateOfficialBean
import com.witstec.sz.badgeepaper.model.db.LocalDataSource.Companion.instance
import com.witstec.sz.badgeepaper.model.event.ChangeTemplateListEvent
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.ui.adapter.OfficialTemplateAdapter
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.utils.DownloadUtil
import com.witstec.sz.badgeepaper.utils.FileManager.getExternalFilesDir
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.RxBus
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.view.IosDialog
import com.witstec.sz.badgeepaper.view.SearchRelativeLayout
import com.witstec.sz.badgeepaper.view.SimpleDividerDecoration
import com.yanzhenjie.permission.AndPermission
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_official_templates.*
import kotlinx.android.synthetic.main.header_view_search_layout.view.*
import java.io.File


/**
 *
 */
class OfficialTemplatesFragment : BaseFragment() {

    private var statView: StateView? = null
    private var mAdapter: OfficialTemplateAdapter? = null

    // 标志位，标志已经初始化完成
    private var isPrepared: Boolean = false
    private var mTagId = 2
    private var mPage: Int = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_official_templates, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter =
            OfficialTemplateAdapter(object : OfficialTemplateAdapter.onDownloadOfficialClick {
                override fun onClick(item: BleTemplate.TemplatesBean) {
                    showDialog()
                    var xmlPath = ""
                    var error = ""
                    val disposable = Observable.create(ObservableOnSubscribe<Boolean> { sub ->
                        DownloadUtil.get()
                            .downloadMsg(Constants.URL + item.url,
                                Constant.SAVE_PATH,
                                "${item.id}.xml",
                                object :
                                    DownloadUtil.OnDownloadExistenceListener {
                                    override fun onDownloadSuccess(file: File) {
                                        xmlPath = file.absolutePath
                                        sub.onNext(true)
                                        LogHelper.i("downFile", "downFile=" + file.absolutePath)
                                    }

                                    override fun onDownloading(progress: Int) {
                                        LogHelper.i("downFile", "progress=$progress")
                                    }

                                    override fun onDownloadFailed(e: Exception) {
                                        error = e.message.toString()
                                        sub.onNext(true)
                                        LogHelper.i("downFile", "downFile=" + e.message)
                                    }

                                    override fun onDownloadFailedExistence(e: String?) {
                                        activity!!.runOnUiThread {
                                            dialogDismiss()
                                            ToastUtils.show(getString(R.string.file_is_downloaded_locally))
                                        }
                                    }
                                })
                    })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ isStatus ->
                            var pngPath = ""
                            val disposable =
                                Observable.create(ObservableOnSubscribe<Boolean> { sub ->
                                    DownloadUtil.get()
                                        .downloadMsg(Constants.URL + item.img,
                                            Constant.SAVE_PATH,
                                            "${item.id}.png",
                                            object :
                                                DownloadUtil.OnDownloadExistenceListener {
                                                override fun onDownloadSuccess(file: File) {
                                                    pngPath = file.absolutePath
                                                    sub.onNext(true)
                                                    LogHelper.i(
                                                        "downFile",
                                                        "downFile=" + file.absolutePath
                                                    )
                                                }

                                                override fun onDownloading(progress: Int) {
                                                    LogHelper.i("downFile", "progress=$progress")
                                                }

                                                override fun onDownloadFailed(e: Exception) {
                                                    error = e.message.toString()
                                                    sub.onNext(true)
                                                    LogHelper.i("downFile", "downFile=" + e.message)
                                                }

                                                override fun onDownloadFailedExistence(e: String?) {
                                                    activity!!.runOnUiThread {
                                                        dialogDismiss()
                                                        ToastUtils.show(getString(R.string.file_is_downloaded_locally))
                                                    }
                                                }
                                            })
                                })
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({ isStatus ->
                                        dialogDismiss()
                                        instance.save(
                                            TemplateOfficialBean(
                                                item.id,
                                                item.name,
                                                item.type,
                                                pngPath,
                                                xmlPath,
                                                AppUtils.getCurrentDate(AppUtils.dateFormatYMDHMS)
                                            )
                                        )
                                        ToastUtils.show(getString(R.string.download_end))
                                    }, {
                                        dialogDismiss()
                                        ToastUtils.show(it.message)
                                    })
                            addDisposable(disposable)
                        }, {
                            dialogDismiss()
                            ToastUtils.show(it.message)
                        })
                    addDisposable(disposable)
                }
            })
        mAdapter!!.setEnableLoadMore(false)
        statView = StateView.inject(recycler)
        statView!!.showLoading()
        recycler.adapter = mAdapter
        recycler.layoutManager = LinearLayoutManager(activity)
        recycler.addItemDecoration(SimpleDividerDecoration(activity))
        statView!!.showLoading()
        getBleData()
        isPrepared = true

        statView!!.setOnRetryClickListener {
            mPage = 1
            getBleData()
        }

        relativeLayout.setOnRefreshListener {
            relativeLayout.isRefreshing = true
            mPage = 1
            getBleData()
        }

        val adminKey =
            getStringData(Constants.ADMIN_KEY)
        if (adminKey.isNotEmpty()) {
            root_floatingActionButton!!.visibility = View.VISIBLE
        } else {
            root_floatingActionButton!!.visibility = View.GONE
        }

        floatingActionButton.setOnClickListener {
            val adminKey =
                getStringData(Constants.ADMIN_KEY)
            if (adminKey.isNotEmpty()) {
                MaterialDialog.Builder(activity!!)
                    .title(getString(R.string.device_type))
                    .items("2.13")
                    .positiveText(getString(R.string.fixed))
                    .negativeText(getString(R.string.cancel))
                    .itemsCallbackSingleChoice(0) { _, _, which, text ->
                        var typeId = 0
                        if (which == 0) {
                            typeId = 2
                        } else if (which == 1) {
                            typeId = 3
                        }
                        DrawingMainActivity.start(
                            activity,
                            typeId
                            , false
                        )
                        true
                    }
                    .show()
            }
        }

        mAdapter!!.setOnItemClickListener { adapter, view, position ->
            val adminKey =
                getStringData(Constants.ADMIN_KEY)
            if (adminKey.isNotEmpty()) {
                LogHelper.i(
                    "witstec",
                    "xml文件路径=" + Constants.URL + mAdapter!!.getItem(position)!!.url
                )
                val permission = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                AndPermission.with(this)
                    .runtime()
                    .permission(permission)
                    .onGranted {
                        downFile(
                            mAdapter!!.getItem(position)!!.id,
                            mAdapter!!.getItem(position)!!.name,
                            AppUtils.typeNameToTypeId(mAdapter!!.getItem(position)!!.type),
                            Constants.URL + mAdapter!!.getItem(position)!!.url
                        )
                    }
                    .onDenied {
                        ToastUtils.show(getString(R.string.no_permissions))
                    }.start()
            }
        }
        mAdapter!!.setOnItemLongClickListener { adapter, view, position ->
            val adminKey =
                getStringData(Constants.ADMIN_KEY)
            if (adminKey.isNotEmpty()) {
                IosDialog(activity!!).init()
                    .setTitle(getString(R.string.template_selection_delete))
                    .setMsg(getString(R.string.do_you_want_template_selection_delete))
                    .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                        ApiRepository.modalDel(mAdapter!!.getItem(position)!!.id, activity!!)
                            .subscribe({
                                mAdapter!!.remove(position)
                            }, {
                                ToastUtils.show(it.message)
                            })
                    })
                    .setNegativeButton(getString(R.string.cancel), View.OnClickListener {
                    })
                    .show()
            }
            true
        }


        addDisposable(
            RxBus.register(ChangeTemplateListEvent::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mPage = 1
                    getBleData()
                    val adminKey =
                        getStringData(Constants.ADMIN_KEY)
                    if (adminKey.isNotEmpty()) {
                        root_floatingActionButton!!.visibility = View.VISIBLE
                    } else {
                        root_floatingActionButton!!.visibility = View.GONE
                    }
                })


    }

    private fun getBleData() {
        statView!!.showLoading()
        addDisposable(
            ApiRepository.getTemplateList("", mTagId, mPage, activity)
                .subscribe({
                    val templatesList = it.templates
                    relativeLayout.isRefreshing = false
                    statView!!.showContent()
                    if (mPage == 1) {
                        mAdapter!!.loadMoreComplete()
                        mAdapter!!.replaceData(templatesList)
                        if (mAdapter!!.data.size != 0) {
                            if (mAdapter!!.headerLayoutCount == 0) {
                                val view =
                                    View.inflate(activity, R.layout.header_view_search_layout, null)
                                view!!.searchLayout.setTextChangedListener(object :
                                    SearchRelativeLayout.TextChangedListener {
                                    override fun onTextChanged(text: String) {
                                        if (text.isNotEmpty() || text != "") {
                                            mAdapter!!.searchKeyword = text
                                            addDisposable(
                                                ApiRepository.getTemplateList(
                                                    text,
                                                    mTagId,
                                                    mPage,
                                                    activity
                                                ).subscribe({ search ->
                                                    mAdapter!!.replaceData(search.templates)
                                                }, { error ->
                                                    ToastUtils.show(error.message)
                                                })
                                            )
                                            LogHelper.i("onTextChanged", text)
                                        } else {
                                            mAdapter!!.searchKeyword = ""
                                            mPage = 1
                                            getBleData()
                                        }
                                    }
                                })
                                mAdapter!!.addHeaderView(view)
                            }
                        } else {
                            statView!!.showEmpty()
                        }
                    } else {
                        if (templatesList.size == 0) {
                            mAdapter!!.loadMoreEnd()
                        } else {
                            mAdapter!!.loadMoreComplete()
                            mAdapter!!.addData(templatesList)
                        }
                    }
                }, {
                    relativeLayout.isRefreshing = false
                    if (mPage > 1) {
                        mPage--
                    }
                    if (mPage == 1) {
                        statView!!.showRetry()
                    } else {
                        mAdapter!!.loadMoreFail()
                    }
                    val errSpit = it.message!!.split(",")
                    if (errSpit[0] == "40106") {
                        ToastUtils.show(getString(R.string.Account_expired))
                    } else {
                        ToastUtils.show(it.message)
                    }
                })
        )
    }

    /**
     * 文件下载
     */
    private fun downFile(
        templateId: String,
        templateName: String,
        templateType: String,
        xmlPath: String
    ) {
        showDialog()
        var filePath = ""
        var error = ""
        val disposable = Observable.create(ObservableOnSubscribe<Boolean> { sub ->
            DownloadUtil.get()
                .download(xmlPath,
                    getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)?.path,
                    "${AppUtils.getRandomString(20)}.xml",
                    object : DownloadUtil.OnDownloadListener {
                        override fun onDownloadSuccess(file: File) {
                            filePath = file.absolutePath
                            sub.onNext(true)
                            LogHelper.i("downFile", "downFile=" + file.absolutePath)
                        }

                        override fun onDownloading(progress: Int) {
                            LogHelper.i("downFile", "progress=$progress")
                        }

                        override fun onDownloadFailed(e: Exception) {
                            error = e.message.toString()
                            sub.onNext(true)
                            LogHelper.i("downFile", "downFile=" + e.message)
                        }
                    })
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ isStatus ->
                SystemClock.sleep(200)
                dialogDismiss()
                DrawingMainActivity.start(
                    activity,
                    templateId,
                    templateName,
                    templateType.toInt(),
                    filePath,
                    false
                )
            }, {
                dialogDismiss()
                ToastUtils.show(it.message)
            })
        addDisposable(disposable)
    }
}

