package com.witstec.sz.badgeepaper.draw.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.litesuits.orm.db.assit.WhereBuilder;
import com.litesuits.orm.db.model.ColumnsValue;
import com.litesuits.orm.db.model.ConflictAlgorithm;
import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.ExtKt;
import com.witstec.sz.badgeepaper.R;
import com.witstec.sz.badgeepaper.draw.Constant;
import com.witstec.sz.badgeepaper.draw.MultipleLayout;
import com.witstec.sz.badgeepaper.draw.bean.Pel;
import com.witstec.sz.badgeepaper.draw.bean.Picture;
import com.witstec.sz.badgeepaper.draw.bean.Text;
import com.witstec.sz.badgeepaper.draw.step.CopyPelStep;
import com.witstec.sz.badgeepaper.draw.step.DeletePelStep;
import com.witstec.sz.badgeepaper.draw.step.DrawPelStep;
import com.witstec.sz.badgeepaper.draw.step.Step;
import com.witstec.sz.badgeepaper.draw.touch.DrawBesselTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawBrokenLineTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawFreehandTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawLineTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawOvalTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawPolygonTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawRectTouch;
import com.witstec.sz.badgeepaper.draw.touch.DrawTouch;
import com.witstec.sz.badgeepaper.draw.touch.Touch;
import com.witstec.sz.badgeepaper.draw.touch.TransformTouch;
import com.witstec.sz.badgeepaper.draw.ui.view.CanvasView;
import com.witstec.sz.badgeepaper.model.bean.TemplateBean;
import com.witstec.sz.badgeepaper.model.bean.TemplateOfficialBean;
import com.witstec.sz.badgeepaper.model.bean.TemplateSizeType;
import com.witstec.sz.badgeepaper.model.db.LocalDataSource;
import com.witstec.sz.badgeepaper.model.event.ChangeTemplateListEvent;
import com.witstec.sz.badgeepaper.network.ApiRepository;
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity;
import com.witstec.sz.badgeepaper.utils.LogHelper;
import com.witstec.sz.badgeepaper.utils.RxBus;
import com.witstec.sz.badgeepaper.utils.ToastUtils;
import com.witstec.sz.badgeepaper.utils.ZXingUtils;
import com.witstec.sz.badgeepaper.utils.app.AppUtils;
import com.witstec.sz.badgeepaper.view.IosDialog;
import com.witstec.sz.badgeepaper.view.cop.CropImageView;
import com.wuhenzhizao.titlebar.widget.CommonTitleBar;
import com.zhihu.matisse.Matisse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import io.reactivex.disposables.Disposable;
import top.zibin.luban.Luban;

public class DrawingMainActivity extends AppBaseActivity {

    private MultipleLayout multipleLayout;
    private Paint paint = null;

    @SuppressLint("StaticFieldLeak")
    @NonNull
    public static Context context;
    private ImageView btnSubmit;
    private CommonTitleBar commonTitleBar;
    private static List<Pel> pelList;
    static Pel selectedPel;
    private static Stack<Step> undoStack;
    private static Stack<Step> redoStack;
    static CanvasView canvasView;
    private static TextView tv_mode_type;
    static String templateId;
    private static int templateType;
    private static TemplateSizeType sizeType;
    private Bitmap bitmapEffect;
    private int image_effect_size = Constants.image_effect_size;
    private int mTextSize = 2;
    /**
     * 数据文件路径
     */
    private String templateXmlString;
    private String error = "";
    private static String templateName;
    private static boolean isLocal;
    private static boolean isCoexistencel;

    public static void start(Context context, int typeId, boolean isLocal) {
        Intent intent = new Intent(context, DrawingMainActivity.class);
        intent.putExtra("extra_typeId", typeId);
        intent.putExtra("extra_isLocal", isLocal);
        context.startActivity(intent);
    }

    public static void start(Context context, String templateId, String templateName, int typeId, String xmlPath, boolean isLocal) {
        Intent intent = new Intent(context, DrawingMainActivity.class);
        intent.putExtra("extra_templateId", templateId);
        intent.putExtra("extra_templateName", templateName);
        intent.putExtra("extra_typeId", typeId);
        intent.putExtra("templateXmlString", xmlPath);
        intent.putExtra("extra_isLocal", isLocal);
        context.startActivity(intent);
    }

    public static void start(Context context, String templateId, String templateName, int typeId, String xmlPath, boolean isLocal, boolean isCoexistencel) {
        Intent intent = new Intent(context, DrawingMainActivity.class);
        intent.putExtra("extra_templateId", templateId);
        intent.putExtra("extra_templateName", templateName);
        intent.putExtra("extra_typeId", typeId);
        intent.putExtra("templateXmlString", xmlPath);
        intent.putExtra("extra_isLocal", isLocal);
        intent.putExtra("extra_isCoexistencel", isCoexistencel);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawing_main);
        templateId = getIntent().getStringExtra("extra_templateId");
        templateType = getIntent().getIntExtra("extra_typeId", 2);
        isLocal = getIntent().getBooleanExtra("extra_isLocal", false);
        initView();
        setListener();
        if (templateId != null) {
            setToolbar(getString(R.string.template_editing));
            templateXmlString = getIntent().getStringExtra("templateXmlString");
            templateName = getIntent().getStringExtra("extra_templateName");
            isCoexistencel = getIntent().getBooleanExtra("extra_isCoexistencel", false);
            if (isLocal) {
                ArrayList<TemplateBean> templateBeans = LocalDataSource.Companion.getInstance()
                        .getQueryByWhere(TemplateBean.class, "templateId", templateId);
                templateXmlString = templateBeans.get(0).getTemplateXmlString();
            }
            if (templateXmlString != null) {
                try {
                    canvasView.loadFileDatXml(templateXmlString, isLocal);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            setToolbar(getString(R.string.create_template));
        }
    }

    private void setListener() {
        //二维码/条形码监听器
        multipleLayout.setBarCodeClickCallback(new MultipleLayout.BarCodeClickCallback() {
            @Override
            public void onBarCodeContext(String text) {
                if (text.isEmpty()) {
                    return;
                }
                insertBarCode(31, text);
                //可拖动
                clearRedoStack();
                CanvasView.setmChildTouch(new TransformTouch(DrawingMainActivity.this));
            }

            @Override
            public void onQrCodeContext(String text) {
                if (text.isEmpty()) {
                    return;
                }
                insertBarCode(32, text);
                //可拖动
                clearRedoStack();
                CanvasView.setmChildTouch(new TransformTouch(DrawingMainActivity.this));
            }
        });

        //图片监听器
        multipleLayout.setImageViewClickCallback(() -> {
            int index = 0;
            List<Pel> pelList = CanvasView.getPelList();
            for (Pel pel : pelList) {
                if (pel.type == 30) {
                    index++;
                }
            }
            if (index >= 15) {
                ToastUtils.show(getString(R.string.Maximum_number_of_inserted_pictures_reached) + index);
                return;
            }
            ExtKt.selectImage(DrawingMainActivity.this, 1);
        });

        //文字
        multipleLayout.setTextClickCallback(new MultipleLayout.TextClickCallback() {
            @Override
            public void onTextDefault() {

            }

            @Override
            public void onContext(String content, boolean isVertical, int color, int textSize) {
                mTextSize = textSize;
                Rect rect = new Rect();
                (canvasView.getDrawTextPaint()).getTextBounds(content, 0, content.length(), rect);
                //内容宽高
                PointF centerPoint = new PointF();
                //文本中心
                centerPoint.set(new PointF(CanvasView.CANVAS_WIDTH / 2f, new Random().nextInt(CanvasView.CANVAS_HEIGHT)));
                //文本开始坐标
                PointF beginPoint = new PointF(centerPoint.x - (rect.width() >> 1),
                        centerPoint.y - (rect.height() >> 1));
                //文本区域
                Region region = new Region();
                //横向显示
                if (!isVertical) {
                    region.set((int) (centerPoint.x - (rect.width() / 3)),
                            (int) centerPoint.y - (rect.height() / 3),
                            (int) (centerPoint.x + (rect.width() / 3)),
                            (int) centerPoint.y + (rect.height() / 3));
                }
                //竖向显示
                else {
                    if (AppUtils.isContainsLetter(content)) {
                        region.set((int) centerPoint.x - (rect.height() / 2),
                                ((int) centerPoint.y - (rect.width() / 2) + 6),
                                (int) (centerPoint.x + (rect.height() / 2)),
                                ((int) centerPoint.y + (rect.width() / 2) + 6));
                    } else {
                        region.set((int) centerPoint.x - (rect.height() / 2),
                                ((int) centerPoint.y - (rect.width() / 4)),
                                (int) (centerPoint.x + (rect.height() / 2)),
                                ((int) centerPoint.y + (rect.width() / 4)));
                    }

                }
                Pel newPel = new Pel();
                switch (mTextSize) {
                    case 1:
                        newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_45);
                        break;
                    case 2:
                        newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_55);
                        break;
                    case 3:
                        newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_65);
                        break;
                    case 4:
                        newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_75);
                        break;
                    case 5:
                        newPel.paint.setTextSize(Constants.PAINT_DEFAULT_TEXT_SIZE_85);
                        break;
                }
                newPel.textSize = mTextSize;
                Text text = new Text(content, isVertical, newPel.textSize);
                newPel.type = 20;
                newPel.text = text;
                newPel.region = region;
                newPel.beginPoint = beginPoint;
                newPel.centerPoint = centerPoint;
                newPel.bottomRightPointF.set(newPel.region.getBounds().right, newPel.region.getBounds().bottom);
                newPel.paint.setColor(color);
                if (color == Color.RED) {
                    color = 1;
                } else if (color == Color.BLACK) {
                    color = 0;
                } else {
                    color = 2;
                }
                newPel.paintColor = color;
                //添加至文本总链表
                (CanvasView.pelList).add(newPel);
                //记录栈中信息
                undoStack.push(new DrawPelStep(newPel));//将该“步”压入undo栈
                //更新画布
                canvasView.updateSavedBitmap();
                //可拖动
                clearRedoStack();
                CanvasView.setmChildTouch(new TransformTouch(DrawingMainActivity.this));
            }
        });

        //控件编辑监听
        multipleLayout.setControlOperationClickCallback(new MultipleLayout.ControlOperationClickCallback() {

            //文字居中
            @Override
            public void onIntermediate() {
                selectedPel = CanvasView.getSelectedPel();
                if (selectedPel == null) {
                    ToastUtils.show(getString(R.string.Seleccione_ver_primero));
                    return;
                }
                selectedPel.transDx = 0;
                selectedPel.transDy = 0;
                selectedPel.angle = 0;
                if (selectedPel.type == 20) {
                    Rect rect = new Rect();
                    (canvasView.getDrawTextPaint()).getTextBounds(selectedPel.text.getContent(), 0, selectedPel.text.getContent().length(), rect);
                    //内容宽高
                    PointF centerPoint = new PointF();
                    //文本中心
                    centerPoint.set(new PointF(CanvasView.CANVAS_WIDTH / 2f, CanvasView.CANVAS_HEIGHT / 2f));
                    //文本开始坐标
                    PointF beginPoint = new PointF(centerPoint.x - (rect.width() >> 1),
                            centerPoint.y - (rect.height() >> 1));
                    //文本区域
                    Region region = new Region();
                    //横向显示
                    if (!selectedPel.text.isVertical()) {
                        region.set((int) (centerPoint.x - (rect.width() / 3)),
                                (int) centerPoint.y - (rect.height() / 3),
                                (int) (centerPoint.x + (rect.width() / 3)),
                                (int) centerPoint.y + (rect.height() / 3));
                    }
                    //竖向显示
                    else {
                        if (AppUtils.isContainsLetter(selectedPel.text.getContent())) {
                            region.set((int) centerPoint.x - (rect.height() / 2),
                                    ((int) centerPoint.y - (rect.width() / 2) + 6),
                                    (int) (centerPoint.x + (rect.height() / 2)),
                                    ((int) centerPoint.y + (rect.width() / 2) + 6));
                        } else {
                            region.set((int) centerPoint.x - (rect.height() / 2),
                                    ((int) centerPoint.y - (rect.width() / 4)),
                                    (int) (centerPoint.x + (rect.height() / 2)),
                                    ((int) centerPoint.y + (rect.width() / 4)));
                        }
                    }
                    Text text = new Text(selectedPel.text.getContent(), selectedPel.text.isVertical(), selectedPel.textSize);
                    selectedPel.text = text;
                    selectedPel.region = region;
                    selectedPel.beginPoint = beginPoint;
                    selectedPel.centerPoint = centerPoint;
                    selectedPel.bottomRightPointF.set(selectedPel.region.getBounds().right, selectedPel.region.getBounds().bottom);
                }
                CanvasView.setSelectedPel(selectedPel);
                ensurePelFinished();
                canvasView.updateSavedBitmap();
            }

            @Override
            public void onInUpdate() {
                clearRedoStack();
                CanvasView.setmChildTouch(new TransformTouch(DrawingMainActivity.this));
            }

            @Override
            public void onOutUpdate() {
                ensurePelFinished();
                CanvasView.setmChildTouch(new DrawFreehandTouch());
            }

            @Override
            public void onOperatingDelete() {
                selectedPel = CanvasView.getSelectedPel();
                if (selectedPel == null) {
                    ToastUtils.show(getString(R.string.Seleccione_ver_primero));
                    return;
                }
                //选中了图元才能进行删除操作
                undoStack.push(new DeletePelStep(selectedPel));//将该“步”压入undo栈
                (pelList).remove(selectedPel);

                CanvasView.setSelectedPel(selectedPel = null);
                canvasView.updateSavedBitmap();//删除了图元就自然更新缓冲画布
            }

            @Override
            public void onOperatingCopy() {
                selectedPel = CanvasView.getSelectedPel();
                if (selectedPel == null) {
                    ToastUtils.show(getString(R.string.Seleccione_ver_primero));
                    return;
                }
                Pel pel = (Pel) (selectedPel).clone();//以选中图元为模型，拷贝一个新对象
                if (pel.text != null) {
                    PointF beginPoint = pel.beginPoint;
                    PointF centerPoint = pel.centerPoint;
                    Region region = pel.region;
                    Rect bounds = region.getBounds();
                    beginPoint.offset(10, 10);
                    centerPoint.offset(10, 10);
                    region.set(bounds.left + 10, bounds.top + 10, bounds.right + 10, bounds.bottom + 10);
                } else if (pel.picture != null) {
                    PointF beginPoint = pel.beginPoint;
                    PointF centerPoint = pel.centerPoint;
                    Region region = pel.region;
                    Rect bounds = region.getBounds();
                    beginPoint.offset(10, 10);
                    centerPoint.offset(10, 10);
                    region.set(bounds.left + 10, bounds.top + 10, bounds.right + 10, bounds.bottom + 10);
                } else {
                    (pel.path).offset(10, 10);//偏移一定距离友好示意
                    (pel.region).setPath(pel.path, CanvasView.getClipRegion());
                }
                (pelList).add(pel);
                undoStack.push(new CopyPelStep(pel));//将该“步”压入undo栈
                //清空选中
                CanvasView.setSelectedPel(selectedPel = null);
                if (CanvasView.mChildTouch instanceof TransformTouch) {
                    TransformTouch transformTouch = (TransformTouch) CanvasView.mChildTouch;
                    transformTouch.setSelectedPel(null);
                }
                canvasView.updateSavedBitmap();
            }

            @Override
            public void onViewMormal() {
                selectedPel = CanvasView.getSelectedPel();
                if (selectedPel == null) {
                    ToastUtils.show(getString(R.string.Seleccione_ver_primero));
                    return;
                }
                if (selectedPel.type == 14) {
                    float startX = selectedPel.pathPointFList.get(0).x;
                    float startY = selectedPel.pathPointFList.get(0).y;

                    float endX = selectedPel.pathPointFList.get(2).x;
                    float endY = selectedPel.pathPointFList.get(2).y;

                    float resultX = endX - startX;
                    float resultY = endY - startY;
                    if (resultX == resultY) return;
                    selectedPel.path.reset();
                    //垂直线
                    if (resultX < resultY) {
                        LogHelper.INSTANCE.i("selectedPel", "垂直矫正");
                        selectedPel.pathPointFList.set(1, new PointF(startX, endY));
                        selectedPel.pathPointFList.set(2, new PointF(startX, endY));
                        (selectedPel.path).addOval(new RectF(startX, startY,
                                startX, endY), Path.Direction.CCW);
                    } else {
                        LogHelper.INSTANCE.i("selectedPel", "水平矫正");
                        selectedPel.pathPointFList.set(1, new PointF(endX, startY));
                        selectedPel.pathPointFList.set(2, new PointF(endX, startY));
                        (selectedPel.path).addOval(new RectF(startX, startY,
                                endX, startY), Path.Direction.CCW);
                    }
                } else {
                    selectedPel.angle = 0;
                }
                CanvasView.setSelectedPel(selectedPel);
                canvasView.updateSavedBitmap();
                ensurePelFinished();
            }

            @Override
            public void onOperatingRestore() {
                //满屏
                selectedPel = CanvasView.getSelectedPel();
                if (selectedPel == null) {
                    ToastUtils.show(getString(R.string.Seleccione_ver_primero));
                    return;
                }

                if (selectedPel.type != 30) {
                    ToastUtils.show(getString(R.string.Full_screen_support_for_images_only));
                    return;
                }
                Bitmap newBitmap = selectedPel.picture.createContent();
                int new_image_effect_size = selectedPel.image_effect_size;
                //选中了图元才能进行删除操作
                undoStack.push(new DeletePelStep(selectedPel));//将该“步”压入undo栈
                (pelList).remove(selectedPel);

                CanvasView.setSelectedPel(selectedPel = null);

                PointF centerPoint = new PointF(CanvasView.CANVAS_WIDTH, CanvasView.CANVAS_HEIGHT);
                Picture picture = new Picture(newBitmap);
                //区域
                Region region = new Region();
                //左，上，右，下
                region.set(0, 0,
                        CanvasView.getCanvasWidth(), CanvasView.getCanvasHeight());
                Pel newPel = new Pel();
                newPel.type = 30;
                newPel.picture = picture;
                newPel.region = region;
                newPel.image_effect_size = new_image_effect_size;
                newPel.beginPoint = new PointF(newPel.region.getBounds().left, newPel.region.getBounds().top);
                newPel.centerPoint = new PointF(centerPoint.x, centerPoint.y);
                newPel.bottomRightPointF.set(newPel.region.getBounds().right, newPel.region.getBounds().bottom);
                //添加至文本总链表
                (CanvasView.pelList).add(newPel);
                //记录栈中信息
                undoStack.push(new DrawPelStep(newPel));//将该“步”压入undo栈
                //更新画布
                CanvasView.setSelectedPel(newPel);
                canvasView.updateSavedBitmap();
                ensurePelFinished();
            }

            @Override
            public void onOperatingEmpty() {
                new MaterialDialog.Builder(DrawingMainActivity.this)
                        .title(getString(R.string.clear_screen))
                        .content(getString(R.string.sure_you_want_to_empty_the_artboard))
                        .positiveText(getString(R.string.fixed))
                        .positiveColor(ContextCompat.getColor(DrawingMainActivity.this, R.color.red))
                        .onPositive((dialog, which) -> clearData()).negativeText(getString(R.string.cancel)).show();

            }

            @Override
            public void onOperatingDIsUpdate(boolean isUpdate) {

            }
        });

        //画笔编辑
        multipleLayout.setBrushClickCallback(new MultipleLayout.BrushClickCallback() {
            @Override
            public void onSquare() {
                ensurePelFinished();
                CanvasView.setmChildTouch(new DrawRectTouch());
            }

            @Override
            public void onRound() {
                ensurePelFinished();
                CanvasView.setmChildTouch(new DrawOvalTouch());
            }

            @Override
            public void onBrushDefault() {
                ensurePelFinished();
                paint.setStyle(Paint.Style.STROKE);
                DrawTouch.setFillColor(-1);
                CanvasView.setmChildTouch(new DrawFreehandTouch());
                paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
                paint.setPathEffect(new CornerPathEffect(10));
                DrawTouch.getCurPaint().setColor(Color.BLACK);
                DrawTouch.setPnWidth(1);
                DrawTouch.setDotted_line(0);
            }

            @Override
            public void onFillingFalse() {
                paint.setStyle(Paint.Style.STROKE);
                DrawTouch.setFillColor(-1);
            }

            @Override
            public void onFillingTue() {
                paint.setStyle(Paint.Style.FILL);
                if (DrawTouch.getCurPaint().getColor() == Color.RED) {
                    DrawTouch.setFillColor(1);
                } else if (DrawTouch.getCurPaint().getColor() == Color.BLACK) {
                    DrawTouch.setFillColor(0);
                } else {
                    DrawTouch.setFillColor(2);
                }
            }

            @Override
            public void onStraight() {
                ensurePelFinished();
                CanvasView.setmChildTouch(new DrawFreehandTouch());
            }

            @Override
            public void onCurveBend() {
                CanvasView.setmChildTouch(new DrawLineTouch());
            }

            @Override
            public void onSolidLine() {
                paint.setPathEffect(new CornerPathEffect(10));
                DrawTouch.setDotted_line(0);
            }

            @Override
            public void onDottedLine() {
                paint.setPathEffect(new DashPathEffect(new float[]{30, 30}, 1));
                DrawTouch.setDotted_line(1);
            }

            @Override
            public void onPointMax() {
                DrawTouch.setPnWidth(3);
                paint.setStrokeWidth(Constants.PAINT_WIDTH_L);//改变粗细
            }

            @Override
            public void onPointMedium() {
                DrawTouch.setPnWidth(2);
                paint.setStrokeWidth(Constants.PAINT_WIDTH_M);
            }

            @Override
            public void onPointMin() {
                DrawTouch.setPnWidth(1);
                paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
            }

            @Override
            public void onRed() {
                if (DrawTouch.getFillColor() == 0 || DrawTouch.getFillColor() == 2) {
                    DrawTouch.setFillColor(1);
                }
                DrawTouch.getCurPaint().setColor(Color.RED);
            }

            @Override
            public void onBlack() {
                if (DrawTouch.getFillColor() == 1 || DrawTouch.getFillColor() == 2) {
                    DrawTouch.setFillColor(0);
                }
                DrawTouch.getCurPaint().setColor(Color.BLACK);
            }

            @Override
            public void onWhite() {
                if (DrawTouch.getFillColor() == 1 || DrawTouch.getFillColor() == 0) {
                    DrawTouch.setFillColor(2);
                }
                DrawTouch.getCurPaint().setColor(Color.WHITE);
            }
        });
        //返回
        commonTitleBar.setListener((v, action, extra) -> onBackPressed());

        //保存
        btnSubmit.setOnClickListener(v -> {
            ensurePelFinished();
            if (CanvasView.getPelList().size() == 0) {
                ToastUtils.show(DrawingMainActivity.this, getString(R.string.Nothing_is_added_to_the_template));
                return;
            }
            if (templateId == null) {
                newTemplate();
            } else {
                new IosDialog(this).init()
                        .setTitle(getString(R.string.Template_Setup))
                        .setMsg(getString(R.string.Save_new_template))
                        .setPositiveButton(getString(R.string.Template_Save), v12 -> {
                            senData(null);
                        })
                        .setNegativeButton(getString(R.string.Template_new), v13 -> {
                            templateId = null;
                            newTemplate();
                        })
                        .show();
            }
        });
    }

    void newTemplate() {
        new IosDialog(DrawingMainActivity.getContext())
                .init()
                .setTitle(getString(R.string.Template_name))
                .setEditTextContext(
                        "",
                        getString(R.string.input_Template_name)
                )
                .setPositiveButton(getString(R.string.fixed), listener -> {
                }, editText -> {
                    String name = editText.getText().toString();
                    LogHelper.INSTANCE.i("witstec", "context=" + name);
                    if (name.isEmpty()) {
                        ToastUtils.show(getString(R.string.input_Template_name));
                        return;
                    }
                    senData(name);
                }).setNegativeButton(getString(R.string.cancel), v1 -> {
        }).show();
    }

    void senData(String name) {
        try {
            //生成xml文件名称
            String currentDate = AppUtils.getCurrentDate(AppUtils.dateFormatYMDHMS);
            //保存xml
            String xmlString = canvasView.saveFileDataXml("");
            //图片转base64
            String imageBase64 = AppUtils.bitmapToBase64Tep(CanvasView.getSavedBitmap());
            LogHelper.INSTANCE.i("isLocal", "" + isLocal);
            if (isLocal) {
                if (templateId == null) {
                    ArrayList<TemplateBean> templateBean =
                            LocalDataSource.Companion.getInstance().queryAll(TemplateBean.class);
                    boolean isExit = false;
                    for (int i = 0; i < templateBean.size(); i++) {
                        if (name.equals(templateBean.get(i).getTemplateName())) {
                            isExit = true;
                        }
                    }
                    if (isExit) {
                        showDialogNo(getString(R.string.Duplicate_template_name));
                    } else {
                        //创建模板
                        LocalDataSource.Companion.getInstance().save(new TemplateBean(AppUtils.getRandomString(8),
                                name, String.valueOf(templateType), imageBase64, xmlString, currentDate
                        ));
                        dialogDismiss();
                        ToastUtils.show(DrawingMainActivity.this, getString(R.string.Template_saved_successfully));
                        RxBus.Companion.post(new ChangeTemplateListEvent());
                        finish();
                    }

                } else {
                    //修改模板
                    ColumnsValue columnsValue = new ColumnsValue(new String[]{"templateXmlString", "templateImage", "templateCreateTime"},
                            new Object[]{xmlString, imageBase64, currentDate});
                    LocalDataSource.Companion.getInstance()
                            .getLiteOrm()
                            .update(
                                    WhereBuilder.create(TemplateBean.class)
                                            .where("templateId=?", templateId),
                                    columnsValue, ConflictAlgorithm.None);
                    dialogDismiss();
                    ToastUtils.show(DrawingMainActivity.this, getString(R.string.Template_update_successfully));
                    RxBus.Companion.post(new ChangeTemplateListEvent());
                    finish();
                }
            } else {
                showDialog();
                try {
                    if (templateId == null) {
                        Disposable disposable = ApiRepository.createTemplate(name, templateType, String.valueOf(templateType),
                                AppUtils.bitmapToBase64(CanvasView.getSavedBitmap()), xmlString, this)
                                .subscribe(response -> {
                                    dialogDismiss();
                                    ToastUtils.show(DrawingMainActivity.this, getString(R.string.Template_saved_successfully));
                                    RxBus.Companion.post(new ChangeTemplateListEvent());
                                    finish();
                                }, throwable -> {
                                    dialogDismiss();
                                    ToastUtils.show(throwable.getMessage());
                                });
                        addDisposable(disposable);
                    } else {
                        if (isCoexistencel) {
                            String xmlPath = Constant.SAVE_PATH + "/" + templateId + Constant.SAVE_DATA_FILE_SUFFIX_XML;
                            String imagePath = Constant.SAVE_PATH + "/" + templateId + Constant.SAVE_IMAGE_FILE_SUFFIX;
                            //保存图片
                            FileOutputStream fileOutputStreamImg = new FileOutputStream(imagePath);
                            FileOutputStream fileOutputStreamXml = new FileOutputStream(xmlPath);
                            fileOutputStreamXml.write(xmlString.getBytes());
                          AppUtils.zoomImg(CanvasView.getSavedBitmap(),Constants.W_021,Constants.H_021).compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStreamImg);
                            fileOutputStreamImg.close();
                            fileOutputStreamXml.close();
                            //修改模板
                            ColumnsValue columnsValue = new ColumnsValue(new String[]{"templateFileUrl", "templateImage", "templateCreateTime"},
                                    new Object[]{xmlPath, imagePath, currentDate});
                            LocalDataSource.Companion.getInstance()
                                    .getLiteOrm()
                                    .update(
                                            WhereBuilder.create(TemplateOfficialBean.class)
                                                    .where("templateId=?", templateId),
                                            columnsValue, ConflictAlgorithm.None);
                            dialogDismiss();
                            ToastUtils.show(DrawingMainActivity.this, getString(R.string.Template_update_successfully));
                            RxBus.Companion.post(new ChangeTemplateListEvent());
                            finish();
                        } else {
                            Disposable disposable = ApiRepository.updateTemplate(templateId, templateName,
                                    AppUtils.bitmapToBase64(CanvasView.getSavedBitmap()), xmlString, this)
                                    .subscribe(response -> {
                                        dialogDismiss();
                                        ToastUtils.show(DrawingMainActivity.this, getString(R.string.Template_update_successfully));
                                        RxBus.Companion.post(new ChangeTemplateListEvent());
                                        finish();
                                    }, throwable -> {
                                        dialogDismiss();
                                        ToastUtils.show(throwable.getMessage());
                                    });
                            addDisposable(disposable);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //插入图片
    private void generateBarCodeBitmap(int image_effect_size, Bitmap newBitmap) {
        PointF centerPoint = new PointF(CanvasView.CANVAS_WIDTH / 2f, CanvasView.CANVAS_HEIGHT / 2f);
        Picture picture = new Picture(newBitmap);
        //区域
        Region region = new Region();
        //左，上，右，下
        region.set((int) centerPoint.x - (newBitmap.getWidth() / 2), (int) centerPoint.y - (newBitmap.getHeight() / 2),
                (int) centerPoint.x + (newBitmap.getWidth() / 2), (int) centerPoint.y + (newBitmap.getHeight() / 2));
        Pel newPel = new Pel();
        newPel.type = 30;
        newPel.picture = picture;
        newPel.region = region;
        newPel.image_effect_size = image_effect_size;
        newPel.beginPoint = new PointF(newPel.region.getBounds().left, newPel.region.getBounds().top);
        newPel.centerPoint = new PointF(centerPoint.x, centerPoint.y);
        newPel.bottomRightPointF.set(newPel.region.getBounds().right, newPel.region.getBounds().bottom);
        //添加至文本总链表
        (CanvasView.pelList).add(newPel);
        //记录栈中信息
        undoStack.push(new DrawPelStep(newPel));//将该“步”压入undo栈
        //更新画布
        canvasView.updateSavedBitmap();
    }

    //插入条码/二维码
    private void insertBarCode(int type, String content) {
        Bitmap newBitmap;
        if (type == 31) {
            newBitmap = ZXingUtils.creatBarcode(DrawingMainActivity.this, content, Constants.BAR_W, Constants.BAR_H, false);
        } else {
            newBitmap = ZXingUtils.createQRImage(content, Constants.QR_W, Constants.QR_H);
        }
        PointF centerPoint = new PointF(CanvasView.CANVAS_WIDTH / 2f, new Random().nextInt(CanvasView.CANVAS_HEIGHT));
        Picture picture = new Picture(newBitmap);
        //区域
        Region region = new Region();
        //左，上，右，下
        assert newBitmap != null;
        region.set((int) centerPoint.x - (newBitmap.getWidth() / 2), (int) centerPoint.y - (newBitmap.getHeight() / 2),
                (int) centerPoint.x + (newBitmap.getWidth() / 2), (int) centerPoint.y + (newBitmap.getHeight() / 2));
        Text text = new Text(content, false, Constants.PAINT_DEFAULT_TEXT_SIZE_55);
        Pel newPel = new Pel();
        newPel.type = type;
        newPel.picture = picture;
        newPel.region = region;
        newPel.text = text;
        newPel.image_effect_size = image_effect_size;
        newPel.beginPoint = new PointF(newPel.region.getBounds().left, newPel.region.getBounds().top);
        newPel.centerPoint = new PointF(centerPoint.x, centerPoint.y);
        newPel.bottomRightPointF.set(newPel.region.getBounds().right, newPel.region.getBounds().bottom);
        //添加至文本总链表
        (CanvasView.pelList).add(newPel);
        //记录栈中信息
        undoStack.push(new DrawPelStep(newPel));//将该“步”压入undo栈
        //更新画布
        canvasView.updateSavedBitmap();
    }

    private void initView() {
        commonTitleBar = findViewById(R.id.titlebar);
        btnSubmit = findViewById(R.id.btn_device_manage);
        tv_mode_type = findViewById(R.id.tv_mode_type);
        multipleLayout = findViewById(R.id.layout_drawing_menu);
        canvasView = findViewById(R.id.iv_image_template);
        setCanvasSize();
        multipleLayout.showDefMenuView();
        paint = DrawTouch.getCurPaint();//获取当前绘制画笔;
        //默认配置
        paint.setStyle(Paint.Style.STROKE);
        CanvasView.setmChildTouch(new DrawFreehandTouch());
        DrawTouch.setPnWidth(1);
        paint.setStrokeWidth(Constants.PAINT_WIDTH_S);
        paint.setPathEffect(new CornerPathEffect(10));
        DrawTouch.getCurPaint().setColor(Color.BLACK);
        //事先生成图片被存储的文件夹
        File file = new File(Constant.SAVE_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        context = DrawingMainActivity.this;
        //数据结构
        pelList = CanvasView.getPelList();
        undoStack = CanvasView.getUndoStack();
        redoStack = CanvasView.getRedoStack();
    }

    private void setCanvasSize() {
        if (templateType == 2) {
            tv_mode_type.setText(getString(R.string.ble_mode_text_type_021));
            sizeType = TemplateSizeType.B021;
            App.Companion.setBLE_TYPE(sizeType);
            canvasView.setCanvasSize(Constants.W_021, Constants.H_021);
        } else {
            ToastUtils.show("不支持该尺寸");
            tv_mode_type.setText(getString(R.string.unknown));
            sizeType = TemplateSizeType.B042;
            App.Companion.setBLE_TYPE(sizeType);
            canvasView.setCanvasSize(Constants.W_021, Constants.W_021);
        }
    }

    //确保未画完的图元能够真正敲定
    void ensurePelFinished() {
        Touch touch = CanvasView.getmChildTouch();
        selectedPel = CanvasView.getSelectedPel();

        if (selectedPel != null) {
            //使人为敲定图元的操作(贝塞尔、折线、多边形)
            if (touch instanceof DrawBesselTouch) {
                touch.control = true;
                touch.up();
            } else if (touch instanceof DrawBrokenLineTouch) {
                touch.hasFinished = true;
                touch.up();
            } else if (touch instanceof DrawPolygonTouch) {
                (touch.curPoint).set(touch.beginPoint);
                touch.up();
            } else //单纯选中
            {
                CanvasView.setSelectedPel(null);//失去焦点
                canvasView.updateSavedBitmap();//重绘位图
            }
        }
    }

    public static void openOrCloseTools() {
        if (!redoStack.empty())//redo栈不空
            redoStack.clear();//清空redo栈
    }

    //清空重做栈
    public static void clearRedoStack() {
        if (!redoStack.empty())//redo栈不空
            redoStack.clear();//清空redo栈
    }


    //清空内部所有数据
    public void clearData() {
        paint.setPathEffect(new CornerPathEffect(10));
        DrawTouch.setDotted_line(0);
        pelList.clear();
        CanvasView.pelList.clear();
        undoStack.clear();
        CanvasView.undoStack.clear();
        redoStack.clear();
        CanvasView.redoStack.clear();
        CanvasView.setSelectedPel(null);//若有选中的图元失去焦点
        canvasView.setBackgroundBitmap();//清除填充过颜色的地方
        canvasView.clearFocus();
    }

    public static CanvasView getCanvasView() {
        return canvasView;
    }

    public static Context getContext() {
        return context;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_CHOOSE) {
            try {
                if (data == null) {
                    return;
                }
                //获取选择器返回的数据
                List<String> selected = Matisse.obtainPathResult(data);
                assert selected != null;
                File path = Luban.with(this)
                        .load(selected.get(0))
                        .ignoreBy(50).get().get(0);
                if (path.getPath().isEmpty()) {
                    return;
                }
                View viewcop = View.inflate(getContext(), R.layout.activity_crop, null);
                CropImageView cropImageView = viewcop.findViewById(R.id.cropImageView);
                new MaterialDialog.Builder(getContext())
                        .positiveText(getString(R.string.next))
                        .negativeText(getString(R.string.cancel))
                        .title(getString(R.string.corp))
                        .customView(viewcop, true)
                        .cancelable(false)
                        .onPositive((dialog, which) -> {
                            Bitmap cBitmap = cropImageView.getCroppedImage();
                            generateBarCodeBitmap(image_effect_size, cBitmap);
                            //可拖动
                            clearRedoStack();
                            CanvasView.setmChildTouch(new TransformTouch(DrawingMainActivity.this));

//                            View view = View.inflate(getContext(), R.layout.layout_image_effect_drawing, null);
//                            ImageView imageView = view.findViewById(R.id.iv_drawing_effect);
//                            SeekBar seekBar = view.findViewById(R.id.seekBar_drawing_effect);
//                            seekBar.setProgress(image_effect_size);
////                            int[] whs = AppUtils.bitmapShrink(cBitmap.getWidth(), cBitmap.getHeight());
////                            LogHelper.INSTANCE.i("witstec", "bitmapEffect=" +
////                                    cBitmap.getWidth() + "," + cBitmap.getHeight());
////                            LogHelper.INSTANCE.i("witstec", whs[0] + "," + whs[1]);
////                            Bitmap newBitmap = AppUtils.zoomImg(cBitmap, whs[0], whs[1]);
//                            bitmapEffect = ExtKt.convertGreyImgByFloyd(cBitmap, image_effect_size);
//                            imageView.setImageBitmap(bitmapEffect);
//                            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                                @Override
//                                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//                                }
//
//                                @Override
//                                public void onStartTrackingTouch(SeekBar seekBar) {
//
//                                }
//
//                                @Override
//                                public void onStopTrackingTouch(SeekBar seekBar) {
//                                    image_effect_size =(seekBar.getProgress());
//                                    bitmapEffect = ExtKt.convertGreyImgByFloyd(cBitmap, image_effect_size);
//                                    imageView.setImageBitmap(bitmapEffect);
//                                }
//                            });
//                            new MaterialDialog.Builder(getContext())
//                                    .positiveText(getString(R.string.fixed))
//                                    .title(getString(R.string.edit))
//                                    .negativeText(getString(R.string.cancel))
//                                    .customView(view, true)
//                                    .cancelable(false)
//                                    .onPositive((d, wi) -> {
//                                        generateBarCodeBitmap(image_effect_size, bitmapEffect);
//                                        //可拖动
//                                        clearRedoStack();
//                                        CanvasView.setmChildTouch(new TransformTouch(DrawingMainActivity.this));
//                                    })
//                                    .onNegative((d, wi) -> dialog.dismiss())
//                                    .show();
                        })
                        .showListener(dialogInterface -> {
                            Glide.with(this).load(path).into(cropImageView);
                        })
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearData();
    }
}
