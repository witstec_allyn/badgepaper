package com.witstec.sz.badgeepaper.ui.activity.login

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.myStartActivity
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.view.VerificationCodeButton
import kotlinx.android.synthetic.main.fragment_email_register.*
import kotlinx.android.synthetic.main.layout_code_sms.*

class EmailRegisterActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_email_register)
        setToolbar(getString(R.string.retrieve_password_text))
        btn_submit_sms_code.setOnClickListener {
            if (ed_input_register_phone.text.toString().isEmpty()) {
                ToastUtils.show(getString(R.string.please_input_your_email))
                return@setOnClickListener
            }
            showDialog()
            addDisposable(
                ApiRepository.getNewPwdCode(ed_input_register_phone.text.toString(), this)
                    .subscribe({
                        dialogDismiss()
                        btn_submit_sms_code.start(this, 60)
                        MaterialDialog.Builder(this)
                            .content(getString(R.string.Verification_code_sent))
                            .cancelable(false)
                            .positiveText(getString(R.string.fixed))
                            .onPositive { dialog, which ->
                            }
                            .show()
                    }, {
                        dialogDismiss()
                        it.message?.let { it1 -> ToastUtils.show(it1) }
                    })
            )
        }


        btn_submit_sms_code.setOnCountDownListener(object : VerificationCodeButton.Countdown {
            override fun beforeStart(): Boolean {
                btn_submit_sms_code.setText(R.string.sms_code_send)
                btn_submit_sms_code.background = ContextCompat.getDrawable(
                    this@EmailRegisterActivity,
                    R.drawable.div_sms_code_bg_gray
                )
                btn_submit_sms_code.setTextColor(
                    ContextCompat.getColor(
                        this@EmailRegisterActivity,
                        R.color.gray
                    )
                )
                return true
            }

            override fun timeCountdown(time: Int) {
                btn_submit_sms_code.text = String.format(getString(R.string.repeat_time_sms_miao), time)
            }

            override fun stop() {
                btn_submit_sms_code.setText(R.string.send_sms_register)
                btn_submit_sms_code.background = ContextCompat.getDrawable(
                    this@EmailRegisterActivity,
                    R.drawable.div_sms_code_bg
                )
                btn_submit_sms_code.setTextColor(
                    ContextCompat.getColor(
                        this@EmailRegisterActivity,
                        R.color.colorPrimary
                    )
                )
            }
        })

        btn_register_protocol.setOnClickListener {
            btn_submit_sms_code.stopCountDown()
            myStartActivity(this, ProtocolActivity::class.java)
        }

        btn_submit_register.setOnClickListener {
            if (ed_input_register_phone.text.toString()
                    .isEmpty() || ed_input_register_sms_code.text.toString()
                    .isEmpty() || ed_input_register_pwd_to.text.toString().isEmpty()
            ) {
                ToastUtils.show(getString(R.string.please_input_your_email))
                return@setOnClickListener
            }
            showDialog()
            btn_submit_sms_code.stopCountDown()
            addDisposable(
                ApiRepository.foundback(
                    ed_input_register_phone.text.toString(),
                    ed_input_register_sms_code.text.toString(),
                    ed_input_register_pwd_to.text.toString(), this
                )
                    .subscribe({
                        dialogDismiss()
                        ToastUtils.show(getString(R.string.Successfully_retrieved_the_password))
                        finish()
                    }, {
                        dialogDismiss()
                        ToastUtils.show(it.message)
                    })
            )


        }

        btn_to_login.setOnClickListener {
            btn_submit_sms_code.stopCountDown()
            this.finish()
        }
        check_register_protocol.setOnCheckedChangeListener { _, isChecked ->
            btn_submit_register.isEnabled = isChecked
            if (!isChecked) {
                btn_submit_register.background =
                    this.let { ContextCompat.getDrawable(it, R.drawable.btn_radius_enobled_false) }
            } else {
                btn_submit_register.background =
                    this.let { ContextCompat.getDrawable(it, R.drawable.btn_radius) }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        btn_submit_sms_code.stopCountDown()
    }
}
