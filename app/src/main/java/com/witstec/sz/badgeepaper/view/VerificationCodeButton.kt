package com.witstec.sz.badgeepaper.view

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import java.util.Timer
import java.util.TimerTask


class VerificationCodeButton : AppCompatButton {


    /**
     * 倒计时默认时间  60s
     */
    private var mCountdowntime = 60

    /**
     * 动态记录倒计时时间
     */
    private var mNowtime = 0

    //设置timertask
    internal var validateTask: TimerTask? = null
    private var validateTimer = Timer()
    private var mActivity: Activity? = null

    /**
     * 是否开始
     */
    private var isstart: Boolean = false

    private var onCountDownListener: Countdown? = null


    fun setOnCountDownListener(onCountDownListener: Countdown) {
        this.onCountDownListener = onCountDownListener
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    /**
     * 验证码倒计时开始
     *
     * @param activity 当前activity
     */
    fun start(activity: Activity) {
        //获取按钮点击监听
        setOnClickListener(OnClickListener {
//            if (onCountDownListener != null) {
//                isstart = onCountDownListener!!.beforeStart()
//            }
//            if (!isstart) {
//                return@OnClickListener
//            }

            mActivity = activity
            setValidateCodeTime()
        })
    }

    /**
     * 验证码倒计时开始,设置倒计时时间
     *
     * @param activity 当前activity
     * @param time     单位 s
     */
    fun start(activity: Activity, time: Int) {
        mCountdowntime = time
        mActivity = activity
        setValidateCodeTime()
        //获取按钮点击监听
//        setOnClickListener(OnClickListener {
//            if (onCountDownListener != null) {
//                isstart = onCountDownListener!!.beforeStart()
//            }

//            if (!isstart) {
//                return@OnClickListener
//            }


//        })
    }

    /**
     * 倒计时停止
     */
    fun stopCountDown() {
        if (validateTask == null) {
            return
        }

        validateTask!!.cancel()
        isEnabled = true
        mNowtime = mCountdowntime
        if (onCountDownListener != null) {
            onCountDownListener!!.stop()
        }
    }

    interface Countdown {

        /**
         * 倒计时开始之前的逻辑
         *
         * @return true 倒计时开始 false 倒计时结束
         */
        fun beforeStart(): Boolean

        /**
         * 倒计时剩余时间
         *
         * @param time
         */
        fun timeCountdown(time: Int)

        /**
         * 倒计时停止
         */
        fun stop()
    }


    /**
     * 设置获取验证码间隔时间
     */
    private fun setValidateCodeTime() {
        mNowtime = mCountdowntime
        validateTask = object : TimerTask() {
            override fun run() {
                mActivity!!.runOnUiThread {
                    if (mNowtime <= 0) {
                        isEnabled = true
                        if (onCountDownListener != null) {
                            onCountDownListener!!.stop()
                        }
                        validateTask!!.cancel()
                    } else {
                        isEnabled = false
                        if (onCountDownListener != null) {
                            onCountDownListener!!.timeCountdown(mNowtime)
                        }
                    }
                    mNowtime--
                }
            }
        }
        mNowtime = mNowtime - 1
        validateTimer.schedule(validateTask, 0, 1000)
    }
}