package com.witstec.sz.badgeepaper.model.bean;

public class OTADataMsg {


    /**
     *     {"success":true,"status":0,"data"
     *     :{"id":"1","version":"1.5","type":"wtcard","url":"\/Tmp\/Card\/2020-12-15\/upgrade.bin",
     *     "filesize":"20064","desc":"card-v1.5","create_date":"2020-12-15 18:02:32"}}
     */

    private String url;
    private String type;
    private String version;
    private String md5;
    private String create_time;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
