package com.witstec.sz.badgeepaper.model.bean;

public class DeviceGroup {

    /**
     * gid : 13
     * name : allyn4
     */

    private String gid;
    private String name;

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
