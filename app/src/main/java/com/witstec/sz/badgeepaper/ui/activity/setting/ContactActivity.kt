package com.witstec.sz.badgeepaper.ui.activity.setting

import android.os.Bundle
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity

class ContactActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        setToolbar(getString(R.string.contact_us))
    }
}
