package com.witstec.sz.badgeepaper.ui.activity.device.add

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.manage.BitmapBleBmpManagement
import com.witstec.sz.badgeepaper.model.bean.DeviceBean
import com.witstec.sz.badgeepaper.model.bean.GattType
import com.witstec.sz.badgeepaper.model.db.LocalDataSource
import com.witstec.sz.badgeepaper.model.event.ChangeDeviceDataEvent
import com.witstec.sz.badgeepaper.ui.activity.MainActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.BleConnectionActivity
import com.witstec.sz.badgeepaper.utils.*
import com.witstec.sz.badgeepaper.view.IosDialog
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_success.*
import java.util.*
import java.util.concurrent.TimeUnit


/**
 *
 */
class AddSuccessActivity : BleConnectionActivity() {

    private var version = ""
    var index: Int = 0
    var isCloseLoop = false
    var mDeviceName = ""
    private var isConfirmSend: Boolean = true

    companion object {
        fun start(
            context: Context,
            mac: String,
            deviceType: String,
            version: String
        ) {
            val intent = Intent(context, AddSuccessActivity::class.java)
            intent.putExtra("extra_mac", mac)
            intent.putExtra("extra_deviceType", deviceType)
            intent.putExtra("extra_version", version)
            context.startActivity(intent)
        }
    }

    override fun getContentViewLayout(): Int {
        return R.layout.activity_add_success
    }

    override fun initView() {
        setToolbar(getString(R.string.add_device))
        version = intent.getStringExtra("extra_version").toString()
        mDeviceAddress = intent.getStringExtra("extra_mac")
        (item_device_name.rightTextView ?: return).text = App.device_name
    }

    override fun listener() {
        item_device_name.setOnClickListener {
            IosDialog(this@AddSuccessActivity).init()
                .setTitle(getString(R.string.device_name))
                .setEditTextContext(
                    (item_device_name.rightTextView ?: return@setOnClickListener).text.toString(),
                    getString(R.string.input_device_manage_name)
                )
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                }, object : IosDialog.EditOnClickListener {
                    override fun editContext(context: EditText) {
                        val contByte = StringUtils.toByte(context.text.toString())
                        LogHelper.i("chagndu", ByteUtils.byteArrayToHexStringLX(contByte))
//                        LogHelper.i("chagndu", hexStr2Str(byteArrayToHexStringLX(contByte)))
                        if (contByte.size > 7) {
                            ToastUtils.show(getString(R.string.Name_exceeds_length_limit))
                            return
                        }
                        (item_device_name.rightTextView ?: return).text = context.text.toString()
                    }
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {

                })
                .show()
        }

        //完成
        btn_cary_out.setOnClickListener {
            val name = (item_device_name.rightTextView ?: return@setOnClickListener).text.toString()
            val qrCodeBleBeans: ArrayList<DeviceBean> =
                LocalDataSource.instance.queryAll(DeviceBean::class.java)
            var isExit = false
            qrCodeBleBeans.forEach {
                if (it.mac == mDeviceAddress) {
                    isExit = true
                }
                if (it.deviceName == name) {
                    isExit = true
                }
            }
            if (isExit) {
                ToastUtils.show(getString(R.string.Youhave_added_this_device))
                return@setOnClickListener
            }

            if (TextUtils.isEmpty(item_device_name.rightTextView!!.text.toString())) {
                ToastUtils.show(getString(R.string.device_name_is_null))
                return@setOnClickListener
            }
            if (name == App.device_name) {
                runOnUiThread {
                    dialogDismiss()
                    LocalDataSource.instance.save(
                        DeviceBean(
                            (item_device_name.rightTextView
                                ?: return@runOnUiThread).text.toString()
                            , mDeviceAddress,
                            "", version
                        )
                    )
                    dialogDismiss()
                    ToastUtils.show(getString(R.string.add_ok))
                    RxBus.post(ChangeDeviceDataEvent())
                    ActivityManager.getInstance().backTo(MainActivity::class.java)
                }
            } else {
                runOnUiThread {
                    showDialog()
                    ToastUtils.show(getString(R.string.loading_text))
                }
                mDeviceName = name
                if (connectManager != null) {
                    if ((connectManager
                            ?: return@setOnClickListener).isConnectDevice && gattCharacteristicData.size != 0
                        && gattCharacteristicData[0][0].isNotEmpty()
                    ) {
                        sendData(
                            gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                            GattType.UPDATE_DEVICE_NAME
                        )
                    } else {
                        connectionBle()
                    }
                } else {
                    connectionBle()
                }
            }
        }
    }

    override fun startSubscribeError() {
        if (timeout != null) {
            timeout!!.dispose()
        }
    }

    override fun onMtuChangedSuper(gatt: BluetoothGatt?, mtu: Int, status: Int) {
    }

    override fun onCharacteristicReadSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    ) {
    }

    override fun onCharacteristicWriteSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        status: Int
    ) {
    }

    override fun onCharacteristicChangedSuper(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic
    ) {
        retultNotice(characteristic)
    }

    private fun retultNotice(characteristicData: BluetoothGattCharacteristic) {
        if (!(connectManager ?: return).isConnectDevice) {
            dialogDismiss()
            ToastUtils.show(getString(R.string.Device_disconnected))
            if (connectManager != null) {
                connectManager!!.disconnect(mDeviceAddress)
            }
            if (timeout != null) {
                timeout!!.dispose()
            }
            return
        }
        isCloseLoop = true

        val returnBytes = ByteUtils.byteArrayToHexString(characteristicData.value).split(",")
        val commandStr = (returnBytes[5] + returnBytes[6])
        if (commandStr == "0011") {
            BleClear()
            if (timeout != null) {
                (timeout ?: return).dispose()
            }
            runOnUiThread {
                dialogDismiss()
                LocalDataSource.instance.save(
                    DeviceBean(
                        (item_device_name.rightTextView
                            ?: return@runOnUiThread).text.toString()
                        , mDeviceAddress,
                        "", version
                    )
                )
                dialogDismiss()
                ToastUtils.show(getString(R.string.add_ok))
                RxBus.post(ChangeDeviceDataEvent())
                ActivityManager.getInstance().backTo(MainActivity::class.java)
            }
        }
    }

    private fun getBaseEapearData() {
        isCloseLoop = false
        val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
            val byteList =
                BitmapBleBmpManagement.writeEntityOta(StringUtils.toByte(mDeviceName), 7)
            val byteListChange =
                BitmapBleBmpManagement.setOteVersion(17, byteList[0])
            var isSendState3 = false
            while (!isSendState3) {
                if (isCloseLoop) {
                    LogHelper.i("witstec", "退出循环")
                    break
                }
                if (characteristic != null) {
                    LogHelper.i("witstec", "发送了数据")
                    characteristic!!.value = byteListChange
                    isSendState3 = gatt!!.writeCharacteristic(characteristic)
                } else {
                    LogHelper.i("witstec", "没有服务特性，无法发送")
                }
                LogHelper.i(
                    "sendImage", "修改名称=-${Utils.bytesToHexString(byteListChange)}"
                )
                if (isSendState3) {
                    e.onNext(true)
                    break
                }
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isStatus ->
                if (timeout != null) {
                    (timeout ?: return@subscribe).dispose()
                }
                runOnUiThread {
                    dialogDismiss()
                    LocalDataSource.instance.save(
                        DeviceBean(
                            (item_device_name.rightTextView
                                ?: return@runOnUiThread).text.toString()
                            , mDeviceAddress,
                            "", version
                        )
                    )
                    dialogDismiss()
                    BleClear()
                    ToastUtils.show(getString(R.string.add_ok))
                    RxBus.post(ChangeDeviceDataEvent())
                    ActivityManager.getInstance().backTo(MainActivity::class.java)
                }
            }
        addDisposable(disposable)
    }

    override fun onConnectionStateChangeSuper(gatt: BluetoothGatt, status: Int, newState: Int) {
    }

    override fun displayGattServicesInit() {
        if (gattCharacteristicData.size != 0) {
            sendData(
                gattCharacteristicData[0][0][LIST_UUID] ?: error(""),
                GattType.UPDATE_DEVICE_NAME
            )
        } else {
            dialogDismiss()
            connectionBle()
        }
    }

    override fun sendDataInit(gattType: GattType) {
        if (!(connectManager ?: return).isConnectDevice) {
            dialogDismiss()
            connectionBle()
        }
        when (gattType) {
            GattType.UPDATE_DEVICE_NAME -> {
                LogHelper.i("gattService", "开始修改设备名称")
                getBaseEapearData()
            }
            else -> {

            }
        }
    }

    override fun onServicesDiscoveredSuper(gatt: BluetoothGatt) {
        if (isConfirmSend) {
            isConfirmSend = false
            runOnUiThread {
                LogHelper.i("gattService", "连接成功")
                addDisposable(
                    Observable.timer(400, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            displayGattServices(gatt.services)
                        })
            }
        }
    }

    override fun timeOut() {
        dialogDismiss()
        if (timeout != null) {
            (timeout ?: return).dispose()
        }
        timeout = Observable.timer(
            1000 * 13, TimeUnit.MILLISECONDS
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                LogHelper.i("witstec", "修改名称失败")
                ToastUtils.show(getString(R.string.add_error))
                if (timeout != null) {
                    (timeout ?: return@subscribe).dispose()
                }
            }
        addDisposable(timeout!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        isConfirmSend = false
        isCloseLoop = true
        if (timeout != null) {
            timeout!!.dispose()
        }
        if (connectManager != null) {
            if (connectManager!!.isConnectDevice) {
                connectManager!!.disconnect(mDeviceAddress)
                connectManager!!.release()
            }
        }
    }
}
