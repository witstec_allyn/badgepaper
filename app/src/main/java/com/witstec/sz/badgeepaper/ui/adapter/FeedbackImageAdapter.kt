package com.witstec.sz.badgeepaper.ui.adapter

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.utils.GlideUtils
import java.util.ArrayList

class FeedbackImageAdapter : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_feedback_iamge, ArrayList()) {

    override fun convert(helper: BaseViewHolder, item: String) {
        // 动态设置图片大小 保证宽高相等
        val ivSize = (getScreenContentWidth(helper.itemView.context) - dp2px(helper.itemView.context, 10) * 6) / 3
        (helper.getView<View>(R.id.iv_feedback_image) as ImageView).layoutParams.height = ivSize
        GlideUtils.loadImageView(helper.itemView.context,item, (helper.getView<View>(R.id.iv_feedback_image) as ImageView))
    }

    companion object {
        fun dp2px(context: Context, dps: Int): Int {
            return Math.round(dps.toFloat() * getDensityDpiScale(context))
        }

        private fun getDensityDpiScale(context: Context): Float {
            return context.resources.displayMetrics.xdpi / 160.0f
        }

        fun getScreenContentWidth(context: Context): Int {
            val displayMetrics = context.resources.displayMetrics
            return displayMetrics.widthPixels
        }
    }

}