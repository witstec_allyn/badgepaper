package com.witstec.sz.badgeepaper.ui.activity.setting

import android.annotation.SuppressLint
import android.os.Bundle
import com.witstec.sz.badgeepaper.BuildConfig
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.myStartActivity
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppBaseActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        tv_version.setText("V"+AppUtils.getAppVersionName(this))
    }
}
