package com.witstec.sz.badgeepaper.model.event;

import com.witstec.sz.badgeepaper.model.bean.InputBoxBean;

import java.util.List;

public class DeviceMsgUpdateEvent {

    private List<InputBoxBean> data;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public DeviceMsgUpdateEvent(List<InputBoxBean> data, int position) {
        this.data = data;
        this.position = position;
    }

    public DeviceMsgUpdateEvent(List<InputBoxBean> data) {
        this.data = data;
    }

    public List<InputBoxBean> getData() {
        return data;
    }

    public void setData(List<InputBoxBean> data) {
        this.data = data;
    }
}
