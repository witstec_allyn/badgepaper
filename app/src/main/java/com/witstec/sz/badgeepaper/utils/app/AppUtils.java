package com.witstec.sz.badgeepaper.utils.app;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.LocationManager;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextDirectionHeuristics;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.room.util.StringUtil;

import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.utils.LogHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.View.DRAWING_CACHE_QUALITY_HIGH;

/**
 * author : Allyn
 */
public class AppUtils {

    /**
     *   * 获取系统的语言类型
     */
    public static String getLocaleLanguage() {
        Locale l = Locale.getDefault();
        String language = String.format("%s-%s", l.getLanguage(), l.getCountry());
        String languageStr = "";
        if (language.contains("zh-CN")) {
            languageStr = "cn";
        } else {
            languageStr = "en";
        }
        return languageStr;
    }


    //获取渠道名称
    public static String getChannelName(Activity ctx) {
        if (ctx == null) {
            return null;
        }
        String channelName = "";
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                //注意此处为ApplicationInfo 而不是 ActivityInfo,因为友盟设置的meta-data是在application标签中，而不是某activity标签中，所以用ApplicationInfo
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        channelName = applicationInfo.metaData.getString("UMENG_CHANNEL");
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return channelName;
    }

    /**
     * ver1 > ver2 = 1
     * ver1 = ver2 = 0
     * ver1 < ver2 = 2
     *
     * @param ver1
     * @param ver2
     * @return
     */
    public static boolean isUpgrade(String ver1, String ver2) {
        if (ver1.equals(ver2)) {
            return false;
        }
        String[] verArr1 = ver1.split("\\.");
        String[] verArr2 = ver2.split("\\.");
        int maxflag = 1;
        int minLen = 0;
        if (verArr1.length > verArr2.length) {
            minLen = verArr2.length;
        } else {
            minLen = verArr1.length;
            maxflag = 2;
        }
        for (int i = 0; i < minLen; i++) {
            if (Integer.valueOf(verArr1[i]) - Integer.valueOf(verArr2[i]) > 0) {
                return false;
            } else if (Integer.valueOf(verArr1[i]) - Integer.valueOf(verArr2[i]) < 0) {
                return true;
            }
        }
        if (maxflag == 1) {
            for (int j = minLen; j < verArr1.length; j++) {
                if (Integer.valueOf(verArr1[j]).intValue() > 0) {
                    return false;
                }
            }
        } else {
            for (int k = minLen; k < verArr2.length; k++) {
                if (Integer.valueOf(verArr2[k]).intValue() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取机型
     */
    public static String getPhoneModel() {
        String brand = android.os.Build.BRAND;//手机品牌
        String model = android.os.Build.MODEL;//手机型号
        LogHelper.INSTANCE.i("witstec", "手机型号：" + brand + model);
        return brand + model;
    }

    /**
     * 手机是否开启位置服务，如果没有开启那么所有app将不能使用定位功能
     */
    public static boolean isLocServiceEnable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }
        return false;
    }

    // 缩放图片
    public static Bitmap zoomImg(Bitmap bm, int newWidth, int newHeight) {
        // 获得图片的宽高
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return newbm;
    }

    /**
     * 图片按比例大小压缩方法
     *
     * @param image （根据Bitmap图片压缩）
     * @return
     */
    public static Bitmap compressScale(Bitmap image, int newWidth, int newHeight) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        // 判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
        if (baos.toByteArray().length / 1024 > 1024) {
            baos.reset();// 重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, 80, baos);// 这里压缩50%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        // 开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        // 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        // float hh = 800f;// 这里设置高度为800f
        // float ww = 480f;// 这里设置宽度为480f
        float hh = newHeight;
        float ww = newWidth;
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;// be=1表示不缩放
        if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) { // 如果高度高的话根据高度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be; // 设置缩放比例
        // newOpts.inPreferredConfig = Config.RGB_565;//降低图片从ARGB888到RGB565
        // 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        isBm = new ByteArrayInputStream(baos.toByteArray());
        bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
//        return compressImage(bitmap);// 压缩好比例大小后再进行质量压缩
        return bitmap;
    }

    /**
     * @param foreground 前景图
     * @return
     */
    public static Bitmap combineBitmap(Bitmap foreground) {
        Rect rect = new Rect(0, 0, foreground.getWidth(), foreground.getHeight());
        Bitmap newmap = Bitmap.createBitmap(Constants.SEND_W_021, Constants.SEND_H_021, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newmap);
        canvas.drawBitmap(foreground, null, rect, null);//设置二维码所在的位置 这个可以写死
        canvas.save();
        canvas.restore();
        return newmap;
    }

    public static String eslIdMacStr(String mac) {
        StringBuilder buffer = new StringBuilder();
        String[] macStr = mac.split(":");
        if (macStr.length > 2) {
            buffer.append(macStr[2]);
            buffer.append(macStr[3]);
            buffer.append(macStr[4]);
            buffer.append(macStr[5]);
        } else {
            return mac;
        }
        return buffer.toString();
    }

    //验证价格
    public static boolean checkPriceString(String s) {
        return s.matches("/^(?:0\\.\\d{0,1}[1-9]|(?!0)\\d{1,9}(?:\\.\\d{0,1}[1-9])?)$/");
    }

    public static int getTextViewLines(TextView textView, int textViewWidth) {
        int width = textViewWidth - textView.getCompoundPaddingLeft() - textView.getCompoundPaddingRight();
        StaticLayout staticLayout;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            staticLayout = getStaticLayout23(textView, width);
        } else {
            staticLayout = getStaticLayout(textView, width);
        }
        int lines = staticLayout.getLineCount();
        int maxLines = textView.getMaxLines();
        if (maxLines > lines) {
            return lines;
        }
        return maxLines;
    }

    /**
     * 不同大小不同压缩倍数
     *
     * @return
     */
    public static int[] bitmapShrink(int w, int h) {
        int[] whs = new int[2];
        if (w < 2000 || h < 2000) {
            whs[0] = w / 2;
            whs[1] = h / 2;
        } else if (w < 3000 || h < 3000) {
            whs[0] = w / 3;
            whs[1] = h / 3;
        } else if (w < 4000 || h < 4000) {
            whs[0] = w / 4;
            whs[1] = h / 4;
        } else if (w < 5000 || h < 5000) {
            whs[0] = w / 5;
            whs[1] = h / 5;
        } else if (w < 6000 || h < 6000) {
            whs[0] = w / 6;
            whs[1] = h / 6;
        } else if (w < 7000 || h < 7000) {
            whs[0] = w / 7;
            whs[1] = h / 7;
        } else if (w < 8000 || h < 8000) {
            whs[0] = w / 8;
            whs[1] = h / 8;
        } else if (w < 9000 || h < 9000) {
            whs[0] = w / 9;
            whs[1] = h / 9;
        } else {
            whs[0] = w / 2;
            whs[1] = h / 2;
        }
        return whs;
    }


    /**
     * sdk>=23
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private static StaticLayout getStaticLayout23(TextView textView, int width) {
        StaticLayout.Builder builder = StaticLayout.Builder.obtain(textView.getText(),
                0, textView.getText().length(), textView.getPaint(), width)
                .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                .setTextDirection(TextDirectionHeuristics.FIRSTSTRONG_LTR)
                .setLineSpacing(textView.getLineSpacingExtra(), textView.getLineSpacingMultiplier())
                .setIncludePad(textView.getIncludeFontPadding())
                .setBreakStrategy(textView.getBreakStrategy())
                .setHyphenationFrequency(textView.getHyphenationFrequency())
                .setMaxLines(textView.getMaxLines() == -1 ? Integer.MAX_VALUE : textView.getMaxLines());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setJustificationMode(textView.getJustificationMode());
        }
        if (textView.getEllipsize() != null && textView.getKeyListener() == null) {
            builder.setEllipsize(textView.getEllipsize())
                    .setEllipsizedWidth(width);
        }
        return builder.build();
    }

    /**
     * sdk<23
     */
    private static StaticLayout getStaticLayout(TextView textView, int width) {
        return new StaticLayout(textView.getText(),
                0, textView.getText().length(),
                textView.getPaint(), width, Layout.Alignment.ALIGN_NORMAL,
                textView.getLineSpacingMultiplier(),
                textView.getLineSpacingExtra(), textView.getIncludeFontPadding(), textView.getEllipsize(),
                width);
    }

    /**
     * 本地图片转换Base64的方法
     */

    public static String imageToBase64(String path) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
            is = new FileInputStream(path);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return Constants.BASE64 + result;
    }


    /**
     * 获取当前app version name
     */
    public static String getAppVersionName(Context context) {
        String appVersionName = "";
        try {
            PackageInfo packageInfo = context.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            appVersionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("", e.getMessage());
        }
        return appVersionName;
    }

    /*
     * bitmap转base64
     * */
    public static String bitmapToBase64(Bitmap bitmap) {
        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                baos.flush();
                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Constants.BASE64 + result;
    }

    /*
     * bitmap转base64 不添加前缀
     * */
    public static String bitmapToBase64Tep(Bitmap bitmap) {
        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                baos.flush();
                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public static Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    /**
     * xml to string
     *
     * @param path
     * @return
     */
    public static String xmlToString(String path) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(path));
            int len = fileInputStream.available();
            byte[] strBytes = new byte[len];
            fileInputStream.read(strBytes);
            String xmlStr = new String(strBytes);
            return xmlStr;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取一条随机字符串
     *
     * @param length
     * @return
     */
    public static String getRandomString(int length) { //length表示生成字符串的长度
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }


    //保存文件到sd卡
    public static void saveToFile(String name, String content) {
        BufferedWriter out = null;
        //获取SD卡状态
        String state = Environment.getExternalStorageState();
        //判断SD卡是否就绪
        if (!state.equals(Environment.MEDIA_MOUNTED)) {
            LogHelper.INSTANCE.i("alln", "请检查SD卡");
            return;
        }
        //取得SD卡根目录
        File file = Environment.getExternalStorageDirectory();
        try {
            LogHelper.INSTANCE.i("alln", "SD卡根目录=" + file.getCanonicalPath());
            if (file.exists()) {
                LogHelper.INSTANCE.i("alln", "文件已存在=" + file.getCanonicalPath());
            }
            /*
            输出流的构造参数1：可以是File对象  也可以是文件路径
            输出流的构造参数2：默认为False=>覆盖内容； true=>追加内容
             */
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getCanonicalPath() + "/" + name + ".txt", true)));
            out.newLine();
            out.write(content);
            LogHelper.INSTANCE.i("alln", "保存成功");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getDate(String timestampString) {
        Long timestamp = Long.parseLong(timestampString) * 1000;
        String date = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date(timestamp));
        return date;
    }

    public static String typeIdToTypeName(String bleType) {
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(String.valueOf(bleType));
        if (m.matches()) {
            int id = Integer.parseInt(bleType);
            if (id == 1) {
                return "1.54";
            } else if (id == 2) {
                return "2.13";
            } else if (id == 3) {
                return "2.9";
            } else if (id == 4) {
                return "4.2";
            } else if (id == 5 || id == 6) {
                return "7.5";
            } else {
                return "0";
            }
        } else {
            if (bleType.contains("29")) {
                return "2.9";
            } else if (bleType.contains("42")) {
                return "4.2";
            } else if (bleType.contains("15")) {
                return "1.54";
            } else if (bleType.contains("21")) {
                return "2.13";
            } else if (bleType.contains("75")) {
                return "7.5";
            } else {
                return "0";
            }
        }
    }

    public static String typeNameToTypeId(String bleType) {
        if (bleType.contains("B")) {
            if (bleType.contains("29")) {
                return String.valueOf(3);
            } else if (bleType.contains("42")) {
                return String.valueOf(4);
            } else if (bleType.contains("15")) {
                return String.valueOf(1);
            } else if (bleType.contains("21")) {
                return String.valueOf(2);
            } else if (bleType.contains("75")) {
                if (bleType.substring(bleType.length() - 1, bleType.length()).equals("C")) {
                    return String.valueOf(5);
                } else {
                    return String.valueOf(6);
                }
            } else {
                return String.valueOf(1);
            }
        } else {
            return bleType;
        }
    }

    public static String xmlToString(InputStream inputStream) {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder("");
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * 把两个位图覆盖合成为一个位图，以底层位图的长宽为基准
     *
     * @param backBitmap  在底部的位图
     * @param frontBitmap 盖在上面的位图
     * @return
     */
    public static Bitmap mergeBitmap(Bitmap backBitmap, Bitmap frontBitmap) {
        if (backBitmap == null || backBitmap.isRecycled()
                || frontBitmap == null || frontBitmap.isRecycled()) {
            return null;
        }
        Bitmap bitmap = backBitmap.copy(Bitmap.Config.RGB_565, true);
        Canvas canvas = new Canvas(bitmap);
        Rect baseRect = new Rect(0, 0, backBitmap.getWidth(), backBitmap.getHeight());
        Rect frontRect = new Rect(0, 0, frontBitmap.getWidth(), frontBitmap.getHeight());
        canvas.drawBitmap(frontBitmap, frontRect, baseRect, null);
        return bitmap;
    }

    /**
     * 该方式原理主要是：View组件显示的内容可以通过cache机制保存为bitmap
     */
    public static Bitmap createBitmapFromView(View view) {
        Bitmap bitmap = null;
        //开启view缓存bitmap
        view.setDrawingCacheEnabled(true);
        //设置view缓存Bitmap质量
        view.setDrawingCacheQuality(DRAWING_CACHE_QUALITY_HIGH);
        //获取缓存的bitmap
        Bitmap cache = view.getDrawingCache();
        if (cache != null && !cache.isRecycled()) {
            bitmap = Bitmap.createBitmap(cache);
        }
        //销毁view缓存bitmap
        view.destroyDrawingCache();
        //关闭view缓存bitmap
        view.setDrawingCacheEnabled(false);
        return bitmap;
    }


    /**
     * 时间日期格式化到年月日时分秒.
     */
    public static String dateFormatYMDHMS = "yyyy-MM-dd HH:mm:ss";

    /**
     * 描述：获取表示当前日期时间的字符串.
     *
     * @param format 格式化字符串，如："yyyy-MM-dd HH:mm:ss"
     * @return String String类型的当前日期时间
     */
    public static String getCurrentDate(String format) {
        String curDateTime = "";
        try {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
            Calendar c = new GregorianCalendar();
            curDateTime = mSimpleDateFormat.format(c.getTime());
        } catch (Exception e) {
        }
        return curDateTime;

    }


    public static boolean deleteFolderFile(String filePath) {
        try {
            File file = new File(filePath);//获取SD卡指定路径
            file.delete();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void hideSoftInputKeyBoard(Context context, View focusView) {
        if (focusView != null) {
            IBinder binder = focusView.getWindowToken();
            if (binder != null) {
                InputMethodManager imd = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imd.hideSoftInputFromWindow(binder, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        }
    }

    public static void showSoftInputKeyBoard(Context context, View focusView) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(focusView, InputMethodManager.SHOW_FORCED);
    }

    /**
     * 判断字符串中是否包含字母
     **/
    public static boolean isContainsLetter(String input) {
        if (!input.isEmpty()) {
            Matcher matcher = Pattern.compile(".*[a-zA-Z]+.*").matcher(input);
            return matcher.matches();
        } else {
            return false;
        }
    }
}
