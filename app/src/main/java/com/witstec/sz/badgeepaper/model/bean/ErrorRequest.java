package com.witstec.sz.badgeepaper.model.bean;

public class ErrorRequest {

    private int errcode;

    public ErrorRequest(int errcode) {
        this.errcode = errcode;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }
}
