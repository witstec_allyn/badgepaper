package com.witstec.sz.badgeepaper.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import cn.com.heaton.blelibrary.ble.Ble
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.meituan.android.walle.WalleChannelReader
import com.witstec.sz.badgeepaper.BuildConfig
import com.witstec.sz.badgeepaper.Constants
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.model.bean.CheckAPP
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.services.UpdateIntentService
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.freagment.DeviceFragment
import com.witstec.sz.badgeepaper.ui.freagment.LocalTemplateHomeFragment
import com.witstec.sz.badgeepaper.ui.freagment.OfficialTemplatesFragment
import com.witstec.sz.badgeepaper.ui.freagment.UserFragment
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.StringUtils
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.LogToFileUtils
import com.witstec.sz.badgeepaper.utils.app.LogToFileUtils.resetLogFile
import com.witstec.sz.badgeepaper.utils.app.StorageHelper
import com.yanzhenjie.permission.AndPermission
import kotlinx.android.synthetic.main.activity_main.*
import q.rorbin.badgeview.Badge
import q.rorbin.badgeview.QBadgeView
import java.util.*


class MainActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val testData = ByteArray(4)
//        testData[0] = 0x52
//        testData[1] = 0x53
//        testData[2] = 0x54
//        testData[3] = 0x55
//
//        LogHelper.i(
//            "sendImage",
//            "CRC16 ==" + ByteUtils.byteArrayToHexString(
//                testData
//            )
//        )
//        val result = CRC16CheckUtil.getCRC2(testData)
//        LogHelper.i(
//            "sendImage",
//            "CRC16  result==" +
//                    result
//
//        )

        val fragmentList = ArrayList<Fragment>()
        fragmentList.add(DeviceFragment())
        fragmentList.add(LocalTemplateHomeFragment())
        fragmentList.add(OfficialTemplatesFragment())
        fragmentList.add(UserFragment())
        val adapter = FragmentAdapter(supportFragmentManager, fragmentList)
        view_pager.adapter = adapter
        view_pager.currentItem = 0
        view_pager.offscreenPageLimit = 3
        bottom_Navigation.setupWithViewPager(view_pager)
        //禁用导航栏启用动画
        bottom_Navigation.enableAnimation(false)
        bottom_Navigation.enableShiftingMode(false)
        bottom_Navigation.enableItemShiftingMode(false)
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {
            }

            override fun onPageSelected(i: Int) {
            }

            override fun onPageScrollStateChanged(i: Int) {
            }
        })

//        val isNowDay2 = StringUtils.IsToday(StorageHelper.getTheSameDayLogs())
//        if (!isNowDay2) {
//            logsUpload()
//        }


        val permission = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        AndPermission.with(this)
            .runtime()
            .permission(permission)
            .onGranted {

            }
            .onDenied {
            }.start()

        //APP检查
        val isNowDay = StringUtils.IsToday(StorageHelper.getTheSameDay())
        if (!isNowDay) {
            LogHelper.i("checkapp", "执行了吗")
            checkAppUpdate()
        }
    }

    private fun logsUpload() {
        addDisposable(
            ApiRepository.uploadLogs(LogToFileUtils.getLogFile(), this)
                .subscribe({
                    LogHelper.i("logs", "日志上传成功")
                    resetLogFile()
                    LogHelper.i("logs", "日志已清空")
                    StorageHelper.saveTheSameDayLogs()
                }, {
                    dialogDismiss()
                    LogHelper.i("logs", "" + it.message)
                })
        )
    }

    @SuppressLint("WrongConstant")
    private fun checkAppUpdate() {
        addDisposable(
            ApiRepository.appCheck(this)
                .subscribe({ appStr ->
                    val version = appStr.v.ver
                    LogHelper.i("checkapp", "" + version)
                    if (version > BuildConfig.VERSION_CODE) {
                        val channel = WalleChannelReader.getChannel(this.applicationContext)
                        when (channel) {
                            "google" -> {
                                MaterialDialog.Builder(this)
                                    .title(getString(R.string.latest_version) + appStr.v.name)
                                    .content(appStr.v.desc.replace("，","\n"))
                                    .contentColor(Color.BLACK)
                                    .positiveText(R.string.google_play_download)
                                    .onPositive { dialog, which ->
                                        val intent = Intent()
                                        intent.action = "android.intent.action.VIEW"
                                        intent.data =
                                            Uri.parse("https://play.google.com/store/apps/details?id=com.witstec.badgeepaper")
                                        startActivity(intent)
                                    }
                                    .negativeText(R.string.not_update)
                                    .dismissListener {
                                        StorageHelper.saveTheSameDay()
                                    }
                                    .show()
                            }

//                            "witstec" -> {
//
//                            }
//                            "huawei" -> {
//
//                            }
                            else -> {
                                MaterialDialog.Builder(this)
                                    .title(getString(R.string.latest_version) + appStr.v.name)
                                    .content(appStr.v.desc.replace("，","\n"))
                                    .contentColor(Color.BLACK)
                                    .positiveText(R.string.update_now)
                                    .onPositive { dialog, which ->
                                        AndPermission.with(this)
                                            .runtime()
                                            .permission(
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                                Manifest.permission.READ_EXTERNAL_STORAGE
                                            )
                                            .onGranted {
                                                val updateIntent =
                                                    Intent(this, UpdateIntentService::class.java)
                                                updateIntent.action =
                                                    UpdateIntentService.ACTION_UPDATE
                                                updateIntent.putExtra(
                                                    "appName",
                                                    "WtCard.apk"
                                                )
                                                updateIntent.putExtra(
                                                    "downUrl", Constants.APK_URL + appStr.v.url
                                                )
//                                    updateIntent.putExtra(
//                                        "downUrl", "http://d5.kunyouxi.cn/down/xhdvr_7_5.apk"
//                                    )
                                                this.startService(updateIntent)
                                            }
                                            .onDenied {
                                                val packageURI =
                                                    Uri.parse("package:" + this.packageName)
                                                val intent = Intent(
                                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                    packageURI
                                                )
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                                startActivity(intent)
                                                Toast.makeText(
                                                    this,
                                                    getString(R.string.no_permissions),
                                                    Toast.LENGTH_LONG
                                                )
                                                    .show()
                                            }.start()
                                    }
                                    .negativeText(R.string.not_update)
                                    .onNegative() { dialog, which ->
                                        StorageHelper.saveTheSameDay()
                                    }
                                    .show()
                            }
                        }
                    }
                }, {
                    ToastUtils.show(it.message)
                })
        )
    }

    private fun addBadgeAt(position: Int, number: Int): Badge {
        return QBadgeView(this)
            .setBadgeNumber(number)
            .setGravityOffset(28F, 0F, true)
            .bindTarget(bottom_Navigation.getBottomNavigationItemView(position))
            .setOnDragStateChangedListener { dragState, _, _ ->
                if (Badge.OnDragStateChangedListener.STATE_SUCCEED === dragState) {
                    ToastUtils.show(this@MainActivity, "ok")
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        Ble.getInstance<BleRssiDevice>().disconnectAll()
    }

    internal class FragmentAdapter(
        manager: FragmentManager,
        private val fragmentList: List<Fragment>
    ) :
        FragmentPagerAdapter(manager) {

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getItem(i: Int): Fragment {
            return fragmentList[i]
        }
    }

}
