package com.witstec.sz.badgeepaper.ui.activity

import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.BuildConfig
import com.afollestad.materialdialogs.MaterialDialog
import com.blakequ.bluetooth_manager_lib.BleParamsOptions
import com.blakequ.bluetooth_manager_lib.connect.ConnectConfig
import com.witstec.sz.badgeepaper.*
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.activity.setting.HelpActivity
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_splash_activity.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppBaseActivity() {
    // 1. 创建一个 MainScope
    private var alphaAnimation: AlphaAnimation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_activity)

        runOnUiThread {
            if (!getSaveBoolean(Constant.USER_LICENSE)) {
                showLicenseDialog()
            } else {
                start()
            }
        }

    }

    private fun start() {
        setAnimation()
        startMain()
        BleParamsOptions.Builder()
            .setBackgroundScanPeriod(10000)
            .setForegroundBetweenScanPeriod(5000)
            .setForegroundScanPeriod(10000)
            .setBackgroundBetweenScanPeriod(1000)
            .setDebugMode(BuildConfig.DEBUG)
            .setMaxConnectDeviceNum(1)
            .setReconnectBaseSpaceTime(5000)
            .setReconnectMaxTimes(Integer.MAX_VALUE)
            .setReconnectStrategy(ConnectConfig.RECONNECT_FIXED_TIME)
            .setConnectTimeOutTimes(4000)
            .setReconnectedLineToExponentTimes(Integer.MAX_VALUE)
            .build()
    }

    private fun showLicenseDialog() {
        val view = View.inflate(this, R.layout.dialog_privacy_olicy_layout, null)
        val dialog = MaterialDialog.Builder(this)
            .customView(view, false)
            .canceledOnTouchOutside(false)
            .show()
        val tv_in_service_agreement = view.findViewById<TextView>(R.id.tv_in_service_agreement)
        val tv_in_service_policy = view.findViewById<TextView>(R.id.tv_in_service_policy)
        val disagree = view.findViewById<TextView>(R.id.disagree)
        val agree = view.findViewById<TextView>(R.id.agree)
        tv_in_service_agreement.setOnClickListener {
            HelpActivity.start(
                this,
                if (AppUtils.getLocaleLanguage() == "zh_cn") "file:///android_asset/service_agreement_zh.html"
                else "file:///android_asset/service_agreement_en.html",
                getString(R.string.Service_Agreement)
            )
        }
        tv_in_service_policy.setOnClickListener {
            HelpActivity.start(
                this,
                if (AppUtils.getLocaleLanguage() == "zh_cn") "file:///android_asset/privacy_zh.html"
                else "file:///android_asset/privacy_en.html",
                getString(R.string.Service_Agreement)
            )
        }

        disagree.setOnClickListener {
            dialog.dismiss()
            saveBoolean(Constant.USER_LICENSE, false)
            btn_submit_login.isEnabled = false
            check_is_agree.isChecked = false
            btn_submit_login.setBackgroundResource(R.drawable.btn_radius_enobled_false)
        }

        agree.setOnClickListener {
            saveBoolean(Constant.USER_LICENSE, true)
            dialog.dismiss()
            start()
        }
    }

    fun setAnimation() {
        layout_splash.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        alphaAnimation = AlphaAnimation(0.2F, 1F)
        alphaAnimation?.run {
            duration = 300
            isFillEnabled = true
            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {

                }

                override fun onAnimationStart(p0: Animation?) {
                }
            })
        }
        layout_splash.startAnimation(alphaAnimation)
    }

    fun startMain() {
        addDisposable(
            Observable.timer(300, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    myStartActivity(
                        this@SplashActivity,
                        MainActivity::class.java
                    )
                    finish()
                    overridePendingTransition(
                        android.R.anim.fade_in,
                        android.R.anim.fade_out
                    )
                })
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
