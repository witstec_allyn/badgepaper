package com.witstec.sz.badgeepaper.model.bean;

import java.util.List;

public class BleTemplate {

    private List<TemplatesBean> templates;

    public List<TemplatesBean> getTemplates() {
        return templates;
    }

    public void setTemplates(List<TemplatesBean> templates) {
        this.templates = templates;
    }

    public static class TemplatesBean {
        /**
         * id : string
         * type : string
         * tags : string
         * name : string
         * img : string
         * url : string
         */

        private String id;
        private String type;
        private String tags;
        private String name;
        private String img;
        private String url;
        private int time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }
    }
}
