package com.witstec.sz.badgeepaper.model.bean;

public class RegisterRequest {

    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
