package com.witstec.sz.badgeepaper.ui.activity

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.myStartActivity
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_start.*
import kotlinx.android.synthetic.main.item_start_four_layout.view.*
import java.util.ArrayList

class StartActivity : AppBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        val one = View.inflate(this, R.layout.item_start_one_layout, null)
        val two = View.inflate(this, R.layout.item_start_two_layout, null)
        val three = View.inflate(this, R.layout.item_start_three_layout, null)
        val four = View.inflate(this, R.layout.item_start_four_layout, null)

        val layList = ArrayList<View>()
        layList.add(one!!)
        layList.add(two!!)
        layList.add(three!!)
        layList.add(four!!)

        four.btn_submit_start.setOnClickListener {
            myStartActivity(this, MainActivity::class.java)
            finish()
        }

        val adapter = ViewPagerAdapter(layList)
        viewPager.adapter = adapter
        indicator.setViewPager(viewPager)
        adapter.registerDataSetObserver(indicator.dataSetObserver);
        viewPager.currentItem = 0
        viewPager.offscreenPageLimit = 4
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
            }
        })
    }
}
