package com.witstec.sz.badgeepaper.ui.activity.device

import android.Manifest
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.core.content.ContextCompat
import cn.com.heaton.blelibrary.ble.Ble
import cn.com.heaton.blelibrary.ble.BleLog
import cn.com.heaton.blelibrary.ble.callback.BleConnectCallback
import cn.com.heaton.blelibrary.ble.callback.BleNotifyCallback
import cn.com.heaton.blelibrary.ble.callback.BleWriteCallback
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.litesuits.orm.db.assit.WhereBuilder
import com.litesuits.orm.db.model.ColumnsValue
import com.litesuits.orm.db.model.ConflictAlgorithm
import com.witstec.sz.badgeepaper.App
import com.witstec.sz.badgeepaper.Constants
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity
import com.witstec.sz.badgeepaper.manage.BitmapBleBmpManagement
import com.witstec.sz.badgeepaper.model.bean.DeviceBean
import com.witstec.sz.badgeepaper.model.bean.OTADataMsg
import com.witstec.sz.badgeepaper.model.bean.TemplateBean
import com.witstec.sz.badgeepaper.model.db.LocalDataSource
import com.witstec.sz.badgeepaper.model.db.LocalDataSource.Companion.instance
import com.witstec.sz.badgeepaper.model.event.ChangeDeviceDataEvent
import com.witstec.sz.badgeepaper.network.ApiRepository
import com.witstec.sz.badgeepaper.newble.BleRssiDevice
import com.witstec.sz.badgeepaper.ui.activity.MainActivity
import com.witstec.sz.badgeepaper.ui.activity.base.AppBaseActivity
import com.witstec.sz.badgeepaper.ui.activity.device.longConnection.DeviceManageActivity
import com.witstec.sz.badgeepaper.utils.*
import com.witstec.sz.badgeepaper.utils.ByteUtils.byteArrayToHexString
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.utils.app.CacheActivity
import com.witstec.sz.badgeepaper.view.IosDialog
import com.yanzhenjie.permission.AndPermission
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_device_manage_mone.*
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.zip.CRC32
import com.witstec.sz.badgeepaper.view.IosDialog.EditOnClickListener as EditOnClickListener1


/**
 * 设备设置
 */
class DeviceManageMoreActivity : AppBaseActivity() {

    private val TAG = "DeviceManageMoreActivity"
    private var extra_ble_type: String = ""
    private var isCloseLoop = false
    private var isUpdateName = false
    private var isCard = false
    private var mDeviceName = ""
    private var mCardVersion = ""
    private var mBaseVersion = ""
    private var OTAfILEUrl = ""
    private var mDeviceAddress = ""
    private var timeoutMain: Disposable? = null
    private var gattServices: ArrayList<BluetoothGattService> = ArrayList()
    private var mNewCardVersion = ""
    private var mNewBaseVersion = ""

    companion object {
        fun start(
            context: Context,
            mac: String,
            bletype: String,
            baseVersion: String,
            cardVersion: String
        ) {
            val intent = Intent(context, DeviceManageMoreActivity::class.java)
            intent.putExtra("extra_mac", mac)
            intent.putExtra("extra_ble_type", bletype)
            intent.putExtra("extra_base_version", baseVersion)
            intent.putExtra("extra_card_version", cardVersion)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_manage_mone)
        if (CacheActivity.activityList.contains(this)) {
            CacheActivity.addActivity(this);
        }
        initView()
        //检查更新
        OTAHint()
        listener()
    }

    fun contectBLE() {
        if (App.isBleInitSuccess) {
            runOnUiThread {
                showDialog(getString(R.string.loading_text))
            }
            Ble.getInstance<BleRssiDevice>().disconnectAll()
            val dis=Observable.timer(2500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Ble.options().isAutoConnect = true
                    Ble.getInstance<BleRssiDevice>().connect(mDeviceAddress, connectCallback)
                    timeoutMain = Observable.timer(
                        1000 * 15, TimeUnit.MILLISECONDS
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            dialogDismiss()
                            ToastUtils.show(getString(R.string.Check_OTA_update_timeout))
                        }
                    addDisposable(disposable = timeoutMain ?: return@subscribe)
                }
            addDisposable(dis)
        }
    }

    private val connectCallback: BleConnectCallback<BleRssiDevice> =
        object :
            BleConnectCallback<BleRssiDevice>() {
            override fun onConnectionChanged(device: BleRssiDevice?) {
                Log.e(
                    "witstecota",
                    "onConnectionChanged: " + device!!.connectionState
                )
            }

            override fun onConnectFailed(
                device: BleRssiDevice?,
                errorCode: Int
            ) {
                super.onConnectFailed(device, errorCode)

            }

            override fun onConnectCancel(device: BleRssiDevice) {
                super.onConnectCancel(device)
                Log.e(
                    "witstecota",
                    "onConnectCancel: " + device.bleName
                )
            }

            override fun onServicesDiscovered(
                device: BleRssiDevice?,
                gatt: BluetoothGatt
            ) {
                super.onServicesDiscovered(device, gatt)
                gattServices!!.addAll(gatt.services)
                if (timeoutMain != null) {
                    timeoutMain!!.dispose()
                }
            }

            override fun onReady(device: BleRssiDevice?) {
                super.onReady(device)
                if (gattServices.size == 0) return
                //连接成功后，设置通知
                val characteristic = gattServices.get(0).characteristics?.get(0)
                val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
                val serviceUuid: UUID? = characteristic?.getService()?.getUuid()
                val characteristicUuid: UUID? = characteristic?.getUuid()
                if (connetedDevices.size == 0) return
                SystemClock.sleep(250)
                Ble.getInstance<BleRssiDevice>().enableNotifyByUuid(
                    connetedDevices[0],
                    true,
                    serviceUuid,
                    characteristicUuid,
                    object : BleNotifyCallback<BleRssiDevice>() {
                        override fun onChanged(
                            device: BleRssiDevice,
                            characteristic: BluetoothGattCharacteristic
                        ) {
                            val byteDatas =
                                cn.com.heaton.blelibrary.ble.utils.ByteUtils.toHexString(
                                    characteristic.value
                                )
                            BleLog.e(TAG, "onChanged==data:$byteDatas")
                            val returnBytes = byteArrayToHexString(characteristic.value).split(",")
                            val commandStr = (returnBytes[5] + returnBytes[6])
                            when (commandStr) {
                                "0010" -> {
                                }
                            }
                        }

                        override fun onNotifySuccess(device: BleRssiDevice) {
                            super.onNotifySuccess(device)
                            BleLog.e(
                                TAG,
                                "onNotifySuccess: " + device.bleName
                            )
                            val dis = Observable.timer(1000 * 2, TimeUnit.MILLISECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {
                                    runOnUiThread {
                                        dialogDismiss()
                                        update(OTAfILEUrl)
                                    }
                                }
                            addDisposable(dis)
                        }
                    })
            }
        }

    fun initView() {
        extra_ble_type = intent.getStringExtra("extra_ble_type").toString()
        mDeviceAddress = intent.getStringExtra("extra_mac")
        mBaseVersion = intent.getStringExtra("extra_base_version").toString()
        mCardVersion = intent.getStringExtra("extra_card_version").toString()
        setToolbar(getString(R.string.more))
        (item_device_manage_name.rightTextView ?: return).text = App.device_name
        if (extra_ble_type.isNotEmpty())
            (item_device_manage_type.getTvRightText() ?: return).text = extra_ble_type
        (item_device_manage_id.getTvRightText() ?: return).text =
            AppUtils.eslIdMacStr(mDeviceAddress)
        if (mCardVersion.isNotEmpty())
            (item_wt_device_manage_upgrade.getTvRightText() ?: return).text = "v $mCardVersion"
        (item_device_manage_upgrade.getTvRightText() ?: return).text = "v $mBaseVersion"
    }

    fun listener() {
        item_device_manage_name.setOnClickListener {
            IosDialog(this@DeviceManageMoreActivity).init()
                .setTitle(getString(R.string.device_name))
                .setEditTextContext(
                    item_device_manage_name.rightText,
                    getString(R.string.input_device_manage_name)
                )
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                }, object : EditOnClickListener1 {
                    override fun editContext(context: EditText) {
                        if (context.text.toString() == App.device_name) {
                            return
                        }
                        val contByte = StringUtils.toByte(context.text.toString())
//                        LogHelper.i("chagndu", byteArrayToHexStringLX(contByte))
//                        LogHelper.i("chagndu", hexStr2Str(byteArrayToHexStringLX(contByte)))
                        if (contByte.size > 7) {
                            ToastUtils.show(getString(R.string.Name_exceeds_length_limit))
                            return
                        }
                        isUpdateName = true
                        mDeviceName = context.text.toString()
                        showDialog()
                        updateDeviceName()
                    }
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {

                })
                .show()


        }

        btn_delete_device.setOnClickListener {
            IosDialog(this@DeviceManageMoreActivity)
                .init()
                .setTitle(getString(R.string.delete_device))
                .setMsg(getString(R.string.delete_device_yes_no))
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                    instance
                        .liteOrm
                        .delete(
                            WhereBuilder.create(DeviceBean::class.java)
                                .where("mac=?", mDeviceAddress)
                        )
                    ToastUtils.show(getString(R.string.delete_ok))
                    dialogDismiss()
                    RxBus.post(ChangeDeviceDataEvent())
                    ActivityManager.getInstance().backTo(MainActivity::class.java)
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {

                })
                .show()
        }
    }

    fun updateDeviceName() {
        Ble.getInstance<BleRssiDevice>()
            .connect(mDeviceAddress, object : BleConnectCallback<BleRssiDevice>() {
                override fun onConnectionChanged(device: BleRssiDevice?) {
                    Log.e(
                        "witstecota",
                        "onConnectionChanged: " + device!!.connectionState
                    )
                }

                override fun onConnectFailed(
                    device: BleRssiDevice?,
                    errorCode: Int
                ) {
                    super.onConnectFailed(device, errorCode)

                }

                override fun onConnectCancel(device: BleRssiDevice) {
                    super.onConnectCancel(device)
                    Log.e(
                        "witstecota",
                        "onConnectCancel: " + device.bleName
                    )
                }

                override fun onServicesDiscovered(
                    device: BleRssiDevice?,
                    gatt: BluetoothGatt
                ) {
                    super.onServicesDiscovered(device, gatt)
                    gattServices!!.addAll(gatt.services)
                    if (timeoutMain != null) {
                        timeoutMain!!.dispose()
                    }
                }

                override fun onReady(device: BleRssiDevice?) {
                    super.onReady(device)
                    if (gattServices.size == 0) return
                    //连接成功后，设置通知
                    val characteristic = gattServices.get(0).characteristics?.get(0)
                    val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
                    val serviceUuid: UUID? = characteristic?.getService()?.getUuid()
                    val characteristicUuid: UUID? = characteristic?.getUuid()
                    if (connetedDevices.size == 0) return
                    SystemClock.sleep(250)
                    Ble.getInstance<BleRssiDevice>().enableNotifyByUuid(
                        connetedDevices[0],
                        true,
                        serviceUuid,
                        characteristicUuid,
                        object : BleNotifyCallback<BleRssiDevice>() {
                            override fun onChanged(
                                device: BleRssiDevice,
                                characteristic: BluetoothGattCharacteristic
                            ) {
                                val byteDatas =
                                    cn.com.heaton.blelibrary.ble.utils.ByteUtils.toHexString(
                                        characteristic.value
                                    )
                                BleLog.e(TAG, "onChanged==data:$byteDatas")
                                val returnBytes =
                                    byteArrayToHexString(characteristic.value).split(",")
                                val byteValue = characteristic.value
                                val commandStr = (returnBytes[5] + returnBytes[6])

                            }

                            override fun onNotifySuccess(device: BleRssiDevice) {
                                super.onNotifySuccess(device)
                                BleLog.e(
                                    TAG,
                                    "onNotifySuccess: " + device.bleName
                                )

                                if (gattServices.size == 0) {
                                    runOnUiThread {
                                        ToastUtils.show(getString(R.string.connext_Timeout_retry))
                                    }
                                    return
                                }
                                val byteList = BitmapBleBmpManagement.writeEntityOta(
                                    StringUtils.toByte(mDeviceName),
                                    7
                                )
                                val byteListChange =
                                    BitmapBleBmpManagement.setOteVersion(17, byteList[0])
                                LogHelper.i(
                                    "sendImage", "修改名称--${Utils.bytesToHexString(byteListChange)}"
                                )
                                val characteristic = gattServices[0].characteristics?.get(0)
                                val suid = characteristic?.service?.uuid
                                val cuid = characteristic?.uuid
                                val connetedDevices =
                                    Ble.getInstance<BleRssiDevice>().connectedDevices
                                if (connetedDevices.size == 0) {
                                    disconnection()
                                    SystemClock.sleep(100)
                                    contectBLE()
                                    runOnUiThread {
                                        ToastUtils.show(getString(R.string.Bluetooth_connection_is_disconnected_retry))
                                    }
                                    return
                                }
                                val bleDevice = connetedDevices[0]
                                Ble.getInstance<BleRssiDevice>()
                                    .writeByUuid(bleDevice, byteListChange, suid, cuid,
                                        object :
                                            BleWriteCallback<BleRssiDevice?>() {
                                            override fun onWriteSuccessCharacteristicChanged(
                                                device: BleRssiDevice?,
                                                characteristic: BluetoothGattCharacteristic
                                            ) {
                                                LogHelper.i(
                                                    "writeByUuid",
                                                    "onWriteSuccess value=修改名称回复" +
                                                            ByteUtils.byteArrayToHexString(
                                                                characteristic.value
                                                            )
                                                )
                                                val returnBytes =
                                                    ByteUtils.byteArrayToHexString(
                                                        characteristic.value
                                                    ).split(",")
                                                val commandStr = (returnBytes[5] + returnBytes[6])
                                                if (commandStr == "0011") {
//                                if (returnBytes[8] == "00") {
//
//                                }
                                                    updateNameOk()
                                                }
                                            }

                                            override fun onWriteSuccess(
                                                device: BleRssiDevice?,
                                                characteristic: BluetoothGattCharacteristic
                                            ) {
                                                LogHelper.i(
                                                    "writeByUuid",
                                                    "onWriteSuccess value=" +
                                                            ByteUtils.byteArrayToHexString(
                                                                characteristic.value
                                                            )
                                                )
                                            }

                                            override fun onWriteFailed(
                                                device: BleRssiDevice?,
                                                failedCode: Int
                                            ) {
                                                super.onWriteFailed(device, failedCode)
//                                        tv_hint.post {
//                                            tv_hint.setText("读取描述失败:$failedCode")
//                                        }
                                            }
                                        })
                                val timeout = Observable.timer(
                                    1000 * 2, TimeUnit.MILLISECONDS
                                )
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe {
                                        updateNameOk()
                                    }
                                addDisposable(disposable = timeout ?: return)
                            }
                        })
                }
            })
    }


    fun updateNameOk() {
        timeOUtEnd()
        runOnUiThread {
            dialogDismiss()
            ToastUtils.show(getString(R.string.success))
            item_device_manage_name.rightTextView!!.text =
                mDeviceName
        }
        //修改名称
        val columnsValue = ColumnsValue(
            arrayOf(
                "deviceName"
            ), arrayOf(mDeviceName)
        )
        LocalDataSource.instance
            .liteOrm
            .update(
                WhereBuilder.create(DeviceBean::class.java)
                    .where("id=?", App.template_id),
                columnsValue, ConflictAlgorithm.None
            )
        RxBus.post(ChangeDeviceDataEvent())
    }

    fun timeOut(time: Int) {
        dialogDismiss()
        if (timeoutMain != null) {
            timeoutMain!!.dispose()
        }
        timeoutMain = Observable.timer(
            (1000 * time).toLong(), TimeUnit.MILLISECONDS
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                LogHelper.i("witstec", "OTA超时")
                dialogDismiss()
                errorView()
                disconnection()
            }
        addDisposable(timeoutMain!!)
    }

    fun errorView() {
        runOnUiThread {
            isCloseLoop = true
            dialogDismiss()
            LogHelper.i("witstec", "操作失败")
            ToastUtils.show(getString(R.string.ota_error_reatry))
            timeOUtEnd()
            Ble.getInstance<BleRssiDevice>().disconnectAll()
        }
    }

    fun timeOUtEnd() {
        if (timeoutMain != null) {
            (timeoutMain ?: return).dispose()
        }
    }

    private fun OTAHint() {
        addDisposable(
            ApiRepository.otaCheck(this, "WTCard")
                .subscribe({ data ->
                    val dataItem =
                        Gson().fromJson<OTADataMsg>(data.string(), OTADataMsg::class.java)
                    val newVersion = dataItem.version
                    mNewBaseVersion = newVersion
                    val isUpgrade = AppUtils.isUpgrade(mBaseVersion, newVersion)
                    if (isUpgrade) {
                        if (dataItem.url.isNotEmpty()) {
                            hintOtaUpdate(dataItem)
                        }
                    }
                }, {
                    dialogDismiss()
                })
        )
        addDisposable(
            ApiRepository.otaCheck(this, "wtc021a")
                .subscribe({ data ->
                    val dataItem =
                        Gson().fromJson<OTADataMsg>(data.string(), OTADataMsg::class.java)
                    val newVersion = dataItem.version
                    mNewCardVersion = newVersion
                    val isUpgrade = AppUtils.isUpgrade(mCardVersion, newVersion)
                    if (isUpgrade) {
                        if (dataItem.url.isNotEmpty()) {
                            hintOtaUpdateWt(dataItem)
                        }
                    }
                }, {
                    dialogDismiss()
                })
        )
    }

    private fun hintOtaUpdate(dataItem: OTADataMsg) {
        item_device_manage_upgrade.getTvRightText()!!.text =
            "v$mBaseVersion" + "-->" + "v" + dataItem.version + "(" + getString(
                R.string.check_have_new_ota
            ) + ")"
        item_device_manage_upgrade.getTvRightText()!!.setTextColor(
            ContextCompat.getColor(
                this,
                R.color.green
            )
        )
        item_device_manage_upgrade.setOnClickListener {
            DeviceManageActivity().finish()
            isCard = false
            OTAfILEUrl = dataItem.url
            contectBLE()
        }
    }

    private fun hintOtaUpdateWt(dataItem: OTADataMsg) {
        item_wt_device_manage_upgrade.getTvRightText()!!.text =
            "v$mCardVersion" + "-->" + "v" + dataItem.version + "(" + getString(
                R.string.check_have_new_ota
            ) + ")"
        item_wt_device_manage_upgrade.getTvRightText()!!.setTextColor(
            ContextCompat.getColor(
                this,
                R.color.green
            )
        )

        item_wt_device_manage_upgrade.setOnClickListener {
            DeviceManageActivity().finish()
            isCard = true
            OTAfILEUrl = dataItem.url
            contectBLE()
        }
    }

    fun update(url: String) {
        IosDialog(this@DeviceManageMoreActivity).init()
            .setTitle(getString(R.string.dialog_hint_ota))
            .setCancelable(false)
            .setMsg(getString(R.string.dialog_hint_ota_confirm_msg))
            .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                showDialog(getString(R.string.download_ota_file))
                val permission = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                AndPermission.with(this)
                    .runtime()
                    .permission(permission)
                    .onGranted {
                        SystemClock.sleep(100)
                        downFile(
                            Constants.URL + url,
                            StringUtils.getRandomString(15) + ".bin"
                        )
                    }
                    .onDenied {
                        ToastUtils.show(getString(R.string.no_permissions))
                    }.start()

            })
            .setNegativeButton(
                getString(R.string.cancel),
                View.OnClickListener {
                }).show()
    }


    /**
     * 文件下载
     */
    private fun downFile(url: String, fileName: String) {
        var filePath = ""
        var error = ""
        val disposable = Observable.create(ObservableOnSubscribe<Boolean> { sub ->
            DownloadUtil.get()
                .download(url, getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)?.path, fileName,
                    object : DownloadUtil.OnDownloadListener {
                        override fun onDownloadSuccess(file: File) {
                            filePath = file.absolutePath
                            sub.onNext(true)
                            LogHelper.i("downFile", "downFile=" + file.absolutePath)
                        }

                        override fun onDownloading(progress: Int) {
                            LogHelper.i("downFile", "progress=$progress")
                        }

                        override fun onDownloadFailed(e: Exception) {
                            error = e.message.toString()
                            sub.onNext(true)
                            LogHelper.i("downFile", "downFile=" + e.message)
                        }
                    })
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ isStatus ->
                dialogDismiss()
                val inputStream: InputStream = FileInputStream(File(filePath))
                val otaBytes = FileUtils.toByteArray(inputStream)
                startOtaView()
                timeOut(45)
                if (isCard) {
                    eraseOTA(otaBytes)
                } else {
                    checkOTA(otaBytes)
                }
            }, {
                dialogDismiss()
            })
        addDisposable(disposable)
    }


    //------开始界面
    fun startOtaView() {
        showDialog(getString(R.string.OTA_request_being_initiated))
    }

    //擦除OTA数据
    fun eraseOTA(otaBytes: ByteArray) {
        var timeout: Disposable? = null
        var isRetry = true
        val bytes = BitmapBleBmpManagement.OTADataClear(if (isCard) 48 else 32, otaBytes.size)

        val characteristic = gattServices[0].characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.size == 0) {
            disconnection()
            SystemClock.sleep(100)
            contectBLE()
            runOnUiThread {
                ToastUtils.show(getString(R.string.Bluetooth_connection_is_disconnected_retry))
            }
            return
        }
        LogHelper.i(
            "writeByUuid",
            "发送擦除数据 value=" +
                    byteArrayToHexString(
                        bytes
                    )
        )
        Ble.getInstance<BleRssiDevice>().writeByUuid(connetedDevices[0], bytes, suid, cuid,
            object :
                BleWriteCallback<BleRssiDevice?>() {
                override fun onWriteSuccessCharacteristicChanged(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0020" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "擦除OTA回复 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            isRetry = false
                            if (timeout != null) {
                                timeout!!.dispose()
                            }
                            runOnUiThread { showDialog() }
                            if (returnBytes[returnBytes.size - 2] == "00") {
                                //发送OTA数据
                                SystemClock.sleep(100)
                                checkOTA(otaBytes)
                            } else {
                                //检查OTA
                                SystemClock.sleep(100)
                                checkOTA(otaBytes)
                            }
                        }
                        "0030" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "擦除OTA回复 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            isRetry = false
                            if (timeout != null) {
                                timeout!!.dispose()
                            }
                            if (returnBytes[returnBytes.size - 2] == "00") {
                                //检查OTA
                                SystemClock.sleep(100)
                                checkOTA(otaBytes)
                            } else {
                                SystemClock.sleep(100)
                                eraseOTA(otaBytes)
                            }
                        }
                    }
                }

                override fun onWriteSuccess(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0020" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "擦除写入成功 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            timeout = Observable.timer(1000 * 12, TimeUnit.MILLISECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {
                                    if (isRetry) {
                                        eraseOTA(otaBytes)
                                    }
                                }
                            addDisposable(timeout ?: return)
                        }
                    }
                }

                override fun onWriteFailed(
                    device: BleRssiDevice?,
                    failedCode: Int
                ) {
                    super.onWriteFailed(device, failedCode)
                    LogHelper.i(
                        "writeByUuid",
                        "擦除写入失败"
                    )
//                        errorView()

                }
            })

    }

    //检查OTA内存
    fun checkOTA(otaBytes: ByteArray) {
        var isRetry = true
        var timeout: Disposable? = null
        runOnUiThread { showDialog() }
        val bytes = BitmapBleBmpManagement.getBmpProtocolDataChange(if (isCard) 49 else 33)

        val characteristic = gattServices[0].characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices

        if (connetedDevices.size == 0) {
            disconnection()
            SystemClock.sleep(100)
            contectBLE()
            runOnUiThread {
                ToastUtils.show(getString(R.string.Bluetooth_connection_is_disconnected_retry))
            }
            return
        }
        LogHelper.i(
            "writeByUuid",
            "发送检查OTA数据 value=" +
                    byteArrayToHexString(
                        bytes
                    )
        )
        val bleDevice = connetedDevices[0]
        Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, bytes, suid, cuid,
            object :
                BleWriteCallback<BleRssiDevice?>() {
                override fun onWriteSuccessCharacteristicChanged(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {

                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0031", "0021" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "检查OTA回复 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            runOnUiThread { showDialog() }
                            isRetry = false
                            if (timeout != null) {
                                (timeout ?: return).dispose()
                            }
                            if (returnBytes[returnBytes.size - 2] == "00") {
                                //发送OTA数据
                                SystemClock.sleep(300)
                                sendOTAData(otaBytes)
                            } else {
                                SystemClock.sleep(100)
                                eraseOTA(otaBytes)
                            }
                        }
                    }
                }

                override fun onWriteSuccess(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0033", "0023" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "检查OTA写入成功 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            timeout = Observable.timer(1000 * 1, TimeUnit.MILLISECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {
                                    if (isRetry) {
                                        checkOTA(otaBytes)
                                    }
                                }
                            addDisposable(timeout ?: return)
                        }
                    }
                }

                override fun onWriteFailed(
                    device: BleRssiDevice?,
                    failedCode: Int
                ) {
                    super.onWriteFailed(device, failedCode)
                    LogHelper.i(
                        "writeByUuid",
                        "检查OTA写入失败"
                    )
//                        errorView()

                }
            })
    }

    //发送OTA数据
    fun sendOTAData(otaBytes: ByteArray) {
        runOnUiThread {
            showDialog(
                getString(R.string.OTA_data_being_initiated)
            )
        }
        val disposable = Observable.create(ObservableOnSubscribe<Boolean> { e ->
            e.onNext(true)
            var mIndex = 0
            var isWaiting = true
            isCloseLoop = false
            var onWriteSuccess: Boolean
            var onChanged: Boolean

//            var isRetry: Boolean
//            var timeout: Disposable? = null

            val byteList = BitmapBleBmpManagement.writeEntity(otaBytes, 140)
            val characteristic = gattServices[0].characteristics?.get(0)
            val suid = characteristic?.service?.uuid
            val cuid = characteristic?.uuid
            val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
            if (connetedDevices.size == 0) {
                disconnection()
                SystemClock.sleep(100)
                contectBLE()
                runOnUiThread {
                    ToastUtils.show(getString(R.string.Bluetooth_connection_is_disconnected_retry))
                }
                return@ObservableOnSubscribe
            }
            LogHelper.i("sendimg", "开始发送OTA数据2")
            val bleDevice = connetedDevices[0]
            val byteListSize = byteList.size
            while (mIndex < byteListSize) {
                if (isCloseLoop) {
                    break
                }
                if (isWaiting) {
                    SystemClock.sleep(12)
//                        isRetry = true
                    onWriteSuccess = false
                    onChanged = false
                    if (mIndex == byteListSize) {
                        LogHelper.i("sendimg", "达到了数量 关闭循环")
                        break
                    }

                    LogHelper.i(
                        "sendimg",
                        "发送OTA数据包下标：byteList.size=" + byteListSize + ",mIndex=" + mIndex
                    )
                    val sendData = BitmapBleBmpManagement.sendOtaData(
                        if (isCard) 50 else 34,
                        byteList[mIndex],
                        byteListSize,
                        mIndex
                    )
//                        i(
//                            "sendImage", "发送数据包数量内容 ==" + ByteUtils.byteArrayToHexString(
//                                sendData
//                            )
//                        )

                    isWaiting = false
                    Ble.getInstance<BleRssiDevice>()
                        .writeByUuid(bleDevice, sendData, suid, cuid,
                            object :
                                BleWriteCallback<BleRssiDevice?>() {
                                override fun onWriteSuccessCharacteristicChanged(
                                    device: BleRssiDevice?,
                                    characteristic: BluetoothGattCharacteristic
                                ) {
                                    val returnBytes =
                                        byteArrayToHexString(
                                            characteristic.value
                                        ).split(",")
                                    when ((returnBytes[5] + returnBytes[6])) {
                                        "0022", "0032" -> {
                                            LogHelper.i(
                                                "writeByUuid",
                                                "发送OTA数据回复 value=" +
                                                        byteArrayToHexString(
                                                            characteristic.value
                                                        )
                                            )
                                            if (returnBytes[returnBytes.size - 2] == "00") {
//                                                    isRetry = false
//                                                    if (timeout != null) {
//                                                        (timeout ?: return).dispose()
//                                                    }
                                                onChanged = true
                                                if ((mIndex == byteListSize - 1) && onWriteSuccess && onChanged) {
                                                    isCloseLoop = true
                                                    LogHelper.i("sendimg", "达到了数量 下一步操作")
                                                    OTACRC(otaBytes)
                                                }
                                                if (mIndex < byteListSize && onWriteSuccess && onChanged) {
                                                    LogHelper.i("sendimg", "发下一条数据包")
                                                    mIndex++
                                                    isWaiting = true
                                                }
                                            }
                                        }
                                    }
                                }

                                override fun onWriteSuccess(
                                    device: BleRssiDevice?,
                                    characteristic: BluetoothGattCharacteristic
                                ) {

                                    val returnBytes =
                                        byteArrayToHexString(
                                            characteristic.value
                                        ).split(",")
                                    when ((returnBytes[5] + returnBytes[6])) {
                                        "0022", "0032" -> {
                                            LogHelper.i(
                                                "writeByUuid",
                                                "写入OTA数据成功 value=" +
                                                        byteArrayToHexString(
                                                            characteristic.value
                                                        )
                                            )
                                            onWriteSuccess = true
                                            if ((mIndex == byteListSize - 1) && onWriteSuccess && onChanged) {
                                                isCloseLoop = true
                                                LogHelper.i("sendimg", "达到了数量 下一步操作")
                                                OTACRC(otaBytes)
                                            }
                                            if (mIndex < byteListSize && onWriteSuccess && onChanged) {
                                                LogHelper.i("sendimg", "发下一条数据包")
                                                mIndex++
                                                isWaiting = true
                                            }
//                                                timeout = Observable.timer(
//                                                    2000,
//                                                    TimeUnit.MILLISECONDS
//                                                )
//                                                    .subscribeOn(Schedulers.io())
//                                                    .observeOn(AndroidSchedulers.mainThread())
//                                                    .subscribe {
//                                                        if (isRetry) {
//                                                            isWaiting = true
//                                                        }
//                                                    }
//                                                addDisposable(timeout!!)
                                        }
                                    }
                                }

                                override fun onWriteFailed(
                                    device: BleRssiDevice?,
                                    failedCode: Int
                                ) {
                                    super.onWriteFailed(device, failedCode)
                                    LogHelper.i(
                                        "writeByUuid",
                                        "写入OTA数据失败 value=" +
                                                byteArrayToHexString(
                                                    characteristic?.value
                                                )
                                    )

//                                        errorView()
                                }
                            })
                }
            }
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isStatus ->
            }
        addDisposable(disposable)
    }


    //CRC校验
    fun OTACRC(otaBytes: ByteArray) {
        var isRetry = true
        var timeout: Disposable? = null

        val crc32 = CRC32()
        crc32.update(otaBytes)
        val bytes =
            BitmapBleBmpManagement.sendOTACheck(if (isCard) 51 else 35, crc32.value.toInt())

        val characteristic = gattServices[0].characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.size == 0) {
            disconnection()
            SystemClock.sleep(100)
            contectBLE()
            runOnUiThread {
                ToastUtils.show(getString(R.string.Bluetooth_connection_is_disconnected_retry))
            }
            return
        }
        LogHelper.i(
            "writeByUuid",
            "CRC校验发送 value=" +
                    byteArrayToHexString(
                        bytes
                    )
        )
        val bleDevice = connetedDevices[0]
        Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, bytes, suid, cuid,
            object :
                BleWriteCallback<BleRssiDevice?>() {
                override fun onWriteSuccessCharacteristicChanged(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {
                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0033", "0023" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "CRC校验回复 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            if (returnBytes[returnBytes.size - 2] == "00") {
                                isRetry = false
                                if (timeout != null) {
                                    timeout!!.dispose()
                                }
                                switchingOTA()
                            }
                        }
                    }
                }

                override fun onWriteSuccess(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {

                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0033", "0023" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "写入CRC校验成功 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            timeout = Observable.timer(1000 * 2, TimeUnit.MILLISECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {
                                    if (isRetry) {
                                        OTACRC(otaBytes)
                                    }
                                }
                            addDisposable(timeout!!)
                        }
                    }
                }

                override fun onWriteFailed(
                    device: BleRssiDevice?,
                    failedCode: Int
                ) {
                    super.onWriteFailed(device, failedCode)
                    LogHelper.i(
                        "writeByUuid",
                        "写入CRC校验失败"
                    )

//                        errorView()
                }
            })
    }

    //切换固件
    fun switchingOTA() {
        var isRetry = true
        val bytes = BitmapBleBmpManagement.getBmpProtocolDataChange(if (isCard) 52 else 36)

        val characteristic = gattServices[0].characteristics?.get(0)
        val suid = characteristic?.service?.uuid
        val cuid = characteristic?.uuid
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.size == 0) {
            disconnection()
            SystemClock.sleep(100)
            contectBLE()
            runOnUiThread {
                ToastUtils.show(getString(R.string.Bluetooth_connection_is_disconnected_retry))
            }
            return
        }
        LogHelper.i(
            "writeByUuid",
            "固件切换发送 value=" +
                    byteArrayToHexString(
                        bytes
                    )
        )
        val bleDevice = connetedDevices[0]
        Ble.getInstance<BleRssiDevice>().writeByUuid(bleDevice, bytes, suid, cuid,
            object :
                BleWriteCallback<BleRssiDevice?>() {
                override fun onWriteSuccessCharacteristicChanged(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {

                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0034", "0024" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "OTA固件切换回复 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            isRetry = false
                            if (timeoutMain != null) {
                                timeoutMain!!.dispose()
                            }
                            if (returnBytes[returnBytes.size - 2] == "00") {
                                runOnUiThread {
                                    rebootOTAView()
                                }
                            }
                        }
                    }
                }

                override fun onWriteSuccess(
                    device: BleRssiDevice?,
                    characteristic: BluetoothGattCharacteristic
                ) {

                    val returnBytes =
                        byteArrayToHexString(
                            characteristic.value
                        ).split(",")
                    when ((returnBytes[5] + returnBytes[6])) {
                        "0034", "0024" -> {
                            LogHelper.i(
                                "writeByUuid",
                                "写入成功OTA固件切换 value=" +
                                        byteArrayToHexString(
                                            characteristic.value
                                        )
                            )
                            Observable.timer(1000 * 3, TimeUnit.MILLISECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {
                                    if (isRetry) {
                                        switchingOTA()
                                    }
                                }
                        }
                    }
                }

                override fun onWriteFailed(
                    device: BleRssiDevice?,
                    failedCode: Int
                ) {
                    super.onWriteFailed(device, failedCode)
                    LogHelper.i(
                        "writeByUuid",
                        "写入功OTA固件切换失败"
                    )

//                        errorView()
                }
            })

    }

    //------重启等待界面
    fun rebootOTAView() {
        runOnUiThread {
            if (timeoutMain != null) {
                timeoutMain!!.dispose()
            }
            showDialog(getString(R.string.Device_reboot_in_progress))
        }


       val dis =Observable.timer(1000 * 1, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                disconnection()
            }
        addDisposable(dis)

        val timeout = Observable.timer(if (isCard) 1000 * 4 else 1000 * 10, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (timeoutMain != null) {
                    timeoutMain!!.dispose()
                }
                if (isCard) {
                    item_wt_device_manage_upgrade.getTvRightText()!!.text = "V" + mNewCardVersion
                    item_wt_device_manage_upgrade.getTvRightText()!!
                        .setTextColor(ContextCompat.getColor(this, R.color.textColor_black))
                    item_wt_device_manage_upgrade.isEnabled=false
                } else {
                    item_device_manage_upgrade.getTvRightText()!!.text = "V" + mNewBaseVersion
                    item_device_manage_upgrade.getTvRightText()!!
                        .setTextColor(ContextCompat.getColor(this, R.color.textColor_black))
                    item_device_manage_upgrade.isEnabled=false
                }

                dialogDismiss()
                try {
                    MaterialDialog.Builder(this)
                        .content(getString(R.string.OTA_update_completed))
                        .cancelable(true)
                        .dismissListener {
                            it.dismiss()
                            ActivityManager.getInstance()
                                .backTo(MainActivity::class.java)
                        }
                        .show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        addDisposable(timeout)
    }


    override fun onDestroy() {
        super.onDestroy()
        isCloseLoop = true
        if (timeoutMain != null) {
            timeoutMain!!.dispose()
        }
    }

    fun disconnection() {
        Ble.options().isAutoConnect = false
        val connetedDevices = Ble.getInstance<BleRssiDevice>().connectedDevices
        if (connetedDevices.size != 0) {
            val bleDevice = connetedDevices[0]
            Ble.getInstance<BleRssiDevice>().disconnect(bleDevice)
        }
    }

}
