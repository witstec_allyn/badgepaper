package com.witstec.sz.badgeepaper.ui.freagment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.ui.adapter.MyFragmentPagerAdapter
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.ui.freagment.local.LocalTemplateMainFragment
import com.witstec.sz.badgeepaper.ui.freagment.local.OfficialMainFragment
import com.witstec.sz.badgeepaper.view.TabView
import kotlinx.android.synthetic.main.activity_filter_template.*
import java.util.*

class LocalTemplateHomeFragment : BaseFragment() {

    private val fragment_list = ArrayList<Fragment>()
    private var adapter: MyFragmentPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_filter_template, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val ios_tab = titlebar.centerCustomView.findViewById<TabView>(R.id.tab_list)
        val tabs =
            arrayListOf(getString(R.string.my_template), getString(R.string.download_template))
        ios_tab.setTabs(tabs)
        fragment_list.add(LocalTemplateMainFragment())
        fragment_list.add(OfficialMainFragment())
        adapter = MyFragmentPagerAdapter(childFragmentManager, tabs, fragment_list)
        viewPager.adapter = adapter
        viewPager.currentItem = 0
        ios_tab.setOnSelectedItemChange(object : TabView.OnSelectedItemChange {
            override fun onChange(select: Int) {
                viewPager.currentItem = select
            }
        })

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                ios_tab.setSelectedPosition(position)
            }
        })
    }


}

