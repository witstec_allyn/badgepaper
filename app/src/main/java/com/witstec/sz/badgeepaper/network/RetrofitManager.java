package com.witstec.sz.badgeepaper.network;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.witstec.sz.badgeepaper.App;
import com.witstec.sz.badgeepaper.BuildConfig;
import com.witstec.sz.badgeepaper.Constants;
import com.witstec.sz.badgeepaper.utils.FileManager;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.net.ssl.*;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

public class RetrofitManager {

    private static RetrofitManager INSTANCE = null;

    private static final long RESPONSE_CACHE_SIZE = 10 * 1024 * 1024;

    private Api mWordPressService;

    private RetrofitManager() {
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(),
                        new SharedPrefsCookiePersistor(App.Companion.getInstance()));
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                .sslSocketFactory(createSSLSocketFactory())
                .hostnameVerifier(new TrustAllHostnameVerifier())
                .cookieJar(cookieJar)
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS);
        httpClientBuilder.addInterceptor(new CommonParamsInterceptor());
        if (BuildConfig.DEBUG) {
            httpClientBuilder.addInterceptor(new LoggingInterceptor());
        }
        httpClientBuilder.cache(new Cache(
                FileManager.getHttpCacheDir("okhttp"), RESPONSE_CACHE_SIZE));

        Retrofit.Builder builder = new Retrofit.Builder();

        builder.client(httpClientBuilder.build())
                .baseUrl(Constants.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        Retrofit retrofit = builder.build();
        mWordPressService = retrofit.create(Api.class);
    }

    public static RetrofitManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RetrofitManager();
        }
        return INSTANCE;
    }

    private static final class HolderClass {
        private static final Api W_INSTANCE = RetrofitManager.getInstance().mWordPressService;
    }

    public static Api getWordPressService() {
        return HolderClass.W_INSTANCE;
    }

    //信任所有证书 start
    private class TrustAllCerts implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    private class TrustAllHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    private SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory ssfFactory = null;
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{new TrustAllCerts()}, new SecureRandom());

            ssfFactory = sc.getSocketFactory();
        } catch (Exception e) {
        }

        return ssfFactory;
    }
}
