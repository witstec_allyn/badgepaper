package com.witstec.sz.badgeepaper.draw;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.witstec.sz.badgeepaper.R;

import java.util.Objects;

public class MultipleLayout extends RelativeLayout {

    private BrushClickCallback brushClickCallback;
    private TextClickCallback textClickCallback;
    private ImageViewClickCallback imageViewClickCallback;
    private BarCodeClickCallback barCodeClickCallback;
    private ControlOperationClickCallback controlOperationClickCallback;
    private boolean mIsVisible = false;
    boolean isVertical = false;
    int textColor = Color.BLACK;
    int textSize = 2;
    String text = "abc";

    public MultipleLayout(Context context) {
        super(context);
    }

    public MultipleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultipleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    //显示首页默认布局
    public void showDefMenuView() {
        removeAllViews();
        View view = inflate(getContext(), R.layout.layout_drawing_menu, null);
        LinearLayout ll_drawing_to_brush = view.findViewById(R.id.ll_drawing_to_brush);
        LinearLayout ll_drawing_to_text = view.findViewById(R.id.ll_drawing_to_text);
        LinearLayout ll_drawing_to_barcode = view.findViewById(R.id.ll_drawing_to_barcode);
        LinearLayout ll_drawing_to_image = view.findViewById(R.id.ll_drawing_to_image);

        final LinearLayout ll_drawing_update = view.findViewById(R.id.ll_drawing_update);
        final LinearLayout ll_control_operation = view.findViewById(R.id.ll_control_operation);
        final ImageButton btn_drawing_intermediate = view.findViewById(R.id.btn_drawing_intermediate);
        final ImageView iv_drawing_drag = view.findViewById(R.id.iv_drawing_drag);
        final TextView tv_drawing_drag = view.findViewById(R.id.tv_drawing_drag);

        final ImageButton btn_drawing_ai_delete = view.findViewById(R.id.btn_drawing_ai_delete);
        final ImageButton btn_drawing_ai_copy = view.findViewById(R.id.btn_drawing_ai_copy);
        final ImageButton btn_normal_view = view.findViewById(R.id.btn_normal_view);
        final ImageButton btn_drawing_full = view.findViewById(R.id.btn_drawing_full);
        final ImageButton btn_drawing_empty = view.findViewById(R.id.btn_drawing_empty);
        btn_drawing_ai_delete.setOnClickListener(v -> controlOperationClickCallback.onOperatingDelete());
        btn_drawing_intermediate.setOnClickListener(v -> controlOperationClickCallback.onIntermediate());
        btn_drawing_ai_copy.setOnClickListener(v -> controlOperationClickCallback.onOperatingCopy());
        btn_normal_view.setOnClickListener(v -> controlOperationClickCallback.onViewMormal());
        btn_drawing_full.setOnClickListener(v -> controlOperationClickCallback.onOperatingRestore());
        btn_drawing_empty.setOnClickListener(v -> controlOperationClickCallback.onOperatingEmpty());
        ll_drawing_update.setOnClickListener(v -> {
            if (mIsVisible) {
                controlOperationClickCallback.onOutUpdate();
                controlOperationClickCallback.onOperatingDIsUpdate(false);
                ll_control_operation.setVisibility(View.GONE);
                iv_drawing_drag.setImageResource(R.mipmap.ic_drawing_drag);
                tv_drawing_drag.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
                mIsVisible = false;
            } else {
                controlOperationClickCallback.onInUpdate();
                controlOperationClickCallback.onOperatingDIsUpdate(true);
                ll_control_operation.setVisibility(View.VISIBLE);
                iv_drawing_drag.setImageResource(R.mipmap.ic_drawing_drag_true);
                tv_drawing_drag.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                mIsVisible = true;
            }
        });


        ll_drawing_to_brush.setOnClickListener(v -> {
            controlOperationClickCallback.onOutUpdate();
            ll_control_operation.setVisibility(View.GONE);
            iv_drawing_drag.setImageResource(R.mipmap.ic_drawing_drag);
            tv_drawing_drag.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
            mIsVisible = false;
            brushClickCallback.onBrushDefault();
            showBrushMenuView();
        });


        ll_drawing_to_text.setOnClickListener(v -> {
            controlOperationClickCallback.onOutUpdate();
            ll_control_operation.setVisibility(View.GONE);
            iv_drawing_drag.setImageResource(R.mipmap.ic_drawing_drag);
            tv_drawing_drag.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
            mIsVisible = false;
            View view1 = inflate(getContext(), R.layout.dialog_drawing_text_input, null);

            final AppCompatEditText text_input = view1.findViewById(R.id.text_input);
            final ImageButton iv_text_horizontal = view1.findViewById(R.id.iv_text_horizontal);
            final ImageButton iv_text_vertical = view1.findViewById(R.id.iv_text_vertical);

            final ImageButton iv_text_red = view1.findViewById(R.id.iv_text_red);
            final ImageButton iv_text_black = view1.findViewById(R.id.iv_text_black);
            final ImageButton iv_text_white = view1.findViewById(R.id.iv_text_white);

            final ImageButton text_size1 = view1.findViewById(R.id.text_size1);
            final ImageButton text_size2 = view1.findViewById(R.id.text_size2);
            final ImageButton text_size3 = view1.findViewById(R.id.text_size3);
            final ImageButton text_size4 = view1.findViewById(R.id.text_size4);
            final ImageButton text_size5 = view1.findViewById(R.id.text_size5);


            text_size1.setOnClickListener(v15 -> {
                textSize = 1;
                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });
            text_size2.setOnClickListener(v15 -> {
                textSize = 2;
                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });
            text_size3.setOnClickListener(v15 -> {
                textSize = 3;
                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });
            text_size4.setOnClickListener(v15 -> {
                textSize = 4;
                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });
            text_size5.setOnClickListener(v15 -> {
                textSize = 5;
                text_size1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size4.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                text_size5.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            });


            iv_text_horizontal.setOnClickListener(v1 -> {
                isVertical = false;
                iv_text_horizontal.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                iv_text_vertical.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });

            iv_text_vertical.setOnClickListener(v12 -> {
                isVertical = true;
                iv_text_horizontal.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                iv_text_vertical.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            });


            iv_text_red.setOnClickListener(v13 -> {
                textColor = Color.RED;
                iv_text_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                iv_text_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });
            iv_text_black.setOnClickListener(v14 -> {
                textColor = Color.BLACK;
                iv_text_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                iv_text_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
                iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            });

            iv_text_white.setOnClickListener(v14 -> {
                textColor = Color.WHITE;
                iv_text_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                iv_text_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
                iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            });
            text_input.requestFocus();
//            text_input.post(() -> {
//                InputMethodManager imm = (InputMethodManager)getContext(). getSystemService(Context.INPUT_METHOD_SERVICE);
//                assert imm != null;
//                imm.showSoftInput(text_input,InputMethodManager.SHOW_FORCED);
//            });
            new MaterialDialog.Builder(getContext())
                    .title(getContext().getString(R.string.text_content))
                    .customView(view1, true)
                    .positiveText(getContext().getString(R.string.fixed))
                    .negativeText(getContext().getString(R.string.cancel))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            text = Objects.requireNonNull(text_input.getText()).toString();
                            textClickCallback.onContext(text, isVertical, textColor, textSize);
                            showDefMenuView();
                            textColor = Color.BLACK;
                            isVertical = false;
                            textSize=2;
                        }
                    })
                    .onNegative((dialog, which) -> showDefMenuView())
                    .show();
        });

        ll_drawing_to_barcode.setOnClickListener(v -> {
            controlOperationClickCallback.onOutUpdate();
            ll_control_operation.setVisibility(View.GONE);
            iv_drawing_drag.setImageResource(R.mipmap.ic_drawing_drag);
            tv_drawing_drag.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
            mIsVisible = false;
            showBarCodeMenuView();
        });

        ll_drawing_to_image.setOnClickListener(v -> {
            controlOperationClickCallback.onOutUpdate();
            ll_control_operation.setVisibility(View.GONE);
            iv_drawing_drag.setImageResource(R.mipmap.ic_drawing_drag);
            tv_drawing_drag.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
            mIsVisible = false;
            imageViewClickCallback.onClick();
        });


        addView(view);
    }

    //画笔布局
    //画笔点击交互
    public void showBrushMenuView() {
        removeAllViews();
        View view = inflate(getContext(), R.layout.layout_drawing_brush_menu, null);
        ImageView iv_drawing_down = view.findViewById(R.id.iv_drawing_down);

        final ImageButton iv_brush_straight = view.findViewById(R.id.iv_brush_straight);
        final ImageButton iv_brush_curve_cr = view.findViewById(R.id.iv_brush_curve_cr);

        final ImageButton iv_brush_line_true = view.findViewById(R.id.iv_brush_line_true);
        final ImageButton iv_brush_line_false = view.findViewById(R.id.iv_brush_line_false);

        final ImageButton iv_brush_point1 = view.findViewById(R.id.iv_brush_point1);
        final ImageButton iv_brush_point2 = view.findViewById(R.id.iv_brush_point2);
        final ImageButton iv_brush_point3 = view.findViewById(R.id.iv_brush_point3);

        final ImageButton iv_brush_red = view.findViewById(R.id.iv_brush_red);
        final ImageButton iv_brush_black = view.findViewById(R.id.iv_brush_black);
        final ImageButton iv_text_white = view.findViewById(R.id.iv_text_white);


        final ImageButton iv_shape_square = view.findViewById(R.id.iv_shape_square);
        final ImageButton iv_shape_round = view.findViewById(R.id.iv_shape_round);

        final ImageButton iv_shape_filling_false = view.findViewById(R.id.iv_shape_filling_false);
        final ImageButton iv_shape_filling_true = view.findViewById(R.id.iv_shape_filling_true);

        iv_drawing_down.setOnClickListener(v -> showDefMenuView());

        //线段
        iv_brush_straight.setOnClickListener(v -> {
            brushClickCallback.onStraight();
            iv_brush_straight.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_brush_curve_cr.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_square.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_round.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });
        iv_brush_curve_cr.setOnClickListener(v -> {
            brushClickCallback.onCurveBend();
            iv_brush_straight.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_curve_cr.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_shape_square.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_round.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });

        iv_shape_square.setOnClickListener(v -> {
            brushClickCallback.onSquare();
            iv_brush_straight.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_curve_cr.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_square.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_shape_round.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });
        iv_shape_round.setOnClickListener(v -> {
            brushClickCallback.onRound();
            iv_brush_straight.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_curve_cr.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_square.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_round.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
        });


        //实线/虚线
        iv_brush_line_true.setOnClickListener(v -> {
            brushClickCallback.onSolidLine();
            iv_brush_line_true.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_brush_line_false.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });
        iv_brush_line_false.setOnClickListener(v -> {
            brushClickCallback.onDottedLine();
            iv_brush_line_true.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_line_false.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
        });


        //大小
        iv_brush_point1.setOnClickListener(v -> {
            brushClickCallback.onPointMin();
            iv_brush_point1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_brush_point2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_point3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });
        iv_brush_point2.setOnClickListener(v -> {
            brushClickCallback.onPointMedium();
            iv_brush_point1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_point2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_brush_point3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });
        iv_brush_point3.setOnClickListener(v -> {
            brushClickCallback.onPointMax();
            iv_brush_point1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_point2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_point3.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
        });


        //颜色
        iv_brush_red.setOnClickListener(v -> {
            brushClickCallback.onRed();
            iv_brush_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_brush_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });

        iv_brush_black.setOnClickListener(v -> {
            brushClickCallback.onBlack();
            iv_brush_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });

        iv_text_white.setOnClickListener(v -> {
            brushClickCallback.onWhite();
            iv_brush_red.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_brush_black.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_text_white.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
        });


        iv_shape_filling_false.setOnClickListener(v -> {
            brushClickCallback.onFillingFalse();
            iv_shape_filling_false.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
            iv_shape_filling_true.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
        });
        iv_shape_filling_true.setOnClickListener(v -> {
            brushClickCallback.onFillingTue();
            iv_shape_filling_false.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_black_bg));
            iv_shape_filling_true.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.div_gay_bg));
        });
        addView(view);
    }


    //二维码布局
    public void showBarCodeMenuView() {
        removeAllViews();
        View view = inflate(getContext(), R.layout.layout_drawing_barcode_menu, null);
        LinearLayout ll_barcode_to = view.findViewById(R.id.ll_barcode_to);
        LinearLayout ll_qrcode_to = view.findViewById(R.id.ll_qrcode_to);
        TextView tv_drawing_barcode_down = view.findViewById(R.id.tv_drawing_barcode_down);

        ll_barcode_to.setOnClickListener(v -> {
            View view1 = inflate(getContext(), R.layout.dialog_drawing_barcode_input, null);
            final AppCompatEditText text_input = view1.findViewById(R.id.text_input);

            new MaterialDialog.Builder(getContext())
                    .title(getContext().getString(R.string.mode_type_barCode))
                    .customView(view1, true)
                    .positiveText(getContext().getString(R.string.fixed))
                    .negativeText(getContext().getString(R.string.cancel))
                    .onPositive((dialog, which) -> {
                        barCodeClickCallback.onBarCodeContext(text_input.getText().toString());
                        showDefMenuView();
                    })
                    .onNegative((dialog, which) -> showDefMenuView())
                    .show();
        });
        ll_qrcode_to.setOnClickListener(v -> {
            View view12 = inflate(getContext(), R.layout.dialog_drawing_qrcode_input, null);
            final AppCompatEditText text_input = view12.findViewById(R.id.text_input);
            new MaterialDialog.Builder(getContext())
                    .title(getContext().getString(R.string.mode_type_qrCode))
                    .customView(view12, true)
                    .positiveText(getContext().getString(R.string.fixed))
                    .negativeText(getContext().getString(R.string.cancel))
                    .onPositive((dialog, which) -> {
                        barCodeClickCallback.onQrCodeContext(text_input.getText().toString());
                        showDefMenuView();
                    })
                    .onNegative((dialog, which) -> showDefMenuView())
                    .show();

        });
        tv_drawing_barcode_down.setOnClickListener(v -> showDefMenuView());
        addView(view);
    }


    public interface BrushClickCallback {

        //随笔
        void onStraight();

        //可弯曲的
        void onCurveBend();

        //实线
        void onSolidLine();

        //虚线
        void onDottedLine();

        //粗体 大
        void onPointMax();

        //粗体 中
        void onPointMedium();

        //粗体 小
        void onPointMin();

        //红色
        void onRed();

        //黑色
        void onBlack();

        //白色
        void onWhite();

        //默认值
        void onBrushDefault();

        //方的
        void onSquare();

        //圆
        void onRound();

        //填充
        void onFillingFalse();

        //不填充
        void onFillingTue();
    }

    public interface ControlOperationClickCallback {
        //删除
        void onOperatingDelete();

        //复制
        void onOperatingCopy();

        //剧中
        void onIntermediate();

        //撤销
        void onViewMormal();

        //满屏
        void onOperatingRestore();

        //清空
        void onOperatingEmpty();

        //是否编辑
        void onOperatingDIsUpdate(boolean isUpdate);

        //进入编辑状态
        void onInUpdate();

        //退出编辑状态
        void onOutUpdate();

    }


    public interface BarCodeClickCallback {
        //条码内容
        void onBarCodeContext(String text);

        //二维码内容
        void onQrCodeContext(String text);
    }

    public interface TextClickCallback {
        //文本内容
        void onContext(String text, boolean isVertical, int color, int textSize);

        //默认值
        void onTextDefault();

    }

    public interface ImageViewClickCallback {
        void onClick();
    }


    public void setBrushClickCallback(BrushClickCallback brushClickCallback) {
        this.brushClickCallback = brushClickCallback;
    }



    public void setTextClickCallback(TextClickCallback textClickCallback) {
        this.textClickCallback = textClickCallback;
    }


    public void setImageViewClickCallback(ImageViewClickCallback imageViewClickCallback) {
        this.imageViewClickCallback = imageViewClickCallback;
    }


    public void setBarCodeClickCallback(BarCodeClickCallback barCodeClickCallback) {
        this.barCodeClickCallback = barCodeClickCallback;
    }

    public void setControlOperationClickCallback(ControlOperationClickCallback controlOperationClickCallback) {
        this.controlOperationClickCallback = controlOperationClickCallback;
    }

}
