package com.witstec.sz.badgeepaper.view

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.RelativeLayout
import com.witstec.sz.badgeepaper.utils.SoftKeyboardUtils
import kotlinx.android.synthetic.main.header_view_search_input.view.*
import com.witstec.sz.badgeepaper.R


class SearchRelativeLayout : RelativeLayout {

    private var textChangedListener: TextChangedListener? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    private fun initView() {
        val view1 = View.inflate(context, R.layout.header_view_search, this)
        val search = view1.findViewById<RelativeLayout>(R.id.rl_search_view)
        search.setOnClickListener {

            removeAllViews()

            val view12 = View.inflate(context, R.layout.header_view_search_input, null)
            val search1 = view12.findViewById<OneKeyClearEditText>(R.id.ed_search_view_input)

            SoftKeyboardUtils.showKeybord(search1, context)

            search1.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    textChangedListener!!.onTextChanged(s.toString())
                }

                override fun afterTextChanged(s: Editable) {}
            })

            search1.setOnEditorActionListener { v, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH//按下搜索键
                    -> {
                        textChangedListener!!.onTextChanged(v.text.toString())
                        return@setOnEditorActionListener true
                    }
                    else -> {
                        return@setOnEditorActionListener true
                    }
                }
                return@setOnEditorActionListener false
            }

            view12.btn_cancel.setOnClickListener {
                textChangedListener!!.onTextChanged("")
                SoftKeyboardUtils.closeKeybord(search1, context)
                removeAllViews()
                initView()
            }

            addView(view12)
        }
    }


    fun setTextChangedListener(textChangedListener: TextChangedListener) {
        this.textChangedListener = textChangedListener
    }

    interface TextChangedListener {
        fun onTextChanged(text: String)
    }
}
