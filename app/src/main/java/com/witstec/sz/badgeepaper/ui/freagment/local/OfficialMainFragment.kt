package com.witstec.sz.badgeepaper.ui.freagment.local

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.nukc.stateview.StateView
import com.witstec.sz.badgeepaper.R
import com.witstec.sz.badgeepaper.draw.ui.activity.DrawingMainActivity
import com.witstec.sz.badgeepaper.model.bean.TemplateOfficialBean
import com.witstec.sz.badgeepaper.model.db.LocalDataSource
import com.witstec.sz.badgeepaper.model.event.ChangeTemplateListEvent
import com.witstec.sz.badgeepaper.ui.adapter.FeliOfficialTemplateAdapter
import com.witstec.sz.badgeepaper.ui.freagment.base.BaseFragment
import com.witstec.sz.badgeepaper.utils.LogHelper
import com.witstec.sz.badgeepaper.utils.RxBus
import com.witstec.sz.badgeepaper.utils.ToastUtils
import com.witstec.sz.badgeepaper.utils.app.AppUtils
import com.witstec.sz.badgeepaper.view.IosDialog
import com.witstec.sz.badgeepaper.view.SimpleDividerDecoration
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_official_templates.*
import kotlinx.android.synthetic.main.fragment_user_local.*
import kotlinx.android.synthetic.main.fragment_user_local.recycler
import java.util.*

class OfficialMainFragment : BaseFragment() {

    private var statView: StateView? = null
    private var mAdapter: FeliOfficialTemplateAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_official_local, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapter = FeliOfficialTemplateAdapter()
        mAdapter!!.setEnableLoadMore(false)
        statView = StateView.inject(recycler)
        statView!!.showLoading()
        recycler.adapter = mAdapter
        recycler.layoutManager = LinearLayoutManager(activity!!)
        recycler.addItemDecoration(SimpleDividerDecoration(activity!!))
        tv_filter.visibility = View.GONE
        getBleData()

        relativeLayout.setOnRefreshListener {
            relativeLayout.isRefreshing = true
            getBleData()
        }

        mAdapter!!.setOnItemClickListener { adapter, view, position ->
            val templateBean = mAdapter!!.getItem(position) as TemplateOfficialBean
            LogHelper.i("witstec", "xml文件路径=" + templateBean.templateFileUrl)
            DrawingMainActivity.start(
                activity,
                templateBean.templateId,
                templateBean.templateName,
                templateBean.templateType.toInt(),
                templateBean.templateFileUrl,
                false, true
            )
        }
        mAdapter!!.setOnItemLongClickListener { adapter, view, position ->
            IosDialog(activity!!).init()
                .setTitle(getString(R.string.template_selection_delete))
                .setMsg(getString(R.string.do_you_want_template_selection_delete))
                .setPositiveButton(getString(R.string.fixed), View.OnClickListener {
                    LogHelper.i("delete", mAdapter!!.getItem(position)!!.templateImage)
                    LogHelper.i("delete", mAdapter!!.getItem(position)!!.templateFileUrl)
                    AppUtils.deleteFolderFile(mAdapter!!.getItem(position)!!.templateImage)
                    val isDelete =
                        AppUtils.deleteFolderFile(mAdapter!!.getItem(position)!!.templateFileUrl)
                    if (isDelete) {
                        LocalDataSource.instance.liteOrm.delete(mAdapter!!.getItem(position))
                        mAdapter!!.remove(position)
                        ToastUtils.show(getString(R.string.delete_ok))
                    }
                })
                .setNegativeButton(getString(R.string.cancel), View.OnClickListener {
                })
                .show()
            true
        }

        addDisposable(
            RxBus.register(ChangeTemplateListEvent::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    getBleData()
                })
    }

    private fun getBleData() {
        val templateBean: ArrayList<TemplateOfficialBean> =
            LocalDataSource.instance.queryAll(TemplateOfficialBean::class.java)
        if (templateBean.size == 0) {
            //添加设备
            recycler.visibility = View.GONE
            statView!!.showContent()
            statView!!.showEmpty()
            relativeLayout.isRefreshing = false
        } else {
            statView!!.showContent()
            recycler.visibility = View.VISIBLE
            mAdapter!!.replaceData(templateBean.reversed())
            relativeLayout.isRefreshing = false
        }
    }

}
